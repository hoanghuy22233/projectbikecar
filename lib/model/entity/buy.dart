import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:project_bike_car/model/entity/barrel_entity.dart';

part 'buy.g.dart';

@JsonSerializable()
class Buy extends Equatable {
  @JsonKey(name: "member_id")
  int memberId;
  @JsonKey(name: "product_id")
  int productId;
  @JsonKey(name: "price")
  int price;
  @JsonKey(name: "status")
  int status;
  String phone;
  String address;
  String content;
  @JsonKey(name: "created_at")
  String created;
  @JsonKey(name: "updated_at")
  String updatedAt;
  int id;
  User member;


  Buy(
      this.memberId,
      this.productId,
      this.price,
      this.status,
      this.phone,
      this.address,
      this.content,
      this.created,
      this.updatedAt,
      this.id,
      this.member);

  @override
  String toString() {
    return 'Buy{memberId: $memberId, productId: $productId,price:$price, status: $status,phone:$phone,address:$address,content:$content ,created: $created ,updateAt: $updatedAt,id:$id,member:$member}';
  }

  factory Buy.fromJson(Map<String, dynamic> json) => _$BuyFromJson(json);

  Map<String, dynamic> toJson() => _$BuyToJson(this);

  @override
  List<Object> get props => [
    memberId,productId,price,
    status,phone,
    address,content,created,updatedAt,id,member

      ];
}
