// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'category_new.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CategoryNew _$CategoryNewFromJson(Map<String, dynamic> json) {
  return CategoryNew(
    json['id'] as int,
    json['name'] as String,
    json['image'] as String,
    json['status'] as int,
    json['created_at'] as String,
    json['updated_at'] as String,
    json['type'] as int,
  );
}

Map<String, dynamic> _$CategoryNewToJson(CategoryNew instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'image': instance.image,
      'status': instance.status,
      'created_at': instance.created,
      'updated_at': instance.updatedAt,
      'type': instance.type,
    };
