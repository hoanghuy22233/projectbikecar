import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/model/entity/brands.dart';
import 'package:project_bike_car/model/entity/category_new.dart';
import 'package:project_bike_car/model/entity/posts.dart';
import 'package:project_bike_car/model/entity/posts_news.dart';
import 'package:project_bike_car/model/entity/products.dart';

import 'category.dart';

part 'new_data.g.dart';

@JsonSerializable()
class NewData extends Equatable {
  @JsonKey(name: "categories")
  List<CategoryNew> categorys;
 // @JsonKey(name: "category")
  // List<CategoryNew> category;
  @JsonKey(name: "posts")
  List<PostsNews> posts;

  NewData({ this.posts,this.categorys});

  factory NewData.fromJson(Map<String, dynamic> json) =>
      _$NewDataFromJson(json);

  Map<String, dynamic> toJson() => _$NewDataToJson(this);

  @override
  List<Object> get props => [posts,categorys];
}
