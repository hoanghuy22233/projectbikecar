import 'package:flutter/cupertino.dart';

class DataTerm {
  final String title;
  final String detail;
  final String menu;


  DataTerm({this.menu,this.title, this.detail});

}
List <DataTerm> dataTerm =[
  DataTerm(

    menu: "",
    title: "Chính sách bảo mật",
    detail: "Cập nhật lần cuối: ngày 23 tháng 4 năm 2021 \nChính sách Bảo mật này mô tả các chính sách và thủ tục của Chúng tôi về việc thu thập, sử dụng và tiết lộ thông tin của Bạn khi Bạn sử dụng Dịch vụ và cho Bạn biết về các quyền riêng tư của Bạn và cách luật pháp bảo vệ Bạn. \nChúng tôi sử dụng dữ liệu Cá nhân của Bạn để cung cấp và cải thiện Dịch vụ. Bằng việc sử dụng Dịch vụ, Bạn đồng ý với việc thu thập và sử dụng thông tin theo Chính sách Bảo mật này. Chính sách Bảo mật này đã được tạo với sự trợ giúp của Trình tạo Chính sách Bảo mật.",
  ),
  DataTerm(
      title: "Giải thích và định nghĩa",
      menu: "Giải thích",
      detail: "Những từ có chữ cái đầu tiên được viết hoa có nghĩa được xác định trong các điều kiện sau. Các định nghĩa sau đây sẽ có cùng ý nghĩa bất kể chúng xuất hiện ở số ít hay số nhiều. "
  ),
  DataTerm(
      title: "",
      menu: "Định nghĩa",
      detail: 'Đối với các mục đích của Chính sách Bảo mật này: \n- Tài khoản có nghĩa là một tài khoản duy nhất được tạo để Bạn truy cập Dịch vụ của chúng tôi hoặc các phần của Dịch vụ của chúng tôi. \n- Đơn vị liên kết có nghĩa là một tổ chức kiểm soát, chịu sự kiểm soát của hoặc dưới sự kiểm soát chung của một bên, trong đó "quyền kiểm soát" có nghĩa là quyền sở hữu từ 50% trở lên cổ phần, lợi ích vốn cổ phần hoặc các chứng khoán khác được quyền bỏ phiếu bầu giám đốc hoặc cơ quan quản lý khác . \n- Ứng dụng có nghĩa là chương trình phần mềm do Công ty cung cấp được Bạn tải xuống trên bất kỳ thiết bị điện tử nào, có tên là gop1 \n- Công ty (được gọi là "Công ty", "Chúng tôi", "Chúng tôi" hoặc "Của Chúng tôi" trong Thỏa thuận này) đề cập đến địa chỉ gop1, 52 Ung Văn Khiêm, P.25, Q. Bình Thạnh, TP. HCM.\n- Quốc gia đề cập đến: Việt Nam \n- Thiết bị có nghĩa là bất kỳ thiết bị nào có thể truy cập Dịch vụ như máy tính, điện thoại di động hoặc máy tính bảng kỹ thuật số. \n- Dữ liệu Cá nhân là bất kỳ thông tin nào liên quan đến một cá nhân được xác định hoặc nhận dạng được. \n- Dịch vụ đề cập đến Ứng dụng. \n- Nhà cung cấp dịch vụ có nghĩa là bất kỳ thể nhân hoặc pháp nhân nào xử lý dữ liệu thay mặt cho Công ty. Nó đề cập đến các công ty bên thứ ba hoặc các cá nhân được Công ty thuê để hỗ trợ Dịch vụ, cung cấp Dịch vụ thay mặt Công ty, thực hiện các dịch vụ liên quan đến Dịch vụ hoặc hỗ trợ Công ty phân tích cách Dịch vụ được sử dụng. \n- Dịch vụ truyền thông xã hội của bên thứ ba đề cập đến bất kỳ trang web nào hoặc bất kỳ trang web mạng xã hội nào mà thông qua đó Người dùng có thể đăng nhập hoặc tạo tài khoản để sử dụng Dịch vụ. \n- Dữ liệu sử dụng đề cập đến dữ liệu được thu thập tự động, được tạo ra bởi việc sử dụng Dịch vụ hoặc từ chính cơ sở hạ tầng Dịch vụ (ví dụ: thời lượng của một lượt truy cập trang). \n- Bạn có nghĩa là cá nhân truy cập hoặc sử dụng Dịch vụ, hoặc công ty hoặc pháp nhân khác thay mặt cho cá nhân đó đang truy cập hoặc sử dụng Dịch vụ, nếu có.  '

  ),
  DataTerm(
      title: "Thu thập và sử dụng dữ liệu cá nhân của bạn ",
      menu: "Dữ liệu cá nhân ",
      detail: 'Trong khi sử dụng Dịch vụ của Chúng tôi, Chúng tôi có thể yêu cầu Bạn cung cấp cho Chúng tôi một số thông tin nhận dạng cá nhân nhất định có thể được sử dụng để liên hệ hoặc nhận dạng Bạn. Thông tin nhận dạng cá nhân có thể bao gồm, nhưng không giới hạn ở: \n- Địa chỉ Email \n- Họ và tên \n- Số điện thoại \n- Địa chỉ \n- Dữ liệu sử dụng '

  ),
  DataTerm(
      title: "",
      menu:"Dữ liệu sử dụng ",
      detail: 'Dữ liệu sử dụng được thu thập tự động khi sử dụng Dịch vụ. \nDữ liệu sử dụng có thể bao gồm thông tin như địa chỉ Giao thức Internet trên thiết bị của Bạn (ví dụ: địa chỉ IP), loại trình duyệt, phiên bản trình duyệt, các trang Dịch vụ của chúng tôi mà Bạn truy cập, ngày và giờ truy cập của Bạn, thời gian dành cho các trang đó, thiết bị duy nhất số nhận dạng và dữ liệu chẩn đoán khác. \nKhi Bạn truy cập Dịch vụ bằng hoặc thông qua thiết bị di động, Chúng tôi có thể tự động thu thập một số thông tin nhất định, bao gồm nhưng không giới hạn ở loại thiết bị di động Bạn sử dụng, ID duy nhất trên thiết bị di động của Bạn, địa chỉ IP của thiết bị di động của Bạn, Điện thoại di động của bạn hệ điều hành, loại trình duyệt Internet di động Bạn sử dụng, số nhận dạng thiết bị duy nhất và dữ liệu chẩn đoán khác. \nChúng tôi cũng có thể thu thập thông tin mà trình duyệt của Bạn gửi bất cứ khi nào Bạn truy cập Dịch vụ của chúng tôi hoặc khi Bạn truy cập Dịch vụ bằng hoặc thông qua thiết bị di động. '
  ),
  DataTerm(
      title: "",
      menu: "Thông tin từ các dịch vụ truyền thông xã hội của bên thứ ba ",
      detail: "Công ty cho phép Bạn tạo tài khoản và đăng nhập để sử dụng Dịch vụ thông qua các Dịch vụ truyền thông xã hội của bên thứ ba sau: \n- Google\n- Facebook \n-Twitter \nNếu Bạn quyết định đăng ký thông qua hoặc cấp cho chúng tôi quyền truy cập vào Dịch vụ truyền thông xã hội của bên thứ ba, Chúng tôi có thể thu thập dữ liệu cá nhân đã được liên kết với tài khoản của dịch vụ truyền thông xã hội bên thứ ba của bạn, chẳng hạn như tên của bạn, địa chỉ email của bạn, hoạt động của bạn hoặc Danh sách liên hệ của bạn được liên kết với tài khoản đó. \nBạn cũng có thể có tùy chọn chia sẻ thông tin bổ sung với Công ty thông qua tài khoản Dịch vụ truyền thông xã hội của bên thứ ba. Nếu Bạn chọn cung cấp thông tin và Dữ liệu Cá nhân như vậy, trong quá trình đăng ký hoặc bằng cách khác, Bạn đang cấp cho Công ty quyền sử dụng, chia sẻ và lưu trữ thông tin đó theo cách phù hợp với Chính sách Bảo mật này. "
  ),
  DataTerm(
      title: "",
      menu: "Thông tin được thu thập khi sử dụng ứng dụng ",
      detail: "While using Our Application, in order to provide features of Our Application, We may collect, with Your prior permission: \n- Ảnh và thông tin khác từ máy ảnh và thư viện ảnh trên Thiết bị của bạn \nChúng tôi sử dụng thông tin này để cung cấp các tính năng của Dịch vụ của chúng tôi, để cải thiện và tùy chỉnh Dịch vụ của chúng tôi. Thông tin có thể được tải lên máy chủ của Công ty và / hoặc máy chủ của Nhà cung cấp dịch vụ hoặc nó có thể được lưu trữ đơn giản trên thiết bị của Bạn. \nBạn có thể bật hoặc tắt quyền truy cập vào thông tin này bất kỳ lúc nào thông qua cài đặt Thiết bị của bạn. "

  ),
  DataTerm(
      title:"Sử dụng dữ liệu cá nhân của bạn",
      menu: "",
      detail: "Công ty có thể sử dụng Dữ liệu Cá nhân cho các mục đích sau: \n- Để cung cấp và duy trì Dịch vụ của chúng tôi, bao gồm cả việc giám sát việc sử dụng Dịch vụ của chúng tôi. \n- Để quản lý Tài khoản của bạn: để quản lý đăng ký của Bạn với tư cách là người dùng Dịch vụ. Dữ liệu Cá nhân Bạn cung cấp có thể cung cấp cho Bạn quyền truy cập vào các chức năng khác nhau của Dịch vụ khả dụng cho Bạn với tư cách là người dùng đã đăng ký. \n-Đối với việc thực hiện hợp đồng: sự phát triển, tuân thủ và cam kết của hợp đồng mua bán đối với các sản phẩm, mặt hàng hoặc dịch vụ mà Bạn đã mua hoặc bất kỳ hợp đồng nào khác với Chúng tôi thông qua Dịch vụ. \nĐể liên hệ với Bạn: Để liên hệ với Bạn qua email, cuộc gọi điện thoại, SMS hoặc các hình thức liên lạc điện tử tương đương khác, chẳng hạn như thông báo đẩy của ứng dụng di động về các bản cập nhật hoặc thông tin liên lạc liên quan đến chức năng, sản phẩm hoặc dịch vụ đã ký hợp đồng, bao gồm các bản cập nhật bảo mật khi cần thiết hoặc hợp lý để thực hiện chúng. \n- Để cung cấp cho Bạn tin tức, ưu đãi đặc biệt và thông tin chung về hàng hóa, dịch vụ và sự kiện khác mà chúng tôi cung cấp tương tự như những sản phẩm mà bạn đã mua hoặc yêu cầu trừ khi Bạn đã chọn không nhận thông tin đó. \n- Để quản lý các yêu cầu của Bạn: Để tham dự và quản lý các yêu cầu của Bạn với Chúng tôi. \n -Đối với chuyển giao doanh nghiệp: Chúng tôi có thể sử dụng thông tin của Bạn để đánh giá hoặc tiến hành sáp nhập, giải thể, tái cơ cấu, tổ chức lại, giải thể hoặc bán hoặc chuyển nhượng một số hoặc tất cả tài sản của Chúng tôi, cho dù là hoạt động liên tục hay là một phần của phá sản, thanh lý, hoặc thủ tục tương tự, trong đó Dữ liệu Cá nhân do Chúng tôi nắm giữ về người dùng Dịch vụ của chúng tôi nằm trong số các tài sản được chuyển giao. \n- Cho các mục đích khác: Chúng tôi có thể sử dụng thông tin của Bạn cho các mục đích khác, chẳng hạn như phân tích dữ liệu, xác định xu hướng sử dụng, xác định hiệu quả của các chiến dịch khuyến mại và để đánh giá và cải thiện Dịch vụ của chúng tôi, sản phẩm, dịch vụ, tiếp thị và trải nghiệm của bạn. \nChúng tôi có thể chia sẻ thông tin cá nhân của Bạn trong các trường hợp sau: \n- Với Nhà cung cấp Dịch vụ: Chúng tôi có thể chia sẻ thông tin cá nhân của Bạn với Nhà cung cấp Dịch vụ để theo dõi và phân tích việc sử dụng Dịch vụ của chúng tôi, để liên hệ với Bạn. \n- Đối với việc chuyển giao kinh doanh: Chúng tôi có thể chia sẻ hoặc chuyển giao thông tin cá nhân của Bạn liên quan đến, hoặc trong quá trình đàm phán, bất kỳ việc sáp nhập, bán tài sản của Công ty, tài trợ hoặc mua lại tất cả hoặc một phần hoạt động kinh doanh của Chúng tôi cho một công ty khác. \n- Với các Chi nhánh: Chúng tôi có thể chia sẻ thông tin của Bạn với các chi nhánh của Chúng tôi, trong trường hợp đó, chúng tôi sẽ yêu cầu các chi nhánh đó tuân theo Chính sách Bảo mật này. Các chi nhánh bao gồm công ty mẹ của Chúng tôi và bất kỳ công ty con nào khác, các đối tác liên doanh hoặc các công ty khác mà Chúng tôi kiểm soát hoặc nằm dưới sự kiểm soát chung của Chúng tôi. \n- Với các đối tác kinh doanh: Chúng tôi có thể chia sẻ thông tin của Bạn với các đối tác kinh doanh của Chúng tôi để cung cấp cho Bạn các sản phẩm, dịch vụ hoặc chương trình khuyến mãi nhất định. \n- Với những người dùng khác: khi Bạn chia sẻ thông tin cá nhân hoặc tương tác ở các khu vực công cộng với những người dùng khác, những thông tin đó có thể được tất cả người dùng xem và có thể bị phân phối công khai ra bên ngoài. Nếu Bạn tương tác với những người dùng khác hoặc đăng ký thông qua Dịch vụ truyền thông xã hội của bên thứ ba, các liên hệ của Bạn trên Dịch vụ truyền thông xã hội của bên thứ ba có thể thấy tên, hồ sơ, ảnh và mô tả hoạt động của Bạn. Tương tự, những người dùng khác sẽ có thể xem mô tả hoạt động của Bạn, giao tiếp với Bạn và xem hồ sơ của Bạn. \n- Với sự đồng ý của Bạn: Chúng tôi có thể tiết lộ thông tin cá nhân của Bạn cho bất kỳ mục đích nào khác với sự đồng ý của Bạn. "
  ),
  DataTerm(
      title:"Lưu giữ dữ liệu cá nhân của bạn ",
      menu:"",
      detail: "Công ty sẽ chỉ lưu giữ Dữ liệu Cá nhân của Bạn chừng nào cần thiết cho các mục đích được nêu trong Chính sách Bảo mật này. Chúng tôi sẽ lưu giữ và sử dụng Dữ liệu Cá nhân của Bạn trong phạm vi cần thiết để tuân thủ các nghĩa vụ pháp lý của chúng tôi (ví dụ: nếu chúng tôi được yêu cầu giữ lại dữ liệu của bạn để tuân thủ luật hiện hành), giải quyết tranh chấp và thực thi các thỏa thuận và chính sách pháp lý của chúng tôi. \nCông ty cũng sẽ giữ lại Dữ liệu sử dụng cho các mục đích phân tích nội bộ. Dữ liệu sử dụng thường được lưu giữ trong một khoảng thời gian ngắn hơn, ngoại trừ khi dữ liệu này được sử dụng để tăng cường bảo mật hoặc để cải thiện chức năng của Dịch vụ của chúng tôi hoặc Chúng tôi có nghĩa vụ pháp lý phải lưu giữ dữ liệu này trong khoảng thời gian dài hơn. "
  ),
  DataTerm(
      title: "Chuyển dữ liệu cá nhân của bạn ",
      menu: "",
      detail: "Thông tin của bạn, bao gồm cả Dữ liệu Cá nhân, được xử lý tại các văn phòng điều hành của Công ty và ở bất kỳ nơi nào khác có trụ sở của các bên liên quan đến việc xử lý. Điều đó có nghĩa là thông tin này có thể được chuyển đến - và duy trì trên - các máy tính nằm bên ngoài tiểu bang, tỉnh, quốc gia của Bạn hoặc khu vực tài phán khác của chính phủ, nơi luật bảo vệ dữ liệu có thể khác với luật pháp thuộc quyền tài phán của Bạn. \nSự đồng ý của Bạn đối với Chính sách Bảo mật này, tiếp theo là việc Bạn gửi thông tin đó thể hiện sự đồng ý của Bạn đối với việc chuyển giao đó. \nCông ty sẽ thực hiện tất cả các bước cần thiết một cách hợp lý để đảm bảo rằng dữ liệu của Bạn được xử lý an toàn và phù hợp với Chính sách quyền riêng tư này và sẽ không có việc chuyển Dữ liệu Cá nhân của Bạn cho một tổ chức hoặc một quốc gia trừ khi có các biện pháp kiểm soát thích hợp bao gồm cả bảo mật của Dữ liệu của bạn và thông tin cá nhân khác."
  ),
  DataTerm(
      title: "Tiết lộ dữ liệu cá nhân của bạn ",
      menu:"Giao dịch kinh doanh",
      detail: "Nếu Công ty tham gia vào việc sáp nhập, mua lại hoặc bán tài sản, Dữ liệu Cá nhân của Bạn có thể được chuyển giao. Chúng tôi sẽ cung cấp thông báo trước khi Dữ liệu Cá nhân của Bạn được chuyển giao và trở thành đối tượng của Chính sách Bảo mật khác. "
  ),
  DataTerm(
      title: "",
      menu: "Thực thi pháp luật ",
      detail: "Trong một số trường hợp nhất định, Công ty có thể được yêu cầu tiết lộ Dữ liệu Cá nhân của Bạn nếu luật pháp yêu cầu làm như vậy hoặc theo yêu cầu hợp lệ của cơ quan công quyền (ví dụ: tòa án hoặc cơ quan chính phủ). "
  ),
  DataTerm(
      title: "",
      menu: "Các yêu cầu pháp lý khác ",
      detail: "Công ty có thể tiết lộ Dữ liệu Cá nhân của Bạn với thiện chí tin rằng hành động đó là cần thiết để: \n- Tuân thủ nghĩa vụ pháp lý\n- Bảo vệ quyền lợi của Công ty\n- Ngăn chặn hoặc điều tra hành vi sai trái có thể xảy ra liên quan đến Dịch vụ\n- Bảo vệ sự an toàn cá nhân của Người dùng Dịch vụ hoặc công chúng\n- Bảo vệ khỏi trách nhiệm pháp lý "
  ),
  DataTerm(
      title: "Bảo mật dữ liệu cá nhân của bạn ",
      menu: "",
      detail: "Việc bảo mật Dữ liệu Cá nhân của Bạn là quan trọng đối với Chúng tôi, nhưng hãy nhớ rằng không có phương thức truyền tải nào qua Internet hoặc phương pháp lưu trữ điện tử là an toàn 100%. Trong khi Chúng tôi cố gắng sử dụng các phương tiện được chấp nhận về mặt thương mại để bảo vệ Dữ liệu Cá nhân của Bạn, Chúng tôi không thể đảm bảo tính bảo mật tuyệt đối của Dữ liệu đó. "
  ),
  DataTerm(
      title: "Quyền riêng tư của trẻ em",
      menu: "",
      detail: "Dịch vụ của chúng tôi không giải quyết bất kỳ ai dưới 13 tuổi. Chúng tôi không cố ý thu thập thông tin nhận dạng cá nhân từ bất kỳ ai dưới 13 tuổi. Nếu Bạn là cha mẹ hoặc người giám hộ và Bạn biết rằng con Bạn đã cung cấp Dữ liệu Cá nhân cho Chúng tôi, vui lòng liên hệ chúng tôi. Nếu Chúng tôi biết rằng Chúng tôi đã thu thập Dữ liệu Cá nhân từ bất kỳ ai dưới 13 tuổi mà không có sự xác minh của sự đồng ý của cha mẹ, Chúng tôi sẽ thực hiện các bước để xóa thông tin đó khỏi máy chủ của Chúng tôi. \nNếu Chúng tôi cần dựa trên sự đồng ý làm cơ sở pháp lý để xử lý thông tin của Bạn và Quốc gia của Bạn yêu cầu sự đồng ý từ cha mẹ, Chúng tôi có thể yêu cầu sự đồng ý của cha mẹ Bạn trước khi Chúng tôi thu thập và sử dụng thông tin đó. "
  ),
  DataTerm(
      title: "Liên kết đến các trang web khác ",
      menu: "",
      detail: "Dịch vụ của Chúng tôi có thể chứa các liên kết đến các trang web khác không do Chúng tôi điều hành. Nếu Bạn nhấp vào liên kết của bên thứ ba, Bạn sẽ được dẫn đến trang web của bên thứ ba đó. Chúng tôi đặc biệt khuyên Bạn nên xem lại Chính sách Bảo mật của mọi trang web Bạn truy cập. \nChúng tôi không kiểm soát và không chịu trách nhiệm về nội dung, chính sách bảo mật hoặc thông lệ của bất kỳ trang web hoặc dịch vụ nào của bên thứ ba. "
  ),
  DataTerm(
      title: "Các thay đổi đối với Chính sách quyền riêng tư này",
      menu: "",
      detail: 'Chúng tôi có thể cập nhật Chính sách Bảo mật của chúng tôi theo thời gian. Chúng tôi sẽ thông báo cho Bạn về bất kỳ thay đổi nào bằng cách đăng Chính sách Bảo mật mới trên trang này.\nChúng tôi sẽ cho Bạn biết qua email và / hoặc một thông báo nổi bật trên Dịch vụ của Chúng tôi, trước khi thay đổi có hiệu lực và cập nhật ngày "Cập nhật lần cuối" ở đầu Chính sách Bảo mật này.\nBạn nên xem lại Chính sách Bảo mật này định kỳ để biết bất kỳ thay đổi nào. Các thay đổi đối với Chính sách Bảo mật này có hiệu lực khi chúng được đăng trên trang này. '
  ),
  DataTerm(
      title: "Liên hệ chúng tôi ",
      menu: "",
      detail: "Nếu bạn có bất kỳ câu hỏi nào về Chính sách Bảo mật này, Bạn có thể liên hệ với chúng tôi: \n- Bằng cách truy cập trang này trên trang web của chúng tôi: shopxecuongk.com\nTheo số điện thoại: 0888898000 "
  )
];
