// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'post.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Post _$PostFromJson(Map<String, dynamic> json) {
  return Post(
    json['id'] as int,
    json['name'] as String,
    json['image'] as String,
    json['description'] as String,
    json['content'] as String,
    json['views'] as int,
    json['status'] as int,
    json['created_at'] as String,
    json['updated_at'] as String,
  );
}

Map<String, dynamic> _$PostToJson(Post instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'image': instance.image,
      'description': instance.description,
      'content': instance.content,
      'views': instance.views,
      'status': instance.status,
      'created_at': instance.created,
      'updated_at': instance.updatedAt,
    };
