import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:project_bike_car/model/entity/post.dart';
import 'package:project_bike_car/model/entity/posts.dart';
import 'package:project_bike_car/model/entity/posts_news.dart';

part 'notification.g.dart';

@JsonSerializable()
class Notification extends Equatable {
  @JsonKey(name: "id")
  int id;
  @JsonKey(name: "post_id")
  int postId;
  @JsonKey(name: "status")
  int status;
  @JsonKey(name: "created_at")
  String createdAt;
  @JsonKey(name: "updated_at")
  String updatedAt;
  @JsonKey(name: "post")
  PostsNews post;
  Notification(
      this.id,
      this.postId,
      this.status,
      this.createdAt,
      this.updatedAt,
      this.post
    );

  @override
  String toString() {
    return 'Notification{id: $id, postId: $postId, status: $status, createdAt: $createdAt, updatedAt: $updatedAt, post: $post}';
  }

  factory Notification.fromJson(Map<String, dynamic> json) => _$NotificationFromJson(json);

  Map<String, dynamic> toJson() => _$NotificationToJson(this);

  @override
  List<Object> get props => [
        id,
    postId,
    status,
    createdAt,
    updatedAt,
    post
      ];
}
