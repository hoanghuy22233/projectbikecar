import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'brands.g.dart';

@JsonSerializable()
class Brands extends Equatable {
  int id;
  String name;
  String image;
  int status;
  @JsonKey(name: "created_at")
  String created;
  @JsonKey(name: "updated_at")
  String updatedAt;


  Brands(this.id, this.name, this.image, this.status, this.created,
      this.updatedAt,);

  @override
  String toString() {
    return 'Brands{id: $id, name: $name, image: $image,status:$status ,created: $created ,updateAt: $updatedAt,}';
  }

  factory Brands.fromJson(Map<String, dynamic> json) => _$BrandsFromJson(json);

  Map<String, dynamic> toJson() => _$BrandsToJson(this);

  @override
  List<Object> get props => [
    id,
    image,
    name,status,created,updatedAt,

      ];
}
