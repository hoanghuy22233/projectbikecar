import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'category_new.g.dart';

@JsonSerializable()
class CategoryNew extends Equatable {
  int id;
  String name;
  String image;
  int status;
  @JsonKey(name: "created_at")
  String created;
  @JsonKey(name: "updated_at")
  String updatedAt;
  int type;


  CategoryNew(this.id, this.name, this.image, this.status, this.created,
      this.updatedAt, this.type);

  @override
  String toString() {
    return 'CategoryNew{id: $id, name: $name, image: $image,status:$status ,created: $created ,updateAt: $updatedAt, type:$type }';
  }

  factory CategoryNew.fromJson(Map<String, dynamic> json) => _$CategoryNewFromJson(json);

  Map<String, dynamic> toJson() => _$CategoryNewToJson(this);

  @override
  List<Object> get props => [
    id,
    image,
    name,status,created,updatedAt,type

      ];
}
