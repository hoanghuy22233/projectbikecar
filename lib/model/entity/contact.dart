import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'contact.g.dart';

@JsonSerializable()
class Contact extends Equatable {
  int id;
  String name;
  @JsonKey(name:"member_id")
  int memberId;
  String email;
  String phone;
  String content;
  int type;
  int status;
  @JsonKey(name: "updated_at")
  String updatedAt;
  @JsonKey(name: "created_at")
  String created;


  Contact(this.id, this.name, this.memberId, this.email, this.phone,
      this.content, this.type,this.status,this.updatedAt,this.created);

  @override
  String toString() {
    return 'Contact{id: $id, name: $name, memberId: $memberId,email:$email ,phone: $phone ,content: $content, type:$type, status:$status, updatedAt:$updatedAt, created:$created }';
  }

  factory Contact.fromJson(Map<String, dynamic> json) => _$ContactFromJson(json);

  Map<String, dynamic> toJson() => _$ContactToJson(this);

  @override
  List<Object> get props => [
    id,
    name,
    memberId,email,phone,content,type,status,updatedAt,created
      ];
}
