// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'new_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NewData _$NewDataFromJson(Map<String, dynamic> json) {
  return NewData(
    posts: (json['posts'] as List<dynamic>)
        .map((e) => PostsNews.fromJson(e as Map<String, dynamic>))
        .toList(),
    categorys: (json['categories'] as List<dynamic>)
        .map((e) => CategoryNew.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$NewDataToJson(NewData instance) => <String, dynamic>{
      'categories': instance.categorys,
      'posts': instance.posts,
    };
