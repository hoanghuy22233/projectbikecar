// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'contact.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Contact _$ContactFromJson(Map<String, dynamic> json) {
  return Contact(
    json['id'] as int,
    json['name'] as String,
    json['member_id'] as int,
    json['email'] as String,
    json['phone'] as String,
    json['content'] as String,
    json['type'] as int,
    json['status'] as int,
    json['updated_at'] as String,
    json['created_at'] as String,
  );
}

Map<String, dynamic> _$ContactToJson(Contact instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'member_id': instance.memberId,
      'email': instance.email,
      'phone': instance.phone,
      'content': instance.content,
      'type': instance.type,
      'status': instance.status,
      'updated_at': instance.updatedAt,
      'created_at': instance.created,
    };
