// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'new_detail_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NewDetailData _$NewDetailDataFromJson(Map<String, dynamic> json) {
  return NewDetailData(
    posts: (json['posts'] as List<dynamic>)
        .map((e) => PostsNews.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$NewDetailDataToJson(NewDetailData instance) =>
    <String, dynamic>{
      'posts': instance.posts,
    };
