import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'posts_news.g.dart';

@JsonSerializable()
class PostsNews extends Equatable {
  int id;
  String name;
  String slug;
  String image;
  String description;
  String content;
  int views;
  int status;
  @JsonKey(name: "created_at")
  String created;
  @JsonKey(name: "updated_at")
  String updatedAt;
  String link;


  PostsNews(this.id, this.name, this.slug, this.image, this.description,this.content, this.views,
      this.status, this.created, this.updatedAt,this.link);

  @override
  String toString() {
    return 'PostsNews{id: $id, name: $name,slug:$slug, image: $image,description:$description,content:$content,views:$views,status:$status ,created: $created ,updateAt: $updatedAt,link:$link}';
  }

  factory PostsNews.fromJson(Map<String, dynamic> json) => _$PostsNewsFromJson(json);

  Map<String, dynamic> toJson() => _$PostsNewsToJson(this);

  @override
  List<Object> get props => [
    id,name,slug,
    image,
    description,content,views,status,created,updatedAt,link
      ];
}
