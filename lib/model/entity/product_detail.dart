import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'product_detail.g.dart';

@JsonSerializable()
class ProductDetail extends Equatable {
  int id;
  String code;
  String name;
  String description;
  String content;
  String image;
  List<String> images;
  int price;
  @JsonKey(name: "brand_id")
  int brandId;
  int status;
  @JsonKey(name: "created_at")
  String created;
  @JsonKey(name: "updated_at")
  String updatedAt;
  @JsonKey(name: "price_old")
  int priceOld;
  int condition;
  int used;
  int kilometer;
  int views;
  int type;
  String year;
  String fuel;
  String engine;
  String color;
  String address;
  dynamic categories;
  dynamic brand;
  @JsonKey(name: "type_of_sim")
  String typeOfSim;
  @JsonKey(name: "link_video")
  String linkVideo;


  ProductDetail(
      this.id,
      this.code,
      this.name,
      this.description,
      this.content,
      this.image,
      this.images,
      this.price,
      this.brandId,
      this.status,
      this.created,
      this.updatedAt,
      this.priceOld,
      this.condition,
      this.used,
      this.kilometer,
      this.views,
      this.type,
      this.year,
      this.fuel,
      this.engine,
      this.color,
      this.address,
      this.categories,
      this.brand,this.typeOfSim,this.linkVideo);

  @override
  String toString() {
    return 'ProductDetail{id: $id,code:$code, name: $name,description:$description,content:$content, image: $image,images:$images,price:$price,brandId:$brandId, status:$status ,created: $created ,updateAt: $updatedAt,categories:$categories,brand:$brand,typeOfSim:$typeOfSim,linkVideo:$linkVideo}';
  }

  factory ProductDetail.fromJson(Map<String, dynamic> json) => _$ProductDetailFromJson(json);

  Map<String, dynamic> toJson() => _$ProductDetailToJson(this);

  @override
  List<Object> get props => [
    id,code,
    name,description,content,image,images,price,brandId,status,created,updatedAt,priceOld,condition,used,kilometer,views,type,year,fuel,engine,color,address,categories,brand,typeOfSim,linkVideo

  ];
}
