// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'posts_news.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PostsNews _$PostsNewsFromJson(Map<String, dynamic> json) {
  return PostsNews(
    json['id'] as int,
    json['name'] as String,
    json['slug'] as String,
    json['image'] as String,
    json['description'] as String,
    json['content'] as String,
    json['views'] as int,
    json['status'] as int,
    json['created_at'] as String,
    json['updated_at'] as String,
    json['link'] as String,
  );
}

Map<String, dynamic> _$PostsNewsToJson(PostsNews instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'slug': instance.slug,
      'image': instance.image,
      'description': instance.description,
      'content': instance.content,
      'views': instance.views,
      'status': instance.status,
      'created_at': instance.created,
      'updated_at': instance.updatedAt,
      'link': instance.link,
    };
