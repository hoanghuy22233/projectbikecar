import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:project_bike_car/model/entity/barrel_entity.dart';
import 'package:project_bike_car/model/entity/products.dart';

part 'order.g.dart';

@JsonSerializable()
class Order extends Equatable {
  @JsonKey(name: "id")
  int id;
  @JsonKey(name: "email")
  String email;
  @JsonKey(name: "phone")
  int phone;
  @JsonKey(name: "member_id")
  int memberId;
  @JsonKey(name: "price")
  int price;
  @JsonKey(name: "product_id")
  int productId;
  String address;
  String content;
  int status;
  @JsonKey(name: "created_at")
  String created;
  @JsonKey(name: "updated_at")
  String updatedAt;
  Products product;


  Order(
      this.id,
      this.email,
      this.phone,
      this.memberId,
      this.price,
      this.productId,
      this.address,
      this.content,
      this.status,
      this.created,
      this.updatedAt,
      this.product);

  @override
  String toString() {
    return 'Order{$id,$email,$phone,$memberId,$price,$productId,$address,$content,$status,$created,$updatedAt,$product}';
  }

  factory Order.fromJson(Map<String, dynamic> json) => _$OrderFromJson(json);

  Map<String, dynamic> toJson() => _$OrderToJson(this);

  @override
  List<Object> get props => [
id,email,phone,memberId,price,productId,address,content,status,created,updatedAt,product

      ];
}
