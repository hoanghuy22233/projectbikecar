import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'post.g.dart';

@JsonSerializable()
class Post extends Equatable {
  int id;
  String name;
  String image;
  String description;
  String content;
  int views;
  int status;
  @JsonKey(name: "created_at")
  String created;
  @JsonKey(name: "updated_at")
  String updatedAt;


  Post(this.id, this.name, this.image, this.description,this.content, this.views,
      this.status, this.created, this.updatedAt);

  @override
  String toString() {
    return 'Post{id: $id, name: $name, image: $image,description:$description,content:$content,views:$views,status:$status ,created: $created ,updateAt: $updatedAt,}';
  }

  factory Post.fromJson(Map<String, dynamic> json) => _$PostFromJson(json);

  Map<String, dynamic> toJson() => _$PostToJson(this);

  @override
  List<Object> get props => [
    id,name,
    image,
    description,content,views,status,created,updatedAt,

      ];
}
