// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'buy.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Buy _$BuyFromJson(Map<String, dynamic> json) {
  return Buy(
    json['member_id'] as int,
    json['product_id'] as int,
    json['price'] as int,
    json['status'] as int,
    json['phone'] as String,
    json['address'] as String,
    json['content'] as String,
    json['created_at'] as String,
    json['updated_at'] as String,
    json['id'] as int,
    User.fromJson(json['member'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$BuyToJson(Buy instance) => <String, dynamic>{
      'member_id': instance.memberId,
      'product_id': instance.productId,
      'price': instance.price,
      'status': instance.status,
      'phone': instance.phone,
      'address': instance.address,
      'content': instance.content,
      'created_at': instance.created,
      'updated_at': instance.updatedAt,
      'id': instance.id,
      'member': instance.member,
    };
