import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:project_bike_car/model/entity/posts.dart';
import 'package:project_bike_car/model/entity/posts_news.dart';

part 'new_detail_data.g.dart';

@JsonSerializable()
class NewDetailData extends Equatable {
  @JsonKey(name: "posts")
  List<PostsNews> posts;

  NewDetailData({this.posts});

  factory NewDetailData.fromJson(Map<String, dynamic> json) =>
      _$NewDetailDataFromJson(json);

  Map<String, dynamic> toJson() => _$NewDetailDataToJson(this);

  @override
  List<Object> get props => [posts];
}
