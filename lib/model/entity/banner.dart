import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'banner.g.dart';

@JsonSerializable()
class Banners extends Equatable {
  int id;
  String name;
  String image;
  @JsonKey(name: "created_at")
  String created;
  @JsonKey(name: "updated_at")
  String updatedAt;


  Banners(
      this.id, this.name, this.image, this.created, this.updatedAt);

  @override
  String toString() {
    return 'Banner{id: $id, name: $name, image: $image,created: $created ,updateAt: $updatedAt }';
  }

  factory Banners.fromJson(Map<String, dynamic> json) => _$BannersFromJson(json);

  Map<String, dynamic> toJson() => _$BannersToJson(this);

  @override
  List<Object> get props => [
    id,
    image,
    name,created,updatedAt

      ];
}
