// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    json['id'] as int,
    json['email'] as String,
    json['name'] as String,
    json['otp'] as String,
    json['avatar'] as String,
    json['cover_image'] as String,
    json['description'] as String,
    json['address'] as String,
    json['email_verified_at'] as String,
    json['status'] as int,
    json['created_at'] as String,
    json['updated_at'] as String,
    json['password'] as String,
    json['phone'] as String,
    json['birthday'] as String,
  );
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'id': instance.id,
      'otp': instance.otp,
      'name': instance.name,
      'email': instance.email,
      'avatar': instance.avatar,
      'cover_image': instance.coverImage,
      'description': instance.description,
      'address': instance.address,
      'email_verified_at': instance.emailVerifiedAt,
      'status': instance.status,
      'created_at': instance.createdAt,
      'updated_at': instance.updatedAt,
      'password': instance.password,
      'phone': instance.phone,
      'birthday': instance.birthday,
    };
