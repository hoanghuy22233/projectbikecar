import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'posts.g.dart';

@JsonSerializable()
class Posts extends Equatable {
  int id;
  String name;
  String image;
  String description;
  String content;
  int views;
  int status;
  @JsonKey(name: "created_at")
  String created;
  @JsonKey(name: "updated_at")
  String updatedAt;


  Posts(this.id, this.name, this.image, this.description,this.content, this.views,
      this.status, this.created, this.updatedAt);

  @override
  String toString() {
    return 'Posts{id: $id, name: $name, image: $image,description:$description,content:$content,views:$views,status:$status ,created: $created ,updateAt: $updatedAt,}';
  }

  factory Posts.fromJson(Map<String, dynamic> json) => _$PostsFromJson(json);

  Map<String, dynamic> toJson() => _$PostsToJson(this);

  @override
  List<Object> get props => [
    id,name,
    image,
    description,content,views,status,created,updatedAt,

      ];
}
