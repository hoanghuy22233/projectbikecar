import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

@JsonSerializable()
class User extends Equatable {
  @JsonKey(name: "id")
  int id;
  @JsonKey(name: "otp")
  String otp;
  @JsonKey(name: "name")
  String name;
  @JsonKey(name: "email")
  String email;
  @JsonKey(name: "avatar")
  String avatar;
  @JsonKey(name: "cover_image")
  String coverImage;
  @JsonKey(name: "description")
  String description;
  @JsonKey(name: "address")
  String address;
  @JsonKey(name: "email_verified_at")
  String emailVerifiedAt;
  @JsonKey(name: "status")
  int status;
  @JsonKey(name: "created_at")
  String createdAt;
  @JsonKey(name: "updated_at")
  String updatedAt;
  @JsonKey(name: "password")
  String password;
  String phone;
  String birthday;
  User(
      this.id,
      this.email,
      this.name,
      this.otp,
      this.avatar,
      this.coverImage,
      this.description,
      this.address,
      this.emailVerifiedAt,
      this.status,
      this.createdAt,
      this.updatedAt,
      this.password,this.phone,this.birthday);

  @override
  String toString() {
    return 'User{id: $id, name: $name, email: $email, otp: $otp, avatar: $avatar, coverImage: $coverImage, description: $description,address: $address, emailVerifiedAt: $emailVerifiedAt,status: $status, createdAt: $createdAt, updatedAt: $updatedAt,password:$password,phone:$phone,birth:$birthday}';
  }

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);

  @override
  List<Object> get props => [
        id,
        email,
        name,
        otp,
        avatar,
        coverImage,
        description,
        address,
        emailVerifiedAt,
        status,
        createdAt,
        updatedAt,
     password,phone,birthday
      ];
}
