// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Order _$OrderFromJson(Map<String, dynamic> json) {
  return Order(
    json['id'] as int,
    json['email'] as String,
    json['phone'] as int,
    json['member_id'] as int,
    json['price'] as int,
    json['product_id'] as int,
    json['address'] as String,
    json['content'] as String,
    json['status'] as int,
    json['created_at'] as String,
    json['updated_at'] as String,
    Products.fromJson(json['product'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$OrderToJson(Order instance) => <String, dynamic>{
      'id': instance.id,
      'email': instance.email,
      'phone': instance.phone,
      'member_id': instance.memberId,
      'price': instance.price,
      'product_id': instance.productId,
      'address': instance.address,
      'content': instance.content,
      'status': instance.status,
      'created_at': instance.created,
      'updated_at': instance.updatedAt,
      'product': instance.product,
    };
