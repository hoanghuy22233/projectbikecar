import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'category.g.dart';

@JsonSerializable()
class Category extends Equatable {
  int id;
  String name;
  String image;
  int status;
  @JsonKey(name: "created_at")
  String created;
  @JsonKey(name: "updated_at")
  String updatedAt;
  int type;


  Category(this.id, this.name, this.image, this.status, this.created,
      this.updatedAt, this.type);

  @override
  String toString() {
    return 'Category{id: $id, name: $name, image: $image,status:$status ,created: $created ,updateAt: $updatedAt, type:$type }';
  }

  factory Category.fromJson(Map<String, dynamic> json) => _$CategoryFromJson(json);

  Map<String, dynamic> toJson() => _$CategoryToJson(this);

  @override
  List<Object> get props => [
    id,
    image,
    name,status,created,updatedAt,type

      ];
}
