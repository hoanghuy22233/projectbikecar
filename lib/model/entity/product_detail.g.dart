// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_detail.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductDetail _$ProductDetailFromJson(Map<String, dynamic> json) {
  return ProductDetail(
    json['id'] as int,
    json['code'] as String,
    json['name'] as String,
    json['description'] as String,
    json['content'] as String,
    json['image'] as String,
    (json['images'] as List<dynamic>).map((e) => e as String).toList(),
    json['price'] as int,
    json['brand_id'] as int,
    json['status'] as int,
    json['created_at'] as String,
    json['updated_at'] as String,
    json['price_old'] as int,
    json['condition'] as int,
    json['used'] as int,
    json['kilometer'] as int,
    json['views'] as int,
    json['type'] as int,
    json['year'] as String,
    json['fuel'] as String,
    json['engine'] as String,
    json['color'] as String,
    json['address'] as String,
    json['categories'],
    json['brand'],
    json['type_of_sim'] as String,
    json['link_video'] as String,
  );
}

Map<String, dynamic> _$ProductDetailToJson(ProductDetail instance) =>
    <String, dynamic>{
      'id': instance.id,
      'code': instance.code,
      'name': instance.name,
      'description': instance.description,
      'content': instance.content,
      'image': instance.image,
      'images': instance.images,
      'price': instance.price,
      'brand_id': instance.brandId,
      'status': instance.status,
      'created_at': instance.created,
      'updated_at': instance.updatedAt,
      'price_old': instance.priceOld,
      'condition': instance.condition,
      'used': instance.used,
      'kilometer': instance.kilometer,
      'views': instance.views,
      'type': instance.type,
      'year': instance.year,
      'fuel': instance.fuel,
      'engine': instance.engine,
      'color': instance.color,
      'address': instance.address,
      'categories': instance.categories,
      'brand': instance.brand,
      'type_of_sim': instance.typeOfSim,
      'link_video': instance.linkVideo,
    };
