import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/model/entity/brands.dart';
import 'package:project_bike_car/model/entity/posts.dart';
import 'package:project_bike_car/model/entity/posts_news.dart';
import 'package:project_bike_car/model/entity/products.dart';

import 'category.dart';

part 'home_data.g.dart';

@JsonSerializable()
class HomeData extends Equatable {
  @JsonKey(name: "banners")
  List<Banners> listBanner;
  @JsonKey(name: "categories")
  List<Category> category;
  @JsonKey(name: "brands")
  List<Brands> brands;
  @JsonKey(name: "products")
  List<Products> products;
  @JsonKey(name: "posts")
  List<PostsNews> posts;

  HomeData({this.listBanner, this.category, this.brands,this.products,this.posts});

  factory HomeData.fromJson(Map<String, dynamic> json) =>
      _$HomeDataFromJson(json);

  Map<String, dynamic> toJson() => _$HomeDataToJson(this);

  @override
  List<Object> get props => [listBanner, category, brands,products,posts];
}
