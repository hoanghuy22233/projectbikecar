// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HomeData _$HomeDataFromJson(Map<String, dynamic> json) {
  return HomeData(
    listBanner: (json['banners'] as List<dynamic>)
        .map((e) => Banners.fromJson(e as Map<String, dynamic>))
        .toList(),
    category: (json['categories'] as List<dynamic>)
        .map((e) => Category.fromJson(e as Map<String, dynamic>))
        .toList(),
    brands: (json['brands'] as List<dynamic>)
        .map((e) => Brands.fromJson(e as Map<String, dynamic>))
        .toList(),
    products: (json['products'] as List<dynamic>)
        .map((e) => Products.fromJson(e as Map<String, dynamic>))
        .toList(),
    posts: (json['posts'] as List<dynamic>)
        .map((e) => PostsNews.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$HomeDataToJson(HomeData instance) => <String, dynamic>{
      'banners': instance.listBanner,
      'categories': instance.category,
      'brands': instance.brands,
      'products': instance.products,
      'posts': instance.posts,
    };
