
import 'dart:io';

import 'package:project_bike_car/model/api/request/add_contact_request.dart';
import 'package:project_bike_car/model/api/request/barrel_request.dart';
import 'package:project_bike_car/model/api/request/buy_product_request.dart';
import 'package:project_bike_car/model/api/request/change_password_app_request.dart';
import 'package:project_bike_car/model/api/request/forgot_password_reset_request.dart';
import 'package:project_bike_car/model/api/request/register_app_request.dart';
import 'package:project_bike_car/model/api/request/resend_otp_request.dart';
import 'package:project_bike_car/model/api/response/add_contact_response.dart';
import 'package:project_bike_car/model/api/request/search_product_can_app_request.dart';
import 'package:project_bike_car/model/api/request/update_address_request.dart';
import 'package:project_bike_car/model/api/request/update_birthday_request.dart';
import 'package:project_bike_car/model/api/request/update_content_request.dart';
import 'package:project_bike_car/model/api/request/update_email_request.dart';
import 'package:project_bike_car/model/api/request/update_name_request.dart';
import 'package:project_bike_car/model/api/request/update_phone_request.dart';
import 'package:project_bike_car/model/api/response/banner_response.dart';
import 'package:project_bike_car/model/api/response/barrel_response.dart';
import 'package:project_bike_car/model/api/response/buy_product_response.dart';
import 'package:project_bike_car/model/api/response/category_response.dart';
import 'package:project_bike_car/model/api/response/change_password_response.dart';
import 'package:project_bike_car/model/api/response/forgot_password_reset_response.dart';
import 'package:project_bike_car/model/api/response/home_response.dart';
import 'package:project_bike_car/model/api/response/new_detail_response.dart';
import 'package:project_bike_car/model/api/response/notification_response.dart';
import 'package:project_bike_car/model/api/response/order_response.dart';
import 'package:project_bike_car/model/api/response/product_detail_response.dart';
import 'package:project_bike_car/model/api/response/profile_response.dart';
import 'package:project_bike_car/model/api/response/new_response.dart';
import 'package:project_bike_car/model/api/response/register_response.dart';
import 'package:project_bike_car/model/api/response/register_update_response.dart';
import 'package:project_bike_car/model/api/response/resend_otp_response.dart';
import 'package:project_bike_car/model/api/response/search_product_can_response.dart';
import 'package:project_bike_car/model/api/response/update_address_response.dart';
import 'package:project_bike_car/model/api/response/update_avatar_response.dart';
import 'package:project_bike_car/model/api/response/update_background_image_response.dart';
import 'package:project_bike_car/model/api/response/update_birthday_response.dart';
import 'package:project_bike_car/model/api/response/update_content_response.dart';
import 'package:project_bike_car/model/api/response/update_email_response.dart';
import 'package:project_bike_car/model/api/response/update_name_response.dart';
import 'package:project_bike_car/model/api/response/update_phone_response.dart';
import 'package:project_bike_car/model/api/rest_client.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:project_bike_car/model/entity/category_new.dart';
import 'package:project_bike_car/model/api/response/register_verify_response.dart';
import 'package:project_bike_car/model/api/request/register_verify_request.dart';
import 'package:project_bike_car/model/api/request/forgot_password_request.dart';
import 'package:project_bike_car/model/api/response/forgot_password_response.dart';
import 'package:project_bike_car/model/api/request/forgot_password_verify_request.dart';
import 'package:project_bike_car/model/api/response/forgot_password_verify_response.dart';
import 'package:project_bike_car/model/entity/new_detail_data.dart';
class UserRepository {
  final Dio dio;

  UserRepository({@required this.dio});
  // final GoogleSignIn _googleSignIn = GoogleSignIn();
  //final facebookLogin = FacebookLogin();

  Future<LoginRegisterResponse> loginApp(
      {@required String username,
      @required String password,
      }) async {
    final client = RestClient(dio);
    return client.loginApp(LoginAppRequest(
        email: username, password: password));
  }
  Future<RegisterUpdateResponse> registerApp(
      {@required String name,
        @required String email,
        @required String password,
        @required String confirmPassword,
      }) async {
    final client = RestClient(dio);
    return client.registerApp(RegisterAppRequest(
      name :name,
      email: email,
      password: password,
      confirmPassword: confirmPassword,

    ));
  }
  Future<ChangePasswordResponse> changePasswordApp(
      {@required String token,
        @required String oldPassword,
        @required String password,
        @required String confirmPassword,
      }) async {
    final client = RestClient(dio);
    return client.changePasswordApp(ChangePasswordAppRequest(
      token :token,
      oldPassword: oldPassword,
      newPassword: password,
      confirmPassword: confirmPassword,

    ));
  }
  Future<RegisterVerifyResponse> registerVerify(
      {@required String username, @required String otpCode}) async {
    final client = RestClient(dio);
    return client.registerVerify(
        RegisterVerifyRequest(emailOrPhone: username, otpCode: otpCode));
  }
  Future<ResendOtpResponse> resendRegisterOtp(
      {@required String username}) async {
    final client = RestClient(dio);
    return client.resendOtpRegister(ResendOtpRequest(emailOrPhone: username));
  }
  Future<ForgotPasswordResponse> forgotPassword(
      {@required String username}) async {
    final client = RestClient(dio);
    return client.forgotPassword(ForgotPasswordRequest(emailOrPhone: username));
  }
  Future<ForgotPasswordVerifyResponse> forgotPasswordVerify(
      {@required String username, @required String otpCode}) async {
    final client = RestClient(dio);

    return client.forgotPasswordVerify(
        ForgotPasswordVerifyRequest(emailOrPhone: username, otpCode: otpCode));
  }
  Future<ResendOtpResponse> resendForgotPasswordOtp(
      {@required String username}) async {
    final client = RestClient(dio);
    return client.resendOtp(ResendOtpRequest(emailOrPhone: username));
  }
  Future<ForgotPasswordResetResponse> forgotPasswordReset(
      {@required String username,
        @required String otpCode,
        @required String password,
 }) async {
    final client = RestClient(dio);
    print("kiem tra ${username}-${otpCode}-${username}");

    return client.forgotPasswordReset(ForgotPasswordResetRequest(
        emailOrPhone: username,
        otpCode: otpCode,
        password: password,
        ));
  }

  Future<BannerResponse> getBanner() async {
    final client = RestClient(dio);
    return client.getBanner();
  }

  Future<CategoryResponse> getCategory() async {
    final client = RestClient(dio);
    return client.getCategory();
  }

  Future<HomeResponse> getHome() async {
    final client = RestClient(dio);
    return client.getHome();
  }
  Future<NewResponse> getNew(int categoryId) async {
    final client = RestClient(dio);
    return client.getNew(categoryId);
  }
  Future<HomeResponse> getHomes( {@required int categoryId,@required int brandsId, @required String sort}) async {
    final client = RestClient(dio);
    return client.getHomes(categoryId,brandsId,sort??'id_desc');
  }


  Future<ProductDetailResponse> getProductDetail({
    @required int productId,
  }) async {
    final client = RestClient(dio);
    return client.getProductDetail(productId);
  }


  Future<ProductBuyResponse> productBuy({
    @required int productId,
    @required String token,
    @required String email,
    @required String address,
    @required String content,
    @required String phone,

  }) async {
    final client = RestClient(dio);
    return client.productBuy(BuyProductRequest(productId,
       token,email,address,phone,content));
  }


  Future<ProfileResponse> getProfile() async {
    final client = RestClient(dio);
    return client.getProfile();
  }

  Future<UpdateAvatarResponse> updateAvatar({@required File avatarFile}) async {
    final client = RestClient(dio);
    return client.updateAvatar(avatarFile);
  }

  Future<UpdateBackgroundImageResponse> updateBackgroundImage(
      {@required File backgroundImageFile}) async {
    final client = RestClient(dio);
    return client.updateBackgroundImage(backgroundImageFile);
  }

  Future<UpdateEmailResponse> updateEmail(
      {@required String email,}) async {
    final client = RestClient(dio);
    return client.updateEmail(UpdateEmailRequest(email: email,));
  }

  Future<UpdateBirthdayResponse> updateBirthDay(
      {@required String birthDay}) async {
    final client = RestClient(dio);
    return client.updateBirthday(UpdateBirthdayRequest(dateOfBirth: birthDay));
  }
  Future<UpdatePhoneResponse> updatePhone(
      {@required String phone,}) async {
    final client = RestClient(dio);
    return client
        .updatePhone(UpdatePhoneRequest(phoneNumber: phone,));
  }
  Future<UpdateNameResponse> updateName({@required String name}) async {
    final client = RestClient(dio);
    return client.updateName(UpdateNameRequest(name: name));
  }
  Future<UpdateContentResponse> updateContent({@required String content}) async {
    final client = RestClient(dio);
    return client.updateContent(UpdateContentRequest(description: content));
  }
  Future<UpdateAddressResponse> updateAddress({@required String address}) async {
    final client = RestClient(dio);
    return client.updateAddress(UpdateAddressRequest(address: address));
  }
  Future<OrderResponse> getOrder() async {
    final client = RestClient(dio);
    return client.getOrder();
  }

  Future<NotificationResponse> getNotification() async {
    final client = RestClient(dio);
    return client.getNotification();
  }
  Future<NewDetailResponse> getNewDetail( {@required int id}) async {
    final client = RestClient(dio);
    return client.getNewDetail(id);
  }
  Future<AddContactResponse> postAddContact({
    @required String phone,
    @required String name,
    @required String email,
    @required String content,
    @required String token,
  }) async {
    final client = RestClient(dio);
    return client.postAddContact(AddContactRequest(phone:phone,
        name:name,email:email,content:content,token:token));
  }

  Future<SearchProductCanResponse> searchProductApp(
      {@required String content,
        @required String name,
        @required String phone
      }) async {
    final client = RestClient(dio);
    return client.searchProductApp(SearchProductCanAppRequest(
      content :content,
      name: name,
      phone: phone,

    ));
  }

}
