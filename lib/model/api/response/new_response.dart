import 'package:json_annotation/json_annotation.dart';
import 'package:project_bike_car/model/entity/category.dart';
import 'package:project_bike_car/model/entity/new_data.dart';
import 'package:project_bike_car/model/entity/posts.dart';
import 'package:project_bike_car/model/entity/posts_news.dart';

import 'base_response.dart';

part 'new_response.g.dart';


@JsonSerializable()
class NewResponse extends BaseResponse {
  NewData data;

  NewResponse({this.data});

  factory NewResponse.fromJson(Map<String, dynamic> json) =>
      _$NewResponseFromJson(json);

  Map<String, dynamic> toJson() => _$NewResponseToJson(this);

}
