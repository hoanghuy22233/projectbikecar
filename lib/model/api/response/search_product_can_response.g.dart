// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'search_product_can_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SearchProductCanResponse _$SearchProductCanResponseFromJson(
    Map<String, dynamic> json) {
  return SearchProductCanResponse()
    ..status = json['status'] as bool
    ..msg = json['msg'] as String;
}

Map<String, dynamic> _$SearchProductCanResponseToJson(
        SearchProductCanResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'msg': instance.msg,
    };
