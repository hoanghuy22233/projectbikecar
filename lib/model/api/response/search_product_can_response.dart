import 'package:json_annotation/json_annotation.dart';
import 'package:project_bike_car/model/entity/user.dart';

import 'base_response.dart';

part 'search_product_can_response.g.dart';

@JsonSerializable()
class SearchProductCanResponse extends BaseResponse {
  SearchProductCanResponse();
  factory SearchProductCanResponse.fromJson(Map<String, dynamic> json) =>
      _$SearchProductCanResponseFromJson(json);

  Map<String, dynamic> toJson() => _$SearchProductCanResponseToJson(this);


}
