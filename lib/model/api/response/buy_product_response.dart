import 'package:json_annotation/json_annotation.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/model/entity/buy.dart';
import 'package:project_bike_car/model/entity/category.dart';
import 'package:project_bike_car/model/entity/product_detail.dart';

import 'base_response.dart';

part 'buy_product_response.g.dart';

@JsonSerializable()
class ProductBuyResponse extends BaseResponse {
  Buy data;

  ProductBuyResponse(this.data);

  factory ProductBuyResponse.fromJson(Map<String, dynamic> json) =>
      _$ProductBuyResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ProductBuyResponseToJson(this);
}
