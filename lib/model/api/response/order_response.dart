import 'package:json_annotation/json_annotation.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/model/entity/buy.dart';
import 'package:project_bike_car/model/entity/category.dart';
import 'package:project_bike_car/model/entity/order.dart';
import 'package:project_bike_car/model/entity/product_detail.dart';

import 'base_response.dart';

part 'order_response.g.dart';

@JsonSerializable()
class OrderResponse extends BaseResponse {
  List<Order> data;

  OrderResponse(this.data);

  factory OrderResponse.fromJson(Map<String, dynamic> json) =>
      _$OrderResponseFromJson(json);

  Map<String, dynamic> toJson() => _$OrderResponseToJson(this);
}
