// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'buy_product_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductBuyResponse _$ProductBuyResponseFromJson(Map<String, dynamic> json) {
  return ProductBuyResponse(
    Buy.fromJson(json['data'] as Map<String, dynamic>),
  )
    ..status = json['status'] as bool
    ..msg = json['msg'] as String;
}

Map<String, dynamic> _$ProductBuyResponseToJson(ProductBuyResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'msg': instance.msg,
      'data': instance.data,
    };
