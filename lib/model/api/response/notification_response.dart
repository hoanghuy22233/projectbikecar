import 'package:json_annotation/json_annotation.dart';
import 'package:project_bike_car/model/api/response/barrel_response.dart';
import 'package:project_bike_car/model/entity/barrel_entity.dart';
import 'package:project_bike_car/model/entity/notification.dart';

part 'notification_response.g.dart';

@JsonSerializable()
class NotificationResponse extends BaseResponse {
  List<Notification> data;

  NotificationResponse(this.data);

  factory NotificationResponse.fromJson(Map<String, dynamic> json) =>
      _$NotificationResponseFromJson(json);

  Map<String, dynamic> toJson() => _$NotificationResponseToJson(this);

  @override
  String toString() {
    return 'NotificationResponse{data: $data}';
  }
}
