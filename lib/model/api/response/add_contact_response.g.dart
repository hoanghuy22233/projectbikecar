// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'add_contact_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AddContactResponse _$AddContactResponseFromJson(Map<String, dynamic> json) {
  return AddContactResponse(
    Contact.fromJson(json['data'] as Map<String, dynamic>),
  )
    ..status = json['status'] as bool
    ..msg = json['msg'] as String;
}

Map<String, dynamic> _$AddContactResponseToJson(AddContactResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'msg': instance.msg,
      'data': instance.data,
    };
