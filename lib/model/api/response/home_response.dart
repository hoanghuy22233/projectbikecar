import 'package:json_annotation/json_annotation.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/model/entity/brands.dart';
import 'package:project_bike_car/model/entity/category.dart';
import 'package:project_bike_car/model/entity/home_data.dart';
import 'package:project_bike_car/model/entity/products.dart';

import 'base_response.dart';

part 'home_response.g.dart';


@JsonSerializable()
class HomeResponse extends BaseResponse {
  HomeData data;

  HomeResponse({this.data});

  factory HomeResponse.fromJson(Map<String, dynamic> json) =>
      _$HomeResponseFromJson(json);

  Map<String, dynamic> toJson() => _$HomeResponseToJson(this);

}
