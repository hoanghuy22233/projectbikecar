// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'update_content_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UpdateContentResponse _$UpdateContentResponseFromJson(
    Map<String, dynamic> json) {
  return UpdateContentResponse()
    ..status = json['status'] as bool
    ..msg = json['msg'] as String;
}

Map<String, dynamic> _$UpdateContentResponseToJson(
        UpdateContentResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'msg': instance.msg,
    };
