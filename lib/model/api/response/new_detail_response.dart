import 'package:json_annotation/json_annotation.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/model/entity/brands.dart';
import 'package:project_bike_car/model/entity/category.dart';
import 'package:project_bike_car/model/entity/home_data.dart';
import 'package:project_bike_car/model/entity/new_detail_data.dart';
import 'package:project_bike_car/model/entity/products.dart';

import 'base_response.dart';

part 'new_detail_response.g.dart';


@JsonSerializable()
class NewDetailResponse extends BaseResponse {
  NewDetailData data;

  NewDetailResponse({this.data});

  factory NewDetailResponse.fromJson(Map<String, dynamic> json) =>
      _$NewDetailResponseFromJson(json);

  Map<String, dynamic> toJson() => _$NewDetailResponseToJson(this);

}
