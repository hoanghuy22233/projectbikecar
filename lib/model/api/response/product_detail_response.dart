import 'package:json_annotation/json_annotation.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/model/entity/category.dart';
import 'package:project_bike_car/model/entity/product_detail.dart';
import 'package:project_bike_car/model/entity/user.dart';

import 'base_response.dart';

part 'product_detail_response.g.dart';

@JsonSerializable()
class ProductDetailResponse extends BaseResponse {
  ProductDetail data;

  ProductDetailResponse(this.data);

  factory ProductDetailResponse.fromJson(Map<String, dynamic> json) =>
      _$ProductDetailResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ProductDetailResponseToJson(this);
}
