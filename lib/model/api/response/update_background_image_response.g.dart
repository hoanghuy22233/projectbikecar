// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'update_background_image_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UpdateBackgroundImageResponse _$UpdateBackgroundImageResponseFromJson(
    Map<String, dynamic> json) {
  return UpdateBackgroundImageResponse()
    ..status = json['status'] as bool
    ..msg = json['msg'] as String;
}

Map<String, dynamic> _$UpdateBackgroundImageResponseToJson(
        UpdateBackgroundImageResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'msg': instance.msg,
    };
