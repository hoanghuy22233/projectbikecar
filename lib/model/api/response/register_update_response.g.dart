// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'register_update_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RegisterUpdateResponse _$RegisterUpdateResponseFromJson(
    Map<String, dynamic> json) {
  return RegisterUpdateResponse()
    ..status = json['status'] as bool
    ..msg = json['msg'] as String;
}

Map<String, dynamic> _$RegisterUpdateResponseToJson(
        RegisterUpdateResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'msg': instance.msg,
    };
