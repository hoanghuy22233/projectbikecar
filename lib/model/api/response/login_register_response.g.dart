// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_register_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginRegisterResponse _$LoginRegisterResponseFromJson(
    Map<String, dynamic> json) {
  return LoginRegisterResponse(
    json['token'] as String,
    User.fromJson(json['data'] as Map<String, dynamic>),
  )
    ..status = json['status'] as bool
    ..msg = json['msg'] as String;
}

Map<String, dynamic> _$LoginRegisterResponseToJson(
        LoginRegisterResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'msg': instance.msg,
      'token': instance.apiToken,
      'data': instance.data,
    };
