// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'new_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NewResponse _$NewResponseFromJson(Map<String, dynamic> json) {
  return NewResponse(
    data: NewData.fromJson(json['data'] as Map<String, dynamic>),
  )
    ..status = json['status'] as bool
    ..msg = json['msg'] as String;
}

Map<String, dynamic> _$NewResponseToJson(NewResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'msg': instance.msg,
      'data': instance.data,
    };
