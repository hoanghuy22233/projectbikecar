// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'register_verify_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RegisterVerifyResponse _$RegisterVerifyResponseFromJson(
    Map<String, dynamic> json) {
  return RegisterVerifyResponse()
    ..status = json['status'] as bool
    ..msg = json['msg'] as String;
}

Map<String, dynamic> _$RegisterVerifyResponseToJson(
        RegisterVerifyResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'msg': instance.msg,
    };
