import 'package:json_annotation/json_annotation.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/model/entity/category.dart';
import 'package:project_bike_car/model/entity/contact.dart';

import 'base_response.dart';

part 'add_contact_response.g.dart';

@JsonSerializable()
class AddContactResponse extends BaseResponse {
  Contact data;

  AddContactResponse(this.data);

  factory AddContactResponse.fromJson(Map<String, dynamic> json) =>
      _$AddContactResponseFromJson(json);

  Map<String, dynamic> toJson() => _$AddContactResponseToJson(this);
}
