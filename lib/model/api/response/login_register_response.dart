import 'package:json_annotation/json_annotation.dart';
import 'package:project_bike_car/model/api/response/barrel_response.dart';
import 'package:project_bike_car/model/entity/barrel_entity.dart';

part 'login_register_response.g.dart';

@JsonSerializable()
class LoginRegisterResponse extends BaseResponse {
  @JsonKey(name: "token")
  String apiToken;
  @JsonKey(name: "data")
  User data;

  LoginRegisterResponse( this.apiToken,this.data
      );


  factory LoginRegisterResponse.fromJson(Map<String, dynamic> json) =>
      _$LoginRegisterResponseFromJson(json);

  Map<String, dynamic> toJson() => _$LoginRegisterResponseToJson(this);

  @override
  List<Object> get props => [];

  @override
  String toString() {
    return 'LoginRegisterResponse{'
        'data: $data,'
        'apiToken:$apiToken}';
  }
}
