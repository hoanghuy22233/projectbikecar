
import 'package:json_annotation/json_annotation.dart';

import 'barrel_response.dart';

part 'update_content_response.g.dart';

@JsonSerializable()
class UpdateContentResponse extends BaseResponse {

  UpdateContentResponse();

  factory UpdateContentResponse.fromJson(Map<String, dynamic> json) =>
      _$UpdateContentResponseFromJson(json);

  Map<String, dynamic> toJson() => _$UpdateContentResponseToJson(this);

}
