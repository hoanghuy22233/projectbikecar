import 'package:json_annotation/json_annotation.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/model/entity/category.dart';

import 'base_response.dart';

part 'category_response.g.dart';

@JsonSerializable()
class CategoryResponse extends BaseResponse {
  List<Category> data;

  CategoryResponse(this.data);

  factory CategoryResponse.fromJson(Map<String, dynamic> json) =>
      _$CategoryResponseFromJson(json);

  Map<String, dynamic> toJson() => _$CategoryResponseToJson(this);
}
