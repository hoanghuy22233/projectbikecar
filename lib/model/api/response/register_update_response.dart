
import 'package:json_annotation/json_annotation.dart';

import 'barrel_response.dart';

part 'register_update_response.g.dart';

@JsonSerializable()
class RegisterUpdateResponse extends BaseResponse {

  RegisterUpdateResponse();

  factory RegisterUpdateResponse.fromJson(Map<String, dynamic> json) =>
      _$RegisterUpdateResponseFromJson(json);

  Map<String, dynamic> toJson() => _$RegisterUpdateResponseToJson(this);

}
