// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'new_detail_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NewDetailResponse _$NewDetailResponseFromJson(Map<String, dynamic> json) {
  return NewDetailResponse(
    data: NewDetailData.fromJson(json['data'] as Map<String, dynamic>),
  )
    ..status = json['status'] as bool
    ..msg = json['msg'] as String;
}

Map<String, dynamic> _$NewDetailResponseToJson(NewDetailResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'msg': instance.msg,
      'data': instance.data,
    };
