import 'package:json_annotation/json_annotation.dart';
import 'package:project_bike_car/model/api/response/barrel_response.dart';
import 'package:project_bike_car/model/entity/barrel_entity.dart';

part 'profile_response.g.dart';

@JsonSerializable()
class ProfileResponse extends BaseResponse {
  User data;

  ProfileResponse(this.data);

  factory ProfileResponse.fromJson(Map<String, dynamic> json) =>
      _$ProfileResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ProfileResponseToJson(this);

  @override
  String toString() {
    return 'ProfileResponse{data: $data}';
  }
}
