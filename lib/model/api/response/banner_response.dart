import 'package:json_annotation/json_annotation.dart';
import 'package:project_bike_car/model/entity/banner.dart';

import 'base_response.dart';

part 'banner_response.g.dart';

@JsonSerializable()
class BannerResponse extends BaseResponse {
  List<Banners> data;

  BannerResponse(this.data);

  factory BannerResponse.fromJson(Map<String, dynamic> json) =>
      _$BannerResponseFromJson(json);

  Map<String, dynamic> toJson() => _$BannerResponseToJson(this);
}
