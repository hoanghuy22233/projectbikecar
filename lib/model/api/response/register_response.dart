import 'package:json_annotation/json_annotation.dart';
import 'package:project_bike_car/model/entity/user.dart';

import 'base_response.dart';

part 'register_response.g.dart';

@JsonSerializable()
class RegisterResponse extends BaseResponse {
  @JsonKey(name:"data")
  User data;
  RegisterResponse( this.data);
  // RegisterResponse();
  @override
  String toString() {
    return ' {data: $data}';
  }
  factory RegisterResponse.fromJson(Map<String, dynamic> json) =>
      _$RegisterResponseFromJson(json);

  Map<String, dynamic> toJson() => _$RegisterResponseToJson(this);

  @override
  List<Object> get props => [data];

}
