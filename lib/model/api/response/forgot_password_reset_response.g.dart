// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'forgot_password_reset_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ForgotPasswordResetResponse _$ForgotPasswordResetResponseFromJson(
    Map<String, dynamic> json) {
  return ForgotPasswordResetResponse()
    ..status = json['status'] as bool
    ..msg = json['msg'] as String;
}

Map<String, dynamic> _$ForgotPasswordResetResponseToJson(
        ForgotPasswordResetResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'msg': instance.msg,
    };
