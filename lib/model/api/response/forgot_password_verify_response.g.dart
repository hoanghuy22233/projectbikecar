// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'forgot_password_verify_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ForgotPasswordVerifyResponse _$ForgotPasswordVerifyResponseFromJson(
    Map<String, dynamic> json) {
  return ForgotPasswordVerifyResponse()
    ..status = json['status'] as bool
    ..msg = json['msg'] as String;
}

Map<String, dynamic> _$ForgotPasswordVerifyResponseToJson(
        ForgotPasswordVerifyResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'msg': instance.msg,
    };
