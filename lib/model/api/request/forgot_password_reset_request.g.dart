// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'forgot_password_reset_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ForgotPasswordResetRequest _$ForgotPasswordResetRequestFromJson(
    Map<String, dynamic> json) {
  return ForgotPasswordResetRequest(
    emailOrPhone: json['email'] as String,
    otpCode: json['otp'] as String,
    password: json['password'] as String,
  );
}

Map<String, dynamic> _$ForgotPasswordResetRequestToJson(
        ForgotPasswordResetRequest instance) =>
    <String, dynamic>{
      'email': instance.emailOrPhone,
      'otp': instance.otpCode,
      'password': instance.password,
    };
