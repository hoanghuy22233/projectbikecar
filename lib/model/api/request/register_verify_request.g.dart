// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'register_verify_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RegisterVerifyRequest _$RegisterVerifyRequestFromJson(
    Map<String, dynamic> json) {
  return RegisterVerifyRequest(
    emailOrPhone: json['email'] as String,
    otpCode: json['otp'] as String,
  );
}

Map<String, dynamic> _$RegisterVerifyRequestToJson(
        RegisterVerifyRequest instance) =>
    <String, dynamic>{
      'email': instance.emailOrPhone,
      'otp': instance.otpCode,
    };
