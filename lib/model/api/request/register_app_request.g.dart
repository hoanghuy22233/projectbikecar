// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'register_app_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RegisterAppRequest _$RegisterAppRequestFromJson(Map<String, dynamic> json) {
  return RegisterAppRequest(
    name: json['name'] as String,
    email: json['user_name'] as String,
    password: json['password'] as String,
    confirmPassword: json['password_confirmation'] as String,
  );
}

Map<String, dynamic> _$RegisterAppRequestToJson(RegisterAppRequest instance) =>
    <String, dynamic>{
      'name': instance.name,
      'user_name': instance.email,
      'password': instance.password,
      'password_confirmation': instance.confirmPassword,
    };
