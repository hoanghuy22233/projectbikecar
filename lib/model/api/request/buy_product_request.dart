import 'package:json_annotation/json_annotation.dart';

part 'buy_product_request.g.dart';

@JsonSerializable()
class BuyProductRequest {
  @JsonKey(name: "product_id")
  final int productId;

  @JsonKey(name: "token")
  final String token;

  @JsonKey(name: "email")
  final String email;

  @JsonKey(name: "address")
  final String address;
  @JsonKey(name: "phone")
  final String phone;
  @JsonKey(name: "content")
  final String content;


  BuyProductRequest(this.productId, this.token, this.email, this.address,
      this.phone, this.content);

  factory BuyProductRequest.fromJson(Map<String, dynamic> json) =>
      _$BuyProductRequestFromJson(json);

  Map<String, dynamic> toJson() => _$BuyProductRequestToJson(this);
}
