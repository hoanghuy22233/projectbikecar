// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'change_password_app_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChangePasswordAppRequest _$ChangePasswordAppRequestFromJson(
    Map<String, dynamic> json) {
  return ChangePasswordAppRequest(
    oldPassword: json['old_password'] as String,
    newPassword: json['new_password'] as String,
    confirmPassword: json['confirm_password'] as String,
    token: json['token'] as String,
  );
}

Map<String, dynamic> _$ChangePasswordAppRequestToJson(
        ChangePasswordAppRequest instance) =>
    <String, dynamic>{
      'old_password': instance.oldPassword,
      'new_password': instance.newPassword,
      'confirm_password': instance.confirmPassword,
      'token': instance.token,
    };
