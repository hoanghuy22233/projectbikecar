// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'resend_otp_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ResendOtpRequest _$ResendOtpRequestFromJson(Map<String, dynamic> json) {
  return ResendOtpRequest(
    emailOrPhone: json['email'] as String,
  );
}

Map<String, dynamic> _$ResendOtpRequestToJson(ResendOtpRequest instance) =>
    <String, dynamic>{
      'email': instance.emailOrPhone,
    };
