import 'package:json_annotation/json_annotation.dart';

part 'register_app_request.g.dart';

@JsonSerializable()
class RegisterAppRequest {
  @JsonKey(name: "name")
  final String name;
  @JsonKey(name: "user_name")
  final String email;
  @JsonKey(name: "password")
  final String password;
  @JsonKey(name: "password_confirmation")
  final String confirmPassword;



  RegisterAppRequest({this.name,this.email, this.password, this.confirmPassword,});

  factory RegisterAppRequest.fromJson(Map<String, dynamic> json) =>
      _$RegisterAppRequestFromJson(json);

  Map<String, dynamic> toJson() => _$RegisterAppRequestToJson(this);
}
