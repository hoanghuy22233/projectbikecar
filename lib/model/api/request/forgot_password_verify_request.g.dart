// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'forgot_password_verify_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ForgotPasswordVerifyRequest _$ForgotPasswordVerifyRequestFromJson(
    Map<String, dynamic> json) {
  return ForgotPasswordVerifyRequest(
    emailOrPhone: json['email'] as String,
    otpCode: json['otp'] as String,
  );
}

Map<String, dynamic> _$ForgotPasswordVerifyRequestToJson(
        ForgotPasswordVerifyRequest instance) =>
    <String, dynamic>{
      'email': instance.emailOrPhone,
      'otp': instance.otpCode,
    };
