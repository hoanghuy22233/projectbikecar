import 'package:json_annotation/json_annotation.dart';

part 'update_content_request.g.dart';
@JsonSerializable()
class UpdateContentRequest {
  final String description;

  UpdateContentRequest({this.description});

  factory UpdateContentRequest.fromJson(Map<String, dynamic> json) =>
      _$UpdateContentRequestFromJson(json);

  Map<String, dynamic> toJson() => _$UpdateContentRequestToJson(this);
}