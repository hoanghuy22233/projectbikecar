// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'add_contact_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AddContactRequest _$AddContactRequestFromJson(Map<String, dynamic> json) {
  return AddContactRequest(
    phone: json['phone'] as String,
    name: json['name'] as String,
    email: json['email'] as String,
    content: json['content'] as String,
    token: json['token'] as String,
  );
}

Map<String, dynamic> _$AddContactRequestToJson(AddContactRequest instance) =>
    <String, dynamic>{
      'name': instance.name,
      'email': instance.email,
      'content': instance.content,
      'phone': instance.phone,
      'token': instance.token,
    };
