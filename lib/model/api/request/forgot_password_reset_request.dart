import 'package:json_annotation/json_annotation.dart';

part 'forgot_password_reset_request.g.dart';
@JsonSerializable()
class ForgotPasswordResetRequest {
  @JsonKey(name: "email")
  final String emailOrPhone;
  @JsonKey(name: "otp")
  final String otpCode;
  @JsonKey(name: "password")
  final String password;



  ForgotPasswordResetRequest(
      {this.emailOrPhone, this.otpCode, this.password});

  factory ForgotPasswordResetRequest.fromJson(Map<String, dynamic> json) =>
      _$ForgotPasswordResetRequestFromJson(json);

  Map<String, dynamic> toJson() => _$ForgotPasswordResetRequestToJson(this);
}