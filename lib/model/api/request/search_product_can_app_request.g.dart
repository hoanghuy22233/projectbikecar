// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'search_product_can_app_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SearchProductCanAppRequest _$SearchProductCanAppRequestFromJson(
    Map<String, dynamic> json) {
  return SearchProductCanAppRequest(
    content: json['content'] as String,
    name: json['name'] as String,
    phone: json['phone'] as String,
  );
}

Map<String, dynamic> _$SearchProductCanAppRequestToJson(
        SearchProductCanAppRequest instance) =>
    <String, dynamic>{
      'content': instance.content,
      'name': instance.name,
      'phone': instance.phone,
    };
