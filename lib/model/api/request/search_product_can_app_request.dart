import 'package:json_annotation/json_annotation.dart';

part 'search_product_can_app_request.g.dart';

@JsonSerializable()
class SearchProductCanAppRequest {
  @JsonKey(name: "content")
  final String content;
  @JsonKey(name: "name")
  final String name;
  @JsonKey(name: "phone")
  final String phone;



  SearchProductCanAppRequest({this.content, this.name, this.phone,});

  factory SearchProductCanAppRequest.fromJson(Map<String, dynamic> json) =>
      _$SearchProductCanAppRequestFromJson(json);

  Map<String, dynamic> toJson() => _$SearchProductCanAppRequestToJson(this);
}
