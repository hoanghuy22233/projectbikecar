import 'package:json_annotation/json_annotation.dart';

part 'change_password_app_request.g.dart';

@JsonSerializable()
class ChangePasswordAppRequest {
  @JsonKey(name: "old_password")
  final String oldPassword;
  @JsonKey(name: "new_password")
  final String newPassword;
  @JsonKey(name: "confirm_password")
  final String confirmPassword;
  @JsonKey(name: "token")
  final String token;



  ChangePasswordAppRequest({this.oldPassword, this.newPassword, this.confirmPassword,this.token});

  factory ChangePasswordAppRequest.fromJson(Map<String, dynamic> json) =>
      _$ChangePasswordAppRequestFromJson(json);

  Map<String, dynamic> toJson() => _$ChangePasswordAppRequestToJson(this);
}
