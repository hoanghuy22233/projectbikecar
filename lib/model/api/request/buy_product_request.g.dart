// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'buy_product_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BuyProductRequest _$BuyProductRequestFromJson(Map<String, dynamic> json) {
  return BuyProductRequest(
    json['product_id'] as int,
    json['token'] as String,
    json['email'] as String,
    json['address'] as String,
    json['phone'] as String,
    json['content'] as String,
  );
}

Map<String, dynamic> _$BuyProductRequestToJson(BuyProductRequest instance) =>
    <String, dynamic>{
      'product_id': instance.productId,
      'token': instance.token,
      'email': instance.email,
      'address': instance.address,
      'phone': instance.phone,
      'content': instance.content,
    };
