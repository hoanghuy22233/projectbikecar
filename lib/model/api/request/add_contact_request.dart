import 'package:json_annotation/json_annotation.dart';

part 'add_contact_request.g.dart';

@JsonSerializable()
class AddContactRequest {
  @JsonKey(name: "name")
  final String name;
  @JsonKey(name: "email")
  final String email;
  @JsonKey(name: "content")
  final String content;
  @JsonKey(name: "phone")
  final String phone;
  @JsonKey(name: "token")
  final String token;



  AddContactRequest({this.phone, this.name, this.email, this.content,
      this.token});

  factory AddContactRequest.fromJson(Map<String, dynamic> json) =>
      _$AddContactRequestFromJson(json);

  Map<String, dynamic> toJson() => _$AddContactRequestToJson(this);
}
