// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'update_content_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UpdateContentRequest _$UpdateContentRequestFromJson(Map<String, dynamic> json) {
  return UpdateContentRequest(
    description: json['description'] as String,
  );
}

Map<String, dynamic> _$UpdateContentRequestToJson(
        UpdateContentRequest instance) =>
    <String, dynamic>{
      'description': instance.description,
    };
