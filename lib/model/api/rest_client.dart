
import 'dart:io';

import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:dio/dio.dart';
import 'package:project_bike_car/model/api/request/add_contact_request.dart';
import 'package:project_bike_car/model/api/request/change_password_app_request.dart';
import 'package:project_bike_car/model/api/request/forgot_password_reset_request.dart';
import 'package:project_bike_car/model/api/request/register_app_request.dart';
import 'package:project_bike_car/model/api/request/resend_otp_request.dart';
import 'package:project_bike_car/model/api/response/add_contact_response.dart';
import 'package:project_bike_car/model/api/request/update_address_request.dart';
import 'package:project_bike_car/model/api/request/update_content_request.dart';
import 'package:project_bike_car/model/api/response/banner_response.dart';
import 'package:project_bike_car/model/api/response/buy_product_response.dart';
import 'package:project_bike_car/model/api/response/category_response.dart';
import 'package:project_bike_car/model/api/response/change_password_response.dart';
import 'package:project_bike_car/model/api/response/forgot_password_reset_response.dart';
import 'package:project_bike_car/model/api/response/new_detail_response.dart';
import 'package:project_bike_car/model/api/response/new_response.dart';
import 'package:project_bike_car/model/api/response/notification_response.dart';
import 'package:project_bike_car/model/api/response/order_response.dart';
import 'package:project_bike_car/model/api/response/product_detail_response.dart';
import 'package:project_bike_car/model/api/response/profile_response.dart';
import 'package:project_bike_car/model/api/response/forgot_password_reset_response.dart';
import 'package:project_bike_car/model/api/response/new_response.dart';
import 'package:project_bike_car/model/api/response/register_response.dart';
import 'package:project_bike_car/model/api/response/home_response.dart';
import 'package:project_bike_car/model/api/response/register_update_response.dart';
import 'package:project_bike_car/model/api/response/resend_otp_response.dart';
import 'package:project_bike_car/model/api/response/update_address_response.dart';
import 'package:project_bike_car/model/api/response/update_avatar_response.dart';
import 'package:project_bike_car/model/api/response/update_background_image_response.dart';
import 'package:project_bike_car/model/api/response/update_content_response.dart';
import 'package:project_bike_car/model/entity/category_new.dart';
import 'package:retrofit/retrofit.dart';
import 'package:project_bike_car/model/api/response/register_verify_response.dart';
import 'package:project_bike_car/model/api/request/register_verify_request.dart';
import 'package:project_bike_car/model/api/request/forgot_password_request.dart';
import 'package:project_bike_car/model/api/response/forgot_password_response.dart';
import 'package:project_bike_car/model/api/request/forgot_password_verify_request.dart';
import 'package:project_bike_car/model/api/response/forgot_password_verify_response.dart';
import 'request/barrel_request.dart';
import 'request/buy_product_request.dart';
import 'request/search_product_can_app_request.dart';
import 'request/update_birthday_request.dart';
import 'request/update_email_request.dart';
import 'request/update_name_request.dart';
import 'request/update_phone_request.dart';
import 'response/barrel_response.dart';
import 'response/search_product_can_response.dart';
import 'response/update_birthday_response.dart';
import 'response/update_email_response.dart';
import 'response/update_name_response.dart';
import 'response/update_phone_response.dart';

part 'rest_client.g.dart';

@RestApi(baseUrl: Endpoint.BASE_URL)
abstract class RestClient {
  factory RestClient(Dio dio, {String baseUrl}) = _RestClient;

  @POST(Endpoint.LOGIN_APP)
  Future<LoginRegisterResponse> loginApp(
      @Body() LoginAppRequest loginAppRequest);
  @POST(Endpoint.REGISTER)
  Future<RegisterUpdateResponse> registerApp(
      @Body() RegisterAppRequest registerAppRequest);
  @POST(Endpoint.CHANGE_PASSWORD)
  Future<ChangePasswordResponse> changePasswordApp(
      @Body() ChangePasswordAppRequest changePasswordAppRequest);
  @POST(Endpoint.REGISTER_BIKE_CAR_VERIFY)
  Future<RegisterVerifyResponse> registerVerify(
      @Body() RegisterVerifyRequest registerVerifyRequest);
  @POST(Endpoint.RESEND_OTP_REGISTER)
  Future<ResendOtpResponse> resendOtpRegister(
      @Body() ResendOtpRequest resendOtpRequest);
  @POST(Endpoint.FORGOT_PASSWORD)
  Future<ForgotPasswordResponse> forgotPassword(
      @Body() ForgotPasswordRequest forgotPasswordRequest);
  @POST(Endpoint.FORGOT_PASSWORD_VERIFY)
  Future<ForgotPasswordVerifyResponse> forgotPasswordVerify(
      @Body() ForgotPasswordVerifyRequest forgotPasswordVerifyRequest);
  @POST(Endpoint.RESEND_OTP)
  Future<ResendOtpResponse> resendOtp(
      @Body() ResendOtpRequest resendOtpRequest);
  @POST(Endpoint.FORGOT_PASSWORD_RESET)
  Future<ForgotPasswordResetResponse> forgotPasswordReset(
      @Body() ForgotPasswordResetRequest forgotPasswordResetRequest);
  @GET(Endpoint.BANNER)
  Future<BannerResponse> getBanner();

  @GET(Endpoint.CATEGORY)
  Future<CategoryResponse> getCategory();

  @GET(Endpoint.HOME)
  Future<HomeResponse> getHome();
  @GET(Endpoint.NEW)
  Future<NewResponse> getNew(
      @Query("category_id") int categoryId,
      );


  @GET(Endpoint.HOME)
  Future<HomeResponse> getHomes(
      @Query("category_id") int categoryId,
      @Query("brand_id") int brandId,
      @Query("sort") String sort,
      );

  @GET(Endpoint.DETAIL_PRODUCT + "/{id}")
  Future<ProductDetailResponse> getProductDetail(@Path("id") int id);

  @POST(Endpoint.BUY_PRODUCT)
  Future<ProductBuyResponse> productBuy(
      @Body() BuyProductRequest buyProductRequest);

  @GET(Endpoint.PROFILE)
  Future<ProfileResponse> getProfile();

  @POST(Endpoint.UPDATE_PROFILE)
  Future<UpdateAvatarResponse> updateAvatar(
      @Part(name: 'avatar') File avatarFile);

  @POST(Endpoint.UPDATE_PROFILE)
  Future<UpdateBackgroundImageResponse> updateBackgroundImage(
      @Part(name: 'cover_image') File backgroundImageFile);


  @POST(Endpoint.UPDATE_PROFILE)
  Future<UpdateEmailResponse> updateEmail(
      @Body() UpdateEmailRequest updateEmailRequest);

  @POST(Endpoint.UPDATE_PROFILE)
  Future<UpdateBirthdayResponse> updateBirthday(
      @Body() UpdateBirthdayRequest updateBirthdayRequest);

  @POST(Endpoint.UPDATE_PROFILE)
  Future<UpdatePhoneResponse> updatePhone(
      @Body() UpdatePhoneRequest updatePhoneRequest);

  @POST(Endpoint.UPDATE_PROFILE)
  Future<UpdateNameResponse> updateName(
      @Body() UpdateNameRequest updateNameRequest);

  @POST(Endpoint.UPDATE_PROFILE)
  Future<UpdateAddressResponse> updateAddress(
      @Body() UpdateAddressRequest updateAddressRequest);

  @POST(Endpoint.UPDATE_PROFILE)
  Future<UpdateContentResponse> updateContent(
      @Body() UpdateContentRequest updateContentRequest);

  @GET(Endpoint.ORDER)
  Future<OrderResponse> getOrder();

  @GET(Endpoint.NOTIFICATION)
  Future<NotificationResponse> getNotification();
  @GET(Endpoint.DETAIL_NEW + "/{id}")
  Future<NewDetailResponse> getNewDetail(@Path("id") int id);
  @POST(Endpoint.ADD_CONTACT)
  Future<AddContactResponse> postAddContact(
      @Body() AddContactRequest addContactRequest);

  @POST(Endpoint.SEARCH_PRODUCT)
  Future<SearchProductCanResponse> searchProductApp(
      @Body() SearchProductCanAppRequest searchProductCanAppRequest);
}
