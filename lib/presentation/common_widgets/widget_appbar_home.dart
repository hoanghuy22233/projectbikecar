import 'package:project_bike_car/app/constants/value/value.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class WidgetAppbarHome extends StatefulWidget {
  final String title;
  final List<Widget> left;
  final List<Widget> right;
  final Color indicatorColor;
  final Color backgroundColor;
  final Color textColor;
  final bool hasIndicator;

  WidgetAppbarHome(
      {Key key,
      this.title,
      this.left,
      this.right,
      this.indicatorColor,
      this.backgroundColor,
      this.hasIndicator = false,
      this.textColor,})
      : super(key: key);

  @override
  _WidgetAppbarHomeState createState() => _WidgetAppbarHomeState();
}

class _WidgetAppbarHomeState extends State<WidgetAppbarHome> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: double.infinity,
          // height: AppValue.ACTION_BAR_HEIGHT * 2,
          height: AppValue.ACTION_BAR_HEIGHT*2 ,
          decoration: BoxDecoration(
              color: Color(0xff33CC66),
          ),
          //padding: EdgeInsets.only(top: 15),
          child: Stack(
            // overflow: Overflow.visible,
            children: [
              widget.left != null
                  ? Positioned.fill(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        //crossAxisAlignment: CrossAxisAlignment.center,
                        children: widget.left,
                      ),
                    )
                  : SizedBox(),
              widget.right != null
                  ? Positioned.fill(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        //crossAxisAlignment: CrossAxisAlignment.center,
                        children: widget.right,
                      ),
                    )
                  : SizedBox(),
              widget.title != null
                  ? Positioned.fill(
                child: Center(
                  child: Image.asset(widget.title,width: 100,height:100),
                ),

                    )
                  : SizedBox(),
            ],
          ),
        ),
      ],
    );
  }

}


