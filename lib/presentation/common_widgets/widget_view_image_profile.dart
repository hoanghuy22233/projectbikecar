import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:photo_view/photo_view.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/model/entity/product_detail.dart';

import 'barrel_common_widgets.dart';
import 'widget_appbar_not_search.dart';

class WidgetViewNetworkImageProfile extends StatelessWidget{
  final String image;

  const WidgetViewNetworkImageProfile({Key key, this.image}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: Scaffold(
        backgroundColor: Colors.black,
        body: Column(
          children: [
            SizedBox(height: 20,),
            GestureDetector(
              onTap: (){
                AppNavigator.navigateBack();
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [

                  Image.asset('assets/images/img_close_round.png',height: 25,width: 25, color: Colors.white,),
                  SizedBox(width: 10,),
                  Text("Đóng", style: TextStyle(color: Colors.white,fontSize: 14),),
                  SizedBox(width: 20,),

                ],
              ),
            ),
            Expanded(child:  Container(
              height: MediaQuery.of(context).size.height-100,
              width: MediaQuery.of(context).size.width,
              child: PhotoView(
                imageProvider: NetworkImage(image),
              ),
            )),
          ],
        ),
      ),
    );


      ;
  }

  Widget _buildAppbar(String title) => WidgetAppbarNotSearch(
    backgroundColor: Colors.black,
    textColor: Colors.white,
    left: [
      WidgetAppbarMenuBack(),
    ],
    title: title,
  );
}