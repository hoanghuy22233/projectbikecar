import 'dart:async';
import 'dart:typed_data';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_save/image_save.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:photo_view/photo_view.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/model/entity/product_detail.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart' as p;

class WidgetViewNetworkImage extends StatefulWidget {
  final String image;
  final ProductDetail detail;

  const WidgetViewNetworkImage({Key key, this.image, this.detail})
      : super(key: key);

  @override
  _WidgetViewNetworkImageState createState() => _WidgetViewNetworkImageState();
}

class _WidgetViewNetworkImageState extends State<WidgetViewNetworkImage> {

  String _result = "";
  Uint8List _data;



  @override
  void initState() {
    super.initState();

    _requestPermission();
   // Permission.storage.request();
   // dio = Dio();

  }

  void _onImageSaveButtonPressed(String url) async {
    print("_onImageSaveButtonPressed");
    var response = await http.get(url);

    debugPrint(response.statusCode.toString());

    // var filePath =
    //     await ImagePickerSaver.saveFile(fileData: response.bodyBytes);
    // var savedFile = File.fromUri(Uri.file(filePath));
    // Navigator.push(
    //   context,
    //   MaterialPageRoute(
    //       builder: (context) => MyTestApp()),
    // );


  }

  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: Scaffold(
        backgroundColor: Colors.black,
        body: Stack(
          alignment: Alignment.topLeft,
          children: [
            Column(
              children: [
                SizedBox(
                  height: 20,
                ),
                Expanded(
                    child: Stack(
                  children: [
                    widget.detail.images != null
                        ? Container(
                            width: MediaQuery.of(context).size.width,
                            height: MediaQuery.of(context).size.height,
                            child: Swiper(
                              itemBuilder: (BuildContext context, int index) {
                                return Stack(
                                  alignment: Alignment.topLeft,
                                  children: [
                                    Container(
                                      child: Stack(
                                        alignment: Alignment.center,
                                        children: [
                                          PhotoView(
                                            imageProvider: NetworkImage(
                                                widget.detail.images[index]),
                                          ),
                                          Image.asset(
                                            "assets/images/logo-removebg-preview.png",
                                            height: 100,
                                            width: 100,
                                          ),
                                        ],
                                      ),
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Container(
                                          padding: EdgeInsets.only(left: 20),
                                          child: GestureDetector(
                                            onTap: () {
                                              // _onImageSaveButtonPressed(
                                              //     widget.detail.images[index]);
                                           //   _downloadImage(widget.detail.images[index]);
                                              _saveImage(widget.detail.images[index]);

                                              _toastInfo("Lưu ảnh thành công!");

                                            },
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: [
                                                Icon(
                                                  Icons.download_sharp,
                                                  size: 30,
                                                  color: Colors.white,
                                                ),
                                                SizedBox(
                                                  width: 10,
                                                ),
                                                Text(
                                                  "Tải về",
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 14),
                                                ),
                                                SizedBox(
                                                  width: 20,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        GestureDetector(
                                          onTap: () {
                                            AppNavigator.navigateBack();
                                          },
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            children: [
                                              Image.asset(
                                                'assets/images/img_close_round.png',
                                                height: 25,
                                                width: 25,
                                                color: Colors.white,
                                              ),
                                              SizedBox(
                                                width: 10,
                                              ),
                                              Text(
                                                "Đóng",
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 14),
                                              ),
                                              SizedBox(
                                                width: 20,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                );
                              },
                              pagination: new SwiperPagination(
                                  builder: DotSwiperPaginationBuilder(
                                      size: 5,
                                      activeSize: 10,
                                      color: AppColor.BANNER_COLOR,
                                      activeColor:
                                          AppColor.BANNER_SELECTED_COLOR)),
                              itemCount: widget.detail.images.length,
                            ),
                          )
                        : Container(
                            height: MediaQuery.of(context).size.height - 100,
                            width: MediaQuery.of(context).size.width,
                            child: Stack(
                              alignment: Alignment.center,
                              children: [
                                PhotoView(
                                  imageProvider: NetworkImage(widget.image),
                                ),
                                Image.asset(
                                  "assets/images/logo-removebg-preview.png",
                                  height: 100,
                                  width: 100,
                                )
                              ],
                            ),
                          ),
                  ],
                )),
              ],
            ),
          ],
        ),
      ),
    );
  }

  ///permission save
  _requestPermission() async {
    Map<Permission, PermissionStatus> statuses = await [
      Permission.storage,
    ].request();

    final info = statuses[Permission.storage].toString();
    print(info);
  }

  ///toast
  _toastInfo(String info) {
    Fluttertoast.showToast(
        msg: info = "Lưu ảnh thành công!", toastLength: Toast.LENGTH_LONG);
  }


  Future<void> _saveImage(String url) async {
    Response<List<int>> res = await Dio().get<List<int>>(
        url,
        options: Options(responseType: ResponseType.bytes));
    _data = Uint8List.fromList(res.data);
    bool success = false;
    try {
      success = await ImageSave.saveImage(_data, "demo.jpg" );
    } on PlatformException catch (e, s) {
      print(e);
      print(s);
    }
    setState(() {
      _result = success ? "Save to album success" : "Save to album failed";
    });
  }




}
