
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:flutter/material.dart';

class WidgetDialog extends StatelessWidget {
  final String title;
  final String content;
  final String bank;
  final String image;
  final String titleAction1;
  final Function action1;
  final Color colorButton1;
  final String titleAction2;
  final Function action2;
  final Color colorButton2;

  const WidgetDialog({Key key, this.title, this.content,this.bank, this.image = 'assets/icons/warning.png', this.action1, this.action2, this.titleAction1, this.titleAction2, this.colorButton1 = Colors.green, this.colorButton2 = Colors.green}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      scrollable: true,
      title: Text(title, style: AppStyle.defaultMEDIUMBOLD.copyWith(color: Colors.red, fontSize: 16),),
      content: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Center(
              child: Container(
                height: 60,
                width: 60,
                child: GestureDetector(
                  onTap: (){

                  },
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(100),
                    child: Image.asset(image),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Text(content, style: AppStyle.defaultSMALLBOLD.copyWith(color: Colors.black,fontSize: 14), textAlign: TextAlign.start,),
            Text(bank, style: AppStyle.defaultSMALLBOLD.copyWith(color: Colors.green,fontSize: 14), textAlign: TextAlign.start,)
          ],
        ),
      ),
      actions: [
        RaisedButton(
            child: Text(titleAction1,style: TextStyle(color: Colors.white),),
            color: colorButton1,
            onPressed: (){
              action1();
            }
        ),
        titleAction2 != null ? RaisedButton(
            child: Text(titleAction2, style: TextStyle(color: Colors.white),),
            color: colorButton2,
            onPressed: (){
              action2();
            }
        ) : Container(),
      ],
    );
  }
}
