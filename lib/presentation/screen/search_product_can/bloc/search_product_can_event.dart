import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class SearchProductCanEvent extends Equatable {
  const SearchProductCanEvent();

  @override
  List<Object> get props => [];
}
class ContentChanged extends SearchProductCanEvent {
  final String content;

  const ContentChanged({@required this.content});

  @override
  List<Object> get props => [content];

  @override
  String toString() {
    return 'ContentChanged{content: $content}';
  }
}
class UsernamesChanged extends SearchProductCanEvent {
  final String username;

  const UsernamesChanged({@required this.username});

  @override
  List<Object> get props => [username];

  @override
  String toString() {
    return 'UsernameChanged{email: $username}';
  }
}

class PhonesChanged extends SearchProductCanEvent {
  final String phone;

  PhonesChanged({@required this.phone,});

  @override
  List<Object> get props => [];

  @override
  String toString() {
    return 'PhoneChanged{phone: $phone,}';
  }
}




class SearchProductCanSubmitted extends SearchProductCanEvent {
  final String content;
  final String username;
  final String phone;

  const SearchProductCanSubmitted({
    @required this.content,
    @required this.username,
    @required this.phone,

  });

  @override
  List<Object> get props => [content,username, phone,];

  @override
  String toString() {
    return 'Submitted{content: $content,email: $username, phone: $phone,  }';
  }
}