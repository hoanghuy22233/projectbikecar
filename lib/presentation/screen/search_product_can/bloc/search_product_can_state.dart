import 'package:flutter/cupertino.dart';

class SearchProductCanState {
  final bool isContentValid;
  final bool isUsernameValid;
  final bool isPhoneValid;
  final bool isSubmitting;
  final bool isSuccess;
  final bool isFailure;
  final String message;

  bool get isFormValid =>
      isUsernameValid && isPhoneValid ;

  SearchProductCanState(
      {@required this.isContentValid,
        @required this.isUsernameValid,
        @required this.isPhoneValid,
        @required this.isSubmitting,
        @required this.isSuccess,
        @required this.isFailure,
        @required this.message});

  factory SearchProductCanState.empty() {
    return SearchProductCanState(
        isContentValid: true,
        isUsernameValid: true,
        isPhoneValid: true,
        isSubmitting: false,
        isSuccess: false,
        isFailure: false,
        message: '');
  }

  factory SearchProductCanState.loading() {
    return SearchProductCanState(
        isContentValid: true,
        isUsernameValid: true,
        isPhoneValid: true,
        isSubmitting: true,
        isSuccess: false,
        isFailure: false,
        message: '');
  }

  factory SearchProductCanState.failure({String message}) {
    return SearchProductCanState(
        isContentValid: true,
        isUsernameValid: true,
        isPhoneValid: true,
        isSuccess: false,
        isSubmitting: false,
        isFailure: true,
        message: message);
  }

  factory SearchProductCanState.success({String message}) {
    return SearchProductCanState(
        isContentValid: true,
        isUsernameValid: true,
        isPhoneValid: true,
        isSuccess: true,
        isSubmitting: false,
        isFailure: false,
        message: message);
  }

  SearchProductCanState update(
      {bool isContentValid,
        bool isUsernameValid,
        bool isPhoneValid,
        String message}) {
    return copyWith(
      isContentValid:isContentValid,
      isUsernameValid: isUsernameValid,
      isPhoneValid: isPhoneValid,
      isSubmitting: false,
      isSuccess: false,
      isFailure: false,
      message: message,
    );
  }

  SearchProductCanState copyWith({
    bool isContentValid,
    bool isUsernameValid,
    bool isPhoneValid,
    bool isSubmitting,
    bool isSuccess,
    bool isFailure,
    String message,
  }) {
    return SearchProductCanState(
      isContentValid:isContentValid?? this.isContentValid,
      isUsernameValid: isUsernameValid ?? this.isUsernameValid,
      isPhoneValid: isPhoneValid ?? this.isPhoneValid,
      isSubmitting: isSubmitting ?? this.isSubmitting,
      isSuccess: isSuccess ?? this.isSuccess,
      isFailure: isFailure ?? this.isFailure,
      message: message ?? this.message,
    );
  }
}
