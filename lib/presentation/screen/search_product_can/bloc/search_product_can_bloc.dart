import 'package:project_bike_car/app/constants/endpoint/app_endpoint.dart';
import 'package:project_bike_car/model/repo/barrel_repo.dart';
import 'package:project_bike_car/presentation/screen/register/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/search_product_can/bloc/search_product_can_state.dart';
import 'package:project_bike_car/utils/utils.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

import 'bloc.dart';

class SearchProductCanBloc extends Bloc<SearchProductCanEvent, SearchProductCanState> {
  final UserRepository _userRepository;

  SearchProductCanBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  SearchProductCanState get initialState => SearchProductCanState.empty();

  @override
  Stream<SearchProductCanState> mapEventToState(SearchProductCanEvent event) async* {
    if (event is UsernamesChanged) {
      yield* _mapUsernameChangedToState(event.username);
    } else if (event is ContentChanged) {
      yield* _mapNameChangedToState(event.content);
    } else if (event is PhonesChanged) {
      yield* _mapPhoneChangedToState(event.phone);
    }
    else if (event is SearchProductCanSubmitted) {
      yield* _mapFormSubmittedToState(
    event.content,event.username,event.phone
      );
    }
  }

  Stream<SearchProductCanState> _mapUsernameChangedToState(String username) async* {
    yield state.update(
      isUsernameValid: Validator.isValidName(username),
    );
  }

  Stream<SearchProductCanState> _mapNameChangedToState(String content) async* {
    yield state.update(
      isContentValid: Validator.isValidName(content),
    );
  }
  Stream<SearchProductCanState> _mapPhoneChangedToState(String phone) async* {
    yield state.update(
      isContentValid: Validator.isValidName(phone),
    );
  }

  Stream<SearchProductCanState> _mapFormSubmittedToState(
      String content,
      String username,
      String phone,
      ) async* {
    yield SearchProductCanState.loading();
    try {
      var response = await _userRepository.searchProductApp(
        content: content,
        name: username,
        phone:phone
      );

      if (response.status == Endpoint.SUCCESS) {
        yield SearchProductCanState.success(message: response.msg);
      } else {
        yield SearchProductCanState.failure(message: response.msg);
      }
    } catch (e) {
      print("------ SearchProductCan: $e");
      yield SearchProductCanState.failure();
    }

  }
}
