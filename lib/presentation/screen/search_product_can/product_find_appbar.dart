import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:project_bike_car/utils/locale/app_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WidgetProductFindAppbar extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Container(
        child: WidgetAppbar(

          title: AppLocalizations.of(context).translate('search_appbar_product_find.title'),
          left: [
            Container(
              padding: EdgeInsets.only(left: 20),
              child: GestureDetector(
                onTap: () {
                  AppNavigator.navigateBack();
                },
                child: Image.asset(
                  "assets/images/ic_arrow.png",
                  width: 20,
                  height: 20,
                  color: Colors.white,
                ),
              ),
            )
          ],

        )
    );

  }}