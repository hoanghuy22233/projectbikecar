import 'package:project_bike_car/app/constants/color/color.dart';
import 'package:project_bike_car/app/constants/navigator/navigator.dart';
import 'package:project_bike_car/app/constants/style/style.dart';
import 'package:project_bike_car/app/constants/string/validator.dart';
import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_spacer.dart';
import 'package:project_bike_car/utils/dialog/barrel_dialog.dart';
import 'package:project_bike_car/utils/snackbar/get_snack_bar_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_icons/flutter_icons.dart';

import 'bloc/bloc.dart';


class WidgetSearchCanForm extends StatefulWidget {
  @override
  _WidgetSearchCanFormState createState() => _WidgetSearchCanFormState();
}

class _WidgetSearchCanFormState extends State<WidgetSearchCanForm> {
  SearchProductCanBloc _searchProductCanBloc;
  final TextEditingController _contentController = TextEditingController();
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();

  bool obscurePassword = true;
  bool obscureConfirmPassword = true;
  bool autoValidate = false;

  bool get isPopulated =>
      _contentController.text.isNotEmpty &&
          _usernameController.text.isNotEmpty &&
          _phoneController.text.isNotEmpty ;

  @override
  void initState() {
    super.initState();
    _searchProductCanBloc = BlocProvider.of<SearchProductCanBloc>(context);
    _contentController.addListener(_onNameChange);
    _usernameController.addListener(_onUsernameChange);
    _phoneController.addListener(_onPhoneChanged);

  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SearchProductCanBloc, SearchProductCanState>(
      listener: (context, state) async {
        if (state.isSubmitting) {
          GetSnackBarUtils.createProgress();
        }

        if (state.isSuccess) {
          AppNavigator.navigateWorkService();
          await GetSnackBarUtils.createSuccess(message: state.message);
        }

        if (state.isFailure) {
          GetDialogUtils.createNotify(
              message: "Vui lòng nhậptin đầy  thông !",
              positiveLabel: "Về trang chủ",
              negativeLabel:"Đóng",
              onPositiveTap: () {
                AppNavigator.navigateWorkService();
              });

          GetSnackBarUtils.createError(message: state.message);
          setState(() {
            autoValidate = true;
          });
        }
      },
      child: BlocBuilder<SearchProductCanBloc, SearchProductCanState>(
        builder: (context, state) {
          return Padding(
            padding: EdgeInsets.all(20),
            child: Form(
              child: Column(
                children: [
                  _buildTextFieldContent(),
                  WidgetSpacer(
                    height: 10,
                  ),
                  _buildTextFieldUsername(),
                  WidgetSpacer(
                    height: 10,
                  ),
                  _buildTextFieldPhone(),
                  WidgetSpacer(
                    height: 20,
                  ),
                  Container(
                    child: _buildButtonSearchProduct(state),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  bool isSearchButtonEnabled() {
    return _searchProductCanBloc.state.isFormValid &&
        isPopulated &&
        !_searchProductCanBloc.state.isSubmitting;
  }

  _buildButtonSearchProduct(SearchProductCanState state) {
    return WidgetLoginButton(
      height: 45,
      onTap: () {
        if (isSearchButtonEnabled()) {
          _searchProductCanBloc.add(SearchProductCanSubmitted(
            content: _contentController.text.trim(),
            username: _usernameController.text.trim(),
            phone: _phoneController.text.trim()
          ));
          FocusScope.of(context).unfocus();
        }
      },
      isEnable: isSearchButtonEnabled(),
      text: "Xác nhận",
    );
  }


  _buildTextFieldContent() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      width: MediaQuery.of(context).size.width / 1.15,
      child: TextFormField(
        enableInteractiveSelection: false,
        controller: _contentController,
        keyboardType: TextInputType.name ?? TextInputType.text,
        onChanged: (value) {
        },
        style: TextStyle(
            color: Colors.white,
            fontSize: 16,
            fontWeight: FontWeight.bold,
            fontFamily: 'Roboto'),
        validator: AppValidation.validateUserName("Vui lòng điền từ khoá"),
        decoration: InputDecoration(
          hintText: "Nhập từ khoá cần tìm ",
          hintStyle: TextStyle(color: Colors.white),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
        ),
        // textAlign: TextAlign.start,
      ),
    );
  }
  _buildTextFieldUsername() {
    return  Container(
      padding: EdgeInsets.only(left: 15,right: 15,top: 10),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height / 13,
      child: Theme(
        data: ThemeData(
            primaryColor: Color(0xff33CC66),
            primaryColorDark: Color(0xff33CC66)),
        child: TextFormField(
          enableInteractiveSelection: false,
          controller: _usernameController,
          keyboardType: TextInputType.emailAddress ?? TextInputType.text,
          onChanged: (value) {
            // _loginBloc.add(LoginUsernameChanged(email: value));
          },
          style: TextStyle(
              color: Colors.white,
              fontSize: 16,
              fontWeight: FontWeight.bold,
              fontFamily: 'Roboto'),
          validator: AppValidation.validateUserName("Vui lòng điền họ tên"),
          decoration: InputDecoration(
            hintText: "Họ và tên ",
            hintStyle: TextStyle(color: Colors.white),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
            ),
          ),
          // textAlign: TextAlign.start,
        ),
      ),
    );


  }
  _buildTextFieldPhone() {
    return  Container(
      padding: EdgeInsets.only(left: 15,right: 15,top: 10),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height / 13,
      child: Theme(
        data: ThemeData(
            primaryColor: Color(0xff33CC66),
            primaryColorDark: Color(0xff33CC66)),
        child: TextFormField(
          enableInteractiveSelection: false,
          controller: _phoneController,
          keyboardType: TextInputType.emailAddress ?? TextInputType.text,
          onChanged: (value) {
            // _loginBloc.add(LoginUsernameChanged(email: value));
          },
          style: TextStyle(
              color: Colors.white,
              fontSize: 16,
              fontWeight: FontWeight.bold,
              fontFamily: 'Roboto'),
          validator: AppValidation.validateUserName("Vui lòng điền số điện thoại"),
          decoration: InputDecoration(
            hintText: "Số điện thoại ",
            hintStyle: TextStyle(color: Colors.white),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
            ),
          ),
          // textAlign: TextAlign.start,
        ),
      ),
    );


  }

  void _onNameChange() {
    _searchProductCanBloc.add(ContentChanged(
      content: _contentController.text,
    ));
  }
  void _onUsernameChange() {
    _searchProductCanBloc.add(UsernamesChanged(
      username: _usernameController.text,
    ));
  }

  void _onPhoneChanged() {
    _searchProductCanBloc.add(PhonesChanged(
        phone: _phoneController.text,
        ));
  }


  @override
  void dispose() {
    super.dispose();
    _contentController.dispose();
    _usernameController.dispose();
    _phoneController.dispose();
  }

}
