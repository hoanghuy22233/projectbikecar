
import 'package:project_bike_car/model/repo/user_repository.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_touch_hide_keyboard.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:project_bike_car/presentation/screen/search_product_can/product_find_appbar.dart';
import 'package:project_bike_car/presentation/screen/search_product_can/widget_search_can_form.dart';

import 'bloc/bloc.dart';

class SearchProductScreen extends StatefulWidget {
  @override
  _SearchProductScreenState createState() => _SearchProductScreenState();
}

class _SearchProductScreenState extends State<SearchProductScreen> {
  @override
  Widget build(BuildContext context) {
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return WidgetTouchHideKeyBoard(
      child: Scaffold(
        backgroundColor: Color(0xff33CC66),
        body: BlocProvider(
          create: (context) => SearchProductCanBloc(userRepository: userRepository),
          child: SafeArea(
            top: false,
            child: Container(
              padding: EdgeInsets.only(top: 50),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    WidgetProductFindAppbar(),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 20),
                      child: Center(
                        child: Text("Vui lòng điền đầy đủ thông tin!",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18,color: Colors.white),),
                      ),
                    ),
                    _buildWidgetSearchCanForm()

                  ],
                )
            ),
          ),
        ),
      ),
    );
  }

  _buildWidgetSearchCanForm() => WidgetSearchCanForm();
}
