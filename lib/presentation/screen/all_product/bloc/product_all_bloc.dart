import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/model/entity/products.dart';
import 'package:project_bike_car/presentation/screen/menu/home/bloc/bloc.dart';

import 'bloc.dart';

class ProductAllBloc extends Bloc<ProductAllEvent, ProductAllState>{
  final HomeBloc homeBloc;
  StreamSubscription subscription;

  ProductAllBloc({@required this.homeBloc}) {
    subscription = homeBloc.listen((state) {
      if (state is HomeLoaded) {
        add(DisplayProductAll(product: state.homeResponse.data.products));
      }
    });
  }

  @override
  ProductAllState get initialState => ProductAllNotLoaded();

  @override
  Stream<ProductAllState> mapEventToState(ProductAllEvent event) async* {
    if(event is DisplayProductAll){
       List<Products> product = List();
      for(int i=0;i<event.product.length;i++){
        product .add(event.product[i]);
      }
      yield* _mapDisplayHomeBannerToState(product);
    }
  }

  Stream<ProductAllState> _mapDisplayHomeBannerToState(List<Products> product) async* {
    yield ProductAllLoaded(product: product);
  }

  @override
  Future<void> close() {
    subscription.cancel();
    return super.close();
  }
}