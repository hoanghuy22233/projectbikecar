import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/model/entity/products.dart';

abstract class ProductAllState extends Equatable {
  const ProductAllState();

  @override
  List<Object> get props => [];
}

class ProductAllNotLoaded extends ProductAllState {}

class ProductAllLoaded extends ProductAllState {
  final List<Products> product;

  ProductAllLoaded({@required this.product});

  @override
  List<Object> get props => [product];
}
