
import 'package:equatable/equatable.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/model/entity/products.dart';

class ProductAllEvent extends Equatable {
  const ProductAllEvent();
  @override
  List<Object> get props => [];
}

class DisplayProductAll extends ProductAllEvent {
  final List<Products> product;

  const DisplayProductAll({this.product});

  @override
  List<Object> get props => [product];
}