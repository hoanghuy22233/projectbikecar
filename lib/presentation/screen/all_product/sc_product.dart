import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/app/constants/value/value.dart';
import 'package:project_bike_car/model/repo/barrel_repo.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_appbar_profile.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_screen_error.dart';
import 'package:project_bike_car/presentation/screen/menu/home/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_new/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/product/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/widget_home_product.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/widget_home_product_all_all.dart';

class ProductPageScreen extends StatefulWidget {
 final int categoryId;
 final int brandId;


 ProductPageScreen({this.categoryId, this.brandId});

  @override
  _ProductPageScreenState createState() => _ProductPageScreenState(categoryId,brandId);
}

class _ProductPageScreenState extends State<ProductPageScreen>
    with AutomaticKeepAliveClientMixin<ProductPageScreen> {
  @override
  int categoryId;
  int brandId;

  _ProductPageScreenState(this.categoryId, this.brandId);

  void initState() {
    super.initState();
   // BlocProvider.of<ProductBloc>(context).add(LoadProduct(categoryId??0,brandId??0,'id_desc'));
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Colors.transparent));
  }
  GlobalKey<ScaffoldState> _drawerKey = GlobalKey();


  @override
  Widget build(BuildContext context) {
    super.build(context);
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return MultiBlocProvider(
      providers: [

        BlocProvider(
          create: (context) =>
              HomeProductBloc(productBloc: BlocProvider.of<ProductBloc>(context)),
        ),
        BlocProvider(
          create: (context) =>
              HomeNewBloc(homeBloc: BlocProvider.of<HomeBloc>(context)),
        ),

      ],
      child: BlocBuilder<HomeBloc, HomeState>(builder: (context, state) {
        return Scaffold(
          key: _drawerKey,
          body: SafeArea(
              top: true,
              child: Container(
                  color: AppColor.WHITE, child: _buildContent(state))),
        );
      }),
    );
  }

  _buildContent(HomeState state) {
    if (state is HomeLoaded) {
      return _buildHomeLoaded(state);
    } else if (state is HomeLoading) {
      return Center(
        child: Container(
          height: 100,
          width: 100,
          child: Lottie.asset(
            'assets/lottie/load_car.json',
          ),
        ),
      );
    } else if (state is HomeNotLoaded) {
      return RefreshIndicator(
        onRefresh: () async {
          BlocProvider.of<HomeBloc>(context).add(RefreshHome(categoryId??0,brandId??0,'id_desc'));
          await Future.delayed(Duration(seconds: 1));
          return true;
        },
        color: AppColor.PRIMARY_COLOR,
        backgroundColor: AppColor.THIRD_COLOR,
        child: WidgetScreenError(
          status: state.status,
        ),
      );
    } else {
      return Center(
        child: Text('Unknown state'),
      );
    }
  }

  _buildHomeLoaded(HomeLoaded state) {
    return Column(
      children: [
        _buildAppbar(),
        Expanded(
          child:   BlocBuilder<ProductBloc, ProductState>(builder: (context, state) {
            return Container(
              child: _buildProductAllContent(state),
            );
          }),
        )
      ],
    );
  }

  _buildProductAllContent(ProductState state) {
    if (state is ProductLoaded) {
      return _buildProductAllLoaded(state);
    } else if (state is ProductLoading) {
      return Center(
        child: Container(
          height: 100,
          width: 100,
          child:Lottie.asset(
            'assets/lottie/load_car.json',
          ),
        ),
      );
    } else if (state is ProductNotLoaded) {
      return RefreshIndicator(
        onRefresh: () async {
          BlocProvider.of<ProductBloc>(context).add(RefreshProduct(categoryId??0,brandId??0,'id_desc'));
          await Future.delayed(Duration(seconds: 1));
          return true;
        },
        color: AppColor.PRIMARY_COLOR,
        backgroundColor: AppColor.THIRD_COLOR,
        child: WidgetScreenError(
          status: state.status,
        ),
      );
    } else {
      return Center(
        child: Text('Unknown state'),
      );
    }
  }
  _buildProductAllLoaded(ProductLoaded state) {
    return  Padding(
      padding: const EdgeInsets.only(left: 20, top: 20),
      child: WidgetHomeProductAllAll(categoryId: categoryId,bandId: brandId,),
    );
  }

  _buildAppbar() => WidgetAppbarProfile(
    backgroundColor: Color(0xff0bccd2),
    textColor: Colors.white,
    title: "Tất cả sản phẩm",
    left: [
      Padding(
        padding: const EdgeInsets.only(left: 10),
        child: GestureDetector(
          onTap: () {
            AppNavigator.navigateBack();
          },
          child: Image.asset(
            "assets/images/ic_arrow.png",
            width: 20,
            height: 20,
            color: Colors.white,
          ),
        ),
      )
    ],
  );



  @override
  bool get wantKeepAlive => true;
}
