import 'package:dio/dio.dart';
import 'package:flutter/services.dart';
import 'package:project_bike_car/app/auth_bloc/bloc.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/app/constants/navigator/navigator.dart';
import 'package:project_bike_car/model/local/barrel_local.dart';
import 'package:project_bike_car/model/repo/user_repository.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_logo.dart';
import 'package:project_bike_car/utils/handler/with_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:shared_preferences/shared_preferences.dart';

// ignore: must_be_immutable
class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override

  Widget build(BuildContext context) {
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    openLogin(userRepository);
    return Scaffold(
      body: Container(
        color: Colors.white,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            _buildLogo(),
            Container(
              height: 100,
              width: 100,
              child: Lottie.asset(
                'assets/lottie/load_car.json',
              ),
            ),
          ],
        ),
      ),
    );
  }

  _buildLogo() => WidgetLogo(
        height: Get.width * 0.5,
        widthPercent: 0.5,
      );


  void openLogin(UserRepository repository) async {

    try {
      final profileResponse = await repository.getProfile();
      if (profileResponse.status == Endpoint.SUCCESS) {
        final prefs = LocalPref();
        final token = await prefs.getString(AppPreferences.auth_token);
        BlocProvider.of<AuthenticationBloc>(Get.context).add(LoggedIn(token));
      }
    } on DioError catch (_) {} finally {
      Future.delayed(Duration(seconds: 4), () {
        AppNavigator.navigateWorkService();
      });
    }
  }
}
