import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/model/repo/barrel_repo.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_appbar_profile.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_screen_error.dart';
import 'package:project_bike_car/presentation/screen/all_brand_screen/widget_all_brands.dart';
import 'package:project_bike_car/presentation/screen/menu/home/all_brands/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/all_brands/brands/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BrandPageScreen extends StatefulWidget {


  @override
  _BrandPageScreenState createState() => _BrandPageScreenState();
}

class _BrandPageScreenState extends State<BrandPageScreen>
    with AutomaticKeepAliveClientMixin<BrandPageScreen> {
  @override
  @override
  void initState() {
    super.initState();
    BlocProvider.of<BrandBloc>(context).add(LoadBrand(0,0,'id_desc'));
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Colors.transparent));
  }


  @override
  Widget build(BuildContext context) {
    super.build(context);
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return MultiBlocProvider(
      providers: [

        BlocProvider(
          create: (context) =>
              HomeBrandBloc(brandBloc: BlocProvider.of<BrandBloc>(context)),
        ),

      ],
      child: BlocBuilder<HomeBloc, HomeState>(builder: (context, state) {
        return Scaffold(
          body: SafeArea(
              top: true,
              child: Container(
                  color: AppColor.WHITE, child: _buildContent(state))),
        );
      }),
    );
  }

  _buildContent(HomeState state) {
    if (state is HomeLoaded) {
      return _buildHomeLoaded(state);
    } else if (state is HomeLoading) {
      return Center(
        child: Container(
          height: 100,
          width: 100,
          child: Lottie.asset(
            'assets/lottie/trail_loading.json',
          ),
        ),
      );
    } else if (state is HomeNotLoaded) {
      return RefreshIndicator(
        onRefresh: () async {
          BlocProvider.of<HomeBloc>(context).add(RefreshHome(0,0,'id_desc'));
          await Future.delayed(Duration(seconds: 1));
          return true;
        },
        color: AppColor.PRIMARY_COLOR,
        backgroundColor: AppColor.THIRD_COLOR,
        child: WidgetScreenError(
          status: state.status,
        ),
      );
    } else {
      return Center(
        child: Text('Unknown state'),
      );
    }
  }

  _buildHomeLoaded(HomeLoaded state) {
    return Column(
      children: [
        _buildAppbar(),
        Expanded(
          child:    BlocBuilder<BrandBloc, BrandState>(builder: (context, state) {
            return Container(
              child: _buildBrandContent(state),
            );
          }),
        )
      ],
    );
  }


  _buildBrandContent(BrandState state) {
    if (state is BrandLoaded) {
      return _buildBrandLoaded(state);
    } else if (state is BrandLoading) {
      return Center(
        child: Container(
          height: 100,
          width: 100,
          child: Lottie.asset(
            'assets/lottie/trail_loading.json',
          ),
        ),
      );
    } else if (state is BrandNotLoaded) {
      return RefreshIndicator(
        onRefresh: () async {
          BlocProvider.of<BrandBloc>(context).add(RefreshBrand(0,0,'id_desc'));
          await Future.delayed(Duration(seconds: 1));
          return true;
        },
        color: AppColor.PRIMARY_COLOR,
        backgroundColor: AppColor.THIRD_COLOR,
        child: WidgetScreenError(
          status: state.status,
        ),
      );
    } else {
      return Center(
        child: Text('Unknown state'),
      );
    }
  }

  _buildBrandLoaded(BrandLoaded state) {
    return  Padding(
      padding: const EdgeInsets.only(left: 20, top: 20),
      child: WidgetAllBrand(),
    );
  }

  _buildAppbar() => WidgetAppbarProfile(
    backgroundColor: Color(0xff0bccd2),
    textColor: Colors.white,
    title: "Tất cả thương hiệu",
    left: [
      Padding(
        padding: const EdgeInsets.only(left: 10),
        child: GestureDetector(
          onTap: () {
            AppNavigator.navigateBack();
          },
          child: Image.asset(
            "assets/images/ic_arrow.png",
            width: 20,
            height: 20,
            color: Colors.white,
          ),
        ),
      )
    ],
  );



  @override
  bool get wantKeepAlive => true;
}
