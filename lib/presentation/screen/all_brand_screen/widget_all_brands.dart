import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/model/entity/brands.dart';
import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:project_bike_car/presentation/screen/menu/home/all_brands/brands/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/bloc/bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';


class WidgetAllBrand extends StatefulWidget {
  final Function(Brands brands, int) onCategoryClick;
  final int brandId;
  final int categoryId;

  WidgetAllBrand({
    Key key,
    this.onCategoryClick,this.brandId,this.categoryId
    //   this.refreshController,
  }) : super(key: key);
  @override
  _WidgetAllBrandState createState() =>
      _WidgetAllBrandState(brandId,categoryId);
}

class _WidgetAllBrandState
    extends State<WidgetAllBrand> {
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  int _selectedIndex =0;
  int brandId;
  int categoryId;


  _WidgetAllBrandState(this.brandId,this.categoryId);

  _onSelected(int index) {
    setState(() => _selectedIndex = index);
  }
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBrandBloc, HomeBrandState>(
      builder: (context, state) {
        if (state is HomeBrandLoaded) {
          return  Container(
            child: _buildBrands(state),
          );
        } else {
          return Container(
            child: Center(child: WidgetCircleProgress()),
          );
        }
      },
    );
  }

  Widget _buildBrands(HomeBrandLoaded state) {
    if(state.brand==null){

      return Center(
        child: Text("Không có thương hiệu nào?"),
      );

    }
    else{
      return GridView.builder(
        // padding: EdgeInsets.only(top: 10, left: 5, right: 5),
        shrinkWrap: true,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          //childAspectRatio: (itemWidth/itemHeight),
        ),
        itemCount: state.brand.length,
        itemBuilder: (context, index) {
          return Padding(
            padding: const EdgeInsets.only(right: 15),
            child: Column(
              children: [
                GestureDetector(
                  onTap: () {
                    // AppNavigator.navigateLogin();
                    _onSelected(index);
                    brandId=state.brand[index].id;
                    print("_____________________");
                    print(brandId);
                    BlocProvider.of<ProductBloc>(context).add(RefreshProduct(categoryId??0,brandId??0,'id_desc'));
                    AppNavigator.navigateBack();
                  },
                  child:                           Container(
                    width: MediaQuery.of(context).size.width / 3.6,
                    height: MediaQuery.of(context).size.height / 8,
                    // height: 100,
                    decoration: BoxDecoration(
                        color: _selectedIndex != null &&
                            _selectedIndex == index
                            ? Color(0xff96e5e7)
                            : Colors.grey[200],
                        // boxShadow: [
                        //   BoxShadow(color: Colors.green, spreadRadius: 5),
                        // ],
                        border: Border.all(  color: _selectedIndex != null &&
                            _selectedIndex == index
                            ? Colors.lightBlue
                            : Colors.grey[200],width: 2),
                        borderRadius:
                        BorderRadius.all(Radius.circular(5))),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Image.network(
                          state.brand[index].image,
                          width: 55,
                          height: 55,
                          // color: _selectedIndex != null &&
                          //     _selectedIndex == index
                          //     ? Colors.blueAccent[600]
                          //     : Colors.grey[500],
                        ),
                        SizedBox(height: 5,),
                        Text(state.brand[index].name,style: TextStyle(fontWeight: FontWeight.bold),),
                      ],
                    ),
                  ),

                ),
                SizedBox(
                  height: 10,
                ),

              ],
            ),
          );
        },
      );
    }

  }




}
