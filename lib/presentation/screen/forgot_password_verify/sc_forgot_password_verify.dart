import 'package:project_bike_car/app/constants/navigator/navigator.dart';
import 'package:project_bike_car/model/repo/user_repository.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_appbar_profile.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_spacer.dart';
import 'package:project_bike_car/presentation/screen/forgot_password_verify/widget_forgot_password_verify_form.dart';
import 'package:project_bike_car/presentation/screen/forgot_password_verify/widget_forgot_password_verify_username.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/forgot_password_verify_bloc.dart';
import 'forgot_password_verify_resend/bloc/forgot_password_verify_resend_bloc.dart';

class ForgotPasswordVerifyScreen extends StatefulWidget {
  @override
  _ForgotPasswordVerifyScreenState createState() =>
      _ForgotPasswordVerifyScreenState();
}

class _ForgotPasswordVerifyScreenState
    extends State<ForgotPasswordVerifyScreen> {
  String _username;

  _onArgument() {
    Future.delayed(Duration.zero, () async {
      final Map arguments = ModalRoute.of(context).settings.arguments as Map;
      setState(() {
        _username = arguments['username'];
        // _phone = arguments['phone'];
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _onArgument();
  }

  @override
  Widget build(BuildContext context) {
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return Scaffold(
      backgroundColor: Color(0xff33CC66),
      body: SafeArea(
        top: true,
        child: MultiBlocProvider(
          providers: [
            BlocProvider(
              create: (context) =>
                  ForgotPasswordVerifyBloc(userRepository: userRepository),
            ),
            BlocProvider(
              create: (context) => ForgotPasswordVerifyResendBloc(
                  userRepository: userRepository),
            ),
          ],
          child: Container(
              // color: AppColor.PRIMARY_BACKGROUND,
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 15),
                    child: _buildAppbar(),
                  ),
                  Expanded(
                    child: Container(
                      child: SingleChildScrollView(
                        child: Column(
                          children: [
                            WidgetSpacer(
                              height: 45,
                            ),
                            _buildUsername(),
                            WidgetSpacer(
                              height: 45,
                            ),
                            _buildForm(),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              )),
        ),
      ),
    );
  }

  _buildAppbar() => WidgetAppbarProfile(
    backgroundColor: Color(0xff33CC66),
    textColor: Colors.white,

    left: [
      Padding(
        padding: const EdgeInsets.only( left: 10),
        child: GestureDetector(
          onTap: () {
            AppNavigator.navigateBack();
          },

          child: Image.asset(
            "assets/images/ic_arrow.png",
            width: 20,
            height: 20,
            color: Colors.white,
          ),
        ),
      )
    ],
  );

  _buildUsername() => WidgetForgotPasswordVerifyUsername(
        username: _username,
      );

  _buildForm() => WidgetForgotPasswordVerifyForm(
        username: _username,
        // phone: _phone,
      );
}
