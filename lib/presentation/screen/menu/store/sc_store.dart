import 'dart:io';

import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:url_launcher/url_launcher.dart';

import 'appbar_store.dart';
class StorePageScreen extends StatefulWidget {
  @override
  _StorePageScreenState createState() => _StorePageScreenState();
}

class _StorePageScreenState extends State<StorePageScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
        child: Scaffold(
          backgroundColor: Colors.white,
          body: OrientationBuilder(builder: (context, orientation) {
            if (MediaQuery.of(context).size.width > 600) {
              return  Container(
                child:  Container(
                  color: Colors.white,
                  child: SingleChildScrollView(
                    physics: BouncingScrollPhysics(),
                    child: Column(
                      children: [
                        StoreAppbar(),
                        Container(
                            width: MediaQuery.of(context).size.width,
                            padding: EdgeInsets.all(11),
                            child:  Stack(
                              alignment: Alignment.bottomCenter,
                              children: [
                                Card(
                                  child: Column(
                                    children: [
                                      Image.asset("assets/images/logo_reapla.png"
                                        , height: MediaQuery.of(context).size.height*0.20,width: MediaQuery.of(context).size.height*0.20,),
                                      Container(
                                        margin: EdgeInsets.symmetric(horizontal: 5),
                                        child:
                                        Divider(
                                          color: Colors.grey,
                                          thickness: 0.4,
                                          height: 1,
                                        ),
                                      ),
                                      Container(
                                          padding: EdgeInsets.only(top:20),
                                          child:
                                          Text("Liên hệ với chúng tôi".toUpperCase(),
                                            style: TextStyle(
                                                fontSize: 14, fontStyle: FontStyle.italic, letterSpacing: 1.5,
                                                fontWeight: FontWeight.bold,color: Color(0xff33CC66)
                                            ),)
                                      ),


                                      Padding(
                                          padding: EdgeInsets.symmetric(horizontal: 20),
                                          child: widgetMailAddress()
                                      ),
                                      SizedBox(height: 10,),
                                      Padding(
                                          padding: EdgeInsets.symmetric(horizontal: 20),
                                          child: widgetInforAddress()
                                      ),
                                      SizedBox(height: 10,),
                                      Padding(
                                          padding: EdgeInsets.symmetric(horizontal: 20),
                                          child: widgetPhone()
                                      ),
                                      SizedBox(height: 10,),
                                      Padding(
                                          padding: EdgeInsets.symmetric(horizontal: 20),
                                          child: widgetWebsite()
                                      ),

                                      Container(
                                        padding: EdgeInsets.all(5),
                                        child:  Stack(
                                          alignment: Alignment.center,
                                          children: [
                                            widgetAddress(context),
                                            Container(
                                              height: 60,
                                              width: 60,
                                              child: Lottie.asset(
                                                'assets/lottie/62047-click-hand-cursor.json',
                                              ),
                                            ),
                                          ],
                                        ),
                                      )


                                    ],
                                  ),
                                ),


                              ],
                            )
                        ),


                      ],
                    ),
                  ),
                ),);
            } else {
              return   Container(
                child:  Container(
                  color: Colors.white,
                  child: SingleChildScrollView(
                    physics: BouncingScrollPhysics(),
                    child: Column(
                      children: [
                        StoreAppbar(),
                        Container(
                            width: MediaQuery.of(context).size.width,
                            padding: EdgeInsets.all(11),
                            child:  Stack(
                              alignment: Alignment.bottomCenter,
                              children: [
                                Card(
                                  child: Column(
                                    children: [
                                      Image.asset("assets/images/logo_reapla.png"
                                        , height: MediaQuery.of(context).size.height*0.20,width: MediaQuery.of(context).size.height*0.20,),
                                      Container(
                                        margin: EdgeInsets.symmetric(horizontal: 5),
                                        child:
                                        Divider(
                                          color: Colors.grey,
                                          thickness: 0.4,
                                          height: 1,
                                        ),
                                      ),
                                      Container(
                                          padding: EdgeInsets.only(top:20),
                                          child:
                                          Text("Liên hệ với chúng tôi".toUpperCase(),
                                            style: TextStyle(
                                                fontSize: 14, fontStyle: FontStyle.italic, letterSpacing: 1.5,
                                                fontWeight: FontWeight.bold,color: Color(0xff33CC66)
                                            ),)
                                      ),


                                      Padding(
                                          padding: EdgeInsets.symmetric(horizontal: 20),
                                          child: widgetMailAddress()
                                      ),
                                      SizedBox(height: 10,),
                                      Padding(
                                          padding: EdgeInsets.symmetric(horizontal: 20),
                                          child: widgetInforAddress()
                                      ),
                                      SizedBox(height: 10,),
                                      Padding(
                                          padding: EdgeInsets.symmetric(horizontal: 20),
                                          child: widgetPhone()
                                      ),
                                      SizedBox(height: 10,),
                                      Padding(
                                          padding: EdgeInsets.symmetric(horizontal: 20),
                                          child: widgetWebsite()
                                      ),

                                      Container(
                                        padding: EdgeInsets.all(5),
                                        child:  Stack(
                                          alignment: Alignment.center,
                                          children: [
                                            widgetAddress(context),
                                            Container(
                                              height: 60,
                                              width: 60,
                                              child: Lottie.asset(
                                                'assets/lottie/62047-click-hand-cursor.json',
                                              ),
                                            ),
                                          ],
                                        ),
                                      )


                                    ],
                                  ),
                                ),


                              ],
                            )
                        ),


                      ],
                    ),
                  ),
                ),);
            }


          }),
        )

    );
  }
}
widgetMailAddress(){
  return Stack(
    alignment: Alignment.centerLeft,
    children: [
      Row(
        children: [
          Container(
            width:30, height: 30,
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
              color: Color(0xff33CC66),
              shape: BoxShape.circle,

            ),
            child: Image.asset("assets/images/zaloo.png"),
          ),
          SizedBox(width: 10,),
          GestureDetector(
              onTap: (){
                _zaloMe();
              },
              child: Text("Liên hệ trực tiếp hoặc qua Zalo",maxLines: 2, style: TextStyle(fontWeight: FontWeight.bold,color: Color(0xff33CC66)),))


        ],
      ),
      Container(
        height: 60,
        width: 60,
        child: Lottie.asset(
          'assets/lottie/62047-click-hand-cursor.json',
        ),
      ),
    ],
  )

    ;
}

widgetInforAddress(){
  return Row(
    children: [
      Container(
        width:30, height: 30,
        padding: EdgeInsets.all(5),
        decoration: BoxDecoration(
          color: Color(0xff33CC66),
          shape: BoxShape.circle,

        ),
        child: Image.asset("assets/images/address.png",color: Colors.white,),
      ),
      SizedBox(width: 10,),
      Text("Địa chỉ : 52 Ung Văn Khiêm-\nPhường 25-Quận Bình Thạnh-TPHCM",
        maxLines: 3, style: TextStyle(fontWeight: FontWeight.bold,color: Color(0xff33CC66)),)


    ],
  );
}
widgetAddress(BuildContext context){
  return GestureDetector(
    onTap: (){
      _openMap();
    },
    child: Image.asset("assets/images/map.png",height: MediaQuery.of(context).size.height/6,width: double.infinity,fit: BoxFit.cover,),
  );
}
widgetPhone(){
  return Stack(
    alignment: Alignment.centerLeft,
    children: [
      Row(
        children: [
          Container(
            width:30, height: 30,
            padding: EdgeInsets.all(6),
            decoration: BoxDecoration(
              color: Color(0xff33CC66),
              shape: BoxShape.circle,

            ),
            child: Image.asset("assets/images/iconfinder_phone-call_2561306.png", color: Colors.white,),
          ),
          SizedBox(width: 10,),
          GestureDetector(
              onTap:()  {
                _callMe1();
              },
              child: Text("0888898000", style: TextStyle(color: Color(0xff33CC66),fontWeight: FontWeight.bold),)),
          GestureDetector(
              onTap:()  {
                _callMe2();
              },


              child: Text(", 0878888850", style: TextStyle(color: Color(0xff33CC66),fontWeight: FontWeight.bold)))

        ],
      ),
      Container(
        height: 60,
        width: 60,
        child: Lottie.asset(
          'assets/lottie/62047-click-hand-cursor.json',
        ),
      ),
    ],
  )

   ;
}
widgetWebsite(){
  return Stack(
    alignment: Alignment.centerLeft,
    children: [
      Row(
        children: [
          Container(
            width:30, height: 30,
            padding: EdgeInsets.all(4),
            decoration: BoxDecoration(
              color: Color(0xff33CC66),
              shape: BoxShape.circle,

            ),
            child: Image.asset("assets/images/iconfinder_world_www_web_website_5340283.png", fit: BoxFit.cover, color: Colors.white,),
          ),
          SizedBox(width: 10,),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                  onTap:() {
                    _openWeb2();
                  },
                  child: Text("http://trumgop.com ,",  style: TextStyle(fontWeight: FontWeight.bold,color: Color(0xff33CC66)),)),
              GestureDetector(
                  onTap:() {
                    _openWeb1();
                  },
                  child: Text("http://shopxecuongk.com",  style: TextStyle(fontWeight: FontWeight.bold,color: Color(0xff33CC66)),))
            ],
          ),


        ],
      ),
      Container(
        height: 60,
        width: 60,
        child: Lottie.asset(
          'assets/lottie/62047-click-hand-cursor.json',
        ),
      ),
    ],
  );



}
_openMap() async {
  // Android
  var url = 'https://goo.gl/maps/MWzp1o2jYGoyrLKQ9';
  if (Platform.isIOS) {
    // iOS
    url = 'https://goo.gl/maps/MWzp1o2jYGoyrLKQ9';
  }
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}


_callMe1() async {
  // Android
  const uri = 'tel://0888898000';
  if (await canLaunch(uri)) {
    await launch(uri);
  } else {
    // iOS
    const uri = 'tel://0888898000';
    if (await canLaunch(uri)) {
      await launch(uri);
    } else {
      throw 'Could not launch $uri';
    }
  }
}

_callMe2() async {
  // Android
  const uri = 'tel://0878888850';
  if (await canLaunch(uri)) {
    await launch(uri);
  } else {
    // iOS
    const uri = 'tel://0878888850';
    if (await canLaunch(uri)) {
      await launch(uri);
    } else {
      throw 'Could not launch $uri';
    }
  }
}

_openWeb1() async {
  // Android
  const uri = 'http://shopxecuongk.com';
  if (await canLaunch(uri)) {
    await launch(uri);
  } else {
    // iOS
    const uri = 'http://shopxecuongk.com';
    if (await canLaunch(uri)) {
      await launch(uri);
    } else {
      throw 'Could not launch $uri';
    }
  }
}
_openWeb2() async {
  // Android
  const uri = 'http://trumgop.com';
  if (await canLaunch(uri)) {
    await launch(uri);
  } else {
    // iOS
    const uri = 'http://trumgop.com';
    if (await canLaunch(uri)) {
      await launch(uri);
    } else {
      throw 'Could not launch $uri';
    }
  }
}

_zaloMe() async {
  // Android
  const uri = 'https://zalo.me/0888898000';
  if (await canLaunch(uri)) {
    await launch(uri);
  } else {
    // iOS
    const uri = 'https://zalo.me/0888898000';
    if (await canLaunch(uri)) {
      await launch(uri);
    } else {
      throw 'Could not launch $uri';
    }
  }
}

