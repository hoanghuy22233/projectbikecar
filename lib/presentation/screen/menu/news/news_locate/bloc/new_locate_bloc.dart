import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:project_bike_car/model/entity/posts_news.dart';
import 'package:project_bike_car/model/repo/barrel_repo.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

import 'bloc.dart';

class NewLocateBloc
    extends Bloc<NewLocateEvent, NewLocateState> {
  final UserRepository invoiceRepository;

  NewLocateBloc({@required this.invoiceRepository});

  @override
  NewLocateState get initialState => NewLocateLoading();

  @override
  Stream<NewLocateState> mapEventToState(
      NewLocateEvent event) async* {
    if (event is LoadNewLocate) {
      yield* _mapLoadNewLocateToState();
    } else if (event is RefreshNewLocate) {
      yield NewLocateLoading();
      yield* _mapLoadNewLocateToState();
    }
  }

  Stream<NewLocateState> _mapLoadNewLocateToState() async* {
    final currentState = state;
    try {
      if (state is NewLocateLoading) {
        final invoices = await _fetchProduct();
        yield NewLocateLoaded(invoices: invoices);
      }
      if (currentState is NewLocateLoaded) {
        final invoices = await _fetchProduct();
      }
    } catch (e) {
      yield NewLocateNotLoaded(DioErrorUtil.handleError(e));
    }
  }

  Future<List<PostsNews>> _fetchProduct() async {
    final response = await invoiceRepository.getNew(40);

    return response.data.posts;
  }
}
