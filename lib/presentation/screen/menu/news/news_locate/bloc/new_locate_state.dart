
import 'package:equatable/equatable.dart';
import 'package:project_bike_car/model/entity/posts.dart';
import 'package:project_bike_car/model/entity/posts_news.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

abstract class NewLocateState extends Equatable {
  const NewLocateState();

  @override
  List<Object> get props => [];
}

class NewLocateLoading extends NewLocateState {}

class NewLocateLoaded extends NewLocateState {
  final List<PostsNews> invoices;

  const NewLocateLoaded({this.invoices});

  NewLocateLoaded copyWith({
    List<PostsNews> invoices,
  }) {
    return NewLocateLoaded(
      invoices: invoices ?? this.invoices,
    );
  }

  @override
  List<Object> get props => [invoices,];

  @override
  String toString() {
    return 'NewBikeLoaded{invoices: $invoices,}';
  }
}

class NewLocateNotLoaded extends NewLocateState {
  final DioStatus status;

  NewLocateNotLoaded(this.status);

  @override
  String toString() {
    return 'NewLocateNotLoaded{error: $status}';
  }
}
