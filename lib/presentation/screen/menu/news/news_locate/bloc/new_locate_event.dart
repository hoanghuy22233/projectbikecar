import 'package:equatable/equatable.dart';

class NewLocateEvent extends Equatable {
  const NewLocateEvent();

  List<Object> get props => [];
}

class LoadNewLocate extends NewLocateEvent {
}

class RefreshNewLocate extends NewLocateEvent {}
