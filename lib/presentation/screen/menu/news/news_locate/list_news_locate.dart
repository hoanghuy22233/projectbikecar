import 'package:intl/intl.dart';
import 'package:project_bike_car/app/constants/color/color.dart';
import 'package:project_bike_car/app/constants/style/style.dart';
import 'package:project_bike_car/app/constants/value/value.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:project_bike_car/model/entity/posts_news.dart';
import 'package:date_format/date_format.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_cached_image.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_spacer.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:project_bike_car/presentation/screen/menu/news/new_detaill/sc_new_detail.dart';
import 'package:url_launcher/url_launcher.dart';

class ListNewsLocate extends StatefulWidget {
  final int id;
  final PostsNews post;
  const ListNewsLocate({Key key, this.id, this.post}) : super(key: key);

  @override
  _ListNewsLocateState createState() => _ListNewsLocateState();
}

class _ListNewsLocateState extends State<ListNewsLocate> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          widget.post.link != null
              ? _openUrl(widget.post.link)
              : Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          NewDetailPageScreen(id: widget.post.id)),
                );
        },
        child: Container(
          child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: Container(
              height: 120,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AspectRatio(
                    aspectRatio: AppValue.NEWS_IMAGE_RATIO,
                    child: WidgetCachedImage(
                      url: widget.post.image,
                    ),
                  ),
                  const SizedBox(
                    width: 4,
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 4),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            widget.post.name,
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                            style: AppStyle.APP_MEDIUM_BOLD
                                .copyWith(color: AppColor.PRIMARY),
                          ),
                          WidgetSpacer(
                            height: 3,
                          ),
                          Expanded(
                            child: Container(
                                child: widget.post.description != null
                                    ? Text(
                                        widget.post.description.length <= 70
                                            ? widget.post.description
                                            : widget.post.description
                                                    .substring(0, 70) +
                                                '...',
                                        style: AppStyle.DEFAULT_SMALL
                                            .copyWith(color: AppColor.PRIMARY))
                                    : Container()),
                          ),
                          WidgetSpacer(
                            height: 3,
                          ),
                          Text(
                            widget.post.created,
                            style: AppStyle.DEFAULT_SMALL
                                .copyWith(color: AppColor.GREY),
                            textAlign: TextAlign.start,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ));
  }

  _openUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
