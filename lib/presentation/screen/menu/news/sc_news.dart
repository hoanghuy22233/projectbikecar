import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/app/constants/color/color.dart';
import 'package:project_bike_car/model/repo/user_repository.dart';
import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_screen_error.dart';
import 'package:project_bike_car/presentation/screen/menu/news/bloc/new_bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/news/bloc/new_event.dart';
import 'package:project_bike_car/presentation/screen/menu/news/bloc/new_state.dart';
import 'package:project_bike_car/presentation/screen/menu/news/news_bike/widget_news_bike.dart';
import 'package:flutter/material.dart';
import 'package:project_bike_car/presentation/screen/menu/news/barrel_news..dart';
import 'package:project_bike_car/presentation/screen/menu/news/news_locate/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/news/news_phone/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/news/news_phone/widget_news_phone.dart';
import 'package:project_bike_car/presentation/screen/menu/news/news_sim/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/news/news_sim/widget_news_sim.dart';
import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';

import 'new_car/bloc/new_car_bloc.dart';
import 'new_car/widget_new_car.dart';
import 'news_bike/bloc/bloc.dart';
import 'news_locate/widget_news_locate.dart';
class NewsPageScreen extends StatefulWidget {
  final int id;

  const NewsPageScreen({Key key, this.id}) : super(key: key);
  @override
  _NewsPageScreenState createState() => _NewsPageScreenState();
}


class _NewsPageScreenState extends State<NewsPageScreen>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 4);
    _tabController.addListener(() {
      print('my index is' + _tabController.index.toString());
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) =>
              NewCarsBloc(invoiceRepository: userRepository),
        ),
        BlocProvider(
          create: (context) =>
              NewLocateBloc(invoiceRepository: userRepository),
        ),

        BlocProvider(
          create: (context) =>
              NewBikeBloc(invoiceRepository: userRepository),
        ),
        BlocProvider(
          create: (context) =>
              NewSimBloc(invoiceRepository: userRepository),
        ),
        BlocProvider(
          create: (context) =>
              NewPhoneBloc(invoiceRepository: userRepository),
        ),


      ],
      child:BlocBuilder<NewBloc, NewState>(builder: (context, state) {
        return SafeArea(
          top: true,
            child:Scaffold(
              backgroundColor: Colors.white,
              body: OrientationBuilder(builder: (context, orientation) {
                if (MediaQuery.of(context).size.width > 600) {
                  return  Container(
                      child: Container(
                        // padding: EdgeInsets.only(top: 20),
                          child: Column(
                            children: [
                              NewsAppbar(),
                              _buildTabBarMenu(),
                              Expanded(
                                child: TabBarView(
                                  controller: _tabController,
                                  children: [
                                    WidgetNewCars(),
                                    WidgetNewsSim(),
                                    WidgetNewsPhone(),
                                    WidgetNewsLocate()],
                                ),
                              ),
                            ],
                          )));
                } else {
                  return   Container(
                      child: Column(
                        children: [
                          NewsAppbar(),
                          _buildTabBarMenu(),
                          Expanded(
                            child: TabBarView(
                              controller: _tabController,
                              children: [
                                WidgetNewCars(),
                                WidgetNewsSim(),
                                WidgetNewsPhone(),
                                WidgetNewsLocate()],
                            ),
                          ),
                        ],
                      ));
                }


              }),
            )

        );
      })


      ,
    );
  }

  Widget _buildTabBarMenu() {
    return new Container(
      decoration: BoxDecoration(
          border: Border.all(color: Color(0xff33CC66)),
      ),
      height: AppValue.ACTION_BAR_HEIGHT,
      child: new TabBar(
        controller: _tabController,
        tabs: [
          Tab(
            text: "Ô tô,xe máy",
          ),
          Tab(
            text: "Sim thẻ",
          ),
          Tab(
            text: "Điện thoại",
          ),
          Tab(
            text: "Định vị",
          ),
        ],
        labelStyle: AppStyle.DEFAULT_SMALL,
        unselectedLabelStyle: AppStyle.DEFAULT_SMALL,
        labelColor: AppColor.WHITE,
        unselectedLabelColor: AppColor.BLACK,
        indicator: BubbleTabIndicator(
          indicatorHeight: AppValue.ACTION_BAR_HEIGHT - 10,
          indicatorColor: Color(0xff33CC66),
          tabBarIndicatorSize: TabBarIndicatorSize.tab,
          indicatorRadius: 500,
        ),
      ),
    );
  }

}