
import 'package:equatable/equatable.dart';
import 'package:project_bike_car/model/entity/posts.dart';
import 'package:project_bike_car/model/entity/posts_news.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

abstract class NewPhoneState extends Equatable {
  const NewPhoneState();

  @override
  List<Object> get props => [];
}

class NewPhoneLoading extends NewPhoneState {}

class NewPhoneLoaded extends NewPhoneState {
  final List<PostsNews> invoices;

  const NewPhoneLoaded({this.invoices});

  NewPhoneLoaded copyWith({
    List<PostsNews> invoices,
  }) {
    return NewPhoneLoaded(
      invoices: invoices ?? this.invoices,
    );
  }

  @override
  List<Object> get props => [invoices,];

  @override
  String toString() {
    return 'NewPhoneLoaded{invoices: $invoices,}';
  }
}

class NewPhoneNotLoaded extends NewPhoneState {
  final DioStatus status;

  NewPhoneNotLoaded(this.status);

  @override
  String toString() {
    return 'NewPhoneNotLoaded{error: $status}';
  }
}
