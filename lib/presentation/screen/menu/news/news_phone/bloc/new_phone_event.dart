import 'package:equatable/equatable.dart';

class NewPhoneEvent extends Equatable {
  const NewPhoneEvent();

  List<Object> get props => [];
}

class LoadNewPhone extends NewPhoneEvent {
}

class RefreshNewPhone extends NewPhoneEvent {}
