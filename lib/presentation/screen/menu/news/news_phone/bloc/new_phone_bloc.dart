import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:project_bike_car/model/entity/posts_news.dart';
import 'package:project_bike_car/model/repo/barrel_repo.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

import 'bloc.dart';

class NewPhoneBloc
    extends Bloc<NewPhoneEvent, NewPhoneState> {
  final UserRepository invoiceRepository;

  NewPhoneBloc({@required this.invoiceRepository});

  @override
  NewPhoneState get initialState => NewPhoneLoading();

  @override
  Stream<NewPhoneState> mapEventToState(
      NewPhoneEvent event) async* {
    if (event is LoadNewPhone) {
      yield* _mapLoadNewPhoneToState();
    } else if (event is RefreshNewPhone) {
      yield NewPhoneLoading();
      yield* _mapLoadNewPhoneToState();
    }
  }

  Stream<NewPhoneState> _mapLoadNewPhoneToState() async* {
    final currentState = state;
    try {
      if (state is NewPhoneLoading) {
        final invoices = await _fetchProduct();
        yield NewPhoneLoaded(invoices: invoices);
      }
      if (currentState is NewPhoneLoaded) {
        final invoices = await _fetchProduct();
      }
    } catch (e) {
      yield NewPhoneNotLoaded(DioErrorUtil.handleError(e));
    }
  }

  Future<List<PostsNews>> _fetchProduct() async {
    final response = await invoiceRepository.getNew(39);

    return response.data.posts;
  }
}
