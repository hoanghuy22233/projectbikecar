import 'package:equatable/equatable.dart';
import 'package:project_bike_car/model/api/response/new_response.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

abstract class NewState extends Equatable {
  const NewState();

  @override
  List<Object> get props => [];
}

class NewLoading extends NewState {}

class NewLoaded extends NewState {
  final NewResponse newResponse;

  const NewLoaded({this.newResponse});

  @override
  List<Object> get props => [newResponse];

  @override
  String toString() {
    return 'NewLoaded{newResponse: $newResponse}';
  }


}

class NewNotLoaded extends NewState {
  final DioStatus status;

  NewNotLoaded({this.status});

  @override
  String toString() {
    return 'NewNotLoaded{error: $status}';
  }
}