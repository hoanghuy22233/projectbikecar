import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:project_bike_car/model/repo/barrel_repo.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

import 'bloc.dart';

class NewBloc extends Bloc<NewEvent, NewState> {
  final UserRepository userRepository;

  NewBloc({ this.userRepository});

  @override
  NewState get initialState => NewLoading();

  @override
  Stream<NewState> mapEventToState(NewEvent event) async* {
    if (event is LoadNew) {
      yield* _mapLoadNewToState();
    } else if (event is RefreshNew) {
      yield NewLoading();
      yield* _mapLoadNewToState();
    }
  }

  Stream<NewState> _mapLoadNewToState() async* {
    try {
      final newResponse = await userRepository.getNew(36);
      print('-----------------');
      print(newResponse);
      await Future.delayed(Duration(milliseconds: 500), () {});
      yield NewLoaded(newResponse: newResponse);
    } catch (e) {
      yield NewNotLoaded(status: DioErrorUtil.handleError(e));
    }
  }
}
