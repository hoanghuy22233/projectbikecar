import 'package:equatable/equatable.dart';

class NewEvent extends Equatable {
  const NewEvent();

  List<Object> get props => [];
}

class LoadNew extends NewEvent {
}

class RefreshNew extends NewEvent {}
