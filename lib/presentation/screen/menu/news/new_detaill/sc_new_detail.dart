import 'package:date_format/date_format.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';
import 'package:lottie/lottie.dart';
import 'package:project_bike_car/app/constants/color/color.dart';
import 'package:project_bike_car/app/constants/navigator/navigator.dart';
import 'package:project_bike_car/app/constants/style/style.dart';
import 'package:project_bike_car/app/constants/value/value.dart';
import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_appbar.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_appbar_profile.dart';
import 'package:flutter/material.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_cached_image.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_circle_progress.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';

import 'bloc/bloc.dart';
import 'bloc/news_detail_bloc.dart';




class NewDetailPageScreen extends StatefulWidget {
  final int id;

  const NewDetailPageScreen({Key key, this.id}) : super(key: key);
  @override
  _NewDetailPageScreenState createState() => _NewDetailPageScreenState();
}

class _NewDetailPageScreenState extends State<NewDetailPageScreen>  with AutomaticKeepAliveClientMixin<NewDetailPageScreen>{
  ScrollController _scrollController;
  @override
  void initState() {
    super.initState();
    BlocProvider.of<NewsDetailBloc>(context).add(LoadNewsDetail(widget.id));
  }
  var logger = Logger(
    printer: PrettyPrinter(),
  );
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return  SafeArea(
      child: Scaffold(
              backgroundColor: AppColor.GREY_LIGHTER_3,
              body: Column(
                children: [
                  _buildAppbar(),
                  Expanded(
                    child: SingleChildScrollView(
                      physics: AlwaysScrollableScrollPhysics(),
                      controller: _scrollController,
                      child: BlocBuilder<NewsDetailBloc, NewsDetailState>(
                        builder: (context, state) {
                          return _buildContent(state);
                        },
                      ),
                    ),
                  )
                ],
              )),
    );
  }


  Widget _buildContent(NewsDetailState state) {
    if (state is NewsDetailLoaded) {
      var news = state.news;
      return Column(
        children: [
     Container(
            color: Colors.red,
            child: WidgetCachedImage(
              url: news.first.image,
            ),
          ),
          Container(
            width: double.infinity,
            color: AppColor.WHITE,
            padding: EdgeInsets.all(AppValue.APP_HORIZONTAL_PADDING),
            child: Text(
              news?.first?.name??'',style: TextStyle(color: Colors.black,fontSize: 14, fontWeight: FontWeight.w300),
            ),
          ),
          Container(
            width: double.infinity,
            color: AppColor.WHITE,
            padding: EdgeInsets.all(AppValue.APP_HORIZONTAL_PADDING),
            child: HtmlWidget(
              news?.first?.content??'',
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Align(
            alignment: Alignment.bottomRight,

              child: Text(
                  news.first.created
              )),
        ],
      );
    } else if (state is NewsDetailLoading) {
      return SizedBox(
        width: Get.width,
        height: Get.height - AppValue.ACTION_BAR_HEIGHT,
        child: Center(child: Container(
          height: 100,
          width: 100,
          child: Lottie.asset(
            'assets/lottie/load_car.json',
          ),
        )),
      );
    } else if (state is NewsDetailNotLoaded) {
      return WidgetScreenError(
        status: state.status,
        inScroll: true,
      );
    } else {
      return Center(
        child: Text('Unknown state'),
      );
    }
  }

  @override
  bool get wantKeepAlive => true;
  _buildAppbar() => WidgetAppbar(
    backgroundColor: Colors.blue,
    title: "CHI TIẾT BÀI VIẾT",
    left: [
      GestureDetector(
        onTap: () {
          AppNavigator.navigateBack();
        },
        child: Padding(
          padding: const EdgeInsets.only(left: 10),
          child: Image.asset("assets/images/ic_arrow.png",width: 20,height: 20,color: Colors.white,),
        ),
      )
    ],
  );
}
