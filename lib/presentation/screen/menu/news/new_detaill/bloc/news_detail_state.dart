
import 'package:equatable/equatable.dart';
import 'package:project_bike_car/model/entity/posts.dart';
import 'package:project_bike_car/model/entity/posts_news.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

abstract class NewsDetailState extends Equatable {
  const NewsDetailState();

  @override
  List<Object> get props => [];
}

class NewsDetailLoading extends NewsDetailState {}

class NewsDetailLoaded extends NewsDetailState {
  final List<PostsNews>  news;

  const NewsDetailLoaded({this.news});

  @override
  List<Object> get props => [news];

  @override
  String toString() {
    return 'NewsDetailLoaded{news: $news}';
  }
}

class NewsDetailNotLoaded extends NewsDetailState {
  final DioStatus status;

  NewsDetailNotLoaded(this.status);

  @override
  String toString() {
    return 'NewsDetailNotLoaded{error: $status}';
  }
}