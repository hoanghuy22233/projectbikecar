
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:project_bike_car/model/repo/barrel_repo.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

import 'bloc.dart';

class NewsDetailBloc extends Bloc<NewsDetailEvent, NewsDetailState> {
  final UserRepository homeRepository;

  NewsDetailBloc({@required this.homeRepository});

  @override
  NewsDetailState get initialState => NewsDetailLoading();

  @override
  Stream<NewsDetailState> mapEventToState(NewsDetailEvent event) async* {
    if (event is LoadNewsDetail) {
      yield* _mapLoadNewsDetailToState(event.newsId);
    } else if (event is RefreshNewsDetail) {
      yield NewsDetailLoading();
      yield* _mapLoadNewsDetailToState(event.newsId);
    }
  }

  Stream<NewsDetailState> _mapLoadNewsDetailToState(int newsId) async* {
    try {
      final response = await homeRepository.getNewDetail(id: newsId);
      yield NewsDetailLoaded(news: response.data.posts);
    } catch (e) {
      yield NewsDetailNotLoaded(DioErrorUtil.handleError(e));
    }
  }
}
