import 'package:equatable/equatable.dart';

class NewSimEvent extends Equatable {
  const NewSimEvent();

  List<Object> get props => [];
}

class LoadNewSim extends NewSimEvent {
}

class RefreshNewSim extends NewSimEvent {}
