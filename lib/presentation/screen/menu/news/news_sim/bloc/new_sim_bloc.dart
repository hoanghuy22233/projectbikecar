import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:project_bike_car/model/entity/posts_news.dart';
import 'package:project_bike_car/model/repo/barrel_repo.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

import 'bloc.dart';

class NewSimBloc
    extends Bloc<NewSimEvent, NewSimState> {
  final UserRepository invoiceRepository;

  NewSimBloc({@required this.invoiceRepository});

  @override
  NewSimState get initialState => NewSimLoading();

  @override
  Stream<NewSimState> mapEventToState(
      NewSimEvent event) async* {
    if (event is LoadNewSim) {
      yield* _mapLoadNewSimToState();
    } else if (event is RefreshNewSim) {
      yield NewSimLoading();
      yield* _mapLoadNewSimToState();
    }
  }

  Stream<NewSimState> _mapLoadNewSimToState() async* {
    final currentState = state;
    try {
      if (state is NewSimLoading) {
        final invoices = await _fetchProduct();
        yield NewSimLoaded(invoices: invoices);
      }
      if (currentState is NewSimLoaded) {
        final invoices = await _fetchProduct();
      }
    } catch (e) {
      yield NewSimNotLoaded(DioErrorUtil.handleError(e));
    }
  }

  Future<List<PostsNews>> _fetchProduct() async {
    final response = await invoiceRepository.getNew(38);

    return response.data.posts;
  }
}
