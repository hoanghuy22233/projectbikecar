
import 'package:equatable/equatable.dart';
import 'package:project_bike_car/model/entity/posts.dart';
import 'package:project_bike_car/model/entity/posts_news.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

abstract class NewSimState extends Equatable {
  const NewSimState();

  @override
  List<Object> get props => [];
}

class NewSimLoading extends NewSimState {}

class NewSimLoaded extends NewSimState {
  final List<PostsNews> invoices;

  const NewSimLoaded({this.invoices});

  NewSimLoaded copyWith({
    List<PostsNews> invoices,
  }) {
    return NewSimLoaded(
      invoices: invoices ?? this.invoices,
    );
  }

  @override
  List<Object> get props => [invoices,];

  @override
  String toString() {
    return 'NewSimLoaded{invoices: $invoices,}';
  }
}

class NewSimNotLoaded extends NewSimState {
  final DioStatus status;

  NewSimNotLoaded(this.status);

  @override
  String toString() {
    return 'NewSimNotLoaded{error: $status}';
  }
}
