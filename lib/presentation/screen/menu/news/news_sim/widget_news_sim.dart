import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/model/entity/posts.dart';
import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:project_bike_car/presentation/screen/menu/news/news_locate/list_news_locate.dart';
import 'package:project_bike_car/presentation/screen/menu/news/news_sim/bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class WidgetNewsSim extends StatefulWidget {
  final Function(Posts posts, int) onCategoryClick;
  const WidgetNewsSim({
    Key key,
    this.onCategoryClick,
  }) : super(key: key);
  @override
  _WidgetNewsSimState createState() => _WidgetNewsSimState();
}

class _WidgetNewsSimState extends State<WidgetNewsSim>
    with AutomaticKeepAliveClientMixin<WidgetNewsSim> {
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  @override
  void initState() {
    super.initState();
    BlocProvider.of<NewSimBloc>(context).add(LoadNewSim());
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocListener<NewSimBloc, NewSimState>(
      listener: (context, state) {
        if (state is NewSimLoaded) {}
      },
      child: BlocBuilder<NewSimBloc, NewSimState>(
        builder: (context, state) {
          return Container(
            child: _buildContent(state),
          );
        },
      ),
    );
  }

  Widget _buildContent(NewSimState state) {
    if (state is NewSimLoaded) {
      return _buildContents(state);
    } else if (state is NewSimLoading) {
      return Container(
        width: double.infinity,
        height: AppValue.PRODUCT_HORIZONTAL_HEIGHT,
        child: Center(child: Container(
          height: 100,
          width: 100,
          child: Lottie.asset(
            'assets/lottie/load_car.json',
          ),
        )),
      );
    } else if (state is NewSimNotLoaded) {
      return WidgetScreenError(
        status: state.status,
      );
    } else {
      return Center(
        child: Text('Unknown state'),
      );
    }
  }

  _buildContents(NewSimLoaded state) {
    return state.invoices.length != 0
        ? Container(
            child: ListView.builder(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemCount: state.invoices.length,
                itemBuilder: (context, index) {
                  return Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height / 5,
                    child: state.invoices != null
                        ? Card(
                            elevation: 4,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(4.0)),
                            child: ListNewsLocate(post: state.invoices[index]))
                        : Container(),
                  );
                }))
        : Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                    height: 120,
                    width: 120,
                    child: Image.asset('assets/images/box.png')),
                SizedBox(
                  height: 20,
                ),
                Text('Bạn chưa tin tức nào!'),
              ],
            ),
          );
  }

  @override
  bool get wantKeepAlive => true;
}
