import 'package:equatable/equatable.dart';

class NewBikeEvent extends Equatable {
  const NewBikeEvent();

  List<Object> get props => [];
}

class LoadNewBike extends NewBikeEvent {
}

class RefreshNewBike extends NewBikeEvent {}
