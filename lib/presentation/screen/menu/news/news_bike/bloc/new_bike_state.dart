
import 'package:equatable/equatable.dart';
import 'package:project_bike_car/model/entity/posts.dart';
import 'package:project_bike_car/model/entity/posts_news.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

abstract class NewBikeState extends Equatable {
  const NewBikeState();

  @override
  List<Object> get props => [];
}

class NewBikeLoading extends NewBikeState {}

class NewBikeLoaded extends NewBikeState {
  final List<PostsNews> invoices;

  const NewBikeLoaded({this.invoices});

  NewBikeLoaded copyWith({
    List<PostsNews> invoices,
  }) {
    return NewBikeLoaded(
      invoices: invoices ?? this.invoices,
    );
  }

  @override
  List<Object> get props => [invoices,];

  @override
  String toString() {
    return 'NewBikeLoaded{invoices: $invoices,}';
  }
}

class NewBikeNotLoaded extends NewBikeState {
  final DioStatus status;

  NewBikeNotLoaded(this.status);

  @override
  String toString() {
    return 'NewBikeNotLoaded{error: $status}';
  }
}
