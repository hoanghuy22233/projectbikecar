import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:project_bike_car/model/entity/posts_news.dart';
import 'package:project_bike_car/model/repo/barrel_repo.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

import 'bloc.dart';

class NewBikeBloc
    extends Bloc<NewBikeEvent, NewBikeState> {
  final UserRepository invoiceRepository;

  NewBikeBloc({@required this.invoiceRepository});

  @override
  NewBikeState get initialState => NewBikeLoading();

  @override
  Stream<NewBikeState> mapEventToState(
      NewBikeEvent event) async* {
    if (event is LoadNewBike) {
      yield* _mapLoadNewBikeToState();
    } else if (event is RefreshNewBike) {
      yield NewBikeLoading();
      yield* _mapLoadNewBikeToState();
    }
  }

  Stream<NewBikeState> _mapLoadNewBikeToState() async* {
    final currentState = state;
    try {
      if (state is NewBikeLoading) {
        final invoices = await _fetchProduct();
        yield NewBikeLoaded(invoices: invoices);
      }
      if (currentState is NewBikeLoaded) {
        final invoices = await _fetchProduct();
      }
    } catch (e) {
      yield NewBikeNotLoaded(DioErrorUtil.handleError(e));
    }
  }

  Future<List<PostsNews>> _fetchProduct() async {
    final response = await invoiceRepository.getNew(36);

    return response.data.posts;
  }
}
