
import 'package:intl/intl.dart';
import 'package:project_bike_car/app/constants/color/color.dart';
import 'package:project_bike_car/app/constants/style/style.dart';
import 'package:project_bike_car/app/constants/value/value.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:project_bike_car/model/entity/category_new.dart';
import 'package:project_bike_car/model/entity/posts_news.dart';
import 'package:date_format/date_format.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_cached_image.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_spacer.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:project_bike_car/presentation/screen/menu/news/new_detaill/sc_new_detail.dart';

class ListNewsBike extends StatefulWidget{
  final int id;
  final CategoryNew categorys;
  const ListNewsBike({Key key, this.id,this.categorys}) : super(key: key);

  @override
  _ListNewsBikeState createState() => _ListNewsBikeState();
}

class _ListNewsBikeState extends State<ListNewsBike> {
  var dateTimeNow = formatDate(DateTime.now(), [dd, '/', mm, '/', yyyy, ' ', HH, ':', nn]);

  @override
  Widget build(BuildContext context) {
    DateTime tempDate = new DateFormat("yyyy-MM-dd hh:mm:ss").parse(widget.categorys.created);
    dateTimeNow = formatDate(tempDate, [dd, '-', mm, '-', yyyy, ' | ', HH, ':', nn]);

    return GestureDetector(
      onTap: (){
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => NewDetailPageScreen(id:widget.categorys.id)),
        );
      },
      child: Container(
        height: 95,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            AspectRatio(
              aspectRatio: AppValue.NEWS_IMAGE_RATIO,
              child: WidgetCachedImage(
                url: "${widget.categorys.image}",
              ),
            ),
            const SizedBox(width: 4,),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 4),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      "${widget.categorys.name}",
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                      style: AppStyle.DEFAULT_SMALL
                          .copyWith(color: AppColor.PRIMARY),
                    ),
                    WidgetSpacer(
                      height: 3,
                    ),
                    // Expanded(
                    //   child: HtmlWidget(
                    //     widget.categorys.content.length <= 70
                    //         ? widget.post.content
                    //         : widget.post.content.substring(0, 70) + '...',
                    //
                    //
                    //     // customStylesBuilder: (element) {
                    //     //   if (element.classes.contains('name')) {
                    //     //     return {'color': 'red'};
                    //     //   }
                    //     //   return null;
                    //     // },
                    //   ),
                    //   // child: Container(
                    //   //     child: Text(
                    //   //       widget.post.content.length <= 70
                    //   //           ? widget.post.content
                    //   //           : widget.post.content.substring(0, 70) + '...',
                    //   //       style: AppStyle.DEFAULT_SMALL
                    //   //           .copyWith(color: AppColor.GREY),
                    //   //       textAlign: TextAlign.start,
                    //   //     )),
                    // ),
                    WidgetSpacer(
                      height: 3,
                    ),
                    Text(
                      "$dateTimeNow",
                      style: AppStyle.DEFAULT_SMALL.copyWith(
                          color: AppColor.GREY),
                      textAlign: TextAlign.start,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );

  }
}