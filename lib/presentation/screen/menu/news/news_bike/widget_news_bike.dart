
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/model/entity/category_new.dart';
import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_circle_progress.dart';
import 'package:project_bike_car/presentation/screen/menu/news/news_bike/bloc/new_bike_bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/news/news_bike/bloc/new_bike_state.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:project_bike_car/presentation/screen/menu/news/news_locate/list_news_locate.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'bloc/bloc.dart';

class WidgetNewsBike extends StatefulWidget {
  final Function(CategoryNew categorys, int) onCategoryClick;
  const WidgetNewsBike({
    Key key,
    this.onCategoryClick,
  }) : super(key: key);
  @override
  _WidgetNewsBikeState createState() => _WidgetNewsBikeState();
}

class _WidgetNewsBikeState extends State<WidgetNewsBike>
    with AutomaticKeepAliveClientMixin<WidgetNewsBike> {
  RefreshController _refreshController =
  RefreshController(initialRefresh: false);

  @override
  void initState() {
    super.initState();
    BlocProvider.of<NewBikeBloc>(context).add(LoadNewBike());
  }

  _onRefresh() async {
    BlocProvider.of<NewBikeBloc>(context).add(RefreshNewBike());
  }

  _onLoadMore() async {
    BlocProvider.of<NewBikeBloc>(context).add(LoadNewBike());
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocListener<NewBikeBloc, NewBikeState>(
      listener: (context, state) {
        if (state is NewBikeLoaded) {
        }
      },
      child: BlocBuilder<NewBikeBloc, NewBikeState>(
        builder: (context, state) {
          return Container(
            child: _buildContent(state),
          );
        },
      ),
    );
  }

  Widget _buildContent(NewBikeState state) {
    if (state is NewBikeLoaded) {
      return _buildContents(state);
    } else if (state is NewBikeLoading) {
      return Container(
        width: double.infinity,
        height: AppValue.PRODUCT_HORIZONTAL_HEIGHT,
        child: Center(
          child: WidgetCircleProgress(),
        ),
      );
    } else if (state is NewBikeNotLoaded) {
      return WidgetScreenError(
        status: state.status,
      );
    } else {
      return Center(
        child: Text('Unknown state'),
      );
    }
  }

  _buildContents(NewBikeLoaded state){
    return state.invoices.length != 0 ?
    Container(
        child: ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: state.invoices.length,
            itemBuilder: (context, index) {
              return Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height / 5,
                child: Card(
                    elevation: 4,
                    shape:
                    RoundedRectangleBorder(borderRadius: BorderRadius.circular(4.0)),
                    child: ListNewsLocate(post:state.invoices[index])),
              );
            }))
        : Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
              height: 120,
              width: 120,
              child: Image.asset('assets/images/box.png')
          ),
          SizedBox(height: 20,),
          Text('Bạn chưa thông báo nào!'),
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
