
import 'package:equatable/equatable.dart';
import 'package:project_bike_car/model/entity/posts.dart';
import 'package:project_bike_car/model/entity/posts_news.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

abstract class NewCarsState extends Equatable {
  const NewCarsState();

  @override
  List<Object> get props => [];
}

class NewCarsLoading extends NewCarsState {}

class NewCarsLoaded extends NewCarsState {
  final List<PostsNews> invoices;

  const NewCarsLoaded({this.invoices});

  NewCarsLoaded copyWith({
    List<PostsNews> invoices,
  }) {
    return NewCarsLoaded(
      invoices: invoices ?? this.invoices,
    );
  }

  @override
  List<Object> get props => [invoices,];

  @override
  String toString() {
    return 'NewCarsLoaded{invoices: $invoices,}';
  }
}

class NewCarsNotLoaded extends NewCarsState {
  final DioStatus status;

  NewCarsNotLoaded(this.status);

  @override
  String toString() {
    return 'NewCarsNotLoaded{error: $status}';
  }
}
