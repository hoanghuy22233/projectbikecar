import 'package:equatable/equatable.dart';

class NewCarsEvent extends Equatable {
  const NewCarsEvent();

  List<Object> get props => [];
}

class LoadNewCars extends NewCarsEvent {
}

class RefreshNewCars extends NewCarsEvent {}
