import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:project_bike_car/model/entity/posts_news.dart';
import 'package:project_bike_car/model/repo/barrel_repo.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

import 'bloc.dart';

class NewCarsBloc
    extends Bloc<NewCarsEvent, NewCarsState> {
  final UserRepository invoiceRepository;

  NewCarsBloc({@required this.invoiceRepository});

  @override
  NewCarsState get initialState => NewCarsLoading();

  @override
  Stream<NewCarsState> mapEventToState(
      NewCarsEvent event) async* {
    if (event is LoadNewCars) {
      yield* _mapLoadNewCarsToState();
    } else if (event is RefreshNewCars) {
      yield NewCarsLoading();
      yield* _mapLoadNewCarsToState();
    }
  }

  Stream<NewCarsState> _mapLoadNewCarsToState() async* {
    final currentState = state;
    try {
      if (state is NewCarsLoading) {
        final invoices = await _fetchProduct();
        yield NewCarsLoaded(invoices: invoices);
      }
      if (currentState is NewCarsLoaded) {
        final invoices = await _fetchProduct();
      }
    } catch (e) {
      yield NewCarsNotLoaded(DioErrorUtil.handleError(e));
    }
  }

  Future<List<PostsNews>> _fetchProduct() async {
    final response = await invoiceRepository.getNew(37);

    return response.data.posts;
  }
}
