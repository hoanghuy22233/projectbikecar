
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:project_bike_car/presentation/screen/menu/news/news_locate/list_news_locate.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'bloc/bloc.dart';

class WidgetNewCars extends StatefulWidget {
  @override
  _WidgetNewCarsState createState() => _WidgetNewCarsState();
}

class _WidgetNewCarsState extends State<WidgetNewCars>
    with AutomaticKeepAliveClientMixin<WidgetNewCars> {
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  @override
  void initState() {
    super.initState();
    BlocProvider.of<NewCarsBloc>(context).add(LoadNewCars());
  }

  _onRefresh() async {
    BlocProvider.of<NewCarsBloc>(context).add(RefreshNewCars());
  }

  _onLoadMore() async {
    BlocProvider.of<NewCarsBloc>(context).add(LoadNewCars());
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocListener<NewCarsBloc, NewCarsState>(
      listener: (context, state) {
        if (state is NewCarsLoaded) {
        }
      },
      child: BlocBuilder<NewCarsBloc, NewCarsState>(
        builder: (context, state) {
          return Container(
            child: _buildContent(state),
          );
        },
      ),
    );
  }

  Widget _buildContent(NewCarsState state) {
    if (state is NewCarsLoaded) {
      return _buildContents(state);
    } else if (state is NewCarsLoading) {
      return Container(
        width: double.infinity,
        height: AppValue.PRODUCT_HORIZONTAL_HEIGHT,
        child: Center(child: Container(
          height: 100,
          width: 100,
          child: Lottie.asset(
            'assets/lottie/load_car.json',
          ),
        )),
      );
    } else if (state is NewCarsNotLoaded) {
      return WidgetScreenError(
        status: state.status,
      );
    } else {
      return Center(
        child: Text('Unknown state'),
      );
    }
  }
  _buildContents(NewCarsLoaded state){
    return state.invoices.length != 0 ?
    Container(
        child: ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: state.invoices.length,
            itemBuilder: (context, index) {
              return Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height / 5,
                child: Card(
                    elevation: 4,
                    shape:
                    RoundedRectangleBorder(borderRadius: BorderRadius.circular(4.0)),
                    child: ListNewsLocate(post:state.invoices[index])),
              );
            }))
        : Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
              height: 120,
              width: 120,
              child: Image.asset('assets/images/box.png')
          ),
          SizedBox(height: 20,),
          Text('Bạn chưa tin tức nào!'),
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
