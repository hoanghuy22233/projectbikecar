import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/model/repo/barrel_repo.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_screen_error.dart';
import 'package:project_bike_car/presentation/screen/all_product/sc_product.dart';
import 'package:project_bike_car/presentation/screen/menu/home/all_brands/bloc/bloc.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_appbar_home.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_banner/barrel_home_categories_new.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_banner/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_category/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_new/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/bike/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/bike/widget_home_product_bike.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/car/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/car/item_car/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/electronic_device/product_item_electronic/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/electronic_device/widget_home_product_etronic.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/gps/item_gps/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/gps/widget_home_product_gps.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/phone/widget_home_product_phone.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/sim/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/sim/item_sim/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/sim/widget_home_product_sim.dart';
import 'package:project_bike_car/utils/handler/barrel_handler.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

import 'all_brands/bloc/brand_bloc.dart';
import 'all_brands/brands/bloc/bloc.dart';
import 'all_brands/widget_home_brands.dart';
import 'bloc/bloc.dart';
import 'home_category/widget_home_category.dart';
import 'home_new/widget_home_post.dart';
import 'home_product/bike/product_item_show/bloc/bloc.dart';
import 'home_product/car/widget_home_product_car.dart';
import 'home_product/electronic_device/bloc/bloc.dart';
import 'home_product/gps/bloc/bloc.dart';
import 'home_product/phone/bloc/bloc.dart';
import 'home_product/phone/item_phone/bloc/bloc.dart';
import 'home_product/product/bloc/bloc.dart';
import 'home_product/widget_home_product.dart';
import 'home_product/widget_home_product_all.dart';

class HomePageScreen extends StatefulWidget {
  final GlobalKey<ScaffoldState> drawer;
  final Function moveTab;
  int categoryId;
  int brandId;
  String sort;
  HomePageScreen({this.drawer, this.moveTab, this.categoryId, this.brandId});

  @override
  _HomePageScreenState createState() => _HomePageScreenState();
}

class _HomePageScreenState extends State<HomePageScreen>
    with AutomaticKeepAliveClientMixin<HomePageScreen> {
  @override
  PanelController _panelController = new PanelController();
  ScrollController _scrollController = new ScrollController();
  bool scrollDown = true;

  _onArgument() {
    Future.delayed(Duration.zero, () async {
      _scrollController.addListener(() {
        if (_scrollController.offset >=
                _scrollController.position.maxScrollExtent - 200 &&
            scrollDown == true)
          setState(() {
            scrollDown = false;
          });
        else if (_scrollController.offset <= 200 && scrollDown == false)
          setState(() {
            scrollDown = true;
          });
      });
    });
  }

  @override
  void initState() {
    super.initState();
    BlocProvider.of<HomeBloc>(context).add(
        LoadHome(widget?.categoryId ?? 0, widget?.brandId ?? 0, 'id_desc'));
    BlocProvider.of<BrandBloc>(context).add(
        LoadBrand(widget?.categoryId ?? 0, widget?.brandId ?? 0, 'id_desc'));
    BlocProvider.of<ProductBloc>(context).add(
        LoadProduct(widget?.categoryId ?? 0, widget?.brandId ?? 0, 'id_desc'));
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Colors.transparent));
    _onArgument();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) =>
              HomeCategoryBloc(homeBloc: BlocProvider.of<HomeBloc>(context)),
        ),
        BlocProvider(
          create: (context) =>
              HomeBannerBloc(homeBloc: BlocProvider.of<HomeBloc>(context)),
        ),
        BlocProvider(
          create: (context) =>
              HomeBrandBloc(brandBloc: BlocProvider.of<BrandBloc>(context)),
        ),
        BlocProvider(
          create: (context) => HomeProductBloc(
              productBloc: BlocProvider.of<ProductBloc>(context)),
        ),
        BlocProvider(
          create: (context) => HomeProductShowBloc(
              productBloc: BlocProvider.of<ProductShowBloc>(context)),
        ),
        BlocProvider(
          create: (context) => HomeProductCarBloc(
              productBloc: BlocProvider.of<ProductCarBloc>(context)),
        ),
        BlocProvider(
          create: (context) => HomeProductPhoneBloc(
              productBloc: BlocProvider.of<ProductPhoneBloc>(context)),
        ),
        BlocProvider(
          create: (context) => HomeProductSimBloc(
              productBloc: BlocProvider.of<ProductSimBloc>(context)),
        ),
        BlocProvider(
          create: (context) => HomeProductGpsBloc(
              productBloc: BlocProvider.of<ProductGpsBloc>(context)),
        ),
        BlocProvider(
          create: (context) => HomeProductEtronicBloc(
              productBloc: BlocProvider.of<ProductElectronicBloc>(context)),
        ),
        BlocProvider(
          create: (context) =>
              HomeNewBloc(homeBloc: BlocProvider.of<HomeBloc>(context)),
        ),
      ],
      child: BlocBuilder<HomeBloc, HomeState>(builder: (context, state) {
        return SafeArea(
          top: false,
          child: Scaffold(
              body: OrientationBuilder(builder: (context, orientation) {
            if (MediaQuery.of(context).size.width > 600) {
              return Container(child: _buildContent(state));
            } else {
              return Container(child: _buildContents(state));
            }
          })),
        );
      }),
    );
  }

  Widget _buildHomeContent(HomeLoaded state) {
    return Container(
      color: Colors.white,
      child: Stack(
        children: <Widget>[
          NestedScrollView(
              controller: _scrollController,
              headerSliverBuilder:
                  (BuildContext context, bool innerBoxIsScrolled) {
                return <Widget>[
                  SliverAppBar(
                    expandedHeight: MediaQuery.of(context).size.width * 0.8,
                    floating: false,
                     pinned: true,
                    flexibleSpace: FlexibleSpaceBar(
                      centerTitle: true,
                      background: Padding(
                        padding: EdgeInsets.only(top: 100),
                        child: _buildBanners(),
                      ),
                    ),
                  ),
                ];
              },
              body: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.only(top: 20),
                      child: WidgetHomeCategoriesItemMenu(
                        categoryId: widget.categoryId,
                        brandId: widget.brandId,
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: 10,
                      color: Colors.grey[200],
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 20, top: 10, right: 20),
                      child: Row(
                        children: [
                          Expanded(
                              flex: 6,
                              child: Text(
                                "Thương hiệu phổ biến",
                                style: TextStyle(
                                    fontSize: 14, fontWeight: FontWeight.bold),
                              )),
                          Expanded(flex: 1, child: Container()),
                          SizedBox(
                            width: 25,
                          ),
                          GestureDetector(
                            onTap: () {
                              AppNavigator.navigateAllBrand();
                            },
                            child: Text(
                              "Xem tất cả",
                              style: TextStyle(
                                  color: Color(0xff66CC99),
                                  fontSize: 13,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    ),
                    BlocBuilder<BrandBloc, BrandState>(builder: (context, state) {
                      return Container(
                        child: _buildBrandContent(state),
                      );
                    }),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: 10,
                      color: Colors.grey[300],
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 20, top: 10, right: 20),
                      child: Row(
                        children: [
                          Expanded(
                              flex: 4,
                              child: Text(
                                "Sản phẩm mới nhất",
                                style: TextStyle(
                                    fontSize: 14, fontWeight: FontWeight.bold),
                              )),
                          // Expanded(flex: 1,child: Container() ),
                          SizedBox(
                            width: 35,
                          ),
                          GestureDetector(
                            onTap: () {
                              AppNavigator.navigateAllProduct();
                            },
                            child: Text(
                              "Xem thêm",
                              style: TextStyle(
                                  color: Color(0xff66CC99),
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    ),
                    BlocBuilder<ProductBloc, ProductState>(
                        builder: (context, state) {
                      return Container(
                        child: _buildProductContent(state),
                      );
                    }),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: 10,
                      color: Colors.grey[300],
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 20, top: 10, right: 20),
                      child: Row(
                        children: [
                          Expanded(
                              flex: 7,
                              child: Text(
                                "Tin mới nhất ",
                                style: TextStyle(
                                    fontSize: 14, fontWeight: FontWeight.bold),
                              )),
                          Expanded(flex: 1, child: Container()),
                          SizedBox(
                            width: 25,
                          ),
                          GestureDetector(
                            onTap: () => widget.moveTab(page: 2),
                            child: Text(
                              "Xem thêm",
                              style: TextStyle(
                                  color: Color(0xff66CC99),
                                  fontSize: 13,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: WidgetHomePost(),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: 10,
                      color: Colors.grey[300],
                    ),
                    // widget.categoryId
                    //     ? BlocBuilder<ProductBloc, ProductState>(
                    //         builder: (context, state) {
                    //         return Column(
                    //           children: [
                    //             Padding(
                    //               padding: const EdgeInsets.only(
                    //                   left: 20, top: 10, right: 20),
                    //               child: Row(
                    //                 children: [
                    //                   Expanded(
                    //                       flex: 4,
                    //                       child: Text(
                    //                         "Tất cả sản phẩm",
                    //                         style: TextStyle(
                    //                             fontSize: 14,
                    //                             fontWeight: FontWeight.bold),
                    //                       )),
                    //                   // Expanded(flex: 1,child: Container() ),
                    //                   SizedBox(
                    //                     width: 35,
                    //                   ),
                    //                   GestureDetector(
                    //                     onTap: () {
                    //                       AppNavigator.navigateAllProduct();
                    //                     },
                    //                     child: Text(
                    //                       "Xem thêm",
                    //                       style: TextStyle(
                    //                           color: Color(0xff66CC99),
                    //                           fontSize: 14,
                    //                           fontWeight: FontWeight.bold),
                    //                     ),
                    //                   ),
                    //                 ],
                    //               ),
                    //             ),
                    //             Container(
                    //               child: _buildProductAllContent(state),
                    //             )
                    //           ],
                    //         );
                    //       })
                    //     :
                    Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 20, top: 10, right: 20, bottom: 10),
                                child: Row(
                                  children: [
                                    Expanded(
                                        flex: 4,
                                        child: Text(
                                          "Tất cả xe máy",
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold),
                                        )),
                                    // Expanded(flex: 1,child: Container() ),
                                    SizedBox(
                                      width: 35,
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => ProductPageScreen(
                                                categoryId:41,

                                              )),
                                        );
                                      },
                                      child: Text(
                                        "Xem thêm",
                                        style: TextStyle(
                                            color: Color(0xff66CC99),
                                            fontSize: 14,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              _buildProductBike(),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 20, top: 10, right: 20, bottom: 10),
                                child: Row(
                                  children: [
                                    Expanded(
                                        flex: 4,
                                        child: Text(
                                          "Tất cả xe Ô tô",
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold),
                                        )),
                                    // Expanded(flex: 1,child: Container() ),
                                    SizedBox(
                                      width: 35,
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => ProductPageScreen(
                                                categoryId:42,

                                              )),
                                        );
                                      },
                                      child: Text(
                                        "Xem thêm",
                                        style: TextStyle(
                                            color: Color(0xff66CC99),
                                            fontSize: 14,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              _buildProductCar(),
                              ///
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 20, top: 10, right: 20, bottom: 10),
                                child: Row(
                                  children: [
                                    Expanded(
                                        flex: 4,
                                        child: Text(
                                          "Tất cả Sim thẻ",
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold),
                                        )),
                                    // Expanded(flex: 1,child: Container() ),
                                    SizedBox(
                                      width: 35,
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => ProductPageScreen(
                                                categoryId:43,

                                              )),
                                        );
                                      },
                                      child: Text(
                                        "Xem thêm",
                                        style: TextStyle(
                                            color: Color(0xff66CC99),
                                            fontSize: 14,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              _buildProductSim(),
                              ///
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 20, top: 10, right: 20, bottom: 10),
                                child: Row(
                                  children: [
                                    Expanded(
                                        flex: 4,
                                        child: Text(
                                          "Tất cả Điện thoại",
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold),
                                        )),
                                    // Expanded(flex: 1,child: Container() ),
                                    SizedBox(
                                      width: 35,
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => ProductPageScreen(
                                                categoryId:44,

                                              )),
                                        );
                                      },
                                      child: Text(
                                        "Xem thêm",
                                        style: TextStyle(
                                            color: Color(0xff66CC99),
                                            fontSize: 14,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              _buildProductPhone(),
                              ///
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 20, top: 10, right: 20, bottom: 10),
                                child: Row(
                                  children: [
                                    Expanded(
                                        flex: 4,
                                        child: Text(
                                          "Tất cả Định vị",
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold),
                                        )),
                                    // Expanded(flex: 1,child: Container() ),
                                    SizedBox(
                                      width: 35,
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => ProductPageScreen(
                                                categoryId:45,

                                              )),
                                        );
                                      },
                                      child: Text(
                                        "Xem thêm",
                                        style: TextStyle(
                                            color: Color(0xff66CC99),
                                            fontSize: 14,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              _buildProductGps(),

                              ///
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 20, top: 10, right: 20, bottom: 10),
                                child: Row(
                                  children: [
                                    Expanded(
                                        flex: 4,
                                        child: Text(
                                          "Tất cả thiết bị điện tử",
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold),
                                        )),
                                    // Expanded(flex: 1,child: Container() ),
                                    SizedBox(
                                      width: 35,
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => ProductPageScreen(
                                                categoryId:46,

                                              )),
                                        );
                                      },
                                      child: Text(
                                        "Xem thêm",
                                        style: TextStyle(
                                            color: Color(0xff66CC99),
                                            fontSize: 14,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              _buildProductEtronic(),

                            ],
                          ),
                  ],
                ),
              )),
          Padding(
            padding: EdgeInsets.only(top: 20),
            child: _buildAppbar(),
          ),
        ],
      ),
    );
  }

  Widget _buildHomeContents(HomeLoaded state) {
    return Container(
      color: Colors.white,
      child: Stack(
        children: <Widget>[
          NestedScrollView(
              controller: _scrollController,
              headerSliverBuilder:
                  (BuildContext context, bool innerBoxIsScrolled) {
                return <Widget>[
                  SliverAppBar(
                    expandedHeight: MediaQuery.of(context).size.width * 0.8,
                    floating: false,
                    pinned: true,
                    flexibleSpace: FlexibleSpaceBar(
                      centerTitle: true,
                      background: Padding(
                        padding: EdgeInsets.only(top: 100),
                        child: _buildBanners(),
                      ),
                    ),
                  ),
                ];
              },
              body: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.only(top: 20),
                      child: WidgetHomeCategoriesItemMenu(
                        categoryId: widget.categoryId,
                        brandId: widget.brandId,
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: 10,
                      color: Colors.grey[200],
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 20, top: 10, right: 20),
                      child: Row(
                        children: [
                          Expanded(
                              flex: 6,
                              child: Text(
                                "Thương hiệu phổ biến",
                                style: TextStyle(
                                    fontSize: 14, fontWeight: FontWeight.bold),
                              )),
                          Expanded(flex: 1, child: Container()),
                          SizedBox(
                            width: 25,
                          ),
                          GestureDetector(
                            onTap: () {
                              AppNavigator.navigateAllBrand();
                            },
                            child: Text(
                              "Xem tất cả",
                              style: TextStyle(
                                  color: Color(0xff66CC99),
                                  fontSize: 13,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    ),
                    BlocBuilder<BrandBloc, BrandState>(builder: (context, state) {
                      return Container(
                        child: _buildBrandContent(state),
                      );
                    }),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: 10,
                      color: Colors.grey[300],
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 20, top: 10, right: 20),
                      child: Row(
                        children: [
                          Expanded(
                              flex: 4,
                              child: Text(
                                "Sản phẩm mới nhất",
                                style: TextStyle(
                                    fontSize: 14, fontWeight: FontWeight.bold),
                              )),
                          // Expanded(flex: 1,child: Container() ),
                          SizedBox(
                            width: 35,
                          ),
                          GestureDetector(
                            onTap: () {
                              AppNavigator.navigateAllProduct();
                            },
                            child: Text(
                              "Xem thêm",
                              style: TextStyle(
                                  color: Color(0xff66CC99),
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    ),
                    BlocBuilder<ProductBloc, ProductState>(
                        builder: (context, state) {
                      return Container(
                        child: _buildProductContent(state),
                      );
                    }),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: 10,
                      color: Colors.grey[300],
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 20, top: 10, right: 20),
                      child: Row(
                        children: [
                          Expanded(
                              flex: 7,
                              child: Text(
                                "Tin mới nhất ",
                                style: TextStyle(
                                    fontSize: 14, fontWeight: FontWeight.bold),
                              )),
                          Expanded(flex: 1, child: Container()),
                          SizedBox(
                            width: 25,
                          ),
                          GestureDetector(
                            onTap: () => widget.moveTab(page: 2),
                            child: Text(
                              "Xem thêm",
                              style: TextStyle(
                                  color: Color(0xff66CC99),
                                  fontSize: 13,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: WidgetHomePost(),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: 10,
                      color: Colors.grey[300],
                    ),
                    Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 20, top: 10, right: 20, bottom: 10),
                          child: Row(
                            children: [
                              Expanded(
                                  flex: 4,
                                  child: Text(
                                    "Tất cả xe máy",
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold),
                                  )),
                              // Expanded(flex: 1,child: Container() ),
                              SizedBox(
                                width: 35,
                              ),
                              GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => ProductPageScreen(
                                          categoryId:41,

                                        )),
                                  );
                                },
                                child: Text(
                                  "Xem thêm",
                                  style: TextStyle(
                                      color: Color(0xff66CC99),
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                        ),
                        _buildProductBike(),
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 20, top: 10, right: 20, bottom: 10),
                          child: Row(
                            children: [
                              Expanded(
                                  flex: 4,
                                  child: Text(
                                    "Tất cả xe Ô tô",
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold),
                                  )),
                              // Expanded(flex: 1,child: Container() ),
                              SizedBox(
                                width: 35,
                              ),
                              GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => ProductPageScreen(
                                          categoryId:42,

                                        )),
                                  );
                                },
                                child: Text(
                                  "Xem thêm",
                                  style: TextStyle(
                                      color: Color(0xff66CC99),
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                        ),
                        _buildProductCar(),
                        ///
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 20, top: 10, right: 20, bottom: 10),
                          child: Row(
                            children: [
                              Expanded(
                                  flex: 4,
                                  child: Text(
                                    "Tất cả Sim thẻ",
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold),
                                  )),
                              // Expanded(flex: 1,child: Container() ),
                              SizedBox(
                                width: 35,
                              ),
                              GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => ProductPageScreen(
                                          categoryId:43,

                                        )),
                                  );
                                },
                                child: Text(
                                  "Xem thêm",
                                  style: TextStyle(
                                      color: Color(0xff66CC99),
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                        ),
                        _buildProductSim(),
                        ///
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 20, top: 10, right: 20, bottom: 10),
                          child: Row(
                            children: [
                              Expanded(
                                  flex: 4,
                                  child: Text(
                                    "Tất cả Điện thoại",
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold),
                                  )),
                              // Expanded(flex: 1,child: Container() ),
                              SizedBox(
                                width: 35,
                              ),
                              GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => ProductPageScreen(
                                          categoryId:44,

                                        )),
                                  );
                                },
                                child: Text(
                                  "Xem thêm",
                                  style: TextStyle(
                                      color: Color(0xff66CC99),
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                        ),
                        _buildProductPhone(),
                        ///
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 20, top: 10, right: 20, bottom: 10),
                          child: Row(
                            children: [
                              Expanded(
                                  flex: 4,
                                  child: Text(
                                    "Tất cả Định vị",
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold),
                                  )),
                              // Expanded(flex: 1,child: Container() ),
                              SizedBox(
                                width: 35,
                              ),
                              GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => ProductPageScreen(
                                          categoryId:45,

                                        )),
                                  );
                                },
                                child: Text(
                                  "Xem thêm",
                                  style: TextStyle(
                                      color: Color(0xff66CC99),
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                        ),
                        _buildProductGps(),
                        ///
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 20, top: 10, right: 20, bottom: 10),
                          child: Row(
                            children: [
                              Expanded(
                                  flex: 4,
                                  child: Text(
                                    "Tất cả thiết bị điện tử",
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold),
                                  )),
                              // Expanded(flex: 1,child: Container() ),
                              SizedBox(
                                width: 35,
                              ),
                              GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => ProductPageScreen(
                                          categoryId:46,

                                        )),
                                  );
                                },
                                child: Text(
                                  "Xem thêm",
                                  style: TextStyle(
                                      color: Color(0xff66CC99),
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                        ),
                        _buildProductEtronic(),

                      ],
                    ),
                  ],
                ),
              )),
          Padding(
            padding: EdgeInsets.only(top: 30),
            child: _buildAppbar(),
          ),
        ],
      ),
    );
  }

  ///
  _buildContent(HomeState state) {
    if (state is HomeLoaded) {
      return Stack(
        children: [_buildHomeContent(state), _buildScrollButton()],
      );
    } else if (state is HomeLoading) {
      return Center(
          child: Container(
        height: 100,
        width: 100,
        child: Lottie.asset(
          'assets/lottie/load_car.json',
        ),
      ));
    } else if (state is HomeNotLoaded) {
      return RefreshIndicator(
        onRefresh: () async {
          BlocProvider.of<HomeBloc>(context).add(RefreshHome(
              widget?.categoryId ?? 0, widget?.brandId ?? 0, 'id_desc'));
          await Future.delayed(Duration(seconds: 1));
          return true;
        },
        color: AppColor.PRIMARY_COLOR,
        backgroundColor: AppColor.WHITE,
        child: WidgetScreenError(
          status: state.status,
        ),
      );
    } else {
      return Center(
        child: Text('Unknown state'),
      );
    }
  }

  _buildContents(HomeState state) {
    if (state is HomeLoaded) {
      return Stack(
        children: [_buildHomeContents(state), _buildScrollButton()],
      );
    } else if (state is HomeLoading) {
      return Center(
          child: Container(
        height: 100,
        width: 100,
        child: Lottie.asset(
          'assets/lottie/load_car.json',
        ),
      ));
    } else if (state is HomeNotLoaded) {
      return RefreshIndicator(
        onRefresh: () async {
          BlocProvider.of<HomeBloc>(context).add(RefreshHome(
              widget?.categoryId ?? 0, widget?.brandId ?? 0, 'id_desc'));
          await Future.delayed(Duration(seconds: 1));
          return true;
        },
        color: AppColor.PRIMARY_COLOR,
        backgroundColor: Color(0xff33CC66),
        child: WidgetScreenError(
          status: state.status,
        ),
      );
    } else {
      return Center(
        child: Text('Unknown state'),
      );
    }
  }

  _buildBrandContent(BrandState state) {
    if (state is BrandLoaded) {
      return _buildBrandLoaded(state);
    } else if (state is BrandLoading) {
      return Center(
          child: Container(
        height: 100,
        width: 100,
        child: Lottie.asset(
          'assets/lottie/load_car.json',
        ),
      ));
    } else if (state is BrandNotLoaded) {
      return WidgetScreenError(
        status: state.status,
      );
    } else {
      return Center(
        child: Text('Unknown state'),
      );
    }
  }

  _buildBrandLoaded(BrandLoaded state) {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: WidgetHomeBrandMenu(
        categoryId: widget.categoryId,
        brandId: widget.brandId,
      ),
    );
  }

  _buildProductContent(ProductState state) {
    if (state is ProductLoaded) {
      return _buildProductLoaded(state);
    } else if (state is ProductLoading) {
      return Center(
          child: Container(
        height: 100,
        width: 100,
        child: Lottie.asset(
          'assets/lottie/load_car.json',
        ),
      ));
    } else if (state is ProductNotLoaded) {
      return WidgetScreenError(
        status: state.status,
      );
    } else {
      return Center(
        child: Text('Unknown state'),
      );
    }
  }

  _buildProductLoaded(ProductLoaded state) {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: WidgetHomeProductItemMenu(),
    );
  }
  _buildAppbar() => WidgetAppbarHome(
        backgroundColor: Color(0xff33CC66),
        left: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: GestureDetector(
              onTap: () {
                AppNavigator.navigateSearch();
              },
              child: Container(
                width: MediaQuery.of(context).size.width - 80,
                decoration: BoxDecoration(
                  color: Color(0xff66CC99),
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                ),
                height: 50,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  // crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(flex: 1, child: Container()),
                    Image.asset(
                      "assets/images/img_search.png",
                      color: Colors.white,
                      width: 15,
                      height: 15,
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Expanded(
                      flex: 8,
                      child: TextField(
                        style: TextStyle(color: Colors.white),
                        // keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                            enabled: false,
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            errorBorder: InputBorder.none,
                            disabledBorder: InputBorder.none,
                            border: InputBorder.none,
                            hintText: 'Nhập giá và sản phẩm tìm kiếm',
                            hintStyle: TextStyle(color: Colors.white)),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
        right: [
          Hero(
            tag: 'search',
            child: GestureDetector(
              onTap: () {
                WithAuth.isAuth(ifNotAuth: () {
                  AppNavigator.navigateLogin();
                }, ifAuth: () {
                  AppNavigator.navigateSearchProduct();
                });
              },
              child: Container(
                padding: EdgeInsets.only(right: 20),
                child: Image.asset(
                  "assets/images/compliant.png",
                  width: 25,
                  height: 25,
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ],
      );

  _buildBanners() => WidgetHomeCategoriesBanners();

  _buildProductBike() => WidgetHomeProductBike(
        categoryId: 41,
      );

  _buildProductCar() => WidgetHomeProductCar(
    categoryId: 42,
  );
  _buildProductSim() => WidgetHomeProductSim(
    categoryId: 43,
  );
  _buildProductPhone() => WidgetHomeProductPhone(
    categoryId: 44,
  );
  _buildProductGps() => WidgetHomeProductGps(
    categoryId: 45,
  );

  _buildProductEtronic() => WidgetHomeProductEtronic(
    categoryId: 46,
  );



  _buildScrollButton() {
    return Positioned(
      bottom: 160,
      right: 24,
      child: Container(
        child: FloatingActionButton(
          onPressed: () {
            if (scrollDown &&
                _scrollController.offset <
                    _scrollController.position.maxScrollExtent - 200) {
              _scrollController.animateTo(_scrollController.offset + Get.width,
                  duration: Duration(milliseconds: 300), curve: Curves.ease);
            } else {
              _scrollController.animateTo(0,
                  duration: Duration(milliseconds: 500), curve: Curves.ease);
              setState(() {
                scrollDown = true;
              });
            }
          },
          child: Icon(scrollDown ? Icons.arrow_downward : Icons.arrow_upward,color: Colors.white,),
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
