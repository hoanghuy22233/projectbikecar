import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/app/constants/value/value.dart';
import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_banner_image.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_banner/widget_categories_banner.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'bloc/home_banner_bloc.dart';
import 'bloc/home_banner_event.dart';
import 'bloc/home_banner_state.dart';

class WidgetHomeCategoriesBanners extends StatefulWidget {
  @override
  _WidgetHomeCategoriesBannersState createState() =>
      _WidgetHomeCategoriesBannersState();
}

class _WidgetHomeCategoriesBannersState
    extends State<WidgetHomeCategoriesBanners> {
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBannerBloc, HomeBannerState>(
      builder: (context, state) {
        if (state is HomeBannerLoaded) {
          return GestureDetector(
           // onTap: widget.action,
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height*0.8,
              child: _buildSlider(state),
            ),
          );
        } else {
          return Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height*0.8,
            child: Center(child: WidgetCircleProgress()),
          );
        }
      },
    );
  }

  Widget _buildSlider(HomeBannerLoaded state) {
    return WidgetBannerWrapper(
      child: Swiper(
          loop: true,
          autoplay: true,
          curve: Curves.ease,
          itemWidth: Get.width,
          itemHeight: Get.height,
          itemCount: state.banners.length,
          itemBuilder: (context, index) {
            return WidgetBannerImage(
              banner: state.banners[index],
            );
          },
          pagination: new SwiperPagination(
              builder: DotSwiperPaginationBuilder(
                  size: 5,
                  activeSize: 10,
                  color: AppColor.BANNER_COLOR,
                  activeColor: AppColor.BANNER_SELECTED_COLOR))),
    );
  }
}
