import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:project_bike_car/model/entity/banner.dart';

abstract class HomeBannerState extends Equatable {
  const HomeBannerState();

  @override
  List<Object> get props => [];
}

class HomeBannerNotLoaded extends HomeBannerState {}

class HomeBannerLoaded extends HomeBannerState {
  final List<Banners> banners;

  HomeBannerLoaded({@required this.banners});

  @override
  List<Object> get props => [banners];
}
