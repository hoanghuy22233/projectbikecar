import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/presentation/screen/menu/home/bloc/bloc.dart';

import 'bloc.dart';

class HomeBannerBloc extends Bloc<HomeBannerEvent, HomeBannerState>{
  final HomeBloc homeBloc;
  StreamSubscription subscription;

  HomeBannerBloc({@required this.homeBloc}) {
    subscription = homeBloc.listen((state) {
      if (state is HomeLoaded) {
        add(DisplayHomeBanner(banners: state.homeResponse.data.listBanner));
      }
    });
  }

  @override
  HomeBannerState get initialState => HomeBannerNotLoaded();

  @override
  Stream<HomeBannerState> mapEventToState(HomeBannerEvent event) async* {
    if(event is DisplayHomeBanner){
       List<Banners> banners = List();
      for(int i=0;i<event.banners.length;i++){
        banners .add(event.banners[i]);
      }
      yield* _mapDisplayHomeBannerToState(banners);
    }
  }

  Stream<HomeBannerState> _mapDisplayHomeBannerToState(List<Banners> banners) async* {
    yield HomeBannerLoaded(banners: banners);
  }

  @override
  Future<void> close() {
    subscription.cancel();
    return super.close();
  }
}