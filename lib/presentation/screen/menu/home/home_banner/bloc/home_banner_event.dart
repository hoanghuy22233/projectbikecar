
import 'package:equatable/equatable.dart';
import 'package:project_bike_car/model/entity/banner.dart';

class HomeBannerEvent extends Equatable {
  const HomeBannerEvent();
  @override
  List<Object> get props => [];
}

class DisplayHomeBanner extends HomeBannerEvent {
  final List<Banners> banners;

  const DisplayHomeBanner({this.banners});

  @override
  List<Object> get props => [banners];
}