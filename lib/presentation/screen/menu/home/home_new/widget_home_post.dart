import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/model/entity/posts.dart';
import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:project_bike_car/presentation/screen/menu/news/new_detaill/sc_new_detail.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:url_launcher/url_launcher.dart';

import 'bloc/bloc.dart';

class WidgetHomePost extends StatefulWidget {
  final Function(Posts posts, int) onCategoryClick;

  WidgetHomePost({
    Key key,
    this.onCategoryClick,
  }) : super(key: key);
  @override
  _WidgetHomePostState createState() => _WidgetHomePostState();
}

class _WidgetHomePostState extends State<WidgetHomePost> {
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeNewBloc, HomeNewState>(
      builder: (context, state) {
        if (state is HomeNewLoaded) {
          return _buildSlider(state);
        } else {
          return Center(child: Container(
            height: 100,
            width: 100,
            child: Lottie.asset(
              'assets/lottie/load_car.json',
            ),
          ));
        }
      },
    );
  }

  Widget _buildSlider(HomeNewLoaded state) {
    Size size = MediaQuery.of(context).size;
    const double appPadding = 20.0;
    return Container(
      width: MediaQuery.of(context).size.width,
      height: size.height * 0.4,
      child: ListView.builder(
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemCount: state.posts.length >= 5 ? 5 : state.posts.length,
          physics: BouncingScrollPhysics(),
          itemBuilder: (BuildContext context, int index) {
            return GestureDetector(
              onTap: () {
                state.posts[index].link != null
                    ? _openUrl(state.posts[index].link)
                    : Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                NewDetailPageScreen(id: state.posts[index].id)),
                      );
              },
              child: Padding(
                padding: EdgeInsets.only(
                  right: appPadding / 3,
                  bottom: appPadding,
                  left: appPadding,
                ),
                child: Container(
                  width: size.width * 0.55,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.2),
                        offset: Offset(5, 5),
                        blurRadius: 10,
                      )
                    ],
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        width: size.width,
                        height: size.height * 0.23,
                        child: WidgetCachedImage(
                          url: state.posts[index].image,
                        ),
                      ),
                      Container(
                        margin:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                        child: Text(
                          state.posts[index].name,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: AppStyle.DEFAULT_MEDIUM.copyWith(
                              color: AppColor.PRIMARY,
                              fontWeight: FontWeight.bold),
                          textAlign: TextAlign.start,
                        ),
                      ),
                      Container(
                        margin:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                        child: Text(
                          state.posts[index].description,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: AppStyle.DEFAULT_MEDIUM.copyWith(
                              color: AppColor.PRIMARY,
                              fontWeight: FontWeight.w300),
                          textAlign: TextAlign.start,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          }),
    );
  }

  _openUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
