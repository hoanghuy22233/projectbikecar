import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/model/entity/posts.dart';
import 'package:project_bike_car/model/entity/posts_news.dart';

abstract class HomeNewState extends Equatable {
  const HomeNewState();

  @override
  List<Object> get props => [];
}

class HomeNewNotLoaded extends HomeNewState {}

class HomeNewLoaded extends HomeNewState {
  final List<PostsNews> posts;

  HomeNewLoaded({@required this.posts});

  @override
  List<Object> get props => [posts];
}
