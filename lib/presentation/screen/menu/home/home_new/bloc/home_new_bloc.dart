import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/model/entity/posts.dart';
import 'package:project_bike_car/model/entity/posts_news.dart';
import 'package:project_bike_car/presentation/screen/menu/home/bloc/bloc.dart';

import 'bloc.dart';

class HomeNewBloc extends Bloc<HomeNewEvent, HomeNewState>{
  final HomeBloc homeBloc;
  StreamSubscription subscription;

  HomeNewBloc({@required this.homeBloc}) {
    subscription = homeBloc.listen((state) {
      if (state is HomeLoaded) {
        add(DisplayHomeNew(posts: state.homeResponse.data.posts));
      }
    });
  }

  @override
  HomeNewState get initialState => HomeNewNotLoaded();

  @override
  Stream<HomeNewState> mapEventToState(HomeNewEvent event) async* {
    if(event is DisplayHomeNew){
       List<PostsNews> posts = List();
      for(int i=0;i<event.posts.length;i++){
        posts .add(event.posts[i]);
      }
      yield* _mapDisplayHomeNewToState(posts);
    }
  }

  Stream<HomeNewState> _mapDisplayHomeNewToState(List<PostsNews> posts) async* {
    yield HomeNewLoaded(posts: posts);
  }

  @override
  Future<void> close() {
    subscription.cancel();
    return super.close();
  }
}