
import 'package:equatable/equatable.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/model/entity/posts.dart';
import 'package:project_bike_car/model/entity/posts_news.dart';

class HomeNewEvent extends Equatable {
  const HomeNewEvent();
  @override
  List<Object> get props => [];
}

class DisplayHomeNew extends HomeNewEvent {
  final List<PostsNews> posts;

  const DisplayHomeNew({this.posts});

  @override
  List<Object> get props => [posts];
}