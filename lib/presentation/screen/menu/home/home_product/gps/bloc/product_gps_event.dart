import 'package:equatable/equatable.dart';

class ProductGpsEvent extends Equatable {
  const ProductGpsEvent();

  List<Object> get props => [];
}

class LoadProductGps extends ProductGpsEvent {
  final int categoryId;
  final int brandsId;
  final String sort;

  LoadProductGps(this.categoryId,this.brandsId,this.sort);

  List<Object> get props => [categoryId,brandsId,sort];

  @override
  String toString() {
    return 'LoadPProductGps{ CarcategoryId: $categoryId, brandsId:$brandsId,sort:$sort}';
  }
}

class RefreshProductGps extends ProductGpsEvent {
  final int categoryId;
  final int brandsId;
  final String sort;

  RefreshProductGps(this.categoryId,this.brandsId,this.sort);

  List<Object> get props => [categoryId,brandsId,sort];

  @override
  String toString() {
    return 'RefreshProductGps{categoryId: $categoryId,brandsId:$brandsId,sort:$sort';
  }

}
