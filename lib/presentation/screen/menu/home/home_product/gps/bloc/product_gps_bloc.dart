import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:project_bike_car/model/repo/barrel_repo.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

import 'bloc.dart';

class ProductGpsBloc extends Bloc<ProductGpsEvent, ProductGpsState> {
  final UserRepository userRepository;

  ProductGpsBloc({ this.userRepository});

  @override
  ProductGpsState get initialState => ProductGpsLoading();

  @override
  Stream<ProductGpsState> mapEventToState(ProductGpsEvent event) async* {
    if (event is LoadProductGps) {
      yield* _mapLoadProductGpsToState(categoryId: event.categoryId,brandsId: event.brandsId,sort: event.sort);
    } else if (event is RefreshProductGps) {
      yield ProductGpsLoading();
      yield* _mapLoadProductGpsToState(categoryId: event.categoryId,brandsId: event.brandsId,sort:event.sort);
    }
  }

  Stream<ProductGpsState> _mapLoadProductGpsToState({ @required int categoryId,@required int brandsId,@required String sort,}) async* {
    try {
      final homeResponse = await userRepository.getHomes(categoryId: categoryId,brandsId: brandsId,sort: sort);
      print('-----------------');
      print(homeResponse);
      await Future.delayed(Duration(milliseconds: 500), () {});
      yield ProductGpsLoaded(homeResponse: homeResponse);
    } catch (e) {
      yield ProductGpsNotLoaded(status: DioErrorUtil.handleError(e));
    }
  }
}
