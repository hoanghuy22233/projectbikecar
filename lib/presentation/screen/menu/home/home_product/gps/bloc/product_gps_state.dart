import 'package:equatable/equatable.dart';
import 'package:project_bike_car/model/api/response/home_response.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

abstract class ProductGpsState extends Equatable {
  const ProductGpsState();

  @override
  List<Object> get props => [];
}

class ProductGpsLoading extends ProductGpsState {}

class ProductGpsLoaded extends ProductGpsState {
  final HomeResponse homeResponse;

  const ProductGpsLoaded({this.homeResponse});

  @override
  List<Object> get props => [homeResponse];

  @override
  String toString() {
    return 'ProductGpsLoaded{homeResponse: $homeResponse}';
  }


}

class ProductGpsNotLoaded extends ProductGpsState {
  final DioStatus status;

  ProductGpsNotLoaded({this.status});

  @override
  String toString() {
    return 'ProductGpsNotLoaded{error: $status}';
  }
}