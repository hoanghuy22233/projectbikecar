import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/model/entity/products.dart';

abstract class HomeProductGpsState extends Equatable {
  const HomeProductGpsState();

  @override
  List<Object> get props => [];
}

class HomeProductGpsNotLoaded extends HomeProductGpsState {}

class HomeProductGpsLoaded extends HomeProductGpsState {
  final List<Products> products;
  final bool hasReachedMax;

  HomeProductGpsLoaded({@required this.products,this.hasReachedMax});

  @override
  List<Object> get props => [products,hasReachedMax];
}
