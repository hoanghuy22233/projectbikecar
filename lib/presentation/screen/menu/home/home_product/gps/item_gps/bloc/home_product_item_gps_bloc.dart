import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/model/entity/products.dart';
import 'package:project_bike_car/presentation/screen/menu/home/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/bike/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/car/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/gps/bloc/bloc.dart';

import 'bloc.dart';

class HomeProductGpsBloc extends Bloc<HomeProductGpsEvent, HomeProductGpsState>{
  final ProductGpsBloc productBloc;
  StreamSubscription subscription;

  HomeProductGpsBloc({@required this.productBloc}) {
    subscription = productBloc.listen((state) {
      if (state is ProductGpsLoaded) {
        add(DisplayHomeProductGps(product: state.homeResponse.data.products, hasReachedMax: false));
      }
    });
  }

  @override
  HomeProductGpsState get initialState => HomeProductGpsNotLoaded();

  @override
  Stream<HomeProductGpsState> mapEventToState(HomeProductGpsEvent event) async* {
    if(event is DisplayHomeProductGps){
       List<Products> products = List();
      for(int i=0;i<event.product.length;i++){
        products .add(event.product[i]);
      }
      yield* _mapDisplayHomeProductGpsToState(products);
    }
  }

  Stream<HomeProductGpsState> _mapDisplayHomeProductGpsToState(List<Products> product) async* {
    yield HomeProductGpsLoaded(products: product, hasReachedMax: false);
  }

  @override
  Future<void> close() {
    subscription.cancel();
    return super.close();
  }
}