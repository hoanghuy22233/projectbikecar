import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/model/entity/products.dart';
import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'bloc/bloc.dart';
import 'product/bloc/bloc.dart';

class WidgetHomeProductItemMenu extends StatefulWidget {
  final Function(Products products, int) onCategoryClick;

  WidgetHomeProductItemMenu({
    Key key,
    this.onCategoryClick,
  }) : super(key: key);
  @override
  _WidgetHomeProductItemMenuState createState() =>
      _WidgetHomeProductItemMenuState();
}

class _WidgetHomeProductItemMenuState
    extends State<WidgetHomeProductItemMenu> {
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeProductBloc, HomeProductState>(
      builder: (context, state) {
        if (state is HomeProductLoaded) {
          return  _buildContents(state);
        } else {
          return Center(child: Container(
            height: 100,
            width: 100,
            child: Lottie.asset(
              'assets/lottie/load_car.json',
            ),
          ));
        }
      },
    );
  }

  _buildContents(HomeProductLoaded state) {
    Size size = MediaQuery.of(context).size;
    const double appPadding = 20.0;
    return state.products.length != 0
        ? Container(
      width: MediaQuery.of(context).size.width,
      height: size.height * 0.4,
      child: ListView.builder(
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemCount: state.products.length>=6?6:state.products.length,
          itemBuilder: (BuildContext context, int index) {
            return GestureDetector(
              onTap: (){
                AppNavigator.detailProduct(productId: state.products[index].id);
              },
              child: Stack(
                children: [
                  Padding(
                    padding: EdgeInsets.only(
                      right: appPadding/3,
                      bottom: appPadding,
                      left: appPadding,
                    ),
                    child: Container(
                      width: size.width * 0.55,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.2),
                            offset: Offset(5, 5),
                            blurRadius: 10,
                          )
                        ],
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Stack(
                            alignment: Alignment.topLeft,
                            children: [
                              Container(
                                width: size.width,
                                height: size.height * 0.23,
                                child: WidgetCachedImage(
                                  url: state.products[index].image,
                                ),
                              ),
                              state.products[index].views!=0?
                              Container(
                                padding: EdgeInsets.only(left: 10,top: 5),
                                child: Stack(
                                  alignment: Alignment.center,
                                  children: [
                                    Image.asset("assets/images/black-suitcase.png",height: 30,width: 30,color: Colors.grey,),
                                    Container(
                                        child:  state.products[index].views != 0
                                            ? Text(
                                            state.products[index].views.toString().length < 4
                                                ? state.products[index].views.toString()
                                                : state.products[index].views.toString().substring(0, 1) + 'k',     style: AppStyle.DEFAULT_SMALL
                                            .copyWith(color: AppColor.WHITE,fontSize: 10,fontWeight: FontWeight.bold)
                                        )
                                            : Container()),
                                  ],
                                ),
                              ):Container()
                            ],
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                            child: Text(state.products[index].name,  maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: AppStyle.DEFAULT_MEDIUM.copyWith(
                                  color: AppColor.PRIMARY,fontWeight: FontWeight.bold
                              ),
                              textAlign: TextAlign.start,),
                          ),
                          state.products[index].priceOld!=0?
                          Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: Text('${AppValue.APP_MONEY_FORMAT.format(state.products[index].priceOld)}',style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold, color: Colors.red
                            ),),
                          ):Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: Text('${AppValue.APP_MONEY_FORMAT.format(state.products[index].price)}',style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold, color: Colors.red
                            ),),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    right: appPadding * 1.5,
                    bottom: appPadding / 2,
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black.withOpacity(0.5),
                              offset: Offset(3, 3),
                              blurRadius: 3,
                            )
                          ]),
                      padding: EdgeInsets.symmetric(
                        vertical: appPadding / 4,
                        horizontal: appPadding / 1.5,
                      ),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Giảm ",style: TextStyle(fontStyle: FontStyle.italic)),
                          state.products[index].price != 0
                              ? Text(
                            '${(((state.products[index].price-state.products[index].priceOld)/state.products[index].priceOld)*100).floor()}%',
                            style: AppStyle.APP_MEDIUM.copyWith(
                                color: AppColor.RED,
                                fontWeight: FontWeight.w800),
                          )
                              : Text(
                            '0 %',
                            style: AppStyle.APP_MEDIUM.copyWith(
                                color: AppColor.RED,
                                fontWeight: FontWeight.w800),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            );
          }),
    )
        : Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
              height: 60,
              width: 60,
              child: Image.asset('assets/images/box.png')),
          SizedBox(
            height: 20,
          ),
          Text('Không có sản phẩm nào?'),
          SizedBox(height: 10,)
        ],
      ),
    );
  }

}
