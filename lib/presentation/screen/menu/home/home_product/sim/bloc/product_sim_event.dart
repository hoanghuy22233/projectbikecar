import 'package:equatable/equatable.dart';

class ProductSimEvent extends Equatable {
  const ProductSimEvent();

  List<Object> get props => [];
}

class LoadProductSim extends ProductSimEvent {
  final int categoryId;
  final int brandsId;
  final String sort;

  LoadProductSim(this.categoryId,this.brandsId,this.sort);

  List<Object> get props => [categoryId,brandsId,sort];

  @override
  String toString() {
    return 'LoadPProductSim{ CarcategoryId: $categoryId, brandsId:$brandsId,sort:$sort}';
  }
}

class RefreshProductSim extends ProductSimEvent {
  final int categoryId;
  final int brandsId;
  final String sort;

  RefreshProductSim(this.categoryId,this.brandsId,this.sort);

  List<Object> get props => [categoryId,brandsId,sort];

  @override
  String toString() {
    return 'RefreshProductSim{categoryId: $categoryId,brandsId:$brandsId,sort:$sort';
  }

}
