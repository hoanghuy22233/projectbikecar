import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:project_bike_car/model/repo/barrel_repo.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

import 'bloc.dart';

class ProductSimBloc extends Bloc<ProductSimEvent, ProductSimState> {
  final UserRepository userRepository;

  ProductSimBloc({ this.userRepository});

  @override
  ProductSimState get initialState => ProductSimLoading();

  @override
  Stream<ProductSimState> mapEventToState(ProductSimEvent event) async* {
    if (event is LoadProductSim) {
      yield* _mapLoadProductSimToState(categoryId: event.categoryId,brandsId: event.brandsId,sort: event.sort);
    } else if (event is RefreshProductSim) {
      yield ProductSimLoading();
      yield* _mapLoadProductSimToState(categoryId: event.categoryId,brandsId: event.brandsId,sort:event.sort);
    }
  }

  Stream<ProductSimState> _mapLoadProductSimToState({ @required int categoryId,@required int brandsId,@required String sort,}) async* {
    try {
      final homeResponse = await userRepository.getHomes(categoryId: categoryId,brandsId: brandsId,sort: sort);
      print('-----------------');
      print(homeResponse);
      await Future.delayed(Duration(milliseconds: 500), () {});
      yield ProductSimLoaded(homeResponse: homeResponse);
    } catch (e) {
      yield ProductSimNotLoaded(status: DioErrorUtil.handleError(e));
    }
  }
}
