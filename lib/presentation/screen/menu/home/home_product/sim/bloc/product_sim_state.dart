import 'package:equatable/equatable.dart';
import 'package:project_bike_car/model/api/response/home_response.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

abstract class ProductSimState extends Equatable {
  const ProductSimState();

  @override
  List<Object> get props => [];
}

class ProductSimLoading extends ProductSimState {}

class ProductSimLoaded extends ProductSimState {
  final HomeResponse homeResponse;

  const ProductSimLoaded({this.homeResponse});

  @override
  List<Object> get props => [homeResponse];

  @override
  String toString() {
    return 'ProductSimLoaded{homeResponse: $homeResponse}';
  }


}

class ProductSimNotLoaded extends ProductSimState {
  final DioStatus status;

  ProductSimNotLoaded({this.status});

  @override
  String toString() {
    return 'ProductSimNotLoaded{error: $status}';
  }
}