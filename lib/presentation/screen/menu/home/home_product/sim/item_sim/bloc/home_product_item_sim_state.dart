import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/model/entity/products.dart';

abstract class HomeProductSimState extends Equatable {
  const HomeProductSimState();

  @override
  List<Object> get props => [];
}

class HomeProductSimNotLoaded extends HomeProductSimState {}

class HomeProductSimLoaded extends HomeProductSimState {
  final List<Products> products;
  final bool hasReachedMax;

  HomeProductSimLoaded({@required this.products,this.hasReachedMax});

  @override
  List<Object> get props => [products,hasReachedMax];
}
