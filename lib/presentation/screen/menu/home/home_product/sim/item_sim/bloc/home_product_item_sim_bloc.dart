import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/model/entity/products.dart';
import 'package:project_bike_car/presentation/screen/menu/home/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/bike/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/car/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/gps/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/sim/bloc/bloc.dart';

import 'bloc.dart';

class HomeProductSimBloc extends Bloc<HomeProductSimEvent, HomeProductSimState>{
  final ProductSimBloc productBloc;
  StreamSubscription subscription;

  HomeProductSimBloc({@required this.productBloc}) {
    subscription = productBloc.listen((state) {
      if (state is ProductSimLoaded) {
        add(DisplayHomeProductSim(product: state.homeResponse.data.products, hasReachedMax: false));
      }
    });
  }

  @override
  HomeProductSimState get initialState => HomeProductSimNotLoaded();

  @override
  Stream<HomeProductSimState> mapEventToState(HomeProductSimEvent event) async* {
    if(event is DisplayHomeProductSim){
       List<Products> products = List();
      for(int i=0;i<event.product.length;i++){
        products .add(event.product[i]);
      }
      yield* _mapDisplayHomeProductSimToState(products);
    }
  }

  Stream<HomeProductSimState> _mapDisplayHomeProductSimToState(List<Products> product) async* {
    yield HomeProductSimLoaded(products: product, hasReachedMax: false);
  }

  @override
  Future<void> close() {
    subscription.cancel();
    return super.close();
  }
}