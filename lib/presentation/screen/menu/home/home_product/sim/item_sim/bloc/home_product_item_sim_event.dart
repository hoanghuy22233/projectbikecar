
import 'package:equatable/equatable.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/model/entity/products.dart';

class HomeProductSimEvent extends Equatable {
  const HomeProductSimEvent();
  @override
  List<Object> get props => [];
}

class DisplayHomeProductSim extends HomeProductSimEvent {
  final List<Products> product;
  final bool hasReachedMax;

  const DisplayHomeProductSim({this.product,this.hasReachedMax});

  @override
  List<Object> get props => [product,hasReachedMax];
}