import 'package:equatable/equatable.dart';

class ProductEvent extends Equatable {
  const ProductEvent();

  List<Object> get props => [];
}

class LoadProduct extends ProductEvent {
  final int categoryId;
  final int brandsId;
  final String sort;

  LoadProduct(this.categoryId,this.brandsId,this.sort);

  List<Object> get props => [categoryId,brandsId,sort];

  @override
  String toString() {
    return 'LoadProduct{categoryId: $categoryId, brandsId:$brandsId,sort:$sort}';
  }
}

class RefreshProduct extends ProductEvent {
  final int categoryId;
  final int brandsId;
  final String sort;

  RefreshProduct(this.categoryId,this.brandsId,this.sort);

  List<Object> get props => [categoryId,brandsId,sort];

  @override
  String toString() {
    return 'RefreshProduct{categoryId: $categoryId,brandsId:$brandsId,sort:$sort';
  }

}
