import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:project_bike_car/model/repo/barrel_repo.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

import 'bloc.dart';

class ProductBloc extends Bloc<ProductEvent, ProductState> {
  final UserRepository userRepository;

  ProductBloc({ this.userRepository});

  @override
  ProductState get initialState => ProductLoading();

  @override
  Stream<ProductState> mapEventToState(ProductEvent event) async* {
    if (event is LoadProduct) {
      yield* _mapLoadProductToState(categoryId: event.categoryId,brandsId: event.brandsId,sort: event.sort);
    } else if (event is RefreshProduct) {
      yield ProductLoading();
      yield* _mapLoadProductToState(categoryId: event.categoryId,brandsId: event.brandsId,sort:event.sort);
    }
  }

  Stream<ProductState> _mapLoadProductToState({ @required int categoryId,@required int brandsId,@required String sort,}) async* {
    try {
      final homeResponse = await userRepository.getHomes(categoryId: categoryId,brandsId: brandsId,sort: sort);
      print('-----------------');
      print(homeResponse);
      await Future.delayed(Duration(milliseconds: 500), () {});
      yield ProductLoaded(homeResponse: homeResponse);
    } catch (e) {
      yield ProductNotLoaded(status: DioErrorUtil.handleError(e));
    }
  }
}
