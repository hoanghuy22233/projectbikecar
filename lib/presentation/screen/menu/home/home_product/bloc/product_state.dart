import 'package:equatable/equatable.dart';
import 'package:project_bike_car/model/api/response/home_response.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

abstract class ProductState extends Equatable {
  const ProductState();

  @override
  List<Object> get props => [];
}

class ProductLoading extends ProductState {}

class ProductLoaded extends ProductState {
  final HomeResponse homeResponse;

  const ProductLoaded({this.homeResponse});

  @override
  List<Object> get props => [homeResponse];

  @override
  String toString() {
    return 'ProductLoaded{homeResponse: $homeResponse}';
  }


}

class ProductNotLoaded extends ProductState {
  final DioStatus status;

  ProductNotLoaded({this.status});

  @override
  String toString() {
    return 'ProductNotLoaded{error: $status}';
  }
}