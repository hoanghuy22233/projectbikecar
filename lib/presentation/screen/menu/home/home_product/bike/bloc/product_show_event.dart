import 'package:equatable/equatable.dart';

class ProductShowEvent extends Equatable {
  const ProductShowEvent();

  List<Object> get props => [];
}

class LoadProductShow extends ProductShowEvent {
  final int categoryId;
  final int brandsId;
  final String sort;

  LoadProductShow(this.categoryId,this.brandsId,this.sort);

  List<Object> get props => [categoryId,brandsId,sort];

  @override
  String toString() {
    return 'LoadProductShow{categoryId: $categoryId, brandsId:$brandsId,sort:$sort}';
  }
}

class RefreshProductShow extends ProductShowEvent {
  final int categoryId;
  final int brandsId;
  final String sort;

  RefreshProductShow(this.categoryId,this.brandsId,this.sort);

  List<Object> get props => [categoryId,brandsId,sort];

  @override
  String toString() {
    return 'RefreshPProductShow{categoryId: $categoryId,brandsId:$brandsId,sort:$sort';
  }

}
