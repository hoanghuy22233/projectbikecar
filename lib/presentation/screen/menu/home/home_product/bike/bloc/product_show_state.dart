import 'package:equatable/equatable.dart';
import 'package:project_bike_car/model/api/response/home_response.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

abstract class ProductShowState extends Equatable {
  const ProductShowState();

  @override
  List<Object> get props => [];
}

class ProductShowLoading extends ProductShowState {}

class ProductShowLoaded extends ProductShowState {
  final HomeResponse homeResponse;

  const ProductShowLoaded({this.homeResponse});

  @override
  List<Object> get props => [homeResponse];

  @override
  String toString() {
    return 'ProductShowLoaded{homeResponse: $homeResponse}';
  }


}

class ProductShowNotLoaded extends ProductShowState {
  final DioStatus status;

  ProductShowNotLoaded({this.status});

  @override
  String toString() {
    return 'ProductShowNotLoaded{error: $status}';
  }
}