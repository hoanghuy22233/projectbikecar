import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:project_bike_car/model/repo/barrel_repo.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

import 'bloc.dart';

class ProductShowBloc extends Bloc<ProductShowEvent, ProductShowState> {
  final UserRepository userRepository;

  ProductShowBloc({ this.userRepository});

  @override
  ProductShowState get initialState => ProductShowLoading();

  @override
  Stream<ProductShowState> mapEventToState(ProductShowEvent event) async* {
    if (event is LoadProductShow) {
      yield* _mapLoadProductToState(categoryId: event.categoryId,brandsId: event.brandsId,sort: event.sort);
    } else if (event is RefreshProductShow) {
      yield ProductShowLoading();
      yield* _mapLoadProductToState(categoryId: event.categoryId,brandsId: event.brandsId,sort:event.sort);
    }
  }

  Stream<ProductShowState> _mapLoadProductToState({ @required int categoryId,@required int brandsId,@required String sort,}) async* {
    try {
      final homeResponse = await userRepository.getHomes(categoryId: categoryId,brandsId: brandsId,sort: sort);
      print('-----------------');
      print(homeResponse);
      await Future.delayed(Duration(milliseconds: 500), () {});
      yield ProductShowLoaded(homeResponse: homeResponse);
    } catch (e) {
      yield ProductShowNotLoaded(status: DioErrorUtil.handleError(e));
    }
  }
}
