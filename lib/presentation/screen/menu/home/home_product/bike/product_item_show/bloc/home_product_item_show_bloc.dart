import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/model/entity/products.dart';
import 'package:project_bike_car/presentation/screen/menu/home/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/bike/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/bloc/bloc.dart';

import 'bloc.dart';

class HomeProductShowBloc extends Bloc<HomeProductShowEvent, HomeProductShowState>{
  final ProductShowBloc productBloc;
  StreamSubscription subscription;

  HomeProductShowBloc({@required this.productBloc}) {
    subscription = productBloc.listen((state) {
      if (state is ProductShowLoaded) {
        add(DisplayHomeProductShow(product: state.homeResponse.data.products, hasReachedMax: false));
      }
    });
  }

  @override
  HomeProductShowState get initialState => HomeProductShowNotLoaded();

  @override
  Stream<HomeProductShowState> mapEventToState(HomeProductShowEvent event) async* {
    if(event is DisplayHomeProductShow){
       List<Products> products = List();
      for(int i=0;i<event.product.length;i++){
        products .add(event.product[i]);
      }
      yield* _mapDisplayHomeProductShowToState(products);
    }
  }

  Stream<HomeProductShowState> _mapDisplayHomeProductShowToState(List<Products> product) async* {
    yield HomeProductShowLoaded(products: product, hasReachedMax: false);
  }

  @override
  Future<void> close() {
    subscription.cancel();
    return super.close();
  }
}