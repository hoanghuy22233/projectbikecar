
import 'package:equatable/equatable.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/model/entity/products.dart';

class HomeProductShowEvent extends Equatable {
  const HomeProductShowEvent();
  @override
  List<Object> get props => [];
}

class DisplayHomeProductShow extends HomeProductShowEvent {
  final List<Products> product;
  final bool hasReachedMax;

  const DisplayHomeProductShow({this.product,this.hasReachedMax});

  @override
  List<Object> get props => [product,hasReachedMax];
}