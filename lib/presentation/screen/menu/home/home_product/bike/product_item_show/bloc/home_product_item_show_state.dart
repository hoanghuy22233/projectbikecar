import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/model/entity/products.dart';

abstract class HomeProductShowState extends Equatable {
  const HomeProductShowState();

  @override
  List<Object> get props => [];
}

class HomeProductShowNotLoaded extends HomeProductShowState {}

class HomeProductShowLoaded extends HomeProductShowState {
  final List<Products> products;
  final bool hasReachedMax;

  HomeProductShowLoaded({@required this.products,this.hasReachedMax});

  @override
  List<Object> get props => [products,hasReachedMax];
}
