import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:loadmore/loadmore.dart';
import 'package:project_bike_car/model/entity/products.dart';
import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/widget_product_grid.dart';
import 'package:project_bike_car/utils/common/common_utils.dart';
import 'package:project_bike_car/utils/snackbar/barrel_snack_bar.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'bloc/bloc.dart';

class WidgetCategoriesProduct extends StatefulWidget {
  final String categoryName;
  final List<Products> products;
  final RefreshController refreshController;
  final Function onLoadMore;
  final Function onRefresh;
  final Function actionMore;
  final Color color;
  final String tag;
  final GlobalKey<ScaffoldState> drawer;

  WidgetCategoriesProduct(
      {Key key,
      this.categoryName,
      this.products,
      this.onLoadMore,
      this.onRefresh,
      this.tag,
      this.color,
      this.actionMore,
      this.refreshController,
      this.drawer})
      : super(key: key);

  @override
  _WidgetCategoriesProductState createState() =>
      _WidgetCategoriesProductState();
}

class _WidgetCategoriesProductState extends State<WidgetCategoriesProduct> {
  final datas = List.generate(20, (i) => i + 1);
  final scrollController = ScrollController();
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    scrollController.addListener(() {
      if (scrollController.offset ==
          scrollController.position.maxScrollExtent) {
        loadMore();
      }
    });
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final itemHeight = size.height / 1.2;
    final itemWidth = size.width;
    return Container(
      color: widget.color ?? Colors.white,
      child: Container(
          child: WidgetRefresher(
            refreshController: widget.refreshController,
             onLoading: widget.onLoadMore,
            onRefresh: widget.onRefresh,
            child: GridView.builder(
              controller: scrollController,
             shrinkWrap: true,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                childAspectRatio: (itemWidth / itemHeight),
              ),
              physics: BouncingScrollPhysics(),
              itemBuilder: (context, index) {
                if (index <datas.length) {
                  return WidgetProductGrid(
                    product: widget.products[index],
                  );
                }
                return Center(
                  child: Opacity(
                    opacity: isLoading ? 1.0 : 0.0,
                   // child: CircularProgressIndicator(),
                  ),
                );
              },
              itemCount:widget.products.length>= datas.length+1?datas.length+1:widget.products.length,
            ),
          )),
      // onRefresh: _refresh,
    );
  }

  loadMore() async {

    if (!isLoading) {
     if(datas.length <= widget.products.length){
       setState(() => isLoading = true);
       final newDatas = await fetchDatas(datas.length+1, datas.length+21);
       datas.addAll(newDatas);
       isLoading = false;
       setState(() {});

     }
     else{
       await Future.delayed(Duration(milliseconds: 500));
       setState(() => isLoading = false);
       await makeAnimation();
       widget.refreshController.loadNoData();
       return;
     }



    }
  }

  makeAnimation() async {
    final offsetFromBottom =
        scrollController.position.maxScrollExtent - scrollController.offset;
    if (offsetFromBottom < 50) {
      await scrollController.animateTo(
        scrollController.offset - (50 - offsetFromBottom),
        duration: Duration(milliseconds: 500),
        curve: Curves.easeOut,
      );
    }
  }
}

Future<List<int>> fetchDatas(int start, int end) {
  return Future.delayed(Duration(seconds: 1), () {
    return List.generate(end - start, (i) => i + start);
  });
}
