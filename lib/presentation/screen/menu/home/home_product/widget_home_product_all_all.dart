import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:project_bike_car/model/entity/products.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/widget_categories_product.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'bloc/bloc.dart';
import 'product/bloc/bloc.dart';
import 'product/bloc/home_product_bloc.dart';

class WidgetHomeProductAllAll extends StatefulWidget {
  final Function(Products products, int) onCategoryClick;
  final GlobalKey<ScaffoldState> drawer;
  final int categoryId;
  final int bandId;

  WidgetHomeProductAllAll({
    Key key,
    this.onCategoryClick,this.drawer,this.categoryId,this.bandId
    //   this.refreshController,
  }) : super(key: key);
  @override
  _WidgetHomeProductAllAllState createState() => _WidgetHomeProductAllAllState(categoryId,bandId);
}

class _WidgetHomeProductAllAllState extends State<WidgetHomeProductAllAll> {
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  final datas = List.generate(20, (i) => i + 1);
  int categoryId;
  int brandId;


  _WidgetHomeProductAllAllState(this.categoryId, this.brandId);

  @override
  void initState() {
    super.initState();
    BlocProvider.of<ProductBloc>(context)
        .add(LoadProduct( categoryId??0, brandId??0,'id_desc'));
  }

  onRefresh(HomeProductLoaded state) async {
    BlocProvider.of<ProductBloc>(context)
        .add(RefreshProduct(categoryId??0, brandId??0,'id_desc'));
    datas
      ..clear()
      ..addAll(await fetchDatas(1, 21));
    if (mounted) {
      setState(() {});
    }
  }

  _onLoadMore(HomeProductLoaded state) async {
    BlocProvider.of<ProductBloc>(context)
        .add(LoadProduct(categoryId??0, brandId??0,'id_desc'));
  }



  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeProductBloc, HomeProductState>(
      builder: (context, state) {
        if (state is HomeProductLoaded) {
          return _buildContents(state);
        } else {
          return Center(child: Container(
            height: 100,
            width: 100,
            child: Lottie.asset(
              'assets/lottie/load_car.json',
            ),
          ));
        }
      },
    );
  }


  _buildContents(HomeProductLoaded state) {
    return state.products.length != 0
        ? Container(
        child: WidgetCategoriesProduct(
          refreshController: _refreshController,
          products: state.products,
          onLoadMore: () => _onLoadMore(state),
          onRefresh: () => onRefresh(state),
        ))
        : Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                    height: 120,
                    width: 120,
                    child: Image.asset('assets/images/box.png')),
                SizedBox(
                  height: 20,
                ),
                Text('Không có sản phẩm nào?'),
              ],
            ),
          );
  }

  Future<List<int>> fetchDatas(int start, int end) {
    return Future.delayed(Duration(seconds: 1), () {
      return List.generate(end - start, (i) => i + start);
    });}
}
