import 'package:equatable/equatable.dart';
import 'package:project_bike_car/model/api/response/home_response.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

abstract class ProductCarState extends Equatable {
  const ProductCarState();

  @override
  List<Object> get props => [];
}

class ProductCarLoading extends ProductCarState {}

class ProductCarLoaded extends ProductCarState {
  final HomeResponse homeResponse;

  const ProductCarLoaded({this.homeResponse});

  @override
  List<Object> get props => [homeResponse];

  @override
  String toString() {
    return 'ProductCarLoaded{homeResponse: $homeResponse}';
  }


}

class ProductCarNotLoaded extends ProductCarState {
  final DioStatus status;

  ProductCarNotLoaded({this.status});

  @override
  String toString() {
    return 'ProductCarNotLoaded{error: $status}';
  }
}