import 'package:equatable/equatable.dart';

class ProductCarEvent extends Equatable {
  const ProductCarEvent();

  List<Object> get props => [];
}

class LoadProductCar extends ProductCarEvent {
  final int categoryId;
  final int brandsId;
  final String sort;

  LoadProductCar(this.categoryId,this.brandsId,this.sort);

  List<Object> get props => [categoryId,brandsId,sort];

  @override
  String toString() {
    return 'LoadProductCarcategoryId: $categoryId, brandsId:$brandsId,sort:$sort}';
  }
}

class RefreshProductCar extends ProductCarEvent {
  final int categoryId;
  final int brandsId;
  final String sort;

  RefreshProductCar(this.categoryId,this.brandsId,this.sort);

  List<Object> get props => [categoryId,brandsId,sort];

  @override
  String toString() {
    return 'RefreshProductCar{categoryId: $categoryId,brandsId:$brandsId,sort:$sort';
  }

}
