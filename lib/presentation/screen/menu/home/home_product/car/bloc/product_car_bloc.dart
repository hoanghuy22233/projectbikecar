import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:project_bike_car/model/repo/barrel_repo.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

import 'bloc.dart';

class ProductCarBloc extends Bloc<ProductCarEvent, ProductCarState> {
  final UserRepository userRepository;

  ProductCarBloc({ this.userRepository});

  @override
  ProductCarState get initialState => ProductCarLoading();

  @override
  Stream<ProductCarState> mapEventToState(ProductCarEvent event) async* {
    if (event is LoadProductCar) {
      yield* _mapLoadProductCarToState(categoryId: event.categoryId,brandsId: event.brandsId,sort: event.sort);
    } else if (event is RefreshProductCar) {
      yield ProductCarLoading();
      yield* _mapLoadProductCarToState(categoryId: event.categoryId,brandsId: event.brandsId,sort:event.sort);
    }
  }

  Stream<ProductCarState> _mapLoadProductCarToState({ @required int categoryId,@required int brandsId,@required String sort,}) async* {
    try {
      final homeResponse = await userRepository.getHomes(categoryId: categoryId,brandsId: brandsId,sort: sort);
      print('-----------------');
      print(homeResponse);
      await Future.delayed(Duration(milliseconds: 500), () {});
      yield ProductCarLoaded(homeResponse: homeResponse);
    } catch (e) {
      yield ProductCarNotLoaded(status: DioErrorUtil.handleError(e));
    }
  }
}
