import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/model/entity/products.dart';
import 'package:project_bike_car/presentation/screen/menu/home/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/bike/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/car/bloc/bloc.dart';

import 'bloc.dart';

class HomeProductCarBloc extends Bloc<HomeProductCarEvent, HomeProductCarState>{
  final ProductCarBloc productBloc;
  StreamSubscription subscription;

  HomeProductCarBloc({@required this.productBloc}) {
    subscription = productBloc.listen((state) {
      if (state is ProductCarLoaded) {
        add(DisplayHomeProductCar(product: state.homeResponse.data.products, hasReachedMax: false));
      }
    });
  }

  @override
  HomeProductCarState get initialState => HomeProductCarNotLoaded();

  @override
  Stream<HomeProductCarState> mapEventToState(HomeProductCarEvent event) async* {
    if(event is DisplayHomeProductCar){
       List<Products> products = List();
      for(int i=0;i<event.product.length;i++){
        products .add(event.product[i]);
      }
      yield* _mapDisplayHomeProductCarToState(products);
    }
  }

  Stream<HomeProductCarState> _mapDisplayHomeProductCarToState(List<Products> product) async* {
    yield HomeProductCarLoaded(products: product, hasReachedMax: false);
  }

  @override
  Future<void> close() {
    subscription.cancel();
    return super.close();
  }
}