import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/model/entity/products.dart';

abstract class HomeProductCarState extends Equatable {
  const HomeProductCarState();

  @override
  List<Object> get props => [];
}

class HomeProductCarNotLoaded extends HomeProductCarState {}

class HomeProductCarLoaded extends HomeProductCarState {
  final List<Products> products;
  final bool hasReachedMax;

  HomeProductCarLoaded({@required this.products,this.hasReachedMax});

  @override
  List<Object> get props => [products,hasReachedMax];
}
