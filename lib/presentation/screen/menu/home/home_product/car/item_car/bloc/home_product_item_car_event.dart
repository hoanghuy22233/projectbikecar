
import 'package:equatable/equatable.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/model/entity/products.dart';

class HomeProductCarEvent extends Equatable {
  const HomeProductCarEvent();
  @override
  List<Object> get props => [];
}

class DisplayHomeProductCar extends HomeProductCarEvent {
  final List<Products> product;
  final bool hasReachedMax;

  const DisplayHomeProductCar({this.product,this.hasReachedMax});

  @override
  List<Object> get props => [product,hasReachedMax];
}