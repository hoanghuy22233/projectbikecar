import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:project_bike_car/model/repo/barrel_repo.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

import 'bloc.dart';

class ProductElectronicBloc extends Bloc<ProductElectronicEvent, ProductElectronicState> {
  final UserRepository userRepository;

  ProductElectronicBloc({ this.userRepository});

  @override
  ProductElectronicState get initialState => ProductElectronicLoading();

  @override
  Stream<ProductElectronicState> mapEventToState(ProductElectronicEvent event) async* {
    if (event is LoadProductElectronic) {
      yield* _mapLoadProductElectronicToState(categoryId: event.categoryId,brandsId: event.brandsId,sort: event.sort);
    } else if (event is RefreshProductElectronic) {
      yield ProductElectronicLoading();
      yield* _mapLoadProductElectronicToState(categoryId: event.categoryId,brandsId: event.brandsId,sort:event.sort);
    }
  }

  Stream<ProductElectronicState> _mapLoadProductElectronicToState({ @required int categoryId,@required int brandsId,@required String sort,}) async* {
    try {
      final homeResponse = await userRepository.getHomes(categoryId: categoryId,brandsId: brandsId,sort: sort);
      print('-----------------');
      print(homeResponse);
      await Future.delayed(Duration(milliseconds: 500), () {});
      yield ProductElectronicLoaded(homeResponse: homeResponse);
    } catch (e) {
      yield ProductElectronicNotLoaded(status: DioErrorUtil.handleError(e));
    }
  }
}
