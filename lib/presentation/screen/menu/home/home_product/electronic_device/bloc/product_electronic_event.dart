import 'package:equatable/equatable.dart';

class ProductElectronicEvent extends Equatable {
  const ProductElectronicEvent();

  List<Object> get props => [];
}

class LoadProductElectronic extends ProductElectronicEvent {
  final int categoryId;
  final int brandsId;
  final String sort;

  LoadProductElectronic(this.categoryId,this.brandsId,this.sort);

  List<Object> get props => [categoryId,brandsId,sort];

  @override
  String toString() {
    return 'LoadProductElectronic{categoryId: $categoryId, brandsId:$brandsId,sort:$sort}';
  }
}

class RefreshProductElectronic extends ProductElectronicEvent {
  final int categoryId;
  final int brandsId;
  final String sort;

  RefreshProductElectronic(this.categoryId,this.brandsId,this.sort);

  List<Object> get props => [categoryId,brandsId,sort];

  @override
  String toString() {
    return 'RefreshProductElectronic{categoryId: $categoryId,brandsId:$brandsId,sort:$sort';
  }

}
