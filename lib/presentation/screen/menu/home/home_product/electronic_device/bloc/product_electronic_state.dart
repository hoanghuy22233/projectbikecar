import 'package:equatable/equatable.dart';
import 'package:project_bike_car/model/api/response/home_response.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

abstract class ProductElectronicState extends Equatable {
  const ProductElectronicState();

  @override
  List<Object> get props => [];
}

class ProductElectronicLoading extends ProductElectronicState {}

class ProductElectronicLoaded extends ProductElectronicState {
  final HomeResponse homeResponse;

  const ProductElectronicLoaded({this.homeResponse});

  @override
  List<Object> get props => [homeResponse];

  @override
  String toString() {
    return 'PProductElectronicLoaded{homeResponse: $homeResponse}';
  }


}

class ProductElectronicNotLoaded extends ProductElectronicState {
  final DioStatus status;

  ProductElectronicNotLoaded({this.status});

  @override
  String toString() {
    return 'ProductElectronicNotLoaded{error: $status}';
  }
}