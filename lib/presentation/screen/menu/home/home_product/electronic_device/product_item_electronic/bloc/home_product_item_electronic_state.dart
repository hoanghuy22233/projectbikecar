import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/model/entity/products.dart';

abstract class HomeProductEtronicState extends Equatable {
  const HomeProductEtronicState();

  @override
  List<Object> get props => [];
}

class HomeProductEtronicNotLoaded extends HomeProductEtronicState {}

class HomeProductEtronicLoaded extends HomeProductEtronicState {
  final List<Products> products;
  final bool hasReachedMax;

  HomeProductEtronicLoaded({@required this.products,this.hasReachedMax});

  @override
  List<Object> get props => [products,hasReachedMax];
}
