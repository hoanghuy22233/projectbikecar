import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/model/entity/products.dart';
import 'package:project_bike_car/presentation/screen/menu/home/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/bike/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/electronic_device/bloc/bloc.dart';

import 'bloc.dart';

class HomeProductEtronicBloc extends Bloc<HomeProductEtronicEvent, HomeProductEtronicState>{
  final ProductElectronicBloc productBloc;
  StreamSubscription subscription;

  HomeProductEtronicBloc({@required this.productBloc}) {
    subscription = productBloc.listen((state) {
      if (state is ProductElectronicLoaded) {
        add(DisplayHomeProductEtronic(product: state.homeResponse.data.products, hasReachedMax: false));
      }
    });
  }

  @override
  HomeProductEtronicState get initialState => HomeProductEtronicNotLoaded();

  @override
  Stream<HomeProductEtronicState> mapEventToState(HomeProductEtronicEvent event) async* {
    if(event is DisplayHomeProductEtronic){
       List<Products> products = List();
      for(int i=0;i<event.product.length;i++){
        products .add(event.product[i]);
      }
      yield* _mapDisplayHomeProductEtronicToState(products);
    }
  }

  Stream<HomeProductEtronicState> _mapDisplayHomeProductEtronicToState(List<Products> product) async* {
    yield HomeProductEtronicLoaded(products: product, hasReachedMax: false);
  }

  @override
  Future<void> close() {
    subscription.cancel();
    return super.close();
  }
}