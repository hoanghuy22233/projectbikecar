
import 'package:equatable/equatable.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/model/entity/products.dart';

class HomeProductEtronicEvent extends Equatable {
  const HomeProductEtronicEvent();
  @override
  List<Object> get props => [];
}

class DisplayHomeProductEtronic extends HomeProductEtronicEvent {
  final List<Products> product;
  final bool hasReachedMax;

  const DisplayHomeProductEtronic({this.product,this.hasReachedMax});

  @override
  List<Object> get props => [product,hasReachedMax];
}