import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/model/entity/products.dart';

abstract class HomeProductState extends Equatable {
  const HomeProductState();

  @override
  List<Object> get props => [];
}

class HomeProductNotLoaded extends HomeProductState {}

class HomeProductLoaded extends HomeProductState {
  final List<Products> products;
  final bool hasReachedMax;

  HomeProductLoaded({@required this.products,this.hasReachedMax});

  @override
  List<Object> get props => [products,hasReachedMax];
}
