import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/model/entity/products.dart';
import 'package:project_bike_car/presentation/screen/menu/home/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/bloc/bloc.dart';

import 'bloc.dart';

class HomeProductBloc extends Bloc<HomeProductEvent, HomeProductState>{
  final ProductBloc productBloc;
  StreamSubscription subscription;

  HomeProductBloc({@required this.productBloc}) {
    subscription = productBloc.listen((state) {
      if (state is ProductLoaded) {
        add(DisplayHomeProduct(product: state.homeResponse.data.products, hasReachedMax: false));
      }
    });
  }

  @override
  HomeProductState get initialState => HomeProductNotLoaded();

  @override
  Stream<HomeProductState> mapEventToState(HomeProductEvent event) async* {
    if(event is DisplayHomeProduct){
       List<Products> products = List();
      for(int i=0;i<event.product.length;i++){
        products .add(event.product[i]);
      }
      yield* _mapDisplayHomeProductToState(products);
    }
  }

  Stream<HomeProductState> _mapDisplayHomeProductToState(List<Products> product) async* {
    yield HomeProductLoaded(products: product, hasReachedMax: false);
  }

  @override
  Future<void> close() {
    subscription.cancel();
    return super.close();
  }
}