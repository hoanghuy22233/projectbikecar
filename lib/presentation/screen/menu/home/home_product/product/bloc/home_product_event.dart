
import 'package:equatable/equatable.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/model/entity/products.dart';

class HomeProductEvent extends Equatable {
  const HomeProductEvent();
  @override
  List<Object> get props => [];
}

class DisplayHomeProduct extends HomeProductEvent {
  final List<Products> product;
  final bool hasReachedMax;

  const DisplayHomeProduct({this.product,this.hasReachedMax});

  @override
  List<Object> get props => [product,hasReachedMax];
}