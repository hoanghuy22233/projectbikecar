import 'package:flutter/material.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/model/entity/products.dart';
import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';

class WidgetProductGrid extends StatelessWidget {
  final Products product;

  const WidgetProductGrid(
      {Key key, this.product})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    const double appPadding = 20.0;
    return GestureDetector(
      onTap: () {
        AppNavigator.detailProduct(
            productId: product.id);
      },
      child: Stack(
        children: [
          Padding(
            padding: EdgeInsets.only(
              right: appPadding / 2,
              bottom: appPadding,
            ),
            child: Container(
              width: size.width * 0.55,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.2),
                    offset: Offset(5, 5),
                    blurRadius: 10,
                  )
                ],
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Stack(
                    alignment: Alignment.topLeft,
                    children: [
                      Container(
                        width: size.width,
                        height: size.height * 0.23,
                        child: WidgetCachedImage(
                          url: product.image,
                        ),
                      ),
                      product.views!=0?
                      Container(
                        padding: EdgeInsets.only(top: 5),
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            Image.asset("assets/images/black-suitcase.png",height: 30,width: 30,color: Colors.grey,),
                            Container(
                                child:  product.views != 0
                                    ? Text(
                                 product.views.toString().length < 4
                                        ? product.views.toString()
                                        : product.views.toString().substring(0, 1) + 'k',     style: AppStyle.DEFAULT_SMALL
                                    .copyWith(color: AppColor.WHITE,fontSize: 10,fontWeight: FontWeight.bold)
                                )
                                    : Container()),
                          ],
                        ),
                      ):Container()
                    ],
                  ),

                  Container(
                    margin: EdgeInsets.symmetric(
                        horizontal: 10, vertical: 10),
                    child: Text(
                      product.name,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: AppStyle.DEFAULT_MEDIUM.copyWith(
                          color: AppColor.PRIMARY,
                          fontWeight: FontWeight.bold),
                      textAlign: TextAlign.start,
                    ),
                  ),
                  product.priceOld != 0
                      ? Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: Text(
                      '${AppValue.APP_MONEY_FORMAT.format(product.priceOld)}',
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: Colors.red),
                    ),
                  )
                      : Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: Text(
                      '${AppValue.APP_MONEY_FORMAT.format(product.price)}',
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: Colors.red),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            right: appPadding * 1.5,
            bottom: appPadding / 2,
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(20),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.5),
                      offset: Offset(3, 3),
                      blurRadius: 3,
                    )
                  ]),
              padding: EdgeInsets.symmetric(
                vertical: appPadding / 4,
                horizontal: appPadding / 1.5,
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Giảm ",
                    style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                  product.price != 0
                      ? Text(
                    '${(((product.price - product.priceOld) / product.priceOld) * 100).floor()}%',
                    style: AppStyle.APP_MEDIUM.copyWith(
                        color: AppColor.RED,
                        fontWeight: FontWeight.w800),
                  )
                      : Text(
                    '0 %',
                    style: AppStyle.APP_MEDIUM.copyWith(
                        color: AppColor.RED,
                        fontWeight: FontWeight.w800),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
