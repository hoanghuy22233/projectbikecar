import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/model/entity/products.dart';

abstract class HomeProductPhoneState extends Equatable {
  const HomeProductPhoneState();

  @override
  List<Object> get props => [];
}

class HomeProductPhoneNotLoaded extends HomeProductPhoneState {}

class HomeProductPhoneLoaded extends HomeProductPhoneState {
  final List<Products> products;
  final bool hasReachedMax;

  HomeProductPhoneLoaded({@required this.products,this.hasReachedMax});

  @override
  List<Object> get props => [products,hasReachedMax];
}
