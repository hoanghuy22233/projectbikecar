import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/model/entity/products.dart';
import 'package:project_bike_car/presentation/screen/menu/home/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/bike/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/car/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/gps/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/phone/bloc/bloc.dart';

import 'bloc.dart';

class HomeProductPhoneBloc extends Bloc<HomeProductPhoneEvent, HomeProductPhoneState>{
  final ProductPhoneBloc productBloc;
  StreamSubscription subscription;

  HomeProductPhoneBloc({@required this.productBloc}) {
    subscription = productBloc.listen((state) {
      if (state is ProductPhoneLoaded) {
        add(DisplayHomeProductPhone(product: state.homeResponse.data.products, hasReachedMax: false));
      }
    });
  }

  @override
  HomeProductPhoneState get initialState => HomeProductPhoneNotLoaded();

  @override
  Stream<HomeProductPhoneState> mapEventToState(HomeProductPhoneEvent event) async* {
    if(event is DisplayHomeProductPhone){
       List<Products> products = List();
      for(int i=0;i<event.product.length;i++){
        products .add(event.product[i]);
      }
      yield* _mapDisplayHomeProductPhoneToState(products);
    }
  }

  Stream<HomeProductPhoneState> _mapDisplayHomeProductPhoneToState(List<Products> product) async* {
    yield HomeProductPhoneLoaded(products: product, hasReachedMax: false);
  }

  @override
  Future<void> close() {
    subscription.cancel();
    return super.close();
  }
}