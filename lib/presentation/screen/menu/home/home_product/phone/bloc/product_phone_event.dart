import 'package:equatable/equatable.dart';

class ProductPhoneEvent extends Equatable {
  const ProductPhoneEvent();

  List<Object> get props => [];
}

class LoadProductPhone extends ProductPhoneEvent {
  final int categoryId;
  final int brandsId;
  final String sort;

  LoadProductPhone(this.categoryId,this.brandsId,this.sort);

  List<Object> get props => [categoryId,brandsId,sort];

  @override
  String toString() {
    return 'LoadPProductPhone{ CarcategoryId: $categoryId, brandsId:$brandsId,sort:$sort}';
  }
}

class RefreshProductPhone extends ProductPhoneEvent {
  final int categoryId;
  final int brandsId;
  final String sort;

  RefreshProductPhone(this.categoryId,this.brandsId,this.sort);

  List<Object> get props => [categoryId,brandsId,sort];

  @override
  String toString() {
    return 'RefreshProductPhone{categoryId: $categoryId,brandsId:$brandsId,sort:$sort';
  }

}
