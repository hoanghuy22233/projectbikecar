import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:project_bike_car/model/repo/barrel_repo.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

import 'bloc.dart';

class ProductPhoneBloc extends Bloc<ProductPhoneEvent, ProductPhoneState> {
  final UserRepository userRepository;

  ProductPhoneBloc({ this.userRepository});

  @override
  ProductPhoneState get initialState => ProductPhoneLoading();

  @override
  Stream<ProductPhoneState> mapEventToState(ProductPhoneEvent event) async* {
    if (event is LoadProductPhone) {
      yield* _mapLoadProductPhoneToState(categoryId: event.categoryId,brandsId: event.brandsId,sort: event.sort);
    } else if (event is RefreshProductPhone) {
      yield ProductPhoneLoading();
      yield* _mapLoadProductPhoneToState(categoryId: event.categoryId,brandsId: event.brandsId,sort:event.sort);
    }
  }

  Stream<ProductPhoneState> _mapLoadProductPhoneToState({ @required int categoryId,@required int brandsId,@required String sort,}) async* {
    try {
      final homeResponse = await userRepository.getHomes(categoryId: categoryId,brandsId: brandsId,sort: sort);
      print('-----------------');
      print(homeResponse);
      await Future.delayed(Duration(milliseconds: 500), () {});
      yield ProductPhoneLoaded(homeResponse: homeResponse);
    } catch (e) {
      yield ProductPhoneNotLoaded(status: DioErrorUtil.handleError(e));
    }
  }
}
