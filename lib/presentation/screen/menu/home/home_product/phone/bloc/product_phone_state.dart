import 'package:equatable/equatable.dart';
import 'package:project_bike_car/model/api/response/home_response.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

abstract class ProductPhoneState extends Equatable {
  const ProductPhoneState();

  @override
  List<Object> get props => [];
}

class ProductPhoneLoading extends ProductPhoneState {}

class ProductPhoneLoaded extends ProductPhoneState {
  final HomeResponse homeResponse;

  const ProductPhoneLoaded({this.homeResponse});

  @override
  List<Object> get props => [homeResponse];

  @override
  String toString() {
    return 'ProductPhoneLoaded{homeResponse: $homeResponse}';
  }


}

class ProductPhoneNotLoaded extends ProductPhoneState {
  final DioStatus status;

  ProductPhoneNotLoaded({this.status});

  @override
  String toString() {
    return 'ProductPhoneNotLoaded{error: $status}';
  }
}