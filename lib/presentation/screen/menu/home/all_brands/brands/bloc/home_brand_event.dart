
import 'package:equatable/equatable.dart';
import 'package:project_bike_car/model/entity/brands.dart';

class HomeBrandEvent extends Equatable {
  const HomeBrandEvent();
  @override
  List<Object> get props => [];
}

class DisplayHomeBrand extends HomeBrandEvent {
  final List<Brands> brand;

  const DisplayHomeBrand({this.brand,});

  @override
  List<Object> get props => [brand];
}