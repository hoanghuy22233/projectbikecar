import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:project_bike_car/model/entity/brands.dart';
import 'package:project_bike_car/presentation/screen/menu/home/all_brands/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/bloc/bloc.dart';

import 'bloc.dart';

class HomeBrandBloc extends Bloc<HomeBrandEvent, HomeBrandState>{
  final BrandBloc brandBloc;
  StreamSubscription subscription;

  HomeBrandBloc({@required this.brandBloc}) {
    subscription = brandBloc.listen((state) {
      if (state is BrandLoaded) {
        add(DisplayHomeBrand(brand: state.homeResponse.data.brands,));
      }
    });
  }

  @override
  HomeBrandState get initialState => HomeBrandNotLoaded();

  @override
  Stream<HomeBrandState> mapEventToState(HomeBrandEvent event) async* {
    if(event is DisplayHomeBrand){
       List<Brands> brands = List();
      for(int i=0;i<event.brand.length;i++){
        brands .add(event.brand[i]);
      }

      yield* _mapDisplayHomeBrandToState(brands);
    }
  }

  Stream<HomeBrandState> _mapDisplayHomeBrandToState(List<Brands> brands,) async* {
    yield HomeBrandLoaded(brand: brands,);
  }

  @override
  Future<void> close() {
    subscription.cancel();
    return super.close();
  }
}