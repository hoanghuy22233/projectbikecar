
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:project_bike_car/model/entity/brands.dart';

abstract class HomeBrandState extends Equatable {
  const HomeBrandState();

  @override
  List<Object> get props => [];
}

class HomeBrandNotLoaded extends HomeBrandState {}

class HomeBrandLoaded extends HomeBrandState {
  final List<Brands> brand;

  HomeBrandLoaded({@required this.brand,});

  @override
  List<Object> get props => [brand,];
}
