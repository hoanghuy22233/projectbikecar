import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:project_bike_car/model/repo/barrel_repo.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

import 'bloc.dart';

class BrandBloc extends Bloc<BrandEvent, BrandState> {
  final UserRepository userRepository;

  BrandBloc({ this.userRepository});

  @override
  BrandState get initialState => BrandLoading();

  @override
  Stream<BrandState> mapEventToState(BrandEvent event) async* {
    if (event is LoadBrand) {
      yield* _mapLoadBrandToState(categoryId: event.categoryId,brandsId: event.brandsId,sort:event.sort);
    } else if (event is RefreshBrand) {
      yield BrandLoading();
      yield* _mapLoadBrandToState(categoryId: event.categoryId,brandsId: event.brandsId,sort: event.sort);
    }
  }

  Stream<BrandState> _mapLoadBrandToState({ @required int categoryId,@required int brandsId,@required String sort,}) async* {
    try {
      final homeResponse = await userRepository.getHomes(categoryId: categoryId,brandsId: brandsId,sort: sort);
      print('-----------------');
      print(homeResponse);
      await Future.delayed(Duration(milliseconds: 500), () {});
      yield BrandLoaded(homeResponse: homeResponse);
    } catch (e) {
      yield BrandNotLoaded(status: DioErrorUtil.handleError(e));
    }
  }
}
