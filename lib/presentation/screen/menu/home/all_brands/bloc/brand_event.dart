import 'package:equatable/equatable.dart';

class BrandEvent extends Equatable {
  const BrandEvent();

  List<Object> get props => [];
}

class LoadBrand extends BrandEvent {
  final int categoryId;
  final int brandsId;
  final String sort;

  LoadBrand(this.categoryId,this.brandsId,this.sort);

  List<Object> get props => [categoryId,brandsId,sort];

  @override
  String toString() {
    return 'LoadBrand{categoryId: $categoryId, brandsId:$brandsId,sort:$sort}';
  }
}

class RefreshBrand extends BrandEvent {
  final int categoryId;
  final int brandsId;
  final String sort;

  RefreshBrand(this.categoryId,this.brandsId,this.sort);

  List<Object> get props => [categoryId,brandsId,sort];

  @override
  String toString() {
    return 'RefreshBrand{categoryId: $categoryId,brandsId:$brandsId,sort:$sort}';
  }

}
