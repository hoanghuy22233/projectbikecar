import 'package:equatable/equatable.dart';
import 'package:project_bike_car/model/api/response/home_response.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

abstract class BrandState extends Equatable {
  const BrandState();

  @override
  List<Object> get props => [];
}

class BrandLoading extends BrandState {}

class BrandLoaded extends BrandState {
  final HomeResponse homeResponse;

  const BrandLoaded({this.homeResponse});

  @override
  List<Object> get props => [homeResponse];

  @override
  String toString() {
    return 'BrandLoaded{homeResponse: $homeResponse}';
  }


}

class BrandNotLoaded extends BrandState {
  final DioStatus status;

  BrandNotLoaded({this.status});

  @override
  String toString() {
    return 'BrandNotLoaded{error: $status}';
  }
}