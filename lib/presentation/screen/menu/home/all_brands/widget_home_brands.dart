import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/model/entity/brands.dart';
import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/bloc/bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'bloc/brand_bloc.dart';
import 'brands/bloc/bloc.dart';

class WidgetHomeBrandMenu extends StatefulWidget {
  final Function(Brands brands, int) onCategoryClick;
  final int brandId;
  final int categoryId;

  WidgetHomeBrandMenu({
    Key key,
    this.onCategoryClick,this.brandId,this.categoryId
  }) : super(key: key);
  @override
  _WidgetHomeBrandMenuState createState() =>
      _WidgetHomeBrandMenuState(brandId,categoryId);
}

class _WidgetHomeBrandMenuState
    extends State<WidgetHomeBrandMenu> {
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  int _selectedIndex =0;
  int brandId;
  int categoryId;


  _WidgetHomeBrandMenuState(this.brandId,this.categoryId);

  _onSelected(int index) {
    setState(() => _selectedIndex = index);
  }
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBrandBloc, HomeBrandState>(
      builder: (context, state) {
        if (state is HomeBrandLoaded) {
          return  Container(
            child: _buildBrands(state),
          );
        } else {
          return Center(child: Container(
            height: 100,
            width: 100,
            child: Lottie.asset(
              'assets/lottie/load_car.json',
            ),
          ));
        }
      },
    );
  }

  Widget _buildBrands(HomeBrandLoaded state) {
    if(state.brand.length==0){

      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
                height: 60,
                width: 60,
                child: Image.asset('assets/images/box.png')),
            SizedBox(
              height: 20,
            ),
            Text('Không có thương hiệu nào?'),
            SizedBox(height: 10,)
          ],
        ),
      );
    }
    else{
      return Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height / 7,
        child: ListView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemCount: state.brand.length+1,
            itemBuilder: (BuildContext context, int index) {
              if (index == 0) {
                return Padding(
                  padding: const EdgeInsets.only(right: 15,left: 20),
                  child: Column(
                    children: [
                      GestureDetector(
                        onTap: () {
                          _onSelected(index);
                          brandId=0;
                          print("_____________________");
                          print(brandId);
                          BlocProvider.of<ProductBloc>(context).add(RefreshProduct(categoryId??0,brandId??0,'id_desc'));
                        },
                        child:                           Container(
                          width: MediaQuery.of(context).size.height / 9,
                          height: MediaQuery.of(context).size.height / 9,
                          // height: 100,
                          decoration: BoxDecoration(
                              color: _selectedIndex != null &&
                                  _selectedIndex == index
                                  ? Color(0xff66CC99)
                                  : Colors.grey[200],
                              border: Border.all(  color: _selectedIndex != null &&
                                  _selectedIndex == index
                                  ? Colors.lightBlue
                                  : Colors.grey[200],width: 2),
                              borderRadius:
                              BorderRadius.all(Radius.circular(5))),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Image.asset(
                                "assets/images/ic_grid.png",
                                width: 55,
                                height: 55,
                              ),
                              SizedBox(height: 5,),
                              Text("Tất cả",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 10),),
                            ],
                          ),
                        ),

                      ),
                    ],
                  ),
                );
              }
              return Padding(
                padding: const EdgeInsets.only(right: 15),
                child: Column(
                  children: [
                    GestureDetector(
                      onTap: () {
                        _onSelected(index);
                        brandId=state.brand[index-1].id;
                        print("_____________________");
                        print(brandId);
                        BlocProvider.of<ProductBloc>(context).add(RefreshProduct(categoryId??0,brandId??0,'id_desc'));
                      },
                      child:                           Container(
                        width: MediaQuery.of(context).size.height / 9,
                        height: MediaQuery.of(context).size.height / 9,
                        // height: 100,
                        decoration: BoxDecoration(
                            color: _selectedIndex != null &&
                                _selectedIndex == index
                                ? Color(0xff66CC99)
                                : Colors.grey[200],
                            border: Border.all(  color: _selectedIndex != null &&
                                _selectedIndex == index
                                ? Colors.lightBlue
                                : Colors.grey[200],width: 2),
                            borderRadius:
                            BorderRadius.all(Radius.circular(5))),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              height: 50,width: 50,
                              child: FractionallySizedBox(
                                widthFactor: 0.9,
                                heightFactor: 0.9,
                                child: AspectRatio(
                                  aspectRatio: 1,
                                  child: WidgetCachedImage(
                                    url: state.brand[index-1].image,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(height: 5,),
                            Container(
                                child:  state.brand[index-1].name != null
                                    ? Text(
                                    state.brand[index-1].name.toString().length < 10
                                        ? state.brand[index-1].name.toString()
                                        : state.brand[index-1].name.toString().substring(0, 10) + '...',     style: TextStyle(fontWeight: FontWeight.bold,fontSize: 10)
                                )
                                    : Container()),
                          ],
                        ),
                      ),

                    ),
                    SizedBox(
                      height: 10,
                    ),

                  ],
                ),
              );
            }),
      );
    }

  }


}
