import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:project_bike_car/model/repo/barrel_repo.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

import 'bloc.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final UserRepository userRepository;

  HomeBloc({ this.userRepository});

  @override
  HomeState get initialState => HomeLoading();

  @override
  Stream<HomeState> mapEventToState(HomeEvent event) async* {
    if (event is LoadHome) {
      yield* _mapLoadHomeToState(categoryId: event.categoryId,brandsId: event.brandsId);
    } else if (event is RefreshHome) {
      yield HomeLoading();
      yield* _mapLoadHomeToState(categoryId: event.categoryId,brandsId: event.brandsId);
    }
  }

  Stream<HomeState> _mapLoadHomeToState({ @required int categoryId,@required int brandsId,}) async* {
    try {
      final homeResponse = await userRepository.getHomes(categoryId: categoryId,brandsId: brandsId);
      print('-----------------');
      print(homeResponse);
      await Future.delayed(Duration(milliseconds: 500), () {});
      yield HomeLoaded(homeResponse: homeResponse);
    } catch (e) {
      yield HomeNotLoaded(status: DioErrorUtil.handleError(e));
    }
  }
}
