import 'package:equatable/equatable.dart';

class HomeEvent extends Equatable {
  const HomeEvent();

  List<Object> get props => [];
}

class LoadHome extends HomeEvent {
  final int categoryId;
  final int brandsId;
  final String sort;

  LoadHome(this.categoryId,this.brandsId,this.sort);

  List<Object> get props => [categoryId,brandsId,sort];

  @override
  String toString() {
    return 'LoadHome{categoryId: $categoryId, brandsId:$brandsId, sort:$sort}';
  }
}

class RefreshHome extends HomeEvent {
  final int categoryId;
  final int brandsId;
  final String sort;

  RefreshHome(this.categoryId,this.brandsId,this.sort);

  List<Object> get props => [categoryId,brandsId,sort];

  @override
  String toString() {
    return 'RefreshHome{categoryId: $categoryId,brandsId:$brandsId,sort:$sort}';
  }

}
