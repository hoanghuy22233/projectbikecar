import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/model/entity/brands.dart';
import 'package:project_bike_car/model/entity/category.dart';

abstract class HomeCategoryState extends Equatable {
  const HomeCategoryState();

  @override
  List<Object> get props => [];
}

class HomeCategoryNotLoaded extends HomeCategoryState {}

class HomeCategoryLoaded extends HomeCategoryState {
  final List<Category> category;


  HomeCategoryLoaded({@required this.category,});

  @override
  List<Object> get props => [category];
}
