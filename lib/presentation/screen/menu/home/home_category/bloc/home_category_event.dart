
import 'package:equatable/equatable.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/model/entity/brands.dart';
import 'package:project_bike_car/model/entity/category.dart';

class HomeCategoryEvent extends Equatable {
  const HomeCategoryEvent();
  @override
  List<Object> get props => [];
}

class DisplayHomeCategory extends HomeCategoryEvent {
  final List<Category> category;


  const DisplayHomeCategory({this.category,});

  @override
  List<Object> get props => [category,];
}
