import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:project_bike_car/model/entity/banner.dart';
import 'package:project_bike_car/model/entity/brands.dart';
import 'package:project_bike_car/model/entity/category.dart';
import 'package:project_bike_car/presentation/screen/menu/home/bloc/bloc.dart';

import 'bloc.dart';

class HomeCategoryBloc extends Bloc<HomeCategoryEvent, HomeCategoryState>{
  final HomeBloc homeBloc;
  StreamSubscription subscription;

  HomeCategoryBloc({@required this.homeBloc}) {
    subscription = homeBloc.listen((state) {
      if (state is HomeLoaded) {
        add(DisplayHomeCategory(category: state.homeResponse.data.category,));
      }
    });
  }

  @override
  HomeCategoryState get initialState => HomeCategoryNotLoaded();

  @override
  Stream<HomeCategoryState> mapEventToState(HomeCategoryEvent event) async* {
    if(event is DisplayHomeCategory){
       List<Category> category = List();
      for(int i=0;i<event.category.length;i++){
        category .add(event.category[i]);
      }


      yield* _mapDisplayHomeCategoryToState(category,);
    }

  }


  Stream<HomeCategoryState> _mapDisplayHomeCategoryToState(List<Category> category,) async* {
    yield HomeCategoryLoaded(category: category,);
  }

  @override
  Future<void> close() {
    subscription.cancel();
    return super.close();
  }
}