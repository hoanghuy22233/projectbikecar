import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/model/entity/category.dart';
import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:project_bike_car/presentation/screen/all_product/sc_product.dart';
import 'package:project_bike_car/presentation/screen/menu/home/all_brands/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/bloc/bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'bloc/bloc.dart';

class WidgetHomeCategoriesItemMenu extends StatefulWidget {
  final Function(Category category, int) onCategoryClick;
  final int categoryId;
  final int brandId;

  WidgetHomeCategoriesItemMenu(
      {Key key, this.onCategoryClick, this.categoryId, this.brandId})
      : super(key: key);
  @override
  _WidgetHomeCategoriesItemMenuState createState() =>
      _WidgetHomeCategoriesItemMenuState(
          categoryId: categoryId, brandId: brandId);
}

class _WidgetHomeCategoriesItemMenuState
    extends State<WidgetHomeCategoriesItemMenu> {
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  int _selectedIndex = 0;
  int categoryId;
  int brandId;

  _WidgetHomeCategoriesItemMenuState({this.categoryId, this.brandId});

  _onSelected(int index) {
    setState(() => _selectedIndex = index);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeCategoryBloc, HomeCategoryState>(
      builder: (context, state) {
        if (state is HomeCategoryLoaded) {
          return Container(
            height: MediaQuery.of(context).size.width*0.3,
            child: _buildSlider(state),
          );
        } else {
          return Center(
              child: Container(
            height: 100,
            width: 100,
            child: Lottie.asset(
              'assets/lottie/load_car.json',
            ),
          ));
        }
      },
    );
  }

  Widget _buildSlider(HomeCategoryLoaded state) {
    if (state?.category != null) {
      return ListView.builder(
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemCount: state.category.length + 1,
          itemBuilder: (BuildContext context, int index) {
            if (index == 0) {
              return Padding(
                padding: const EdgeInsets.only(right: 15, left: 20),
                child: Column(
                  children: [
                    GestureDetector(
                      onTap: () async {
                        _onSelected(index);
                        categoryId = 0;
                        print("_____________________");
                        print(categoryId);
                        BlocProvider.of<BrandBloc>(context)
                            .add(RefreshBrand(categoryId ?? 0, 0, 'id_desc'));
                        BlocProvider.of<ProductBloc>(context).add(
                            RefreshProduct(categoryId ?? 0, 0, 'id_desc'));
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ProductPageScreen(
                                categoryId:0,

                              )),
                        );
                      },
                      child: Stack(
                        children: [
                          Container(
                            width: 60,
                            height: 60,
                            padding: EdgeInsets.all(5),
                            decoration: BoxDecoration(
                                color: _selectedIndex != null &&
                                        _selectedIndex == index
                                    ? Color(0xffccf2f3)
                                    : Colors.grey[300],
                                borderRadius:
                                    BorderRadius.all(Radius.circular(500))),
                            child: Image.asset(
                              "assets/images/ic_grid.png",
                              width: 35,
                              height: 35,
                              color: Color(0xff33CC66),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Tất cả",
                      style: TextStyle(fontSize: 12),
                    ),
                  ],
                ),
              );
            }
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: Column(
                children: [
                  GestureDetector(
                    onTap: () async {
                      _onSelected(index);
                      categoryId = state.category[index - 1].id;
                      print("_____________________");
                      print(categoryId);
                      BlocProvider.of<BrandBloc>(context).add(RefreshBrand(
                          categoryId ?? 0, brandId ?? 0, 'id_desc'));
                      BlocProvider.of<ProductBloc>(context).add(RefreshProduct(
                          categoryId ?? 0, brandId ?? 0, 'id_desc'));

                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ProductPageScreen(
                              categoryId:categoryId,

                            )),
                      );
                    },
                    child: Stack(
                      children: [
                        Container(
                          width: 60,
                          height: 60,
                          padding: EdgeInsets.all(5),
                          decoration: BoxDecoration(
                              color: _selectedIndex != null &&
                                      _selectedIndex == index
                                  ? Color(0xffccf2f3)
                                  : Colors.grey[200],
                              borderRadius:
                                  BorderRadius.all(Radius.circular(500))),
                          child:
                          state.category[index - 1].image!=null?
                          FractionallySizedBox(
                            widthFactor: 0.8,
                            heightFactor: 0.8,
                            child: AspectRatio(
                              aspectRatio: 1,
                              child: WidgetCachedImage(
                                url: state.category[index - 1].image,
                              ),
                            ),
                          ):FractionallySizedBox(
                            widthFactor: 0.8,
                            heightFactor: 0.8,
                            child: AspectRatio(
                              aspectRatio: 1,
                              child: Image.asset("assets/images/logo_car_two.png"),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),

                  Container(
                    width:MediaQuery.of(context).size.width/6,
                    child: Text(
                      state.category[index - 1].name,maxLines: 2, textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 12),
                    ),
                  )
                ],
              ),
            );
          });
    } else {
      return Center(
        child: Text("Không có danh mục nào?"),
      );
    }
  }
}
