import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:project_bike_car/utils/locale/app_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WidgetSearchAppbar extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Container(
        child: WidgetAppbar(

          title: AppLocalizations.of(context).translate('search_appbar.title'),
          left: [
            BackButton(
              color: Colors.white,
            )
          ],

        )
    );

  }}