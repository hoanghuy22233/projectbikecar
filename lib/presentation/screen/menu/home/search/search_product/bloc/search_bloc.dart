import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:project_bike_car/model/repo/barrel_repo.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

import 'bloc.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState> {
  final UserRepository userRepository;

  SearchBloc({@required this.userRepository});

  @override
  SearchState get initialState => SearchState.empty();

  @override
  Stream<SearchState> mapEventToState(SearchEvent event) async* {
    if (event is LoadSearch) {
      yield* _mapLoadSearchToState();
    } else if (event is RefreshSearch) {
      yield SearchState.loading(state.copyWith(
          status: DioStatus(
        code: DioStatus.API_PROGRESS,
      )));
      yield* _mapLoadSearchToState();
    }
  }

  Stream<SearchState> _mapLoadSearchToState() async* {
    try {
      final response = await userRepository.getHome();
      yield SearchState.success(state.update(
          product: response.data.products,
          status:
              DioStatus(code: DioStatus.API_SUCCESS, message: response.msg)));
    } catch (e) {
      yield SearchState.failure(
          state.update(status: DioErrorUtil.handleError(e)));
    }
  }

}
