import 'package:meta/meta.dart';
import 'package:project_bike_car/model/entity/products.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';


class SearchState {
  final List<Products> product;
  final bool isLoading;
  final bool isSuccess;
  final bool isFailure;
  final DioStatus status;

  SearchState(
      {@required this.product,
      @required this.isLoading,
      @required this.isSuccess,
      @required this.isFailure,
      @required this.status});

  factory SearchState.empty() {
    return SearchState(
        product: null,
        isLoading: false,
        isSuccess: false,
        isFailure: false,
        status: null);
  }

  factory SearchState.loading(SearchState state) {
    return SearchState(
        product: state.product,
        isLoading: true,
        isSuccess: false,
        isFailure: false,
        status: state.status);
  }

  factory SearchState.failure(SearchState state) {
    return SearchState(
        product: state.product,
        isLoading: false,
        isSuccess: false,
        isFailure: true,
        status: state.status);
  }

  factory SearchState.success(SearchState state) {
    return SearchState(
        product: state.product,
        isLoading: false,
        isSuccess: true,
        isFailure: false,
        status: state.status);
  }

  SearchState update(
      {List<Products> product,
      bool isLoading,
      bool isSuccess,
      bool isFailure,
      DioStatus status}) {
    return copyWith(
      product: product,
      isLoading: false,
      isSuccess: false,
      isFailure: false,
      status: status,
    );
  }

  SearchState copyWith({
    List<Products> product,
    bool isLoading,
    bool isSuccess,
    bool isFailure,
    DioStatus status,
  }) {
    return SearchState(
      product: product?? this.product,
      isLoading: isLoading ?? this.isLoading,
      isSuccess: isSuccess ?? this.isSuccess,
      isFailure: isFailure ?? this.isFailure,
      status: status ?? this.status,
    );
  }

  @override
  String toString() {
    return 'SearchState{product: $product, isLoading: $isLoading, isSuccess: $isSuccess, isFailure: $isFailure, status: $status}';
  }
}
