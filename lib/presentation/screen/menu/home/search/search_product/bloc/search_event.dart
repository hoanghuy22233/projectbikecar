import 'package:equatable/equatable.dart';

class SearchEvent extends Equatable {
  const SearchEvent();

  List<Object> get props => [];
}

class LoadSearch extends SearchEvent {



  LoadSearch();

  List<Object> get props => [];

  @override
  String toString() {
    return 'LoadNews{}';
  }
}

class RefreshSearch extends SearchEvent {


  RefreshSearch();

  List<Object> get props => [];

  @override
  String toString() {
    return 'RefreshSearch{}';
  }
}

