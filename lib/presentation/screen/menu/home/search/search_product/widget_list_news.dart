import 'package:date_format/date_format.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/model/entity/products.dart';
import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/widget_product_grid.dart';


class WidgetListNews extends StatefulWidget {
  final List<Products> products;
  WidgetListNews({this.products});

  @override
  _WidgetListNewsState createState() => _WidgetListNewsState();
}

class _WidgetListNewsState extends State<WidgetListNews> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final itemHeight =size.height /1.2;
    final itemWidth = size.width;
    return widget.products.length !=0?GridView.builder(
      shrinkWrap: true,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        childAspectRatio: (itemWidth/itemHeight),
      ),
      physics: BouncingScrollPhysics(),
      itemCount: widget.products.length,
      itemBuilder: (context, index) {
        return WidgetProductGrid(product: widget.products[index],);
      },
    )
        : Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
              height: 120,
              width: 120,
              child: Image.asset('assets/images/box.png')),
          SizedBox(
            height: 20,
          ),
          Text('Không có sản phẩm nào?'),
        ],
      ),
    );
  }


}
