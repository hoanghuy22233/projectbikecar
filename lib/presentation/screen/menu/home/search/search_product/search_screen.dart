import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/model/entity/products.dart';
import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:project_bike_car/presentation/screen/menu/home/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/search/search_product/widget_list_news.dart';
import 'package:project_bike_car/presentation/screen/menu/home/search/search_product/widget_search_appar.dart';
import 'package:project_bike_car/utils/handler/barrel_handler.dart';

import 'bloc/bloc.dart';

class SearchScreen extends StatefulWidget {
  const SearchScreen();

  @override
  _SearchScreenState createState() => new _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen>
    with SingleTickerProviderStateMixin {
  static final GlobalKey<ScaffoldState> scaffoldKey =
      new GlobalKey<ScaffoldState>();

  TextEditingController _searchQuery;
  bool _isSearching = false;

  List<Products> filteredRecored;
  List<Products> allRecord;
  String msgStatus = '';
  bool _isVisible = true;


  @override
  void initState() {
    super.initState();
    _searchQuery = new TextEditingController();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      setState(() => _isSearching = true);
      BlocProvider.of<SearchBloc>(context).add(LoadSearch());
    });
  }
  Widget _buildTitle(BuildContext context) {
    var horizontalTitleAlignment =
        Platform.isIOS ? CrossAxisAlignment.center : CrossAxisAlignment.start;

    return MaterialApp(
      home: new InkWell(
        onTap: () => scaffoldKey.currentState.openDrawer(),
        child: new Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12.0),
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: horizontalTitleAlignment,
            children: <Widget>[
              new Text(
                'Nhập giá và sản phẩm tìm kiếm',
                style: new TextStyle(color: Colors.white),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildSearchField() {
    return Container(
      width: MediaQuery.of(context).size.width - 100,
      decoration: BoxDecoration(
        color: Color(0xff66CC99),
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
      ),
      height: 50,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        // crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(flex: 1, child: Container()),
          Image.asset(
            "assets/images/img_search.png",
            color: Colors.white,
            width: 15,
            height: 15,
          ),
          SizedBox(
            width: 10,
          ),

          Expanded(
            flex: 7,
            child:    StreamBuilder(
              stream: null,
              builder: (context, snapshot) => TextField(
                style: TextStyle(color: Colors.white),
                controller: _searchQuery,
                autofocus: true,

                onTap: () {
                  showToast();
                },
                // keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    errorText: snapshot.hasError ? snapshot.error : null,
                    enabled: true,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    border: InputBorder.none,
                    hintText: 'Nhập giá và sản phẩm tìm kiếm',
                    hintStyle: TextStyle(color: Colors.white)),
                onChanged: updateSearchQuery,
              ),
            ) ,
          )
        ],
      ),
    );
  }

  void updateSearchQuery(String newQuery) {
    filteredRecored.clear();
    if (newQuery.length > 0) {
      Set<Products> set = Set.from(allRecord);
      set.forEach((element) => filterList(element, newQuery));
    }

    if (newQuery.isEmpty) {
      filteredRecored.addAll(allRecord);
    }

    setState(() {
    });
  }

  filterList(Products item, String searchQuery) {
    setState(() {
      if (item.name.toLowerCase().trim().contains(searchQuery) ||
          item.name.trim().contains(searchQuery)||item.priceOld.toString().toLowerCase().trim().contains(searchQuery)||item.priceOld.toString().trim().contains(searchQuery)) {
        filteredRecored.add(item);
      }
    });
  }


  @override
  Widget build(BuildContext context) {
    return BlocListener<SearchBloc, SearchState>(
      listener: (context, state) async {
        if (state.isLoading) {
          await HttpHandler.resolve(status: state.status);
        }

        if (state.isSuccess) {
          await HttpHandler.resolve(status: state.status);
          allRecord = state.product;
          filteredRecored = new List<Products>();
          filteredRecored.addAll(allRecord);
        }

        if (state.isFailure) {
          await HttpHandler.resolve(status: state.status);
        }
      },
      child: BlocBuilder<SearchBloc, SearchState>(
        builder: (context, state) {
          return SafeArea(
            top: false,
            child: Scaffold(
              body: OrientationBuilder(builder: (context, orientation) {
                if (MediaQuery.of(context).size.width > 600) {
                  return  Container(
                      child:  Padding(
                        padding: EdgeInsets.only(top: 20),
                        child: Column(
                          children: [
                           // _buildNavigationBar(),
                            _buildSearch(),
                            Expanded(
                                child: Container(
                                  child: Visibility(
                                      visible: _isVisible,
                                      child: Padding(
                                        padding:
                                        EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                                        child: Container(
                                          child: filteredRecored != null &&
                                              filteredRecored.length > 0
                                              ? Column(
                                            children: [
                                              Expanded(
                                                child:
                                                WidgetListNews(products: filteredRecored),
                                              )
                                            ],
                                          )
                                              : allRecord == null
                                              ? new Center(child: Container(
                                            height: 100,
                                            width: 100,
                                            child: Lottie.asset(
                                              'assets/lottie/load_car.json',
                                            ),
                                          ))
                                              : new Center(
                                            child:
                                            new Text("Không tìm thấy !"),
                                          ),
                                        ),
                                      )),
                                ))
                          ],
                        ),
                      ));
                } else {
                  return   Padding(
                    padding: EdgeInsets.only(top: 35),
                    child: Column(
                      children: [
                        //_buildNavigationBar(),
                        _buildSearch(),
                        Expanded(
                            child: Container(
                              child: Visibility(
                                  visible: _isVisible,
                                  child: Padding(
                                    padding:
                                    EdgeInsets.symmetric( horizontal: 10),
                                    child: Container(
                                      child: filteredRecored != null &&
                                          filteredRecored.length > 0
                                          ? Column(
                                        children: [
                                          Expanded(
                                            child:
                                            WidgetListNews(products: filteredRecored),
                                          )
                                        ],
                                      )
                                          : allRecord == null
                                          ? new Center(child: Container(
                                        height: 100,
                                        width: 100,
                                        child: Lottie.asset(
                                          'assets/lottie/load_car.json',
                                        ),
                                      ))
                                          : new Center(
                                        child:
                                        new Text("Không tìm thấy !"),
                                      ),
                                    ),
                                  )),
                            ))
                      ],
                    ),
                  );
                }


              }),
            ),
          );
        },
      ),
    );
  }

  void Finishs() {
    Navigator.of(context).pop();
  }

  void showToast() {
    setState(() {
      _isVisible = _isVisible;
    });
  }

  _buildNavigationBar() {
    return WidgetSearchAppbars(
      title: [_isSearching ? _buildSearchField() : _buildTitle(context)],
      backgroundColor: Color(0xff33CC66),
      textColor: Colors.white,
    //  height: 80,
      left: [
        Padding(
          padding: const EdgeInsets.only(left: 20),
          child: GestureDetector(
            onTap: () {
              AppNavigator.navigateBack();
            },
            child: Image.asset(
              "assets/images/ic_arrow.png",
              width: 20,
              height: 20,
              color: Colors.white,
            ),
          ),
        )
      ],
    );
  }

  _buildSearch(){
    return Container(
     color: Color(0xff33CC66),
      padding: EdgeInsets.symmetric(horizontal: 20,vertical: 10),
      child: Row(
        children: [
          Expanded(
            child: Hero(
              tag: 'search',
              child: _isSearching ? _buildSearchField() : _buildTitle(context),
            ),
          ),
          GestureDetector(
            onTap: () {
              AppNavigator.navigateBack();
            },
            child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Text(
                 "Huỷ",
                  style: TextStyle(color: Colors.white,fontSize: 14),
                )),
          )
        ],
      ),
    );
  }
}
