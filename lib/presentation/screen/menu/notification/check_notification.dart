import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_circle_progress.dart';

import 'bloc/bloc.dart';

class WidgetCheckNotificationMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NotificationPromotionBloc, NotificationPromotionState>(
      builder: (context, state) {
        return Container(
          child: _buildContent(state),
        );
      },
    );
  }

  Widget _buildContent(NotificationPromotionState state) {
    if (state is NotificationPromotionLoaded) {
      return state.notification != null
          ? Container(
              height: 16,
              width: 16,
              decoration: BoxDecoration(
                color: Colors.red[700],
                borderRadius: BorderRadius.all(Radius.circular(
                    10.0)), // set rounded corner radius/ make rounded corner of border
              ),
              child: Center(
                child: Container(
                  child: Text(
                      state.notification.length < 9
                          ? state.notification.length
                              .toString()
                          : "9+",
                      style: TextStyle(fontSize: 10, color: Colors.white)),
                ),
              ),
            )
          : Container(
              height: 15,
              child: Text(''),
            );
    } else if (state is NotificationPromotionLoading) {
      return Center(child: Container(
        height: 100,
        width: 100,
        child: Lottie.asset(
          'assets/lottie/load_car.json',
        ),
      ));
    } else if (state is NotificationPromotionNotLoaded) {
      return WidgetScreenError(
        status: state.status,
      );
    } else {
      return Center(
        child: Text('Unknown state'),
      );
    }
  }
}
