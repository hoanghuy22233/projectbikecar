import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logger/logger.dart';
import 'package:lottie/lottie.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/app/constants/color/color.dart';
import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_appbar_profile.dart';
import 'package:flutter/material.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_circle_progress.dart';
import 'package:project_bike_car/presentation/screen/menu/news/new_detaill/sc_new_detail.dart';
import 'package:project_bike_car/presentation/screen/menu/notification/bloc/notification_bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/notification/bloc/notification_event.dart';
import 'package:project_bike_car/presentation/screen/menu/notification/bloc/notification_state.dart';
import 'package:project_bike_car/utils/handler/http_handler.dart';
import 'package:sticky_headers/sticky_headers.dart';
import 'package:url_launcher/url_launcher.dart';

class NotificationPageScreen extends StatefulWidget {
  @override
  _NotificationPageScreenState createState() => _NotificationPageScreenState();
}

class _NotificationPageScreenState extends State<NotificationPageScreen>
    with AutomaticKeepAliveClientMixin<NotificationPageScreen> {
  @override
  void initState() {
    super.initState();
    // BlocProvider.of<NotificationPromotionBloc>(context).add(LoadNotificationPromotion());
  }

  var logger = Logger(
    printer: PrettyPrinter(),
  );
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocListener<NotificationPromotionBloc, NotificationPromotionState>(
      listener: (context, state) async {
        if (state is NotificationPromotionNotLoaded) {
          await HttpHandler.resolve(status: state.status);
        }
      },
      child: BlocBuilder<NotificationPromotionBloc, NotificationPromotionState>(
          builder: (context, state) {
        return SafeArea(
          top: true,
          child: Scaffold(
            backgroundColor: Colors.white,
            body: OrientationBuilder(builder: (context, orientation) {
              if (MediaQuery.of(context).size.width > 600) {
                return  Container(
                  // padding: EdgeInsets.only(top: 20),
                    child: _buildContent(state));
              } else {
                return   Container(
                    child: _buildContentPhone(state));
              }


            }),
          )

          ,
        );
      }),
    );
  }

  _buildContent(NotificationPromotionState state) {
    if (state is NotificationPromotionLoaded) {
      return Column(
        children: [
          _buildAppbar(),
          _buildContents(state),
        ],
      );
    } else if (state is NotificationPromotionLoading) {
      return Center(
          child: Container(
        height: 100,
        width: 100,
        child: Lottie.asset(
          'assets/lottie/load_car.json',
        ),
      ));
    } else if (state is NotificationPromotionNotLoaded) {
      return Center(
        child: Text('${state.status}'),
      );
    } else {
      return Center(
        child: Text('Unknown state'),
      );
    }
  }
  _buildContentPhone(NotificationPromotionState state) {
    if (state is NotificationPromotionLoaded) {
      return Column(
        children: [
          _buildAppbar(),
          _buildContents(state),
        ],
      );
    } else if (state is NotificationPromotionLoading) {
      return Center(
          child: Container(
            height: 100,
            width: 100,
            child: Lottie.asset(
              'assets/lottie/load_car.json',
            ),
          ));
    } else if (state is NotificationPromotionNotLoaded) {
      return Center(
        child: Text('${state.status}'),
      );
    } else {
      return Center(
        child: Text('Unknown state'),
      );
    }
  }

  _buildContents(NotificationPromotionLoaded state) {
    return state.notification.length != 0
        ? Expanded(
            child: RefreshIndicator(
              onRefresh: () async {
                BlocProvider.of<NotificationPromotionBloc>(context)
                    .add(RefreshNotificationPromotion());
                await Future.delayed(Duration(seconds: 3));
                return true;
              },
              color: AppColor.PRIMARY_COLOR,
              backgroundColor: AppColor.THIRD_COLOR,
              child: Container(
                child: ListView.builder(
                    itemCount: state.notification.length,
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        onTap: () {
                          state.notification[index].post.link != null
                              ? _openUrl(state.notification[index].post.link)
                              : Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => NewDetailPageScreen(
                                          id: state
                                              .notification[index].post.id)),
                                );
                        },
                        child: Column(
                          children: [
                            Card(
                              elevation: 0,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(4.0)),
                              child: Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Container(
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        width: 60,
                                        height: 60,
                                        child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(300.0),
                                          child: Image.asset(
                                              "assets/images/logo_reapla.png"),
                                        ),
                                      ),
                                      const SizedBox(
                                        width: 4,
                                      ),
                                      Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 4),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              Text(
                                                state.notification[index].post
                                                    .name,
                                                maxLines: 3,
                                                overflow: TextOverflow.ellipsis,
                                                style: AppStyle.DEFAULT_SMALL
                                                    .copyWith(
                                                        color:
                                                            AppColor.PRIMARY),
                                              ),
                                              WidgetSpacer(
                                                height: 3,
                                              ),
                                              Text(
                                                state.notification[index].post
                                                    .created,
                                                style: AppStyle.DEFAULT_SMALL
                                                    .copyWith(
                                                        color: AppColor.GREY),
                                                textAlign: TextAlign.start,
                                                overflow: TextOverflow.ellipsis,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      const SizedBox(
                                        width: 4,
                                      ),
                                      CachedNetworkImage(
                                        height: 60,
                                        width: 60,
                                        imageUrl: state
                                            .notification[index].post.image,
                                        placeholder: (context, url) {
                                          return Center(
                                              child: Container(
                                            height: 60,
                                            width: 60,
                                            child: Lottie.asset(
                                              'assets/lottie/load_car.json',
                                            ),
                                          ));
                                        },
                                        errorWidget: (context, url, error) {
                                          return Center(
                                              child: WidgetCachedImageError());
                                        },
                                        fit: AppValue.IMAGE_FIT_MODE,
                                        filterQuality: FilterQuality.low,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Divider(
                              height: 0.1,
                              thickness: 0.5,
                              color: AppColor.GREY,
                            ),
                          ],
                        ),
                      );
                    }),
              ),
            ),
          )
        : Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                    height: 120,
                    width: 120,
                    child: Image.asset('assets/images/box.png')),
                SizedBox(
                  height: 20,
                ),
                Text('Bạn chưa thông báo nào!'),
              ],
            ),
          );
  }

  _openUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  bool get wantKeepAlive => true;
  _buildAppbar() => WidgetAppbarProfile(
        backgroundColor: Colors.blue,
        textColor: Colors.white,
        title: "THÔNG BÁO",
      );
}
