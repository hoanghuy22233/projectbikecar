import 'package:equatable/equatable.dart';

class NotificationPromotionEvent extends Equatable {
  const NotificationPromotionEvent();

  List<Object> get props => [];
}

class LoadNotificationPromotion extends NotificationPromotionEvent {
}

class RefreshNotificationPromotion extends NotificationPromotionEvent {}
