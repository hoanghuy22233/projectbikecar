import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:project_bike_car/model/entity/notification.dart';
import 'package:project_bike_car/model/repo/barrel_repo.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

import 'bloc.dart';

class NotificationPromotionBloc
    extends Bloc<NotificationPromotionEvent, NotificationPromotionState> {
  final UserRepository userRepository;

  NotificationPromotionBloc({@required this.userRepository});

  @override
  NotificationPromotionState get initialState => NotificationPromotionLoading();

  @override
  Stream<NotificationPromotionState> mapEventToState(
      NotificationPromotionEvent event) async* {
    if (event is LoadNotificationPromotion) {
      yield* _mapLoadNotificationPromotionToState();
    } else if (event is RefreshNotificationPromotion) {
      yield NotificationPromotionLoading();
      yield* _mapLoadNotificationPromotionToState();
    }
  }

  Stream<NotificationPromotionState>
      _mapLoadNotificationPromotionToState() async* {
    final currentState = state;
    try {
      if (state is NotificationPromotionLoading) {
        final notification = await _fetchNotification(
        );
        yield NotificationPromotionLoaded(
            notification: notification);
      }
      if (currentState is NotificationPromotionLoaded) {
        final notification = await _fetchNotification(
        );
        yield notification.isEmpty
            ? currentState.copyWith()
            : NotificationPromotionLoaded(
                notification: currentState.notification + notification,
              );
      }
    } catch (e) {
      yield NotificationPromotionNotLoaded(status: DioErrorUtil.handleError(e));
    }
  }

  Future<List<Notification>> _fetchNotification() async {
    try {
      final response = await userRepository.getNotification();
      return response.data;
    } catch (e) {
      throw Exception('$e');
    }
  }
}
