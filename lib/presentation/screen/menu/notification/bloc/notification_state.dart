
import 'package:equatable/equatable.dart';
import 'package:project_bike_car/model/entity/notification.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

abstract class NotificationPromotionState extends Equatable {
  const NotificationPromotionState();

  @override
  List<Object> get props => [];
}

class NotificationPromotionLoading extends NotificationPromotionState {}

class NotificationPromotionLoaded extends NotificationPromotionState {
  final List<Notification> notification;

  const NotificationPromotionLoaded({ this.notification});

  NotificationPromotionLoaded copyWith({
    List<Notification> notification,
  }) {
    return NotificationPromotionLoaded(
      notification: notification ?? this.notification,
    );
  }

  @override
  List<Object> get props => [notification,];
}

class NotificationPromotionNotLoaded extends NotificationPromotionState {
  final DioStatus status;

  NotificationPromotionNotLoaded({this.status});

  @override
  String toString() {
    return 'NotificationPromotionNotLoaded{status: $status}';
  }
}
