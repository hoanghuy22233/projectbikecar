import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:project_bike_car/presentation/screen/menu/news/new_detaill/sc_new_detail.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'bloc/bloc.dart';



class WidgetNotificationPromotion extends StatefulWidget {
  @override
  _WidgetNotificationPromotionState createState() =>
      _WidgetNotificationPromotionState();
}

class _WidgetNotificationPromotionState
    extends State<WidgetNotificationPromotion>
    with AutomaticKeepAliveClientMixin<WidgetNotificationPromotion> {
  RefreshController _refreshController =
  RefreshController(initialRefresh: false);

  @override
  void initState() {
    super.initState();
    BlocProvider.of<NotificationPromotionBloc>(context)
        .add(LoadNotificationPromotion());
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocListener<NotificationPromotionBloc, NotificationPromotionState>(
      listener: (context, state) {
        if (state is NotificationPromotionLoaded) {

        }
      },
      child: BlocBuilder<NotificationPromotionBloc, NotificationPromotionState>(
        builder: (context, state) {
          return Container(
            child: _buildContent(state),
          );
        },
      ),
    );
  }

  Widget _buildContent(NotificationPromotionState state) {
    if (state is NotificationPromotionLoaded) {
      return _buildContents(state);
    } else if (state is NotificationPromotionLoading) {
      return Container(
        width: double.infinity,
        height: AppValue.PRODUCT_HORIZONTAL_HEIGHT,
        child: Center(child: Container(
          height: 100,
          width: 100,
          child: Lottie.asset(
            'assets/lottie/load_car.json',
          ),
        )),
      );
    } else {
      return Center(
        child: Text('Unknown state'),
      );
    }
  }


  _buildContents(NotificationPromotionLoaded state){
    return state.notification.length != 0 ?
    ListView.separated(
      scrollDirection: Axis.vertical,
      itemBuilder: (context, index) {
        return GestureDetector(
          onTap: (){
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => NewDetailPageScreen(id:state.notification[index].post.id)),
            );
          },
          child:   Container(
           // color: Color(0xff33CC66),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 50,horizontal: 10),

                  child: Container(
                    width: 100,
                    height: 100,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(300.0),
                      child: WidgetCachedImage(
                        url: state.notification[index].post.image,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: Container(
                    width: 2,
                    height: 130,
                    color: Colors.white,
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                state.notification[index].post!=null?
                Expanded(
                  flex: 7,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Image.asset("assets/images/clock-circular-outline.png",width: 20, height: 20,),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            "${state.notification[index].updatedAt}",
                            style: const TextStyle(
                              fontSize: 14,
                              color: Colors.black87,
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ):Container(),
              ],
            ),
          ),
        );

      },
      itemCount: state.notification.length,
      separatorBuilder: (context, index) {
        return WidgetSpacer(height: 15);
      },
      physics: BouncingScrollPhysics(),
    )
        : Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
              height: 120,
              width: 120,
              child: Image.asset('assets/images/box.png')
          ),
          SizedBox(height: 20,),
          Text('Bạn chưa thông báo nào!'),
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
