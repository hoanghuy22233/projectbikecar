import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:project_bike_car/app/constants/endpoint/app_endpoint.dart';
import 'package:project_bike_car/model/repo/barrel_repo.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

import 'bloc.dart';

class AddContactBloc extends Bloc<AddContactEvent, AddContactState> {
  final UserRepository userRepository;

  AddContactBloc({ this.userRepository});

  @override
  AddContactState get initialState => AddContactState.empty();

  @override
  Stream<AddContactState> mapEventToState(AddContactEvent event) async* {
    if (event is SubmittedAddContact) {
      yield* _mapLoadAddContactToState(event.name,event.email,event.content,event.phone,event.token);
    }
  }

  Stream<AddContactState> _mapLoadAddContactToState(String name, String email,String content,String phone,String token ) async* {
    try {
      final addContactResponse = await userRepository.postAddContact(name:name,email:email,content:content,phone:phone,token:token
      );
          // print("xem kết quả trả về của api response ${response.data}");

      if (addContactResponse.status == Endpoint.SUCCESS) {
        yield AddContactState.success(message: addContactResponse.msg);
      } else {
        yield AddContactState.failure(message: addContactResponse.msg);
      }
    } catch (e) {
      print("------ Register: $e");
      yield AddContactState.failure();
    }
  }
}
