import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class AddContactEvent extends Equatable {
  const AddContactEvent();

  List<Object> get props => [];
}

class SubmittedAddContact extends AddContactEvent {
  final String name;
  final String email;
  final String content;
  final String phone;
  final String token;


  const SubmittedAddContact({
    @required this.name,
    @required this.email,
    @required this.content,
    @required this.phone,
    @required this.token,
  });

  @override
  List<Object> get props => [name,email, content, phone,token];

  @override
  String toString() {
    return 'SubmittedAddContact{name: $name, email: $email, content: $content,phone: $phone,token:$token }';
  }
}

