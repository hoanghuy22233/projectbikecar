import 'package:flutter/cupertino.dart';

class AddContactState {
  final bool isName;
  final bool isEmail;
  final bool isContent;
  final bool isPhone;
  final bool isToken;
  final bool isSubmitting;
  final bool isSuccess;
  final bool isFailure;
  final String message;

  bool get isFormValid =>
      isName && isEmail && isContent && isPhone&& isToken ;

  AddContactState(
      {@required this.isName,
        @required this.isEmail,
        @required this.isContent,
        @required this.isPhone,
        @required this.isToken,
        @required this.isSubmitting,
        @required this.isSuccess,
        @required this.isFailure,
        @required this.message});

  factory AddContactState.empty() {
    return AddContactState(
        isName: true,
        isEmail: true,
        isContent: true,
        isPhone: true,
        isToken: true,
        isSubmitting: false,
        isSuccess: false,
        isFailure: false,
        message: '');
  }

  factory AddContactState.loading() {
    return AddContactState(
        isName: true,
        isEmail: true,
        isContent: true,
        isPhone: true,
        isToken: true,
        isSubmitting: true,
        isSuccess: false,
        isFailure: false,
        message: '');
  }

  factory AddContactState.failure({String message}) {
    return AddContactState(
        isName: true,
        isEmail: true,
        isContent: true,
        isPhone: true,
        isToken: true,
        isSuccess: false,
        isSubmitting: false,
        isFailure: true,
        message: message);
  }

  factory AddContactState.success({String message}) {
    return AddContactState(
        isName: true,
        isEmail: true,
        isContent: true,
        isPhone: true,
        isToken: true,
        isSuccess: true,
        isSubmitting: false,
        isFailure: false,
        message: message);
  }

  AddContactState update(
      {bool isName,
        bool isEmail,
        bool isContent,
        bool isPhone,
        bool isToken,
        String message}) {
    return copyWith(
      isName:isName,
      isEmail: isEmail,
      isContent: isContent,
      isPhone: isPhone,
      isToken:isToken,
      isSubmitting: false,
      isSuccess: false,
      isFailure: false,
      message: message,
    );
  }

  AddContactState copyWith({
    bool isName,
    bool isEmail,
    bool isContent,
    bool isPhone,
    bool isToken,
    bool isSubmitting,
    bool isSuccess,
    bool isFailure,
    String message,
  }) {
    return AddContactState(
      isName:isName?? this.isName,
      isEmail: isEmail ?? this.isEmail,
      isContent: isContent ?? this.isContent,
      isPhone: isPhone ?? this.isPhone,
      isToken: isToken ?? this.isToken,
      isSubmitting: isSubmitting ?? this.isSubmitting,
      isSuccess: isSuccess ?? this.isSuccess,
      isFailure: isFailure ?? this.isFailure,
      message: message ?? this.message,
    );
  }
}
