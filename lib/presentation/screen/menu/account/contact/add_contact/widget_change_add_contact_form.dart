import 'package:project_bike_car/app/constants/color/color.dart';
import 'package:project_bike_car/app/constants/navigator/navigator.dart';
import 'package:project_bike_car/app/constants/preferences/app_preferences.dart';
import 'package:project_bike_car/app/constants/style/style.dart';
import 'package:project_bike_car/app/constants/string/validator.dart';
import 'package:project_bike_car/model/local/pref.dart';
import 'package:project_bike_car/model/repo/user_repository.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_spacer.dart';
import 'package:project_bike_car/presentation/screen/menu/account/contact/add_contact/bloc/add_contact_bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/account/contact/add_contact/bloc/add_contact_event.dart';
import 'package:project_bike_car/presentation/screen/menu/account/contact/add_contact/bloc/add_contact_state.dart';
import 'package:project_bike_car/utils/snackbar/get_snack_bar_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_icons/flutter_icons.dart';

class WidgetChangeAddContactForm extends StatefulWidget {
  @override
  _WidgetChangeAddContactFormState createState() =>
      _WidgetChangeAddContactFormState();
}

class _WidgetChangeAddContactFormState
    extends State<WidgetChangeAddContactForm> {
  AddContactBloc _addContactBloc;
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _contentController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();

  bool obscurePassword = true;
  bool obscureConfirmPassword = true;
  bool autoValidate = false;

  bool get isPopulated =>
      _nameController.text.isNotEmpty &&
      _emailController.text.isNotEmpty &&
      _phoneController.text.isNotEmpty;

  @override
  void initState() {
    super.initState();
    _addContactBloc = BlocProvider.of<AddContactBloc>(context);
  }

  @override
  void dispose() {
    super.dispose();
    _nameController.dispose();
    _emailController.dispose();
    _contentController.dispose();
    _phoneController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<AddContactBloc, AddContactState>(
      listener: (context, state) async {
        if (state.isSubmitting) {
          GetSnackBarUtils.createProgress();
        }

        if (state.isSuccess) {
          await GetSnackBarUtils.createSuccess(message: state.message);
          AppNavigator.navigateBack(
          );
        }

        if (state.isFailure) {
          GetSnackBarUtils.createError(message: "Cần điền đẩy đủ thông tin và chính xác");

          setState(() {
            autoValidate = true;
          });
        }
      },
      child: BlocBuilder<AddContactBloc, AddContactState>(
        builder: (context, state) {
          return Padding(
            padding: EdgeInsets.all(20),
            child: Form(
              child: Column(
                children: [
                  _buildTextFieldName(),
                  WidgetSpacer(
                    height: 10,
                  ),
                  _buildTextFieldEmail(),
                  WidgetSpacer(
                    height: 10,
                  ),
                  _buildTextFieldContent(),
                  WidgetSpacer(
                    height: 20,
                  ),
                  _buildTextFieldPhone(),
                  WidgetSpacer(
                    height: 20,
                  ),
                  Container(
                    child: _buildButtonRegister(state),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  _buildButtonRegister(AddContactState state) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 50,
      child: GestureDetector(
        onTap: () async {
          final prefs = LocalPref();
          final token = await prefs.getString(AppPreferences.auth_token);
          print("Lấy token đổi mật khẩu trên tài khoản ${token}");
          _addContactBloc.add(SubmittedAddContact(
            token: token ?? '',
            name: _nameController.text,
            email: _emailController.text,
            phone: _phoneController.text,
            content: _contentController.text,
          ));
          // AppNavigator.navigateBack();

          FocusScope.of(context).unfocus();
        },
        child: Card(
          elevation: 5,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
          color: Color(0xff33CC66),
          child: Center(
              child: Text("Đồng ý",
                  style: AppStyle.DEFAULT_MEDIUM.copyWith(
                    color: AppColor.WHITE,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Roboto',
                  ))),
        ),
      ),
    );
  }

  _buildTextFieldName() {
    return Container(
      width: MediaQuery.of(context).size.width / 1.15,
      child: TextFormField(
        enableInteractiveSelection: false,
        controller: _nameController,
        keyboardType: TextInputType.text,
        onChanged: (value) {
          // _loginBloc.add(LoginUsernameChanged(email: value));
        },
        style: TextStyle(
            color: Colors.white,
            fontSize: 16,
            fontWeight: FontWeight.bold,
            fontFamily: 'Roboto'),
        validator: AppValidation.validateUserName("Vui lòng điền tài khoản"),
        decoration: InputDecoration(
          hintText: "Tên",
          hintStyle: TextStyle(color: Colors.white),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
        ),
        // textAlign: TextAlign.start,
      ),
    );
  }

  _buildTextFieldEmail() {
    return Container(
      width: MediaQuery.of(context).size.width / 1.15,
      child: TextFormField(
        enableInteractiveSelection: false,
        controller: _emailController,
        keyboardType:TextInputType.emailAddress?? TextInputType.text,
        onChanged: (value) {
          // _loginBloc.add(LoginUsernameChanged(email: value));
        },
        style: TextStyle(
            color: Colors.white,
            fontSize: 16,
            fontWeight: FontWeight.bold,
            fontFamily: 'Roboto'),
        validator: AppValidation.validateUserName("Vui lòng điền tài khoản"),
        decoration: InputDecoration(
          hintText: "Email",
          hintStyle: TextStyle(color: Colors.white),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
        ),
        // textAlign: TextAlign.start,
      ),
    );
  }

  _buildTextFieldContent() {
    return Container(
      width: MediaQuery.of(context).size.width / 1.15,
      child: TextFormField(
        enableInteractiveSelection: false,
        controller: _contentController,
        keyboardType: TextInputType.text,
        onChanged: (value) {
          // _loginBloc.add(LoginUsernameChanged(email: value));
        },
        style: TextStyle(
            color: Colors.white,
            fontSize: 16,
            fontWeight: FontWeight.bold,
            fontFamily: 'Roboto'),
        validator: AppValidation.validateUserName("Vui lòng điền tài khoản"),
        decoration: InputDecoration(
          hintText: "Địa chỉ",
          hintStyle: TextStyle(color: Colors.white),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
        ),
        // textAlign: TextAlign.start,
      ),
    );
  }

  _buildTextFieldPhone() {
    return Container(
      width: MediaQuery.of(context).size.width / 1.15,
      child: TextFormField(
        enableInteractiveSelection: false,
        controller: _phoneController,
        keyboardType: TextInputType.text,
        onChanged: (value) {
          // _loginBloc.add(LoginUsernameChanged(email: value));
        },
        style: TextStyle(
            color: Colors.white,
            fontSize: 16,
            fontWeight: FontWeight.bold,
            fontFamily: 'Roboto'),
        validator: AppValidation.validateUserName("Vui lòng điền tài khoản"),
        decoration: InputDecoration(
          hintText: "Số điện thoại",
          hintStyle: TextStyle(color: Colors.white),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
        ),
        // textAlign: TextAlign.start,
      ),
    );
  }
}
