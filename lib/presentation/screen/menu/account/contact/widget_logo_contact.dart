import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:flutter/material.dart';

class WidgetLogoContact extends StatelessWidget {
  final double widthPercent;
  final double height;
  final bool small;

  WidgetLogoContact({Key key, this.widthPercent, this.height, this.small = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(4.0),
      child: Stack(
        children: [
          Container(
            margin: EdgeInsets.only(top: 10),
            height: height ?? (AppValue.ACTION_BAR_HEIGHT * 1.5),
            child: Center(
              child: FractionallySizedBox(
                widthFactor: widthPercent ?? 0.4,
                heightFactor: 0.4,
                child: Image.asset(small
                    ? 'assets/images/logo_reapla.png'
                    : 'assets/images/logo_reapla.png'),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
