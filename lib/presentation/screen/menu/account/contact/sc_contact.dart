import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_appbar_profile.dart';
import 'package:project_bike_car/presentation/screen/menu/account/change_password/sc_change_password.dart';
import 'package:project_bike_car/presentation/screen/menu/account/contact/add_contact/sc_add_contact.dart';
import 'package:project_bike_car/presentation/screen/menu/account/contact/widget_contact_detail.dart';
import 'package:project_bike_car/presentation/screen/menu/account/contact/widget_logo_contact.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';


class ContactScreen extends StatefulWidget {
  @override
  _ContactScreenState createState() => _ContactScreenState();
}

class _ContactScreenState extends State<ContactScreen>
    with AutomaticKeepAliveClientMixin<ContactScreen> {
  @override
  void initState() {
    super.initState();
    // BlocProvider.of<ProfileBloc>(context).add(LoadProfile());
  }

  var logger = Logger(
    printer: PrettyPrinter(),
  );

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SafeArea(
      top: true,
      child: Scaffold(
        backgroundColor: Color(0xff33CC66),
        body: Container(color: AppColor.PRIMARY_COLOR, child: _buildContent()),
      ),
    );
  }

  _buildContent() {
    return Column(
      children: [_buildAppbar(), Expanded(child: _buildMenu())],
    );
  }

  _buildAppbar() => WidgetAppbarProfile(
    backgroundColor: Color(0xff33CC66),
    textColor: Colors.white,
    title: "Liên hệ",
    right: [
      Padding(
        padding: EdgeInsets.only(top: 3, right: 10),
        child: GestureDetector(
          onTap: () {
            showMaterialModalBottomSheet(
              shape: RoundedRectangleBorder(
                // borderRadius: BorderRadius.circular(10.0),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(15.0),
                    topRight: Radius.circular(15.0)),
              ),
              backgroundColor: Colors.white,
              context: context,
              builder: (context) => AddContactScreen(),
            );
          },
          child: Image.asset(
            "assets/icons/ic_add.png",
            width: 20,
            height: 20,
            color: Colors.white,
          ),
        ),
      )
    ],
    left: [
      Padding(
        padding: const EdgeInsets.only(left: 10),
        child: GestureDetector(
          onTap: () {
            AppNavigator.navigateBack();
          },
          child: Image.asset(
            "assets/images/ic_arrow.png",
            width: 20,
            height: 20,
            color: Colors.white,
          ),
        ),
      )
    ],
  );

  Widget _buildMenu() {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Padding(
        padding: const EdgeInsets.symmetric(
            vertical: AppValue.APP_HORIZONTAL_PADDING),
        child: Column(
          children: [
            _buildLogo(),
            _buildContactDetail(),
          ],
        ),
      ),
    );
  }

  Widget _buildContactDetail() => WidgetContactDetail();

  Widget _buildLogo() => WidgetLogoContact(
      height: MediaQuery.of(context).size.height / 4, widthPercent: 1);

  @override
  bool get wantKeepAlive => true;
}
