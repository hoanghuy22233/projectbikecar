
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
class WidgetContactDetail extends StatelessWidget {
  WidgetContactDetail({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                  flex: 1,
                  child: Image.asset(
                    "assets/icons/ic_address.png",
                    width: 25,
                    height: 25,
                  )),
              Expanded(
                  flex: 9,
                  child: Text(": 52 Ung Văn Khiêm, P.25, Q.Bình Thạnh, TP.HCM",style: TextStyle(color: Color(0xff33CC66),),))
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Image.asset(
                  "assets/icons/ic_phone.png",
                  width: 25,
                  height: 25,
                ),
              ),
              Text(": "),
              Expanded(flex: 9,

                child: GestureDetector(
                  onTap: () {
                    launch("tel:0888898000");

                    // _launchURL();
                  },
                  child: Text('0888898000',style: TextStyle(color: Color(0xff33CC66), decoration: TextDecoration.underline,),),
                ),
              )
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Image.asset(
                  "assets/icons/ic_phone.png",
                  width: 25,
                  height: 25,
                ),
              ),
              Text(": "),
              Expanded(flex: 9,
                // child: Text(": 092 846 84 83")
                // child: RaisedButton(
                //   onPressed: _launchURL,
                //   child: Text('0869258726'),
                // ),
                child: GestureDetector(
                  onTap: () {
                    launch("tel:0901056662");

                    // _launchURL();
                  },
                  child: Text('0901056662',style: TextStyle(color: Color(0xff33CC66), decoration: TextDecoration.underline,),),
                ),
              )
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Image.asset(
                  "assets/icons/ic_phone.png",
                  width: 25,
                  height: 25,
                ),
              ),
              Text(": "),
              Expanded(flex: 9,
                // child: Text(": 092 846 84 83")
                // child: RaisedButton(
                //   onPressed: _launchURL,
                //   child: Text('0869258726'),
                // ),
                child: GestureDetector(
                  onTap: () {
                    launch("tel:0778889000");

                    // _launchURL();
                  },
                  child: Text('0778889000',style: TextStyle(color: Color(0xff33CC66), decoration: TextDecoration.underline,),),
                ),
              )
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Image.asset(
                  "assets/icons/ic_phone.png",
                  width: 25,
                  height: 25,
                ),
              ),
              Text(": "),
              Expanded(flex: 9,
                // child: Text(": 092 846 84 83")
                // child: RaisedButton(
                //   onPressed: _launchURL,
                //   child: Text('0869258726'),
                // ),
                child: GestureDetector(
                  onTap: () {
                    launch("tel:0939921691");

                    // _launchURL();
                  },
                  child: Text('0939921691',style: TextStyle(color: Color(0xff33CC66), decoration: TextDecoration.underline,),),
                ),
              )
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Image.asset(
                  "assets/icons/ic_phone.png",
                  width: 25,
                  height: 25,
                ),
              ),
              Text(": "),
              Expanded(flex: 9,
                // child: Text(": 092 846 84 83")
                // child: RaisedButton(
                //   onPressed: _launchURL,
                //   child: Text('0869258726'),
                // ),
                child: GestureDetector(
                  onTap: () {
                    launch("tel:0707703955");

                    // _launchURL();
                  },
                  child: Text('0707703955',style: TextStyle(color: Color(0xff33CC66), decoration: TextDecoration.underline,),),
                ),
              )
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Image.asset(
                  "assets/icons/ic_phone.png",
                  width: 25,
                  height: 25,
                ),
              ),
              Text(": "),
              Expanded(flex: 9,
                // child: Text(": 092 846 84 83")
                // child: RaisedButton(
                //   onPressed: _launchURL,
                //   child: Text('0869258726'),
                // ),
                child: GestureDetector(
                  onTap: () {
                    launch("tel:0388551054");

                    // _launchURL();
                  },
                  child: Text('0388551054',style: TextStyle(color: Color(0xff33CC66), decoration: TextDecoration.underline,),),
                ),
              )
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Image.asset(
                  "assets/icons/ic_phone.png",
                  width: 25,
                  height: 25,
                ),
              ),
              Text(": "),
              Expanded(flex: 9,
                // child: Text(": 092 846 84 83")
                // child: RaisedButton(
                //   onPressed: _launchURL,
                //   child: Text('0869258726'),
                // ),
                child: GestureDetector(
                  onTap: () {
                    launch("tel:0888840000");

                    // _launchURL();
                  },
                  child: Text('0888840000',style: TextStyle(color: Color(0xff33CC66), decoration: TextDecoration.underline,),),
                ),
              )
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Image.asset(
                  "assets/icons/ic_phone.png",
                  width: 25,
                  height: 25,
                ),
              ),
              Text(": "),
              Expanded(flex: 9,
                // child: Text(": 092 846 84 83")
                // child: RaisedButton(
                //   onPressed: _launchURL,
                //   child: Text('0869258726'),
                // ),
                child: GestureDetector(
                  onTap: () {
                    launch("tel:0338000040");

                    // _launchURL();
                  },
                  child: Text('0338000040',style: TextStyle(color: Color(0xff33CC66), decoration: TextDecoration.underline,),),
                ),
              )
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Image.asset(
                  "assets/icons/ic_phone.png",
                  width: 25,
                  height: 25,
                ),
              ),
              Text(": "),
              Expanded(flex: 9,
                // child: Text(": 092 846 84 83")
                // child: RaisedButton(
                //   onPressed: _launchURL,
                //   child: Text('0869258726'),
                // ),
                child: GestureDetector(
                  onTap: () {
                    launch("tel:0988226276");

                    // _launchURL();
                  },
                  child: Text('0988226276',style: TextStyle(color: Color(0xff33CC66), decoration: TextDecoration.underline,),),
                ),
              )
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Image.asset(
                  "assets/icons/ic_internet.png",
                  width: 25,
                  height: 25,
                ),
              ),
              Text(": "),
              Expanded(flex: 9, child: GestureDetector(
                onTap: () {
                  launch("http://shopxecuongk.com/");
                },
                  child: Text("http://shopxecuongk.com/",style: TextStyle(color: Color(0xff33CC66), decoration: TextDecoration.underline,),)))
            ],
          ),
          SizedBox(
            height: 15,
          ),
        ],
      ),
    );
  }
}
