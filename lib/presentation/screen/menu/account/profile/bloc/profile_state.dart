
import 'package:equatable/equatable.dart';
import 'package:project_bike_car/model/entity/barrel_entity.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

abstract class ProfileState extends Equatable {
  const ProfileState();

  @override
  List<Object> get props => [];
}

class ProfileLoading extends ProfileState {}

class ProfileLoaded extends ProfileState {
  final User user;

  ProfileLoaded(this.user,); //  final User user;



  @override
  List<Object> get props => [user,];

  @override
  String toString() {
    return 'ProfileLoaded{user: $user,}';
  }
}

class ProfileNotLoaded extends ProfileState {
  final DioStatus error;

  ProfileNotLoaded(this.error);

  @override
  String toString() {
    return 'ProfileNotLoaded{error: $error}';
  }
}