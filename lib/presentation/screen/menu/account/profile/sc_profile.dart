
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logger/logger.dart';
import 'package:lottie/lottie.dart';
import 'package:project_bike_car/app/auth_bloc/authentication_bloc.dart';
import 'package:project_bike_car/app/auth_bloc/bloc.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:project_bike_car/presentation/screen/menu/account/profile/widget_profile_appbar.dart';
import 'package:project_bike_car/presentation/screen/menu/account/profile/widget_profile_infor.dart';
import 'package:project_bike_car/utils/handler/barrel_handler.dart';
import 'package:project_bike_car/utils/locale/app_localization.dart';
import 'package:url_launcher/url_launcher.dart';

import 'bloc/bloc.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen>
    with AutomaticKeepAliveClientMixin<ProfileScreen> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<ProfileBloc>(context).add(LoadProfile());
  }

  var logger = Logger(
    printer: PrettyPrinter(),
  );

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocListener<ProfileBloc, ProfileState>(
      listener: (context, state) async {
        if (state is ProfileNotLoaded) {
          await HttpHandler.resolve(status: state.error);
        }
      },
      child: BlocBuilder<ProfileBloc, ProfileState>(builder: (context, state) {
        return SafeArea(
          top: true,
            child: Scaffold(
              backgroundColor: Colors.white,
              body: OrientationBuilder(builder: (context, orientation) {
                if (MediaQuery.of(context).size.width > 600) {
                  return    Container(
                      child: _buildContent(state));
                } else {
                  return     Container(
                      child: _buildContent(state));
                }


              }),
            )


        );
      }),
    );
  }

  _buildContent(ProfileState state) {
    if (state is ProfileLoaded) {
      return Column(
        children: [_buildAppbar(), _buildMenu(state)],
      );
    } else if (state is ProfileLoading) {
      return Center(
        child: Container(
          height: 100,
          width: 100,
          child: Lottie.asset(
            'assets/lottie/load_car.json',
          ),
        ),
      );
    } else if (state is ProfileNotLoaded) {
      return Center(
        child: Text('${state.error}'),
      );
    } else {
      return Center(
        child: Text('Unknown state'),
      );
    }
  }

  Widget _buildAppbar() => WidgetProfileAppbar();

  Widget _buildMenu(ProfileLoaded state) {
    return Expanded(
      child: RefreshIndicator(
        onRefresh: () async {
          BlocProvider.of<ProfileBloc>(context).add(LoadProfile());
          await Future.delayed(Duration(seconds: 3));
          return true;
        },
        color: AppColor.PRIMARY_COLOR,
        backgroundColor: AppColor.THIRD_COLOR,
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Padding(
            padding: const EdgeInsets.all(21.0),
            child: Column(
              children: [
                WidgetProfileInfor(
                  name: state?.user?.name ??
                      AppLocalizations.of(context).translate('profile.no_name'),
                  phone: state?.user?.email ??
                      AppLocalizations.of(context)
                          .translate('profile.no_setup'),
                  avatar: WidgetCachedImage(
                    url: state?.user?.avatar??'',
                  ),
                  onTap: () {
                    AppNavigator.navigateProfileDetail();
                  },
                ),
                WidgetProfileMenu(
                  text: "Lịch sử đặt hàng",
                  onTap: () {
                    AppNavigator.navigateHistoryOder();
                  },
                ),
                WidgetProfileMenu(
                  text: "Đổi mật khẩu",
                  onTap: () {
                    AppNavigator.navigateChangePassWord();
                  },
                ),
                WidgetProfileMenu(
                  text:
                      "Liên hệ",
                  onTap: () {
                    AppNavigator.navigateContact();
                  },
                ),
                WidgetProfileMenu(
                  text: "Thông tin trả góp",
                    onTap:(){
                    _openUrl("http://trumgop.com/tra-gop");
                    },
                ),
                WidgetProfileMenu(
                  text: "Điều khoản sử dụng",
                  onTap: () {
                    AppNavigator.navigateTerm();

                  },
                ),
                WidgetProfileMenu(
                  text:
                      AppLocalizations.of(context).translate('profile.logout'),
                  onTap: () async {
                    Navigator.of(context, rootNavigator: true).pop('dialog');
                    BlocProvider.of<AuthenticationBloc>(context).add(LoggedOut());
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            scrollable: true,
                            content: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                height: 100,
                                width: 100,
                                child: Lottie.asset(
                                  'assets/lottie/load_car.json',
                                ),
                              ),
                            ),
                          );
                        });
                    Future.delayed(Duration(seconds: 2), () {
                      AppNavigator.navigateWorkService();
                    });
                  },
                ),
                WidgetSpacer(
                  height: 100,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  _openUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  bool get wantKeepAlive => true;
}
