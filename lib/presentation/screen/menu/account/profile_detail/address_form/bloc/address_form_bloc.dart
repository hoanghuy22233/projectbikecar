
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/model/repo/barrel_repo.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';
import 'package:project_bike_car/utils/validator/barrel_validator.dart';
import 'package:rxdart/rxdart.dart';

import 'bloc.dart';

class AddressFormBloc extends Bloc<AddressFormEvent, AddressFormState> {
  final UserRepository _userRepository;

  AddressFormBloc({
    @required UserRepository userRepository,
  })  : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  get initialState => AddressFormState.empty();

  @override
  Stream<Transition<AddressFormEvent, AddressFormState>> transformEvents(
      Stream<AddressFormEvent> events, transitionFn) {
    final nonDebounceStream = events.where((event) {
      return (event is! AddressFormChanged);
    });

    final debounceStream = events.where((event) {
      return (event is AddressFormChanged);
    }).debounceTime(Duration(milliseconds: 300));

    return super.transformEvents(
        nonDebounceStream.mergeWith([debounceStream]), transitionFn);
  }

  @override
  Stream<AddressFormState> mapEventToState(AddressFormEvent event) async* {
    if (event is AddressFormSubmitEvent) {
      yield* _mapAddressFormSubmitEventToState(event.AddressForm);
    } else if (event is AddressFormChanged) {
      yield* _mapAddressFormChangedToState(event.AddressForm);
    }
  }

  Stream<AddressFormState> _mapAddressFormSubmitEventToState(String AddressForm) async* {
    try {
      yield AddressFormState.loading();

      var response = await _userRepository.updateAddress(address: AddressForm);
      print('---token----');
      print(response);

      if (response.status == Endpoint.SUCCESS) {
        yield AddressFormState.success(
            DioStatus(
                message: response.msg, code: DioStatus.API_SUCCESS_NOTIFY));
      } else {
        yield AddressFormState.failure(
            DioStatus(
                message: response.msg, code: DioStatus.API_FAILURE_NOTIFY));
      }
    } catch (e) {
      yield AddressFormState.failure(DioErrorUtil.handleError(e));
    }
  }

  Stream<AddressFormState> _mapAddressFormChangedToState(String AddressForm) async* {
    yield state.update(isNameValid: Validator.isValidFullname(AddressForm));
  }
}
