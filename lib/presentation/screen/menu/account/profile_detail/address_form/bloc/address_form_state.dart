
import 'package:flutter/material.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

class AddressFormState {
  final bool isAddressValid;
  final bool isSubmitting;
  final bool isSuccess;
  final bool isFailure;
  final DioStatus status;

  bool get isFormValid => isAddressValid;

  AddressFormState({
    @required this.isAddressValid,
    @required this.isSubmitting,
    @required this.isSuccess,
    @required this.isFailure,
    @required this.status,
  });

  factory AddressFormState.empty() {
    return AddressFormState(
        isAddressValid: true,
        isSubmitting: false,
        isSuccess: false,
        isFailure: false,
        status: null);
  }

  factory AddressFormState.loading() {
    return AddressFormState(
        isAddressValid: true,
        isSubmitting: true,
        isSuccess: false,
        isFailure: false,
        status: null);
  }

  factory AddressFormState.failure(DioStatus status) {
    return AddressFormState(
        isAddressValid: true,
        isSuccess: false,
        isSubmitting: false,
        isFailure: true,
        status: status);
  }

  factory AddressFormState.success(DioStatus status) {
    return AddressFormState(
        isAddressValid: true,
        isSubmitting: false,
        isSuccess: true,
        isFailure: false,
        status: status);
  }

  AddressFormState update({bool isNameValid, DioStatus status}) {
    return copyWith(
        isAddressValid:isAddressValid,
        isSubmitting: false,
        isSuccess: false,
        isFailure: false,
        status: status);
  }

  AddressFormState copyWith({
    bool isAddressValid,
    bool isSubmitting,
    bool isSuccess,
    bool isFailure,
    DioStatus status,
  }) {
    return AddressFormState(
      isAddressValid: isAddressValid ?? this.isAddressValid,
      isSubmitting: isSubmitting ?? this.isSubmitting,
      isSuccess: isSuccess ?? this.isSuccess,
      isFailure: isFailure ?? this.isFailure,
      status: status ?? this.status,
    );
  }

  @override
  String toString() {
    return 'NameFormState{isAddressValid: $isAddressValid, isSubmitting: $isSubmitting, isSuccess: $isSuccess, isFailure: $isFailure, status: $status}';
  }
}
