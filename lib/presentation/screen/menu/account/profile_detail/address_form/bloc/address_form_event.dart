import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class AddressFormEvent extends Equatable {
  const AddressFormEvent();

  @override
  List<Object> get props => [];
}

class AddressFormChanged extends AddressFormEvent {
  final String AddressForm;

  AddressFormChanged({@required this.AddressForm});

  @override
  List<Object> get props => [AddressForm];

  @override
  String toString() {
    return 'AddressFormChanged{AddressForm: $AddressForm}';
  }
}


class AddressFormSubmitEvent extends AddressFormEvent {
  final String AddressForm;

  AddressFormSubmitEvent(
      {@required this.AddressForm});

  @override
  List<Object> get props => [AddressForm];

  @override
  String toString() {
    return 'AddressFormSubmitEvent{AddressForm: $AddressForm}';
  }
}



