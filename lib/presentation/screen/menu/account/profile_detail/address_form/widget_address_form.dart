
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:project_bike_car/presentation/screen/menu/account/profile/bloc/bloc.dart';
import 'package:project_bike_car/utils/handler/barrel_handler.dart';
import 'package:project_bike_car/utils/locale/app_localization.dart';
import 'package:project_bike_car/utils/snackbar/barrel_snack_bar.dart';

import 'bloc/bloc.dart';

class WidgetAddressForm extends StatefulWidget {
  final String address;
  final Function onCloseTap;

  const WidgetAddressForm(
      {Key key, @required this.onCloseTap, @required this.address})
      : super(key: key);

  @override
  _WidgetAddressFormState createState() => _WidgetAddressFormState();
}

class _WidgetAddressFormState extends State<WidgetAddressForm> {
  AddressFormBloc _addressFormBloc;
  final TextEditingController _nameController = TextEditingController();

  final underline = new UnderlineInputBorder(
    borderSide: BorderSide(
      color: AppColor.GREY,
    ),
  );

  @override
  void initState() {
    super.initState();
    _addressFormBloc = BlocProvider.of<AddressFormBloc>(context);
    _nameController.text = widget.address ?? '';
    _nameController.addListener(_onNameChange);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<AddressFormBloc, AddressFormState>(
      listener: (context, state) async {
        if (state.isSubmitting) {
          GetSnackBarUtils.createProgress();
        }

        if (state.isSuccess) {
          BlocProvider.of<ProfileBloc>(context).add(LoadProfile());
          await HttpHandler.resolve(status: state.status);
        }

        if (state.isFailure) {
          await HttpHandler.resolve(status: state.status);
        }
      },
      child:
          BlocBuilder<AddressFormBloc, AddressFormState>(builder: (context, state) {
        return Container(
          padding: EdgeInsets.all(AppValue.APP_HORIZONTAL_PADDING),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10), topRight: Radius.circular(10))),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Row(
                children: [
                  Expanded(
                    child: Text(
                      "Địa chỉ",
                      style: TextStyle(color: Colors.green),
                    ),
                  ),
                  GestureDetector(
                    onTap: widget.onCloseTap,
                    child: Container(
                      width: 20,
                      height: 20,
                      child: Image.asset('assets/images/img_close_round.png'),
                    ),
                  )
                ],
              ),
              _buildTextFieldName(),
              _buildButtonSubmit()
            ],
          ),
        );
      }),
    );
  }

  bool get isPopulated => _nameController.text.isNotEmpty;

  bool isSubmitButtonEnabled() {
    return _addressFormBloc.state.isFormValid &&
        isPopulated &&
        !_addressFormBloc.state.isSubmitting;
  }

  _buildButtonSubmit() {
    return WidgetLoginButton(
      onTap: () {
        if (isSubmitButtonEnabled()) {
          _addressFormBloc.add(AddressFormSubmitEvent(
            AddressForm: _nameController.text.trim(),
          ));
        }
      },
      height: 45,
      isEnable: isSubmitButtonEnabled(),
      text: AppLocalizations.of(context).translate('profile_detail.update'),
    );
  }

  _buildTextFieldName() {
    return TextFormField(
        controller: _nameController,
        validator: (_) {
          print('validator');
          return !_addressFormBloc.state.isAddressValid ? '' : null;
        },
        style: AppStyle.DEFAULT_MEDIUM.copyWith(color: AppColor.PRIMARY),
        maxLines: 1,
        decoration: InputDecoration(
            disabledBorder: underline,
            enabledBorder: underline,
            focusedBorder: underline,
            hintText: "Nhập địa chỉ của bạn",
            hintStyle: AppStyle.DEFAULT_MEDIUM.copyWith(color: AppColor.GREY)));
  }

  void _onNameChange() {
    _addressFormBloc.add(AddressFormChanged(
      AddressForm: _nameController.text,
    ));
  }
}
