import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/model/repo/barrel_repo.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

import 'bloc.dart';

class ProfileDetailAvatarBloc extends Bloc<ProfileDetailAvatarEvent, ProfileDetailAvatarState> {
  final UserRepository _userRepository;

  ProfileDetailAvatarBloc({
    @required UserRepository userRepository,
  })  : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  get initialState => ProfileDetailAvatarState.empty();


  @override
  Stream<ProfileDetailAvatarState> mapEventToState(ProfileDetailAvatarEvent event) async* {
    if (event is ProfileDetailAvatarUploadEvent) {
      yield* _mapProfileDetailAvatarUploadEventToState(event.avatarFile);
    }
  }

  Stream<ProfileDetailAvatarState> _mapProfileDetailAvatarUploadEventToState(File avatarFile) async* {
    try {
      yield ProfileDetailAvatarState.loading();

      var response = await _userRepository.updateAvatar(avatarFile: avatarFile);
      print('---token----');
      print(response);

      if (response.status == Endpoint.SUCCESS) {
        yield ProfileDetailAvatarState.success(
            status: DioStatus(
                message: response.msg, code: DioStatus.API_SUCCESS_NOTIFY));
      } else {
        yield ProfileDetailAvatarState.failure(
            status: DioStatus(
                message: response.msg, code: DioStatus.API_FAILURE_NOTIFY));
      }
    } catch (e) {
      yield ProfileDetailAvatarState.failure(status: DioErrorUtil.handleError(e));
    }
  }

//  Stream<ProfileDetailAvatarState> _mapFullBirthdayChangedToState(String Birthday) async* {
//    yield state.update(isBirthdayValid: Validator.isValidFullBirthday(Birthday));
//  }
}
