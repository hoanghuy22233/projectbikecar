
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:project_bike_car/presentation/screen/menu/account/profile/bloc/bloc.dart';
import 'package:project_bike_car/utils/handler/barrel_handler.dart';
import 'package:project_bike_car/utils/locale/app_localization.dart';
import 'package:project_bike_car/utils/snackbar/barrel_snack_bar.dart';

import 'bloc/bloc.dart';

class WidgetNameForm extends StatefulWidget {
  final String name;
  final Function onCloseTap;

  const WidgetNameForm(
      {Key key, @required this.onCloseTap, @required this.name})
      : super(key: key);

  @override
  _WidgetNameFormState createState() => _WidgetNameFormState();
}

class _WidgetNameFormState extends State<WidgetNameForm> {
  NameFormBloc _nameFormBloc;
  final TextEditingController _nameController = TextEditingController();

  final underline = new UnderlineInputBorder(
    borderSide: BorderSide(
      color: AppColor.GREY,
    ),
  );

  @override
  void initState() {
    super.initState();
    _nameFormBloc = BlocProvider.of<NameFormBloc>(context);
    _nameController.text = widget.name ?? '';
    _nameController.addListener(_onNameChange);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<NameFormBloc, NameFormState>(
      listener: (context, state) async {
        if (state.isSubmitting) {
          GetSnackBarUtils.createProgress();
        }

        if (state.isSuccess) {
          BlocProvider.of<ProfileBloc>(context).add(LoadProfile());
          await HttpHandler.resolve(status: state.status);
        }

        if (state.isFailure) {
          await HttpHandler.resolve(status: state.status);
        }
      },
      child:
          BlocBuilder<NameFormBloc, NameFormState>(builder: (context, state) {
        return Container(
          padding: EdgeInsets.all(AppValue.APP_HORIZONTAL_PADDING),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10), topRight: Radius.circular(10))),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Row(
                children: [
                  Expanded(
                    child: Text(
                      "Họ tên",
                      style: TextStyle(color: Colors.green),
                    ),
                  ),
                  GestureDetector(
                    onTap: widget.onCloseTap,
                    child: Container(
                      width: 20,
                      height: 20,
                      child: Image.asset('assets/images/img_close_round.png'),
                    ),
                  )
                ],
              ),
              _buildTextFieldName(),
              _buildButtonSubmit()
            ],
          ),
        );
      }),
    );
  }

  bool get isPopulated => _nameController.text.isNotEmpty;

  bool isSubmitButtonEnabled() {
    return _nameFormBloc.state.isFormValid &&
        isPopulated &&
        !_nameFormBloc.state.isSubmitting;
  }

  _buildButtonSubmit() {
    return WidgetLoginButton(
      onTap: () {
        if (isSubmitButtonEnabled()) {
          _nameFormBloc.add(NameFormSubmitEvent(
            fullname: _nameController.text.trim(),
          ));
        }
      },
      height: 45,
      isEnable: isSubmitButtonEnabled(),
      text: AppLocalizations.of(context).translate('profile_detail.update'),
    );
  }

  _buildTextFieldName() {
    return TextFormField(
        controller: _nameController,
        validator: (_) {
          print('validator');
          return !_nameFormBloc.state.isNameValid ? '' : null;
        },
        style: AppStyle.DEFAULT_MEDIUM.copyWith(color: AppColor.PRIMARY),
        maxLines: 1,
        decoration: InputDecoration(
            disabledBorder: underline,
            enabledBorder: underline,
            focusedBorder: underline,
            hintText: AppLocalizations.of(context)
                .translate('profile_detail.fullname_hint'),
            hintStyle: AppStyle.DEFAULT_MEDIUM.copyWith(color: AppColor.GREY)));
  }

  void _onNameChange() {
    _nameFormBloc.add(FullnameChanged(
      fullname: _nameController.text,
    ));
  }
}
