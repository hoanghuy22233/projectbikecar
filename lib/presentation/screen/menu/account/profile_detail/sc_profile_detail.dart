import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/model/repo/barrel_repo.dart';
import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:project_bike_car/presentation/screen/menu/account/profile/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/account/profile_detail/address_form/barrel_address_form.dart';
import 'package:project_bike_car/presentation/screen/menu/account/profile_detail/address_form/bloc/address_form_bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/account/profile_detail/barrel_profile_detail.dart';
import 'package:project_bike_car/presentation/screen/menu/account/profile_detail/content_form/barrel_content_form.dart';
import 'package:project_bike_car/utils/common/common_utils.dart';
import 'package:project_bike_car/utils/locale/app_localization.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

import 'avatar/bloc/bloc.dart';
import 'background_image/bloc/bloc.dart';
import 'birthday_form/barrel_birthday_form.dart';
import 'birthday_form/bloc/bloc.dart';
import 'bloc/bloc.dart';
import 'content_form/bloc/bloc.dart';
import 'email_form/barrel_email_form.dart';
import 'email_form/bloc/bloc.dart';
import 'name_form/barrel_name_form.dart';
import 'name_form/bloc/bloc.dart';
import 'phone_form/barrel_phone_form.dart';
import 'phone_form/bloc/bloc.dart';

class ProfileDetailScreen extends StatefulWidget {
  @override
  _ProfileDetailScreenState createState() => _ProfileDetailScreenState();
}

class _ProfileDetailScreenState extends State<ProfileDetailScreen>
    with AutomaticKeepAliveClientMixin<ProfileDetailScreen> {
  PanelController _panelController = new PanelController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return MultiBlocProvider(
      providers: [

        BlocProvider(
          create: (context) => NameFormBloc(userRepository: userRepository),
        ),
        BlocProvider(
          create: (context) => PhoneFormBloc(userRepository: userRepository),
        ),
        BlocProvider(
          create: (context) => EmailFormBloc(userRepository: userRepository),
        ),
        BlocProvider(
          create: (context) =>
              BirthdayFormBloc(userRepository: userRepository),
        ),
        BlocProvider(
          create: (context) =>
              ProfileDetailAvatarBloc(userRepository: userRepository),
        ),
        BlocProvider(
          create: (context) => ProfileDetailBackgroundImageBloc(
              userRepository: userRepository),
        ),
        BlocProvider(
          create: (context) => AddressFormBloc(
              userRepository: userRepository),
        ),
        BlocProvider(
          create: (context) => ContentFormBloc(
              userRepository: userRepository),
        ),
      ],
      child: BlocBuilder<ProfileBloc, ProfileState>(builder: (context, state) {
        return SafeArea(
          child: Scaffold(
            body: Container(child: _buildContent(state)),
          ),
        );
      }),
    );
  }

  _buildContent(ProfileState state) {
    if (state is ProfileLoaded) {
      return Column(
        children: [
          _buildAppbar(),
          Expanded(
              child: SlidingUpPanel(
                controller: _panelController,
                body: _buildMenu(state),
                panel: _buildPanel(state),
                minHeight: 0,
                maxHeight: 250,
                backdropEnabled: true,
                renderPanelSheet: false,
              ))
        ],
      );
    } else if (state is ProfileLoading) {
      return Center(
        child: Container(
          height: 100,
          width: 100,
          child: Lottie.asset(
            'assets/lottie/load_car.json',
          ),
        ),
      );
    } else if (state is ProfileNotLoaded) {
      return Center(
        child: Text('${state.error}'),
      );
    } else {
      return Center(
        child: Text('Unknown state'),
      );
    }
  }

  Widget _buildAppbar() => WidgetProfileDetailAppbar();

  Widget _buildPanel(ProfileLoaded profileState) {
    return BlocBuilder<ProfileDetailBloc, ProfileDetailState>(
        builder: (context, state) {
          if (state is ProfileDetailFullnameFormOpened) {
            return WidgetNameForm(
              name: profileState.user.name,
              onCloseTap: () {
                _panelController.close();
                AppCommonUtils.disposeKeyboard();
              },
            );
          }
          if (state is ProfileDetailPhoneFormOpened) {
            return WidgetPhoneForm(
              phone: profileState.user.phone.toString(),
              onCloseTap: () {
                _panelController.close();
                AppCommonUtils.disposeKeyboard();
              },
            );
          }
          if (state is ProfileDetailEmailFormOpened) {
            return WidgetEmailForm(
              email: profileState.user.email,
              onCloseTap: () {
                _panelController.close();
                AppCommonUtils.disposeKeyboard();
              },
            );
          }

          if (state is ProfileDetailContentFormOpened) {
            return WidgetContentForm(content: profileState.user.description,
              onCloseTap: () {
                _panelController.close();
                AppCommonUtils.disposeKeyboard();
              },
            );
          }
          if (state is ProfileDetailAddressFormOpened) {
            return WidgetAddressForm(address: profileState.user.address,
              onCloseTap: () {
                _panelController.close();
                AppCommonUtils.disposeKeyboard();
              },
            );
          }
          if (state is ProfileDetailBirthdayFormOpened) {
            return WidgetBirthdayForm(
              birthDay: profileState.user.birthday,
              onCloseTap: () {
                _panelController.close();
              },
            );
          }


          return WidgetSpacer(
            height: 0,
          );
        });
  }

  Widget _buildMenu(ProfileLoaded state) {
    return RefreshIndicator(
      onRefresh: () async {
        BlocProvider.of<ProfileBloc>(context).add(LoadProfile());
        await Future.delayed(Duration(seconds: 3));
        return true;
      },
      color: AppColor.PRIMARY_COLOR,
//      backgroundColor: AppColor.PRIMARY_COLOR,
      child: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          children: [
            WidgetProfileDetailImage(
              backgroundUrl: state.user.coverImage,
              avatarUrl: state.user.avatar,
            ),
            WidgetProfileDetailRow(
              title: AppLocalizations.of(context)
                  .translate('profile_detail.fullname'),
              content: state.user.name ??
                  AppLocalizations.of(context).translate('profile.no_setup'),
              onTap: () {
                _panelController.open();
                BlocProvider.of<ProfileDetailBloc>(context)
                    .add(OpenFullnameForm());
              },
            ),
            WidgetProfileDetailRow(
              title: AppLocalizations.of(context)
                  .translate('profile_detail.phone'),
              content: state.user.phone.toString() ??
                  AppLocalizations.of(context).translate('profile.no_setup'),
              onTap: () {
               _panelController.open();
                BlocProvider.of<ProfileDetailBloc>(context)
                    .add(OpenPhoneForm());
              },
            ),
            WidgetProfileDetailRow(
              title: AppLocalizations.of(context)
                  .translate('profile_detail.email'),
              content: state.user.email ??
                  AppLocalizations.of(context).translate('profile.no_setup'),
              onTap: () {
                _panelController.open();
                BlocProvider.of<ProfileDetailBloc>(context)
                    .add(OpenEmailForm());
              },
            ),
            WidgetProfileDetailRow(
              title: "Mô tả",
              content: state.user.description??
                  AppLocalizations.of(context).translate('profile.no_setup'),
              onTap: () {
                _panelController.open();
                BlocProvider.of<ProfileDetailBloc>(context)
                    .add(OpenContentForm());
              },
            ),
            WidgetProfileDetailRow(
              title: "Địa chỉ",
              content: state.user.address??
                  AppLocalizations.of(context).translate('profile.no_setup'),
              onTap: () {
                _panelController.open();
                BlocProvider.of<ProfileDetailBloc>(context)
                    .add(OpenAddressForm());
              },
            ),

            WidgetProfileDetailRow(
              title: AppLocalizations.of(context)
                  .translate('profile_detail.birthday'),
              content: state.user.birthday??
                  AppLocalizations.of(context).translate('profile.no_setup'),
              onTap: () {
                _panelController.open();
                BlocProvider.of<ProfileDetailBloc>(context)
                    .add(OpenBirthdayForm());
              },
            ),
            Container(
              height: 300,
              width: double.maxFinite,
              color: AppColor.PRIMARY_COLOR,
            ),
          ],
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
