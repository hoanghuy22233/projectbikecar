import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/model/repo/barrel_repo.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

import 'bloc.dart';

class BirthdayFormBloc extends Bloc<BirthdayFormEvent, BirthdayFormState> {
  final UserRepository _userRepository;

  BirthdayFormBloc({
    @required UserRepository userRepository,
  })  : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  get initialState => BirthdayFormState.empty();


  @override
  Stream<BirthdayFormState> mapEventToState(BirthdayFormEvent event) async* {
    if (event is BirthdayFormSubmitEvent) {
      yield* _mapBirthdayFormSubmitEventToState(event.birthDay);
    }
  }

  Stream<BirthdayFormState> _mapBirthdayFormSubmitEventToState(String birthDay) async* {
    try {
      yield BirthdayFormState.loading();

      var response = await _userRepository.updateBirthDay(birthDay: birthDay);
      print('---token----');
      print(response);

      if (response.status == Endpoint.SUCCESS) {
        yield BirthdayFormState.success(
            status: DioStatus(
                message: response.msg, code: DioStatus.API_SUCCESS_NOTIFY));
      } else {
        yield BirthdayFormState.failure(
            status: DioStatus(
                message: response.msg, code: DioStatus.API_FAILURE_NOTIFY));
      }
    } catch (e) {
      yield BirthdayFormState.failure(status: DioErrorUtil.handleError(e));
    }
  }

}
