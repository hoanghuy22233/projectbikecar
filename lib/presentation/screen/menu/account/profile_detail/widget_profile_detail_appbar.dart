
import 'package:flutter/material.dart';
import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:project_bike_car/utils/locale/app_localization.dart';

class WidgetProfileDetailAppbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: WidgetAppbar(
        title: AppLocalizations.of(context).translate('profile_detail.title').toUpperCase(),
        left: [
          WidgetAppbarMenuBack()
        ],
      ),
    );
  }
}
