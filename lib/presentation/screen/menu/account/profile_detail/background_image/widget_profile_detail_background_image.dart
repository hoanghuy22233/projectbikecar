import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_view_image_profile.dart';
import 'package:project_bike_car/presentation/screen/menu/account/profile/bloc/bloc.dart';
import 'package:project_bike_car/utils/handler/barrel_handler.dart';
import 'package:project_bike_car/utils/snackbar/barrel_snack_bar.dart';

import 'bloc/bloc.dart';

class WidgetProfileDetailBackgroundImage extends StatefulWidget {
  final String backgroundImageUrl;

  const WidgetProfileDetailBackgroundImage(
      {Key key, @required this.backgroundImageUrl})
      : super(key: key);

  @override
  _WidgetProfileDetailBackgroundImageState createState() =>
      _WidgetProfileDetailBackgroundImageState();
}

class _WidgetProfileDetailBackgroundImageState
    extends State<WidgetProfileDetailBackgroundImage> {
  final _picker = ImagePicker();
  PickedFile backgroundImageFile;
  File croppedFile;

  @override
  Widget build(BuildContext context) {
    return BlocListener<ProfileDetailBackgroundImageBloc,
        ProfileDetailBackgroundImageState>(
      listener: (context, state) async {
        if (state.isSubmitting) {
          GetSnackBarUtils.createProgress();
        }

        if (state.isSuccess) {
          BlocProvider.of<ProfileBloc>(context).add(LoadProfile());
          await HttpHandler.resolve(status: state.status);
        }

        if (state.isFailure) {
          await HttpHandler.resolve(status: state.status);
        }
      },
      child: BlocBuilder<ProfileDetailBackgroundImageBloc,
          ProfileDetailBackgroundImageState>(builder: (context, state) {
        return Stack(
          children: [
            Container(
              child: GestureDetector(
                onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => WidgetViewNetworkImageProfile(image: widget.backgroundImageUrl)),
                  );
                },
                child: AspectRatio(
                  aspectRatio: 2.5,
                  child: WidgetCachedImage(
                    url: widget?.backgroundImageUrl??'',
                  ),
                ),
              ),
            ),
            Positioned(
              bottom: 10,
              right: 10,
              child: GestureDetector(
                onTap:(){
                  _buildSheetMethod(context);
                },
                child: Container(
                  width: 40,
                  height: 40,
                  child: WidgetCircleAvatar(
                    border: 3,
                    padding: EdgeInsets.all(6.0),
                    image: Image.asset(
                      'assets/images/img_camera2.png',
                      fit: BoxFit.fitWidth,
                    ),
                    backgroundColor: Color(0xFFD6D5D5),
                  ),
                ),
              ),
            )
          ],
        );
      }),
    );
  }

  Future<Null> _onPickAvatar() async {
    backgroundImageFile = null;
    backgroundImageFile =
    await _picker.getImage(source: ImageSource.gallery, imageQuality: 25);
    croppedFile = File(backgroundImageFile.path);
    if (backgroundImageFile != null) {
      BlocProvider.of<ProfileDetailBackgroundImageBloc>(context)
          .add(ProfileDetailBackgroundImageUploadEvent(backgroundImageFile: croppedFile));



    }
  }

  Future<Null> _onTakeAvatar() async {
    backgroundImageFile = null;
    backgroundImageFile =
    await _picker.getImage(source: ImageSource.camera, imageQuality: 25);
    croppedFile = File(backgroundImageFile.path);
    if (backgroundImageFile != null) {
      BlocProvider.of<ProfileDetailBackgroundImageBloc>(context)
          .add(ProfileDetailBackgroundImageUploadEvent(backgroundImageFile: croppedFile));

    }
  }

  _buildSheetMethod(BuildContext context) {
    showCupertinoModalPopup(
        context: context,
        builder: (context) {
          return CupertinoActionSheet(
            title: Text('Ảnh nền'),
            cancelButton: CupertinoActionSheetAction(
              child: Text('Huỷ'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            actions: [
              CupertinoActionSheetAction(
                onPressed: () {
                  _onPickAvatar();
                },
                child: Text('Chọn ảnh có sẵn'),
              ),
              CupertinoActionSheetAction(
                onPressed: () {
                  _onTakeAvatar();
                },
                child: Text('Chụp ảnh mới'),
              )
            ],
          );
        });
  }
}
