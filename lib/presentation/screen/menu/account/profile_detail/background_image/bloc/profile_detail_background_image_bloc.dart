import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/model/repo/barrel_repo.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

import 'bloc.dart';

class ProfileDetailBackgroundImageBloc extends Bloc<
    ProfileDetailBackgroundImageEvent, ProfileDetailBackgroundImageState> {
  final UserRepository _userRepository;

  ProfileDetailBackgroundImageBloc({
    @required UserRepository userRepository,
  })  : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  get initialState => ProfileDetailBackgroundImageState.empty();

  @override
  Stream<ProfileDetailBackgroundImageState> mapEventToState(
      ProfileDetailBackgroundImageEvent event) async* {
    if (event is ProfileDetailBackgroundImageUploadEvent) {
      yield* _mapProfileDetailBackgroundImageUploadEventToState(
          event.backgroundImageFile);
    }
  }

  Stream<ProfileDetailBackgroundImageState>
      _mapProfileDetailBackgroundImageUploadEventToState(
          File backgroundImageFile) async* {
    try {
      yield ProfileDetailBackgroundImageState.loading();

      var response = await _userRepository.updateBackgroundImage(
          backgroundImageFile: backgroundImageFile);

      if (response.status == Endpoint.SUCCESS) {
        yield ProfileDetailBackgroundImageState.success(
            status: DioStatus(
                message: response.msg, code: DioStatus.API_SUCCESS_NOTIFY));
      } else {
        yield ProfileDetailBackgroundImageState.failure(
            status: DioStatus(
                message: response.msg, code: DioStatus.API_FAILURE_NOTIFY));
      }
    } catch (e) {
      yield ProfileDetailBackgroundImageState.failure(
          status: DioErrorUtil.handleError(e));
    }
  }
}
