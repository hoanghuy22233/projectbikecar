
import 'package:flutter/material.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

class ContentFormState {
  final bool isContentValid;
  final bool isSubmitting;
  final bool isSuccess;
  final bool isFailure;
  final DioStatus status;

  bool get isFormValid => isContentValid;

  ContentFormState({
    @required this.isContentValid,
    @required this.isSubmitting,
    @required this.isSuccess,
    @required this.isFailure,
    @required this.status,
  });

  factory ContentFormState.empty() {
    return ContentFormState(
        isContentValid: true,
        isSubmitting: false,
        isSuccess: false,
        isFailure: false,
        status: null);
  }

  factory ContentFormState.loading() {
    return ContentFormState(
        isContentValid: true,
        isSubmitting: true,
        isSuccess: false,
        isFailure: false,
        status: null);
  }

  factory ContentFormState.failure(DioStatus status) {
    return ContentFormState(
        isContentValid: true,
        isSuccess: false,
        isSubmitting: false,
        isFailure: true,
        status: status);
  }

  factory ContentFormState.success(DioStatus status) {
    return ContentFormState(
        isContentValid: true,
        isSubmitting: false,
        isSuccess: true,
        isFailure: false,
        status: status);
  }

  ContentFormState update({bool isNameValid, DioStatus status}) {
    return copyWith(
        isContentValid:isContentValid,
        isSubmitting: false,
        isSuccess: false,
        isFailure: false,
        status: status);
  }

  ContentFormState copyWith({
    bool isContentValid,
    bool isSubmitting,
    bool isSuccess,
    bool isFailure,
    DioStatus status,
  }) {
    return ContentFormState(
      isContentValid: isContentValid ?? this.isContentValid,
      isSubmitting: isSubmitting ?? this.isSubmitting,
      isSuccess: isSuccess ?? this.isSuccess,
      isFailure: isFailure ?? this.isFailure,
      status: status ?? this.status,
    );
  }

  @override
  String toString() {
    return 'NameFormState{isContentValid: $isContentValid, isSubmitting: $isSubmitting, isSuccess: $isSuccess, isFailure: $isFailure, status: $status}';
  }
}
