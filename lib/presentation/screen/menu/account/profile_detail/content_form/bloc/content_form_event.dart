import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class ContentFormEvent extends Equatable {
  const ContentFormEvent();

  @override
  List<Object> get props => [];
}

class ContentFormChanged extends ContentFormEvent {
  final String ContentForm;

  ContentFormChanged({@required this.ContentForm});

  @override
  List<Object> get props => [ContentForm];

  @override
  String toString() {
    return 'ContentFormChanged{ContentForm: $ContentForm}';
  }
}


class ContentFormSubmitEvent extends ContentFormEvent {
  final String ContentForm;

  ContentFormSubmitEvent(
      {@required this.ContentForm});

  @override
  List<Object> get props => [ContentForm];

  @override
  String toString() {
    return 'ContentFormSubmitEvent{ContentForm: $ContentForm}';
  }
}



