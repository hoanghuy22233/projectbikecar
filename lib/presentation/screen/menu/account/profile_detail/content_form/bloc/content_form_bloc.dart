
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/model/repo/barrel_repo.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';
import 'package:project_bike_car/utils/validator/barrel_validator.dart';
import 'package:rxdart/rxdart.dart';

import 'bloc.dart';

class ContentFormBloc extends Bloc<ContentFormEvent, ContentFormState> {
  final UserRepository _userRepository;

  ContentFormBloc({
    @required UserRepository userRepository,
  })  : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  get initialState => ContentFormState.empty();

  @override
  Stream<Transition<ContentFormEvent, ContentFormState>> transformEvents(
      Stream<ContentFormEvent> events, transitionFn) {
    final nonDebounceStream = events.where((event) {
      return (event is! ContentFormChanged);
    });

    final debounceStream = events.where((event) {
      return (event is ContentFormChanged);
    }).debounceTime(Duration(milliseconds: 300));

    return super.transformEvents(
        nonDebounceStream.mergeWith([debounceStream]), transitionFn);
  }

  @override
  Stream<ContentFormState> mapEventToState(ContentFormEvent event) async* {
    if (event is ContentFormSubmitEvent) {
      yield* _mapContentFormSubmitEventToState(event.ContentForm);
    } else if (event is ContentFormChanged) {
      yield* _mapContentFormChangedToState(event.ContentForm);
    }
  }

  Stream<ContentFormState> _mapContentFormSubmitEventToState(String contentForm) async* {
    try {
      yield ContentFormState.loading();

      var response = await _userRepository.updateContent(content: contentForm);
      print('---token----');
      print(response);

      if (response.status == Endpoint.SUCCESS) {
        yield ContentFormState.success(
            DioStatus(
                message: response.msg, code: DioStatus.API_SUCCESS_NOTIFY));
      } else {
        yield ContentFormState.failure(
            DioStatus(
                message: response.msg, code: DioStatus.API_FAILURE_NOTIFY));
      }
    } catch (e) {
      yield ContentFormState.failure(DioErrorUtil.handleError(e));
    }
  }

  Stream<ContentFormState> _mapContentFormChangedToState(String contentForm) async* {
    yield state.update(isNameValid: Validator.isValidFullname(contentForm));
  }
}
