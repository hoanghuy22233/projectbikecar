import 'package:project_bike_car/app/constants/navigator/navigator.dart';
import 'package:project_bike_car/model/repo/user_repository.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_login_logo.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_spacer.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_touch_hide_keyboard.dart';
import 'package:project_bike_car/presentation/screen/menu/account/change_password/widget_change_password_form.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/change_password_bloc.dart';

class ChangePasswordScreen extends StatefulWidget {
  @override
  _ChangePasswordScreenState createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen> {
  @override
  Widget build(BuildContext context) {
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return WidgetTouchHideKeyBoard(
      child: Scaffold(
        backgroundColor: Color(0xff33CC66),
        body: BlocProvider(
          create: (context) => ChangePasswordBloc(userRepository: userRepository),
          child: SafeArea(
            top: true,
            child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 25,vertical: 20),
                      child: GestureDetector(
                        onTap: () {
                          AppNavigator.navigateBack();
                        },
                        child: Image.asset(
                          "assets/images/ic_arrow.png",
                          width: 20,
                          height: 20,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    Expanded(
                      child: ListView(
                        children: [
                          WidgetSpacer(
                            height: 20,
                          ),
                          _buildImage(),
                          _buildChangePasswordForm(),
                          // _buildBottomChangePassWord(),
                          WidgetSpacer(
                            height: 20,
                          ),
                        ],
                      ),
                    )
                  ],
                )
            ),
          ),
        ),
      ),
    );
  }

  _buildImage() => WidgetLoginLogo();
  _buildChangePasswordForm() => WidgetChangePassWordForm();
}
