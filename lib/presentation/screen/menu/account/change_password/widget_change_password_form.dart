import 'package:project_bike_car/app/constants/color/color.dart';
import 'package:project_bike_car/app/constants/navigator/navigator.dart';
import 'package:project_bike_car/app/constants/preferences/app_preferences.dart';
import 'package:project_bike_car/app/constants/style/style.dart';
import 'package:project_bike_car/app/constants/string/validator.dart';
import 'package:project_bike_car/model/local/pref.dart';
import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_spacer.dart';
import 'package:project_bike_car/utils/snackbar/get_snack_bar_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_icons/flutter_icons.dart';

import 'bloc/change_password_bloc.dart';
import 'bloc/change_password_event.dart';
import 'bloc/change_password_state.dart';

class WidgetChangePassWordForm extends StatefulWidget {
  @override
  _WidgetChangePassWordFormState createState() => _WidgetChangePassWordFormState();
}

class _WidgetChangePassWordFormState extends State<WidgetChangePassWordForm> {
  ChangePasswordBloc _changePasswordBloc;
  final TextEditingController _oldPasswordController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _confirmPasswordController = TextEditingController();

  bool obscurePassword = true;
  bool obscureConfirmPassword = true;
  bool autoValidate = false;

  bool get isPopulated => _oldPasswordController.text.isNotEmpty &&
          _passwordController.text.isNotEmpty &&
          _confirmPasswordController.text.isNotEmpty;

  @override
  void initState() {
    super.initState();
    _changePasswordBloc = BlocProvider.of<ChangePasswordBloc>(context);
    _oldPasswordController.addListener(_onOldPasswordChanged);
    _passwordController.addListener(_onPasswordChanged);
    _confirmPasswordController.addListener(_onPasswordConfirmChanged);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ChangePasswordBloc, ChangePasswordState>(
      listener: (context, state) async {
        if (state.isSubmitting) {
          GetSnackBarUtils.createProgress();
        }

        if (state.isSuccess) {
          await GetSnackBarUtils.createSuccess(message: state.message);
          AppNavigator.navigateLogin(
          );
        }

        if (state.isFailure) {
          GetSnackBarUtils.createError(message: state.message);
          setState(() {
            autoValidate = true;
          });
        }
      },
      child: BlocBuilder<ChangePasswordBloc, ChangePasswordState>(
        builder: (context, state) {
          return Padding(
            padding: EdgeInsets.all(20),
            child: Form(
              child: Column(
                children: [
                  _buildTextFieldUsername(),
                  WidgetSpacer(
                    height: 10,
                  ),
                  _buildTextFieldPassword(),
                  WidgetSpacer(
                    height: 10,
                  ),
                  _buildTextFieldConfirmPassword(),
                  WidgetSpacer(
                    height: 20,
                  ),
                  Container(
                    child: _buildButtonRegister(state),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  bool isRegisterButtonEnabled() {
    return _changePasswordBloc.state.isFormValid &&
        isPopulated &&
        !_changePasswordBloc.state.isSubmitting;
  }

  _buildButtonRegister(ChangePasswordState state) {
    return WidgetLoginButton(
      height: 45,
      onTap: () async {
        final prefs = LocalPref();
        final token = await prefs.getString(AppPreferences.auth_token);
        if (isRegisterButtonEnabled()) {
          _changePasswordBloc.add(ChangePasswordSubmitted(
            token: token ??'',
            oldPassword: _oldPasswordController.text,
            password: _passwordController.text,
            confirmPassword: _confirmPasswordController.text,
          ));
          FocusScope.of(context).unfocus();
        }
      },
      isEnable: isRegisterButtonEnabled(),
      text: "Đồng ý",
    );
  }

  _buildTextFieldUsername() {
    return Container(
      width: MediaQuery.of(context).size.width / 1.15,
      child: TextFormField(
        enableInteractiveSelection: false,
        controller: _oldPasswordController,
        keyboardType: TextInputType.emailAddress ?? TextInputType.text,
        onChanged: (value) {
          // _loginBloc.add(LoginUsernameChanged(email: value));
        },
        style: TextStyle(
            color: Colors.white,
            fontSize: 16,
            fontWeight: FontWeight.bold,
            fontFamily: 'Roboto'),
        validator: AppValidation.validateUserName("Vui lòng điền tài khoản"),
        decoration: InputDecoration(
          hintText: "Mật khẩu cũ",
          hintStyle: TextStyle(color: Colors.white),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
        ),
        // textAlign: TextAlign.start,
      ),
    );
  }
  _buildTextFieldPassword() {
    return Container(
      width: MediaQuery.of(context).size.width / 1.15,
      child: TextFormField(
        enableInteractiveSelection: false,
        controller: _passwordController,
        obscureText: obscurePassword,
        onChanged: (value) {
          // _loginBloc
          //     .add(LoginPasswordChanged(password: value));
        },
        autovalidate: autoValidate ?? false,
        validator: AppValidation.validatePassword("Vui lòng nhập mật khẩu"),
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: "Mật khẩu mới",
          hintStyle: TextStyle(color: Colors.white),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          suffixIcon: IconButton(
            icon: Icon(
              obscurePassword
                  ? MaterialCommunityIcons.eye_outline
                  : MaterialCommunityIcons.eye_off_outline,
              color: AppColor.WHITE,
            ),
            onPressed: () {
              setState(() {
                obscurePassword = !obscurePassword;
              });
            },
          ),
        ),
        style: TextStyle(color: Colors.white, fontSize: 16),
        textAlign: TextAlign.start,
      ),
    ); // Container(

  }

  _buildTextFieldConfirmPassword() {
    return Container(
      width: MediaQuery.of(context).size.width / 1.15,
      child: TextFormField(
        enableInteractiveSelection: false,
        controller: _confirmPasswordController,
        obscureText: obscureConfirmPassword,
        onChanged: (value) {
          // _loginBloc
          //     .add(LoginPasswordChanged(password: value));
        },
        validator: AppValidation.validatePassword("Mật khẩu không hợp lệ"),
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: "Nhập lại mật khẩu mới",
          hintStyle: TextStyle(color: Colors.white),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          suffixIcon: IconButton(
            icon: Icon(
              obscureConfirmPassword
                  ? MaterialCommunityIcons.eye_outline
                  : MaterialCommunityIcons.eye_off_outline,
              color: AppColor.WHITE,
            ),
            onPressed: () {
              setState(() {
                obscureConfirmPassword = !obscureConfirmPassword;
              });
            },
          ),
        ),
        style: TextStyle(color: Colors.white, fontSize: 16),
        textAlign: TextAlign.start,
      ),
    ); // Container(

  }

  void _onOldPasswordChanged() {
    _changePasswordBloc.add(OldPasswordChanged(
      oldPassword: _oldPasswordController.text,
    ));
  }

  void _onPasswordChanged() {
    _changePasswordBloc.add(PasswordChangedTwo(
        password: _passwordController.text,
        confirmPassword: _confirmPasswordController.text));
  }

  void _onPasswordConfirmChanged() {
    _changePasswordBloc.add(ConfirmPasswordChangedTwo(
        password: _passwordController.text,
        confirmPassword: _confirmPasswordController.text));
  }

  @override
  void dispose() {
    super.dispose();
    _oldPasswordController.dispose();
    _passwordController.dispose();
    _confirmPasswordController.dispose();
  }

}
