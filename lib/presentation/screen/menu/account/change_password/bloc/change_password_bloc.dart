import 'package:project_bike_car/app/constants/endpoint/app_endpoint.dart';
import 'package:project_bike_car/model/repo/barrel_repo.dart';
import 'package:project_bike_car/presentation/screen/menu/account/change_password/bloc/change_password_event.dart';
import 'package:project_bike_car/presentation/screen/menu/account/change_password/bloc/change_password_state.dart';
import 'package:project_bike_car/utils/utils.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

class ChangePasswordBloc extends Bloc<ChangePasswordEvent, ChangePasswordState> {
  final UserRepository _userRepository;

  ChangePasswordBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  ChangePasswordState get initialState => ChangePasswordState.empty();

  @override
  Stream<ChangePasswordState> mapEventToState(ChangePasswordEvent event) async* {
   if (event is OldPasswordChanged) {
      yield* _mapOldPasswordChangedToState(event.oldPassword);
    } else if (event is PasswordChangedTwo) {
      yield* _mapPasswordChangedToState(event.password, event.confirmPassword);
    } else if (event is ConfirmPasswordChangedTwo) {
      yield* _mapConfirmPasswordChangedToState(
          event.password, event.confirmPassword);
    } else if (event is ChangePasswordSubmitted) {
      yield* _mapFormSubmittedToState(
        event.token,
        event.oldPassword,
        event.password,
        event.confirmPassword,
      );
    }
  }


  Stream<ChangePasswordState> _mapOldPasswordChangedToState(String oldPassword) async* {
    yield state.update(
      isOldPassword: Validator.isValidName(oldPassword),
    );
  }

  Stream<ChangePasswordState> _mapPasswordChangedToState(
      String password, String confirmPassword) async* {
    var isPasswordValid = Validator.isValidPassword(password);
    var isMatched = true;

    if (confirmPassword.isNotEmpty) {
      isMatched = password == confirmPassword;
    }

    yield state.update(
        isPasswordValid: isPasswordValid, isConfirmPasswordValid: isMatched);
  }

  Stream<ChangePasswordState> _mapConfirmPasswordChangedToState(
      String password, String confirmPassword) async* {
    var isConfirmPasswordValid = Validator.isValidPassword(confirmPassword);
    var isMatched = true;

    if (password.isNotEmpty) {
      isMatched = password == confirmPassword;
    }

    yield state.update(
      isConfirmPasswordValid: isConfirmPasswordValid && isMatched,
    );
  }

  Stream<ChangePasswordState> _mapFormSubmittedToState(
      String token,
      String oldPassword,
      String password,
      String confirmPassword,
      ) async* {
    yield ChangePasswordState.loading();
    print("token:${token} oldPassword:${oldPassword} matkhau:${password} matkhaulai:${confirmPassword}");
    try {
      var response = await _userRepository.changePasswordApp(
        token: token,
        oldPassword: oldPassword,
        password: password,
        confirmPassword: confirmPassword,
      );
      // print("xem kết quả trả về của api response ${response.data}");

      if (response.status == Endpoint.SUCCESS) {
        yield ChangePasswordState.success(message: response.msg);
      } else {
        yield ChangePasswordState.failure(message: "Thông tin điền vào chưa đúng  ");
      }
    } catch (e) {
      print("------ Register: $e");
      yield ChangePasswordState.failure();
    }

  }
}
