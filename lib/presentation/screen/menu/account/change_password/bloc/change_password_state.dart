import 'package:flutter/cupertino.dart';

class ChangePasswordState {
  final bool istoken;
  final bool isOldPassword;
  final bool isPasswordValid;
  final bool isConfirmPasswordValid;
  final bool isSubmitting;
  final bool isSuccess;
  final bool isFailure;
  final String message;

  bool get isFormValid =>
      istoken && isOldPassword && isPasswordValid && isConfirmPasswordValid  ;

  ChangePasswordState(
      {@required this.istoken,
        @required this.isOldPassword,
        @required this.isPasswordValid,
        @required this.isConfirmPasswordValid,
        @required this.isSubmitting,
        @required this.isSuccess,
        @required this.isFailure,
        @required this.message});

  factory ChangePasswordState.empty() {
    return ChangePasswordState(
        istoken: true,
        isOldPassword: true,
        isPasswordValid: true,
        isConfirmPasswordValid: true,
        isSubmitting: false,
        isSuccess: false,
        isFailure: false,
        message: '');
  }

  factory ChangePasswordState.loading() {
    return ChangePasswordState(
        istoken: true,
        isOldPassword: true,
        isPasswordValid: true,
        isConfirmPasswordValid: true,
        isSubmitting: true,
        isSuccess: false,
        isFailure: false,
        message: '');
  }

  factory ChangePasswordState.failure({String message}) {
    return ChangePasswordState(
        istoken: true,
        isOldPassword: true,
        isPasswordValid: true,
        isConfirmPasswordValid: true,
        isSuccess: false,
        isSubmitting: false,
        isFailure: true,
        message: message);
  }

  factory ChangePasswordState.success({String message}) {
    return ChangePasswordState(
        istoken: true,
        isOldPassword: true,
        isPasswordValid: true,
        isConfirmPasswordValid: true,
        isSuccess: true,
        isSubmitting: false,
        isFailure: false,
        message: message);
  }

  ChangePasswordState update(
      {bool istoken,
        bool isOldPassword,
        bool isPasswordValid,
        bool isConfirmPasswordValid,
        String message}) {
    return copyWith(
      istoken:istoken,
      isOldPassword: isOldPassword,
      isPasswordValid: isPasswordValid,
      isConfirmPasswordValid: isConfirmPasswordValid,
      isSubmitting: false,
      isSuccess: false,
      isFailure: false,
      message: message,
    );
  }

  ChangePasswordState copyWith({
    bool istoken,
    bool isOldPassword,
    bool isPasswordValid,
    bool isConfirmPasswordValid,
    bool isSubmitting,
    bool isSuccess,
    bool isFailure,
    String message,
  }) {
    return ChangePasswordState(
      istoken:istoken?? this.istoken,
      isOldPassword: isOldPassword ?? this.isOldPassword,
      isPasswordValid: isPasswordValid ?? this.isPasswordValid,
      isConfirmPasswordValid:
      isConfirmPasswordValid ?? this.isConfirmPasswordValid,
      isSubmitting: isSubmitting ?? this.isSubmitting,
      isSuccess: isSuccess ?? this.isSuccess,
      isFailure: isFailure ?? this.isFailure,
      message: message ?? this.message,
    );
  }
}
