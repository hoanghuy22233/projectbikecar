import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class ChangePasswordEvent extends Equatable {
  const ChangePasswordEvent();

  @override
  List<Object> get props => [];
}
class OldPasswordChanged extends ChangePasswordEvent {
  final String oldPassword;

  const OldPasswordChanged({@required this.oldPassword});

  @override
  List<Object> get props => [oldPassword];

  @override
  String toString() {
    return 'OldPasswordChanged{oldPassword: $oldPassword}';
  }
}
class PasswordChangedTwo extends ChangePasswordEvent {
  final String password;
  final String confirmPassword;

  const PasswordChangedTwo({@required this.password, @required this.confirmPassword});

  @override
  List<Object> get props => [password,confirmPassword];

  @override
  String toString() {
    return 'PasswordChangedTwo{password: $password, confirmPassword: $confirmPassword}';
  }
}
class ConfirmPasswordChangedTwo extends ChangePasswordEvent {
  final String password;
  final String confirmPassword;

  ConfirmPasswordChangedTwo(
      {@required this.password, @required this.confirmPassword});

  @override
  List<Object> get props => [];

  @override
  String toString() {
    return 'ConfirmPasswordChanged{password: $password, confirmPassword: $confirmPassword}';
  }
}

class ChangePasswordSubmitted extends ChangePasswordEvent {
  final String token;
  final String oldPassword;
  final String password;
  final String confirmPassword;

  const ChangePasswordSubmitted({
    @required this.token,
    @required this.oldPassword,
    @required this.password,
    @required this.confirmPassword,

  });

  @override
  List<Object> get props => [token,oldPassword, password, confirmPassword];

  @override
  String toString() {
    return 'Submitted{token: $token, oldPassword: $oldPassword, confirmPassword: $confirmPassword,password: $password, }';
  }
}