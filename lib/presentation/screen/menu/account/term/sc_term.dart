import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/model/entity/term.dart';
import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_appbar_profile.dart';
import 'package:flutter/material.dart';
import 'package:project_bike_car/presentation/screen/menu/account/term/barrel_term.dart';

class TermScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SafeArea(
        child: OrientationBuilder(builder: (context, orientation) {
          if (MediaQuery.of(context).size.width > 600) {
            return   Column(
              children: [
                WidgetTermAppbar(),
                Expanded(
                  child: ListView.builder(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemCount: dataTerm.length,
                    itemBuilder: (context, index){
                      return Padding(
                        padding: EdgeInsets.all(5),
                        child: Column(

                          children: [
                            Row(
                              children: [
                                widgetTitle("${dataTerm[index].title}"),
                              ],
                            ),
                            Row(
                              children: [
                                widgetMenu("${dataTerm[index].menu}"),
                              ],
                            ),
                            Row(
                              children: [
                                widgetDetail("${dataTerm[index].detail}")
                              ],
                            ),



                          ],
                        ),
                      );
                    },

                  ),
                )


              ],
            );
          } else {
            return    Column(
              children: [
                WidgetTermAppbar(),
                Expanded(
                  child: ListView.builder(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemCount: dataTerm.length,
                    itemBuilder: (context, index){
                      return Padding(
                        padding: EdgeInsets.all(5),
                        child: Column(

                          children: [
                            Row(
                              children: [
                                widgetTitle("${dataTerm[index].title}"),
                              ],
                            ),
                            Row(
                              children: [
                                widgetMenu("${dataTerm[index].menu}"),
                              ],
                            ),
                            Row(
                              children: [
                                widgetDetail("${dataTerm[index].detail}")
                              ],
                            ),



                          ],
                        ),
                      );
                    },

                  ),
                )


              ],
            );
          }


        })

       ,
      ),
    );

  }

widgetTitle(text){
    return Flexible(child: Text(text, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),));
}

widgetDetail(text){
    return Flexible(child: Text(text, style: TextStyle(fontSize: 12),));
}
widgetMenu(text){
    return Flexible(child:Text(text, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14)));
}







}
