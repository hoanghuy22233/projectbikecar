import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:project_bike_car/utils/utils.dart';
import 'package:flutter/material.dart';

class WidgetTermAppbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: WidgetAppbar(
        title: AppLocalizations.of(context).translate('term_title.appbar'),
        left: [
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: GestureDetector(
              onTap: () {
                AppNavigator.navigateBack();
              },
              child: Image.asset(
                "assets/images/ic_arrow.png",
                width: 20,
                height: 20,
                color: Colors.white,
              ),
            ),
          )
        ],
      ),
    );
  }
}
