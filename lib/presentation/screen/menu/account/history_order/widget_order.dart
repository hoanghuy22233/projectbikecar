import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:project_bike_car/presentation/screen/menu/account/history_order/bloc/bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:sticky_headers/sticky_headers.dart';

class WidgetOrder extends StatefulWidget {
  @override
  _WidgetOrderState createState() =>
      _WidgetOrderState();
}

class _WidgetOrderState extends State<WidgetOrder>
    with AutomaticKeepAliveClientMixin<WidgetOrder> {
  RefreshController _refreshController =
  RefreshController(initialRefresh: false);

  @override
  void initState() {
    super.initState();
    BlocProvider.of<HistoryBloc>(context)
        .add(LoadHistory());
  }

  _onRefresh() async {
    BlocProvider.of<HistoryBloc>(context)
        .add(RefreshHistory());
  }

  _onLoadMore() async {
    BlocProvider.of<HistoryBloc>(context)
        .add(LoadHistory());
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocListener<HistoryBloc, HistoryState>(
      listener: (context, state) {
        if (state is HistoryLoaded) {
        }
      },
      child: BlocBuilder<HistoryBloc, HistoryState>(
        builder: (context, state) {
          return Container(
            child: _buildContent(state),
          );
        },
      ),
    );
  }

  Widget _buildContent(HistoryState state) {
    if (state is HistoryLoaded) {
      return Container(
        margin: EdgeInsets.only(top: 10),
          child: _buildContents(state));
    } else if (state is HistoryLoading) {
      return Container(
        width: double.infinity,
        height: AppValue.PRODUCT_HORIZONTAL_HEIGHT,
        child: Center(
          child: Container(
            height: 100,
            width: 100,
            child: Lottie.asset(
              'assets/lottie/load_car.json',
            ),
          ),
        ),
      );
    } else {
      return Center(
        child: Text('Unknown state'),
      );
    }
  }
  _buildContents(HistoryLoaded state){
    return state.order.length != 0 ?
    WidgetRefresher(
      refreshController: _refreshController,
      onLoading: _onLoadMore,
      onRefresh: _onRefresh,
      scrollDirection: Axis.vertical,
      child: ListView.separated(
        scrollDirection: Axis.vertical,
        itemBuilder: (context, index) {
          return StickyHeader(
            header: Container(
              height: 50.0,
              color: Color(0xff33CC66),                        // decoration: BoxDecoration(
              padding: EdgeInsets.symmetric(horizontal: 20.0),
              alignment: Alignment.centerLeft,
              child: Row(
                children: [
                  Expanded(child:  Text(
                    "${state.order[index].created}",
                    style: const TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  )),
                  state.order[index].status!=0?Text(
                    "Đã xử lý",
                    style: const TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ):Text(
                    "Chờ xử lý",
                    style: const TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  )

                ],
              ),
            ),
            content: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start ,
                children: [
                  Text("Tên Xe: "+ state.order[index].product.name,  maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: AppStyle.DEFAULT_MEDIUM.copyWith(
                        color: Colors.blue,fontWeight: FontWeight.bold
                    ),
                    textAlign: TextAlign.start,),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    children: [
                      Text("Giá xe: ",style: const TextStyle(
                          color: Colors.blue, fontWeight: FontWeight.bold),),
                      SizedBox(
                        width: 5,
                      ),
                      state.order[index].product.priceOld!=0?

                      Text("${AppValue.APP_MONEY_FORMAT.format(state.order[index].product.priceOld)}",style: const TextStyle(
                          color: Colors.blue, fontWeight: FontWeight.bold),)
                          :Text("${AppValue.APP_MONEY_FORMAT.format(state.order[index].product.price)} ",style: const TextStyle(
                          color: Colors.blue, fontWeight: FontWeight.bold),),
                    ],
                  ),

                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    children: [
                      Text("Người đặt: ",style: const TextStyle(
                          color: Colors.blue, fontWeight: FontWeight.bold),),
                      SizedBox(
                        width: 5,
                      ),
                      Text("${state.order[index].email}",style: const TextStyle(
                          color: Colors.blue, fontWeight: FontWeight.bold),)
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    children: [
                      Text("Số điện thoại: ",style: const TextStyle(
                          color: Colors.blue, fontWeight: FontWeight.bold),),
                      SizedBox(
                        width: 5,
                      ),
                      Text("0"+"${ state.order[index].phone}",style: const TextStyle(
                          color: Colors.blue, fontWeight: FontWeight.bold),)
                    ],
                  ),
                ],
              ),
            ),
          );

        },
        itemCount: state.order.length,
        separatorBuilder: (context, index) {
          return WidgetSpacer(height: 15);
        },
        physics: BouncingScrollPhysics(),
      ),
    )
        : Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
              height: 120,
              width: 120,
              child: Image.asset('assets/images/box.png')
          ),
          SizedBox(height: 20,),
          Text('Bạn chưa hoàn thành đơn hàng nào!'),
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
