
import 'package:equatable/equatable.dart';
import 'package:project_bike_car/model/entity/order.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

abstract class HistoryState extends Equatable {
  const HistoryState();

  @override
  List<Object> get props => [];
}

class HistoryLoading extends HistoryState {}

class HistoryLoaded extends HistoryState {
  final List<Order> order;

  const HistoryLoaded({ this.order});

  HistoryLoaded copyWith({
    List<Order> order,
  }) {
    return HistoryLoaded(
      order: order ?? this.order,
    );
  }

  @override
  List<Object> get props => [order,];
}

class HistoryNotLoaded extends HistoryState {
  final DioStatus status;

  HistoryNotLoaded({this.status});

  @override
  String toString() {
    return 'HistoryNotLoaded{status: $status}';
  }
}
