
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:project_bike_car/model/entity/order.dart';
import 'package:project_bike_car/model/repo/barrel_repo.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

import 'bloc.dart';

class HistoryBloc
    extends Bloc<HistoryEvent, HistoryState> {
  final UserRepository userRepository;

  HistoryBloc({@required this.userRepository});

  @override
  HistoryState get initialState => HistoryLoading();

  @override
  Stream<HistoryState> mapEventToState(
      HistoryEvent event) async* {
    if (event is LoadHistory) {
      yield* _mapLoadHistoryToState();
    } else if (event is RefreshHistory) {
      yield HistoryLoading();
      yield* _mapLoadHistoryToState();
    }
  }

  Stream<HistoryState> _mapLoadHistoryToState() async* {
    final currentState = state;
    try {
      if (state is HistoryLoading) {
        final notification = await _fetchNotification();
        yield HistoryLoaded(
            order: notification);
      }
      if (currentState is HistoryLoaded) {
        final notification = await _fetchNotification(
        );
//      await Future.delayed(Duration(seconds: 5), () {});
        yield HistoryLoaded(
                order: currentState.order,

              );
      }
    } catch (e) {
      yield HistoryNotLoaded(status: DioErrorUtil.handleError(e));
    }
  }

  Future<List<Order>> _fetchNotification() async {
    try {
      final response = await userRepository.getOrder();
      return response.data;
    } catch (e) {
      throw Exception('$e');
    }
  }
}
