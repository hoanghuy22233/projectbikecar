import 'package:equatable/equatable.dart';

class HistoryEvent extends Equatable {
  const HistoryEvent();

  List<Object> get props => [];
}

class LoadHistory extends HistoryEvent {
}

class RefreshHistory extends HistoryEvent {}
