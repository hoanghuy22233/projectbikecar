import 'package:project_bike_car/app/constants/navigator/navigator.dart';
import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:flutter/material.dart';
import 'package:project_bike_car/presentation/screen/menu/account/history_order/barrel_order.dart';

class HistoryOrderPageScreen extends StatefulWidget {
  @override
  _HistoryOrderPageScreenState createState() => _HistoryOrderPageScreenState();
}

class _HistoryOrderPageScreenState extends State<HistoryOrderPageScreen>
    with
        AutomaticKeepAliveClientMixin<HistoryOrderPageScreen>,
        TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      body: SafeArea(
        child: OrientationBuilder(builder: (context, orientation) {
          if (MediaQuery.of(context).size.width > 600) {
            return    Column(
              children: [
                Container(
                  //padding: EdgeInsets.only(top: 10),
                  child: WidgetAppbar(
                    title: "Lịch sử đơn hàng".toUpperCase(),
                    left: [
                      Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: GestureDetector(
                          onTap: () {
                            AppNavigator.navigateBack();
                          },
                          child: Image.asset(
                            "assets/images/ic_arrow.png",
                            width: 20,
                            height: 20,
                            color: Colors.white,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Expanded(child: _buildOrder())
              ],
            );
          } else {
            return     Column(
              children: [
                Container(
                  child: WidgetAppbar(
                    title: "Lịch sử đơn hàng".toUpperCase(),
                    left: [
                      Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: GestureDetector(
                          onTap: () {
                            AppNavigator.navigateBack();
                          },
                          child: Image.asset(
                            "assets/images/ic_arrow.png",
                            width: 20,
                            height: 20,
                            color: Colors.white,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Expanded(child: _buildOrder())
              ],
            );
          }


        })

      ,
      ),
    );
  }

  Widget _buildOrder() => WidgetOrder();

  @override
  bool get wantKeepAlive => true;
}
