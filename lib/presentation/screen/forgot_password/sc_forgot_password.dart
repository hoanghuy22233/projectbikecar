import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/model/repo/user_repository.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_appbar_profile.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_touch_hide_keyboard.dart';
import 'package:project_bike_car/presentation/screen/forgot_password/widget_bottom_login.dart';
import 'package:project_bike_car/presentation/screen/forgot_password/widget_forgot_password_form.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/forgot_password_bloc.dart';

class ForgotPasswordScreen extends StatefulWidget {
  @override
  _ForgotPasswordScreenState createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  @override
  Widget build(BuildContext context) {
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return WidgetTouchHideKeyBoard(
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Color(0xff33CC66),
          body: BlocProvider(
            create: (context) =>
                ForgotPasswordBloc(userRepository: userRepository),
            child: Container(
                // color: AppColor.PRIMARY_BACKGROUND,
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 15),
                      child: _buildAppbar(),
                    ),

                    Expanded(
                        child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 20,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 25),
                            child: Text("Đổi Mật Khẩu",style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold,color: Colors.white,fontFamily: 'Roboto'),),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          _buildForgotPasswordForm(),
                          _buildBottomLogin()
                        ],
                      ),
                    )),
                  ],
                )),
          ),
        ),
      ),
    );
  }

  _buildAppbar() => WidgetAppbarProfile(
    backgroundColor: Color(0xff33CC66),
    textColor: Colors.white,
    left: [
      Padding(
        padding: const EdgeInsets.only( left: 10),
        child: GestureDetector(
          onTap: () {
            AppNavigator.navigateBack();
          },
          child: Image.asset(
            "assets/images/ic_arrow.png",
            width: 20,
            height: 20,
            color: Colors.white,
          ),
        ),
      )
    ],
  );


  _buildForgotPasswordForm() => WidgetForgotPasswordForm();
  _buildBottomLogin() => WidgetBottomLogin();
}
