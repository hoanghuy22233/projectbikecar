import 'package:project_bike_car/app/constants/navigator/navigator.dart';
import 'package:project_bike_car/model/repo/user_repository.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_login_logo.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_spacer.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_touch_hide_keyboard.dart';
import 'package:project_bike_car/presentation/screen/register/widget_bottom_login.dart';
import 'package:project_bike_car/presentation/screen/register/widget_register_form.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/register_bloc.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  @override
  Widget build(BuildContext context) {
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return WidgetTouchHideKeyBoard(
      child: Scaffold(
        backgroundColor: Color(0xff33CC66),
        body: BlocProvider(
          create: (context) => RegisterBloc(userRepository: userRepository),
          child: SafeArea(
            top: false,
            child: Container(
              padding: EdgeInsets.only(top: 50),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 25,vertical: 20),
                      child: GestureDetector(
                        onTap: () {
                          AppNavigator.navigateBack();
                        },
                        child: Image.asset(
                          "assets/images/ic_arrow.png",
                          width: 20,
                          height: 20,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    Expanded(
                      child: ListView(
                        children: [
                          WidgetSpacer(
                            height: 20,
                          ),
                          _buildImage(),
                          _buildRegisterForm(),
                          _buildBottomLogin(),
                          WidgetSpacer(
                            height: 20,
                          ),
                        ],
                      ),
                    )
                  ],
                )
            ),
          ),
        ),
      ),
    );
  }

  _buildImage() => WidgetLoginLogo();
  _buildRegisterForm() => WidgetRegisterForm();
  _buildBottomLogin() => WidgetBottomLogin();
}
