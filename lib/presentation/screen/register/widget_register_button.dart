import 'package:flutter/material.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';

class WidgetRegisterButton extends StatelessWidget {
  final Function onTap;
  final String text;
  final Color backgroundColor;
  final double height;
  final isEnable;

  const WidgetRegisterButton({Key key, this.onTap, this.text,this.height, this.isEnable, this.backgroundColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width-40,
      height: height.toString() != null ? height : 45,
      child: GestureDetector(
        onTap: onTap,
        child: Card(
          elevation: 10,
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          color: AppColor.WORK_COLOR,
          child: Center(
              child: Text(
                text,
                style: AppStyle.DEFAULT_MEDIUM.copyWith(color: AppColor.WHITE),
              )),
        ),
      ),
    );
  }
}
