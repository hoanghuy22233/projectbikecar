import 'package:project_bike_car/app/constants/color/color.dart';
import 'package:project_bike_car/app/constants/navigator/navigator.dart';
import 'package:project_bike_car/app/constants/style/style.dart';
import 'package:project_bike_car/app/constants/string/validator.dart';
import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_spacer.dart';
import 'package:project_bike_car/presentation/screen/register/widget_register_button.dart';
import 'package:project_bike_car/utils/snackbar/get_snack_bar_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_icons/flutter_icons.dart';

import 'bloc/register_bloc.dart';
import 'bloc/register_event.dart';
import 'bloc/register_state.dart';

class WidgetRegisterForm extends StatefulWidget {
  @override
  _WidgetRegisterFormState createState() => _WidgetRegisterFormState();
}

class _WidgetRegisterFormState extends State<WidgetRegisterForm> {
  RegisterBloc _registerBloc;
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _confirmPasswordController = TextEditingController();

  bool obscurePassword = true;
  bool obscureConfirmPassword = true;
  bool autoValidate = false;
  bool _visible = false;

  bool get isPopulated =>
      _nameController.text.isNotEmpty &&
          _emailController.text.isNotEmpty &&
          _passwordController.text.isNotEmpty &&
          _confirmPasswordController.text.isNotEmpty;
  void _toggle() {
    setState(() {
      _visible = !_visible;
    });
  }

  @override
  void initState() {
    super.initState();
    _registerBloc = BlocProvider.of<RegisterBloc>(context);
    _nameController.addListener(_onNameChange);
    _emailController.addListener(_onEmailChange);
    _passwordController.addListener(_onPasswordChanged);
    _confirmPasswordController.addListener(_onPasswordConfirmChanged);

    _nameController.text = '';
    _emailController.text = '';
    _passwordController.text = '';
    _confirmPasswordController.text = '';
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<RegisterBloc, RegisterState>(
      listener: (context, state) async {
        if (state.isSubmitting) {
          GetSnackBarUtils.createProgress();
        }

        if (state.isSuccess) {
          await GetSnackBarUtils.createSuccess(message: state.message);
         AppNavigator.navigateLogin();
        }

        if (state.isFailure) {
          GetSnackBarUtils.createError(message: state.message);
          setState(() {
            autoValidate = true;
          });
        }
      },
      child: BlocBuilder<RegisterBloc, RegisterState>(
        builder: (context, state) {
          return Padding(
            padding: EdgeInsets.all(20),
            child: Form(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _buildTextFieldName(),
                  WidgetSpacer(
                    height: 10,
                  ),
                  _buildTextFieldUEmail(),
                  WidgetSpacer(
                    height: 10,
                  ),
                  _buildTextFieldPassword(),
                  WidgetSpacer(
                    height: 10,
                  ),
                  _buildTextFieldConfirmPassword(),
                  WidgetSpacer(
                    height: 20,
                  ),
                  Container(
                    child: _buildButtonRegister(state),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  bool isRegisterButtonEnabled() {
    return _registerBloc.state.isFormValid &&
        isPopulated &&
        !_registerBloc.state.isSubmitting;
  }

  _buildButtonRegister(RegisterState state) {
    // return WidgetRegisterButton(
    //   height: 45,
    //   onTap: () {
    //     _emailController.text.trim()==''?
    //   _registerBloc.add(RegisterSubmitted(
    //       username: _nameController.text.trim(),
    //       email: _phoneController.text.trim(),
    //       phone: _phoneController.text.trim(),
    //       password: _passwordController.text.trim(),
    //       confirmPassword: _confirmPasswordController.text.trim()
    //   )): _registerBloc.add(RegisterSubmitted(
    //         username: _nameController.text.trim(),
    //         email: _emailController.text.trim(),
    //         phone: _phoneController.text.trim(),
    //         password: _passwordController.text.trim(),
    //         confirmPassword: _confirmPasswordController.text.trim()
    //     ));
    //   FocusScope.of(context).unfocus();
    //     }
    //   ,
    // //  isEnable: isRegisterButtonEnabled(),
    //   text: "Đăng ký",
    // );

    return  WidgetLoginButton(
      height: 45,
      onTap: () {
        if (isRegisterButtonEnabled()) {
         _registerBloc.add(RegisterSubmitted(
              username: _nameController.text.trim(),
              email: _emailController.text.trim(),
              password: _passwordController.text.trim(),
              confirmPassword: _confirmPasswordController.text.trim()
          ));
          FocusScope.of(context).unfocus();
        }
      },
      isEnable: isRegisterButtonEnabled(),
      text: "Đăng ký",
    );
  }
  _buildTextFieldName() {
    return Container(
      width: MediaQuery.of(context).size.width / 1.15,
      child: TextFormField(
        enableInteractiveSelection: false,
        controller: _nameController,
        autovalidate: autoValidate,
        keyboardType: TextInputType.emailAddress ?? TextInputType.text,
        onChanged: (value) {
          // _loginBloc.add(LoginUsernameChanged(email: value));
        },
        style: TextStyle(
            color: Colors.white,
            fontSize: 16,
            fontWeight: FontWeight.bold,
            fontFamily: 'Roboto'),
        validator: AppValidation.validateUserName("Vui lòng điền tên"),
        decoration: InputDecoration(
          hintText: "Tên",
          hintStyle: TextStyle(color: Colors.white),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
        ),
        // textAlign: TextAlign.start,
      ),
    );
  }

  _buildTextFieldUEmail() {
    return Container(
      width: MediaQuery.of(context).size.width / 1.15,
      child: TextFormField(
        enableInteractiveSelection: false,
        controller: _emailController,
        autovalidate: autoValidate,
        keyboardType: TextInputType.emailAddress ?? TextInputType.text,
        onChanged: (value) {
          // _loginBloc.add(LoginUsernameChanged(email: value));
        },
        style: TextStyle(
            color: Colors.white,
            fontSize: 16,
            fontWeight: FontWeight.bold,
            fontFamily: 'Roboto'),
        validator: AppValidation.validateUserName("Vui lòng điền chính xác email hoặc số điện thoại!"),
        decoration: InputDecoration(
          hintText: "Nhập email hoặc số điện thoại",
          hintStyle: TextStyle(color: Colors.white),

          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
        ),
        // textAlign: TextAlign.start,
      ),
    );
  }



  _buildTextFieldPassword() {
    return Container(
      width: MediaQuery.of(context).size.width / 1.15,
      child: TextFormField(
        enableInteractiveSelection: false,
        controller: _passwordController,
        obscureText: obscurePassword,

        onChanged: (value) {
          // _loginBloc
          //     .add(LoginPasswordChanged(password: value));
        },
        autovalidate: autoValidate ?? false,
        validator: AppValidation.validatePassword("Vui lòng nhập mật khẩu"),
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: "Mật khẩu",
          hintStyle: TextStyle(color: Colors.white),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          suffixIcon: IconButton(
            icon: Icon(
              obscurePassword
                  ? MaterialCommunityIcons.eye_outline
                  : MaterialCommunityIcons.eye_off_outline,
              color: AppColor.WHITE,
            ),
            onPressed: () {
              setState(() {
                obscurePassword = !obscurePassword;
              });
            },
          ),
        ),
        style: TextStyle(color: Colors.white, fontSize: 16),
        textAlign: TextAlign.start,
      ),
    ); // Container(

  }

  _buildTextFieldConfirmPassword() {
    return Container(
      width: MediaQuery.of(context).size.width / 1.15,
      child: TextFormField(
        enableInteractiveSelection: false,
        controller: _confirmPasswordController,
        obscureText: obscureConfirmPassword,
        autovalidate: autoValidate,
        onChanged: (value) {
          // _loginBloc
          //     .add(LoginPasswordChanged(password: value));
        },
        validator: AppValidation.validatePassword("Mật khẩu không hợp lệ"),
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: "Nhập lại mật khẩu",
          hintStyle: TextStyle(color: Colors.white),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          suffixIcon: IconButton(
            icon: Icon(
              obscureConfirmPassword
                  ? MaterialCommunityIcons.eye_outline
                  : MaterialCommunityIcons.eye_off_outline,
              color: AppColor.WHITE,
            ),
            onPressed: () {
              setState(() {
                obscureConfirmPassword = !obscureConfirmPassword;
              });
            },
          ),
        ),
        style: TextStyle(color: Colors.white, fontSize: 16),
        textAlign: TextAlign.start,
      ),
    ); // Container(

  }
  void _onNameChange() {
    _registerBloc.add(UsernameChanged(
      username: _nameController.text,
    ));
  }
  void _onEmailChange() {
    _registerBloc.add(EmailChanged(
      email: _emailController.text,
    ));
  }

  void _onPasswordChanged() {
    _registerBloc.add(PasswordChanged(
        password: _passwordController.text,
        confirmPassword: _confirmPasswordController.text));
  }

  void _onPasswordConfirmChanged() {
    _registerBloc.add(ConfirmPasswordChanged(
        password: _passwordController.text,
        confirmPassword: _confirmPasswordController.text));
  }

  @override
  void dispose() {
    super.dispose();
    _nameController.dispose();
    _emailController.dispose();
    _passwordController.dispose();
    _confirmPasswordController.dispose();
  }

}
