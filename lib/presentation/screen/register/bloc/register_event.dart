import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class RegisterEvent extends Equatable {
  const RegisterEvent();

  @override
  List<Object> get props => [];
}

class UsernameChanged extends RegisterEvent {
  final String username;

  const UsernameChanged({@required this.username});

  @override
  List<Object> get props => [username];

  @override
  String toString() {
    return 'UsernameChanged{username: $username}';
  }
}
class EmailChanged extends RegisterEvent {
  final String email;

  const EmailChanged({@required this.email});

  @override
  List<Object> get props => [email];

  @override
  String toString() {
    return 'EmailChanged{email: $email}';
  }
}


class PasswordChanged extends RegisterEvent {
  final String password;
  final String confirmPassword;

  PasswordChanged({@required this.password, @required this.confirmPassword});

  @override
  List<Object> get props => [];

  @override
  String toString() {
    return 'PasswordChanged{password: $password, confirmPassword: $confirmPassword}';
  }
}

class ConfirmPasswordChanged extends RegisterEvent {
  final String password;
  final String confirmPassword;

  ConfirmPasswordChanged(
      {@required this.password, @required this.confirmPassword});

  @override
  List<Object> get props => [];

  @override
  String toString() {
    return 'ConfirmPasswordChanged{password: $password, confirmPassword: $confirmPassword}';
  }
}

class RegisterSubmitted extends RegisterEvent {
  final String username;
  final String email;
  final String password;
  final String confirmPassword;

  const RegisterSubmitted({
    @required this.username,
    @required this.email,
    @required this.password,
    @required this.confirmPassword,
  });

  @override
  List<Object> get props => [username,email, password, confirmPassword];

  @override
  String toString() {
    return 'Submitted{username: $username,email:$email, password: $password, confirmPassword: $confirmPassword}';
  }
}
