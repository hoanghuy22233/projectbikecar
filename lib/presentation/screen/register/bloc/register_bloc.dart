import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/model/repo/barrel_repo.dart';
import 'package:project_bike_car/utils/validator/barrel_validator.dart';

import 'bloc.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final UserRepository _userRepository;

  RegisterBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  RegisterState get initialState => RegisterState.empty();

  @override
  Stream<RegisterState> mapEventToState(RegisterEvent event) async* {
    if (event is UsernameChanged) {
      yield* _mapUsernameChangedToState(event.username);
    } else if (event is EmailChanged) {
      yield* _mapEmailChangedToState(event.email);
    } else if (event is PasswordChanged) {
      yield* _mapPasswordChangedToState(event.password, event.confirmPassword);
    } else if (event is ConfirmPasswordChanged) {
      yield* _mapConfirmPasswordChangedToState(
          event.password, event.confirmPassword);
    } else if (event is RegisterSubmitted) {
      yield* _mapFormSubmittedToState(event.username, event.email,
          event.password, event.confirmPassword);
    }
  }

  Stream<RegisterState> _mapUsernameChangedToState(String userName) async* {
    yield state.update(
      isUsernameValid: Validator.isValidUsername(userName),
    );
  }

  Stream<RegisterState> _mapEmailChangedToState(String email) async* {
    yield state.update(
      isUsernameValid: Validator.isValidEmail(email),
    );
  }


  Stream<RegisterState> _mapPasswordChangedToState(
      String password, String confirmPassword) async* {
    var isPasswordValid = Validator.isValidPassword(password);
    var isMatched = true;

    if (confirmPassword.isNotEmpty) {
      isMatched = password == confirmPassword;
    }

    yield state.update(
        isPasswordValid: isPasswordValid, isConfirmPasswordValid: isMatched);
  }

  Stream<RegisterState> _mapConfirmPasswordChangedToState(
      String password, String confirmPassword) async* {
    var isConfirmPasswordValid = Validator.isValidPassword(confirmPassword);
    var isMatched = true;

    if (password.isNotEmpty) {
      isMatched = password == confirmPassword;
    }

    yield state.update(
      isConfirmPasswordValid: isConfirmPasswordValid && isMatched,
    );
  }

  Stream<RegisterState> _mapFormSubmittedToState(String username, String email, String password, String confirmPassword) async* {
    yield RegisterState.loading();

    try {
      var response = await _userRepository.registerApp(
          email: email,
          name: username,
          password: password,
          confirmPassword: confirmPassword);
      if (response.status == Endpoint.SUCCESS) {
        yield RegisterState.success(message: response.msg);
      } else {
        yield RegisterState.failure(message: response.msg);
      }
    } catch (e) {
      print("------ Register: $e");
      yield RegisterState.failure();
    }
  }
}
