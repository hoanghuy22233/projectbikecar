import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/model/entity/product_detail.dart';
import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_appbar_profile.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_dialog.dart';
import 'package:project_bike_car/presentation/screen/detail_product/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/account/profile/bloc/bloc.dart';

import 'payment_form/widget_payment_form.dart';

class PaymentScreen extends StatefulWidget {
  final ProductDetail productDetail;

  PaymentScreen({this.productDetail});

  @override
  _PaymentScreenState createState() => _PaymentScreenState(productDetail);
}

class _PaymentScreenState extends State<PaymentScreen> {
  ProductDetail productDetail;

  _PaymentScreenState(this.productDetail);
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _phoneNumberController = TextEditingController();
  bool get isPopulated =>
      _usernameController.text.isNotEmpty &&
      _phoneNumberController.text.isNotEmpty;

  @override
  void initState() {
    super.initState();

    // _onArgument();
  }

  @override
  Widget build(BuildContext context) {
    return WidgetTouchHideKeyBoard(
      child: Scaffold(
        backgroundColor: Color(0xff33CC66),
        body: SafeArea(
          top: true,
          child: Container(
            color: Colors.white,
            child: _buildContent(),
          ),
        ),
      ),
    );
  }

  Widget _buildContent() {
    return Container(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
          Stack(
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height / 7,
                color: Color(0xff33CC66),
                child: Column(
                  children: [
                    _buildAppbar(),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 70, left: 15, right: 15),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height / 7,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                  ),
                  child: Row(
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width / 2.5,
                        height: MediaQuery.of(context).size.height,
                        child: ClipRRect(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10),
                              bottomLeft: Radius.circular(10)),
                          child: Image.network(
                            productDetail?.image ?? '',
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 30),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(
                              productDetail.name.length <= 17
                                  ? productDetail.name
                                  : productDetail.name.substring(0, 17) + '...',
                              style: AppStyle.APP_MEDIUM_BOLD.copyWith(
                                  color: AppColor.BLACK,
                                  fontWeight: FontWeight.bold),
                              textAlign: TextAlign.start,
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              '${AppValue.APP_MONEY_FORMAT.format(productDetail.price)}' ??
                                  '',
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
          Expanded(
              child: RefreshIndicator(
                  onRefresh: () async {
//                  BlocProvider.of<PaymentBloc>(context).add(RefreshPayment());
                    await Future.delayed(AppValue.FAKE_TIME_RELOAD);
                    return true;
                  },
                  color: AppColor.PRIMARY_COLOR,
                  backgroundColor: AppColor.THIRD_COLOR,
                  child: SingleChildScrollView(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        color: AppColor.GREY_LIGHTER_3,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width,
                              height: 5,
                              color: Colors.grey[200],
                            ),
                            Padding(
                              padding:
                                  EdgeInsets.only(left: 20, top: 20, right: 20),
                              child: Text(
                                "THÔNG TIN THANH TOÁN ",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                    fontSize: 20),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  left: 20, top: 20, bottom: 20, right: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Row(
                                    children: [
                                      Expanded(
                                          flex: 4,
                                          child: Text(
                                            "Số tiền cần trả",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 18,
                                                color: Colors.grey),
                                          )),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Align(
                                        alignment: Alignment.bottomRight,
                                        child: Text(
                                          '${AppValue.APP_MONEY_FORMAT.format(productDetail.price)}' ??
                                              '',
                                          style: TextStyle(
                                              fontSize: 16,
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Row(
                                    children: [
                                      Expanded(
                                        flex: 5,
                                        child: Text(
                                          "Hình thức thanh toán",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18,
                                              color: Colors.grey),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Row(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(1.0),
                                        child: Stack(
                                          children: [
                                            Container(
                                              width: 30,
                                              height: 30,
                                              child: ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(500),
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                      color: Colors.white,
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              500),
                                                      border: Border.all(
                                                          color:
                                                              Color(0xff33CC66),
                                                          width: 1)),
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(4.0),
                                              child: Container(
                                                width: 22,
                                                height: 22,
                                                decoration: BoxDecoration(
                                                  color: Color(0xff33CC66),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          500),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Text(
                                        "Tiền mặt hoặc chuyển khoản",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 18,
                                            color: Colors.grey),
                                      ),
                                    ],
                                  ),
                            ],
                              ),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              height: 5,
                              color: Colors.grey[200],
                            ),
                            Padding(
                              padding:
                                  EdgeInsets.only(left: 20, top: 20, right: 20),
                              child: Text(
                                "THÔNG TIN VẬN CHUYỂN ",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                    fontSize: 20),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  left: 20, top: 20, bottom: 20, right: 10),
                              child: Text(
                                "Người mua cần đến trực tiếp cửa hàng để nhận sản phẩm!",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 14,
                                    color: Colors.grey),
                              ),
                            ),
                            _buildForm()
                          ],
                        ),
                      )
                    ],
                  )))),
        ]));
  }

  Widget _buildForm() => WidgetPaymentForm(
        productId: productDetail?.id ?? 0,
      );

  _buildAppbar() => WidgetAppbarProfile(
        backgroundColor: Colors.blue,
        textColor: Colors.white,
        title: "Thông tin đơn hàng",
        left: [
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: GestureDetector(
              onTap: () {
                AppNavigator.navigateBack();
              },
              child: Image.asset(
                "assets/images/ic_arrow.png",
                width: 20,
                height: 20,
                color: Colors.white,
              ),
            ),
          )
        ],
      );
}
