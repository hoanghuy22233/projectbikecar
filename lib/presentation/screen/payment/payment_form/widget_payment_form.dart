import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_dialog.dart';
import 'package:project_bike_car/presentation/screen/menu/account/profile/bloc/bloc.dart';
import 'package:project_bike_car/utils/dialog/barrel_dialog.dart';
import 'package:project_bike_car/utils/handler/barrel_handler.dart';
import 'package:project_bike_car/utils/locale/app_localization.dart';
import 'package:project_bike_car/utils/snackbar/barrel_snack_bar.dart';

import 'buy_bloc/bloc.dart';
import 'buy_bloc/buy_bloc.dart';
import 'payment_form_total/widget_payment_form_total.dart';

// ignore: must_be_immutable
class WidgetPaymentForm extends StatefulWidget {
  int productId;

  WidgetPaymentForm({this.productId});

  @override
  _WidgetPaymentFormState createState() => _WidgetPaymentFormState();
}

class _WidgetPaymentFormState extends State<WidgetPaymentForm> {
  BuyBloc _paymentFormBloc;

  @override
  void initState() {
    super.initState();
    _paymentFormBloc = BlocProvider.of<BuyBloc>(context);
    BlocProvider.of<ProfileBloc>(context).add(LoadProfile());
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<BuyBloc, BuyState>(
      listener: (context, state) async {
        if (state.isSubmitting) {
          GetSnackBarUtils.createProgress();
        }
        if (state.isSuccess) {
          GetSnackBarUtils.createSuccess(message: state.message);
          FocusScope.of(context).unfocus();
          AppNavigator.navigateHistoryOder();
          Fluttertoast.showToast(
              msg: "yêu cầu của bạn đã được gửi đi. Vui lòng chờ chủ Shop duyệt và liên hệ!",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.green,
              textColor: Colors.white,
              fontSize: 16.0
          );
        }
        if (state.isFailure) {
          //GetSnackBarUtils.createError(message: state.message);
          GetSnackBarUtils.removeSnackBar();
          Fluttertoast.showToast(
              msg: "Sản phẩm đã có trong danh sách đặt hàng của bạn!",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.green,
              textColor: Colors.white,
              fontSize: 16.0
          );
          AppNavigator.navigateBack();

        }
      },
      child:
      BlocBuilder<BuyBloc, BuyState>(builder: (context, state) {
        return Container(
          child: Column(
            children: [
              Container(
                width: double.infinity,
                padding: EdgeInsets.all(8.0),
                color: AppColor.WHITE,
                child: WidgetPaymentFormTotal(productId: widget.productId,),
              ),
            ],
          ),
        );
      }),
    )

     ;


  }

}
