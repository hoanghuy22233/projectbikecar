import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lottie/lottie.dart';
import 'package:project_bike_car/app/auth_bloc/bloc.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/model/local/barrel_local.dart';
import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_appbar_profile.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_dialog.dart';
import 'package:project_bike_car/presentation/screen/menu/account/profile/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/payment/payment_form/buy_bloc/bloc.dart';
import 'package:project_bike_car/utils/dialog/barrel_dialog.dart';
import 'package:project_bike_car/utils/utils.dart';

// ignore: must_be_immutable
class WidgetPaymentFormTotal extends StatelessWidget {
  int productId;

  WidgetPaymentFormTotal({this.productId});

  @override
  Widget build(BuildContext context) {
    return Container(child: BlocBuilder<BuyBloc, BuyState>(
      builder: (context, state) {
        return _buildContent(context, state);
      },
    ));
  }

  Widget _buildContent(BuildContext context, BuyState state) {
    return Column(
      children: [
        BlocBuilder<ProfileBloc, ProfileState>(builder: (context, state) {
          if (state is ProfileLoaded) {
            return _buildContents(context, state);
          } else if (state is ProfileLoading) {
            return Center(
              child: Container(
                height: 100,
                width: 100,
                child: Lottie.asset(
                  'assets/lottie/load_car.json',
                ),
              ),
            );
          } else if (state is ProfileNotLoaded) {
            return Center(
              child: Text('${state.error}'),
            );
          } else {
            return Center(
              child: Text('Unknown state'),
            );
          }
        }),

      ],
    );
  }

  _buildContents(BuildContext context, ProfileLoaded state) {

    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      child: FlatButton(
        onPressed: () async{

          Dialog(context,state);




        },
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        child: Text(
          "Tiếp tục",
          style: AppStyle.DEFAULT_MEDIUM.copyWith(color: AppColor.WHITE),
        ),
        color: AppColor.WORK_COLOR,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(500.0)),
      ),
    );
  }

  Widget Dialog(BuildContext context, ProfileLoaded state){
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return WidgetDialog(
            title: 'Thông báo',
            image: 'assets/images/logo_car_two.png',
            content: "Đặt cọc tối thiểu là 10 % giá trị mặt hàng.\n"
                "Người đặt cung cấp : họ tên và số điện thoại.\n"
                "Khi đặt cọc xong, quý khách gửi biên lai chuyển tiền đến zalo :0888898000 để chúng tôi xác nhận.\n"
                "Quý khách vui lòng chuyển khoản qua 1 trong những ngân hàng dưới đây:"
               ,
            bank: "+ Vietcombank: 6.9999.66666 Nguyễn Quốc Cường\n"
          "+ Vietcombank: 0371004888888 - Nguyễn Trần Trọng Đạt \n"
          "+ ACB: 88198888888 - Nguyễn Trần Trọng Đạt\n"
          "+ Sacombank: 060195886868 - Nguyễn Trần Trọng Đạt\n"
          "+ Vietinbank: 108803078888 - Nguyễn Trần Trọng Đạt\n"
          "+ Momo: 0888898000 Nguyễn trần trọng đạt",
            action1: () async{
              final prefs = LocalPref();
              final token = await prefs.getString(AppPreferences.auth_token);
              if (BlocProvider.of<AuthenticationBloc>(
                  context)
                  .state is! Authenticated) {
                GetDialogUtils.createNotify(
                    message: AppLocalizations.of(context)
                        .translate(
                        'authentication.message'),
                    positiveLabel: AppLocalizations.of(
                        context)
                        .translate(
                        'authentication.positive'),
                    negativeLabel: AppLocalizations.of(
                        context)
                        .translate(
                        'authentication.negative'),
                    onPositiveTap: () {
                      AppNavigator.navigateLogin();
                    });
              } else {
                BlocProvider.of<BuyBloc>(context).add(AddCart(

                  email: state?.user?.name ?? '' + state?.user?.email ?? '',
                  address: state?.user?.address ?? '',
                  content: "Tôi cần cửa hàng hỗ trợ!",
                  phone: state?.user?.phone.toString()??012345678,
                  apiToken: token ?? '',productId: productId??'',));
                FocusScope.of(context).unfocus();
              }
            },
            titleAction1: 'Đồng ý',

            action2: (){
              AppNavigator.navigateBack();
            },
            titleAction2: 'Huỷ bỏ',
          );
        });
  }



}
