import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/model/repo/barrel_repo.dart';

import 'bloc.dart';

class BuyBloc
    extends Bloc<BuyEvent, BuyState> {
  final UserRepository _userRepository;

  StreamSubscription _streamSubscription;

  BuyBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  BuyState get initialState => BuyState.empty();

  @override
  Stream<BuyState> mapEventToState(
      BuyEvent event) async* {
    if (event is AddCart) {
      yield* _mapAddCartToState(event.productId,event.apiToken,event.email,event.address,event.content,event.phone);
    }
  }


  Stream<BuyState> _mapAddCartToState(
 int productId ,
 String apiToken,
   String email,
   String address,
  String content,
   String phone) async* {
    yield BuyState.loading();
    try {

      var response = await _userRepository.productBuy(productId:productId  ,token: apiToken,email: email,address: address,content: content,phone: phone);

      if (response.status == Endpoint.SUCCESS) {
        yield BuyState.success(message: response.msg);
      } else {
        yield BuyState.failure(message: response.msg);
      }
    } catch (e) {
      print(e.toString());
      yield BuyState.failure(message: e.toString());
    }
  }


  @override
  Future<void> close() {
    _streamSubscription.cancel();
    return super.close();
  }
}
