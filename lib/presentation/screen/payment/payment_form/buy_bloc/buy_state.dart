import 'package:flutter/material.dart';

class BuyState {
  final bool isCourseIdValid;
  final bool isApiTokenValid;
  final bool isSubmitting;
  final bool isSuccess;
  final bool isFailure;
  final String message;
  final String token;

  bool get isFormValid => isCourseIdValid && isApiTokenValid;

  BuyState(
      {@required this.isCourseIdValid,
        @required this.isApiTokenValid,
        @required this.isSubmitting,
        @required this.isSuccess,
        @required this.isFailure,
        @required this.message,
        this.token});

  factory BuyState.empty() {
    return BuyState(
        isCourseIdValid: true,
        isApiTokenValid: true,
        isSubmitting: false,
        isSuccess: false,
        isFailure: false,
        message: '',
        token: '');
  }

  factory BuyState.loading() {
    return BuyState(
        isCourseIdValid: true,
        isApiTokenValid: true,
        isSubmitting: true,
        isSuccess: false,
        isFailure: false,
        message: '',
        token: '');
  }

  factory BuyState.failure({String message}) {
    return BuyState(
        isCourseIdValid: true,
        isApiTokenValid: true,
        isSuccess: false,
        isSubmitting: false,
        isFailure: true,
        message: message,
        token: '');
  }

  factory BuyState.success({String message, @required String token}) {
    return BuyState(
        isCourseIdValid: true,
        isApiTokenValid: true,
        isSubmitting: false,
        isSuccess: true,
        isFailure: false,
        message: message,
        token: token);
  }

  BuyState update(
      {bool isCourseIdValid, bool isApiTokenValid, String message}) {
    return copyWith(
        isCourseIdValid: isCourseIdValid,
        isApiTokenValid: isApiTokenValid,
        isSubmitting: false,
        isSuccess: false,
        isFailure: false,
        message: message);
  }

  BuyState copyWith({
    bool isCourseIdValid,
    bool isApiTokenValid,
    bool isSubmitting,
    bool isSuccess,
    bool isFailure,
    String message,
  }) {
    return BuyState(
      isCourseIdValid: isCourseIdValid ?? this.isCourseIdValid,
      isApiTokenValid: isApiTokenValid ?? this.isApiTokenValid,
      isSubmitting: isSubmitting ?? this.isSubmitting,
      isSuccess: isSuccess ?? this.isSuccess,
      isFailure: isFailure ?? this.isFailure,
      message: message ?? this.message,
    );
  }

  @override
  String toString() {
    return 'AddCartState{isCourseIdValid: $isCourseIdValid, isApiTokenValid: $isApiTokenValid, isSubmitting: $isSubmitting, isSuccess: $isSuccess, isFailure: $isFailure, message: $message}';
  }
}
