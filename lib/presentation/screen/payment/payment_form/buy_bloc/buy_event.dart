import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class BuyEvent extends Equatable {
  const BuyEvent();

  @override
  List<Object> get props => [];
}


class AddCart extends BuyEvent {
  final int productId ;
  final String apiToken;
  final String email;
  final String address;
  final String content;
  final String phone;


  const AddCart({
    @required this.productId,
    @required this.apiToken,@required this.email,
    @required this.address,@required this.content,
    @required this.phone,
  });

  @override
  List<Object> get props => [productId,apiToken,email,address,content,phone];

  @override
  String toString() {
    return 'AddCart{productId: $productId, apiToken: $apiToken,email:$email,address:$address,content:$content,phone:$phone}';
  }
}
