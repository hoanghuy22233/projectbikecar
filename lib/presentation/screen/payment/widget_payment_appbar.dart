import 'package:flutter/material.dart';
import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';

class WidgetPaymentAppbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: WidgetAppbar(
        title: ('Tiếp tục').toUpperCase(),
        left: [WidgetAppbarMenuBack()],
      ),
    );
  }
}
