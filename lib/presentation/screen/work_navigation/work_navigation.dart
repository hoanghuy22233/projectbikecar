import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/presentation/screen/menu/account/profile/sc_profile.dart';
import 'package:project_bike_car/presentation/screen/menu/home/sc_home.dart';
import 'package:project_bike_car/presentation/screen/menu/news/sc_news.dart';
import 'package:project_bike_car/presentation/screen/menu/notification/sc_notification.dart';
import 'package:project_bike_car/presentation/screen/menu/store/sc_store.dart';
import 'package:project_bike_car/presentation/screen/work_navigation/widget_work_bottom_nav.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:project_bike_car/utils/handler/with_auth.dart';

class TabNavigatorRoutesWork {
//  static const String root = '/';
  static const String home = '/home';
  static const String store = '/store';
  static const String news = '/news';
  static const String notification = '/notification';
  static const String account = '/account';
}

class WorkNavigationScreen extends StatefulWidget {
  @override
  _WorkNavigationScreen createState() => _WorkNavigationScreen();
}

class _WorkNavigationScreen extends State<WorkNavigationScreen> {
  PageController _pageController;
  int id;

  List<WorkFABBottomNavItem> _navMenus = List();
  int _selectedIndex = 0;
  GlobalKey<ScaffoldState> _drawerKey = GlobalKey();

  List<WorkFABBottomNavItem> _getTab() {
    return List.from([
      WorkFABBottomNavItem.asset(
          route: TabNavigatorRoutesWork.home,
          tabItem: TabWorkItem.home,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/icons/home.png',
          text: 'Trang chủ'),
      WorkFABBottomNavItem.asset(
          route: TabNavigatorRoutesWork.store,
          tabItem: TabWorkItem.store,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/images/store123.png',
          text: "Cửa hàng"),
      WorkFABBottomNavItem.asset(
          route: TabNavigatorRoutesWork.news,
          tabItem: TabWorkItem.news,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/icons/new.png',
          text: "Tin tức"),
      WorkFABBottomNavItem.asset(
          route: TabNavigatorRoutesWork.notification,
          tabItem: TabWorkItem.notification,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/images/notification (1).png',
          text: "Thông báo"),
      WorkFABBottomNavItem.asset(
          route: TabNavigatorRoutesWork.account,
          tabItem: TabWorkItem.account,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/icons/businessman.png',
          text: "Tài khoản"),
    ]);
  }

  goToPage({int page, int id = 0,}) {
    WithAuth.isAuth(ifNotAuth: () {
      if (page == 4||page==3) {
        AppNavigator.navigateLogin();
      } else {
        if (page != _selectedIndex) {
          setState(() {
            this._selectedIndex = page;
          });
          _pageController.jumpToPage(_selectedIndex);
        }
      }
    }, ifAuth: () {
      if (page != _selectedIndex) {
        setState(() {
          this._selectedIndex = page;
        });
        _pageController.jumpToPage(_selectedIndex);
      }
    });

  }

  @override
  void initState() {
    _pageController =
    new PageController(initialPage: _selectedIndex, keepPage: true);
    super.initState();
    // print("Tesst $_drawerKey");

  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _navMenus = _getTab();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: WorkWidgetFABBottomNav(
          notchedShape: CircularNotchedRectangle(),
          backgroundColor: Colors.grey[200],

          // backgroundColor: Colors.white,
          selectedIndex: _selectedIndex,
          onTabSelected: (index) async {
            goToPage(page: index);
          },
          //  centerItemText: "A",
          items: _navMenus,
          selectedColor: AppColor.WORK_COLOR,
          color: Colors.grey,
          // color: AppColor.NAV_ITEM_COLOR,
        ),
        body: PageView(
          controller: _pageController,
          physics: NeverScrollableScrollPhysics(),
          onPageChanged: (newPage) {
            setState(() {
              this._selectedIndex = newPage;
            });
          },
          children: [
            HomePageScreen( drawer: _drawerKey,
              moveTab: goToPage,),
            StorePageScreen(),
            NewsPageScreen(),
            NotificationPageScreen(),
            ProfileScreen(),
          ],
        ));
  }
}
