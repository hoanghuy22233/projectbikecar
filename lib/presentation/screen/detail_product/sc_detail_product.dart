import 'dart:io';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:project_bike_car/app/auth_bloc/bloc.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/app/constants/navigator/navigator.dart';
import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_appbar_profile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_view_image.dart';
import 'package:project_bike_car/presentation/screen/detail_product/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/payment/sc_payment.dart';
import 'package:project_bike_car/utils/dialog/barrel_dialog.dart';
import 'package:project_bike_car/utils/locale/app_localization.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';

class DetailProductPageScreen extends StatefulWidget {
  @override
  _DetailProductPageScreenState createState() =>
      _DetailProductPageScreenState();
}

class _DetailProductPageScreenState extends State<DetailProductPageScreen> {
  int _productId;
  bool isReadDetail = false;
  static const paddingHeight = 8.0;

  _onArgument() {
    Future.delayed(Duration.zero, () async {
      final Map arguments = ModalRoute.of(context).settings.arguments as Map;
      setState(() {
        _productId = arguments['product_id'];
        print('---_productId---');
        print(_productId);

        BlocProvider.of<ProductDetailBloc>(context)
            .add(LoadProductDetail(_productId));
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _onArgument();
  }

  var _selected = 0;
  @override
  Widget build(BuildContext context) {
    return  BlocBuilder<ProductDetailBloc, ProductDetailState>(
      builder: (context, state) {
        return  SafeArea(
          top: true,
          child: Scaffold(
            backgroundColor: Colors.white,
            body: Column(
              children: [
                _buildAppbar(),
                Expanded(child: _buildContents(state)),
                _buildbottom(state)
              ],
            ),
          ),
        );

      },
    );
  }

  Widget _buildContents(ProductDetailState state) {
    if (state is ProductDetailLoaded) {
      var detail = state.detail;
      return ListView(
        children: [
          detail.images != null
              ? Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height / 2.5,
            child: Swiper(
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => WidgetViewNetworkImage(
                            image: state.detail.image,
                            detail: state.detail,
                          )),
                    );
                  },
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height / 2.5,
                        child: WidgetCachedImage(
                          fit: BoxFit.cover,
                          url: state.detail.images[index],
                        ),
                      ),
                      Image.asset(
                        "assets/images/logo-removebg-preview.png",
                        height: 100,
                        width: 100,
                      )
                    ],
                  ),
                );
              },
              autoplay: true,
              itemCount: state.detail.images.length,
            ),
          )
              : Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height / 2.5,
            child: Swiper(
              itemBuilder: (BuildContext context, int index) {
                return state.detail.image != null
                    ? Stack(
                  alignment: Alignment.center,
                  children: [

                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height / 2.5,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    WidgetViewNetworkImage(
                                        image: state.detail.image)),
                          );
                        },
                        child: WidgetCachedImage(
                            url: state.detail.image),
                      ),
                    ),
                    Image.asset(
                      "assets/images/logo-removebg-preview.png",
                      height: 100,
                      width: 100,
                    )
                  ],
                )
                    : Image.asset(
                  "assets/images/logo_xe.jpg",
                  fit: BoxFit.fill,
                );
              },
              autoplay: true,
              itemCount: 1,
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 20, top: 20, bottom: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  detail.name,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                      color: Colors.grey),
                ),
                SizedBox(
                  height: 5,
                ),
                state?.detail?.priceOld != 0
                    ? Text(
                  '${AppValue.APP_MONEY_FORMAT.format(state.detail.priceOld)}',
                  style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      color: Colors.red),
                )
                    : Text(
                    '${AppValue.APP_MONEY_FORMAT.format(state.detail.price)}',
                    style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: Colors.red)),
                SizedBox(
                  height: 5,
                ),
                state?.detail?.price != 0
                    ? Row(
                  children: [
                    Text(
                      "Giá đã giảm: ",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 14,
                          color: Colors.black),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      '${AppValue.APP_MONEY_FORMAT.format(state.detail.price)}',
                      style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                          color: Colors.black),
                    ),
                  ],
                )
                    : Container(),
                SizedBox(
                  height: 10,
                ),
                state.detail.linkVideo != null
                    ? GestureDetector(
                  onTap: () {
                    _openUrl(state.detail.linkVideo);
                  },
                  child: Row(
                    children: [
                      Image.asset(
                        "assets/images/video-player.png",
                        height: 25,
                        width: 25,
                        color: Colors.green,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        "Xem chi tiết về sản phẩm",
                        style: TextStyle(
                          color: Colors.green,
                          fontWeight: FontWeight.bold,
                          decoration: TextDecoration.underline,
                        ),
                      )
                    ],
                  ),
                )
                    : Container()
              ],
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: 5,
            color: Colors.grey[300],
          ),
          _buildDetail(state)
        ],
      );
    } else if (state is ProductDetailLoading) {
      return SizedBox(
        width: Get.width,
        height: Get.height - AppValue.ACTION_BAR_HEIGHT,
        child: Center(
          child: Container(
            height: 100,
            width: 100,
            child:Lottie.asset(
              'assets/lottie/load_car.json',
            ),
          ),
        ),
      );
    } else if (state is ProductDetailNotLoaded) {
      return WidgetScreenError(
        status: state.status,
        inScroll: true,
      );
    } else {
      return Center(
        child: Text('Unknown state'),
      );
    }
  }

  Widget _buildbottom(ProductDetailState state) {
    if (state is ProductDetailLoaded) {
      return Container(
        height: 70,
        color: Color(0xff33CC66),
        child: DefaultTabController(
          length: 3,
          child: TabBar(
            indicatorSize: TabBarIndicatorSize.tab,
            labelColor: AppColor.WHITE,
            labelStyle: AppStyle.DEFAULT_SMALL,
            indicator: BoxDecoration(color: Color(0xFFFF8900)),
            tabs: [
              Tab(
                  icon: Image.asset(
                    "assets/images/iconfinder_phone-call_2561306.png",
                    height: 20,
                    width: 20,
                    color: Colors.white,
                  ),
                  text: 'Gọi điện'),
              Tab(
                  icon: Image.asset("assets/images/shopping-bag.png",
                      height: 20, width: 20, color: Colors.white),
                  text: 'Đặt cọc'),
              Tab(
                icon: Image.asset(
                  "assets/images/zalo.png",
                  height: 20,
                  width: 20,
                ),
                text: "Chat Zalo",
              ),
            ],
            onTap: (value) {
              setState(() {
                _selected = value;
                if (_selected == 0) {
                  _callMe();
                } else if (_selected == 2) {
                  _zaloMe();

                  // _openMap();

                } else if (_selected == 1) {
                  if (BlocProvider.of<AuthenticationBloc>(context).state
                      is! Authenticated) {
                    GetDialogUtils.createNotify(
                        message: AppLocalizations.of(context)
                            .translate('authentication.message'),
                        positiveLabel: AppLocalizations.of(context)
                            .translate('authentication.positive'),
                        negativeLabel: AppLocalizations.of(context)
                            .translate('authentication.negative'),
                        onPositiveTap: () {
                          AppNavigator.navigateLogin();
                        });
                  } else {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) =>
                            PaymentScreen(productDetail: state.detail),
                      ),
                    );
                  }
                }
              });
            },
          ),
        ),
      );
    } else if (state is ProductDetailLoading) {
      return SizedBox(
        width: Get.width,
        height: Get.height - AppValue.ACTION_BAR_HEIGHT,
        child:Center(child: Container(
          height: 100,
          width: 100,
          child: Lottie.asset(
            'assets/lottie/load_car.json',
          ),
        ),
      ));
    } else if (state is ProductDetailNotLoaded) {
      return WidgetScreenError(
        status: state.status,
        inScroll: true,
      );
    } else {
      return Center(
        child: Text('Unknown state'),
      );
    }
  }

  _buildAppbar() => WidgetAppbarProfile(
        backgroundColor: Color(0xff33CC66),
        textColor: Colors.white,
        title: "Chi tiết sản phẩm",
        left: [
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: GestureDetector(
              onTap: () {
                AppNavigator.navigateBack();
              },
              child: Image.asset(
                "assets/images/ic_arrow.png",
                width: 20,
                height: 20,
                color: Colors.white,
              ),
            ),
          )
        ],
      );

  _buildDetail(ProductDetailLoaded state) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Mô tả :",
            style: AppStyle.DEFAULT_LARGE.copyWith(color: AppColor.PRIMARY),
          ),
          WidgetSpacer(
            height: paddingHeight * 0.6,
          ),
          HtmlWidget(
            state.detail.content,
          ),
        ],
      ),
    );
  }

  _callMe() async {
    // Android
    const uri = 'tel://0888898000';
    if (await canLaunch(uri)) {
      await launch(uri);
    } else {
      // iOS
      const uri = 'tel://0888898000';
      if (await canLaunch(uri)) {
        await launch(uri);
      } else {
        throw 'Could not launch $uri';
      }
    }
  }

  _zaloMe() async {
    // Android
    const uri = 'https://zalo.me/0888898000';
    if (await canLaunch(uri)) {
      await launch(uri);
    } else {
      // iOS
      const uri = 'https://zalo.me/0888898000';
      if (await canLaunch(uri)) {
        await launch(uri);
      } else {
        throw 'Could not launch $uri';
      }
    }
  }

  _openUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
