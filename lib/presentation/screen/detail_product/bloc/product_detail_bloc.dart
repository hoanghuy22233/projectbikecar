import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'package:project_bike_car/model/repo/barrel_repo.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

import 'bloc.dart';

class ProductDetailBloc extends Bloc<ProductDetailEvent, ProductDetailState> {
  final UserRepository homeRepository;

  ProductDetailBloc({@required this.homeRepository});

  @override
  ProductDetailState get initialState => ProductDetailLoading();

  @override
  Stream<ProductDetailState> mapEventToState(ProductDetailEvent event) async* {
    if (event is LoadProductDetail) {
      yield* _mapLoadProductDetailToState(event.productId);
    } else if (event is RefreshProductDetail) {
      yield ProductDetailLoading();
      yield* _mapLoadProductDetailToState(event.productId);
    }
  }

  Stream<ProductDetailState> _mapLoadProductDetailToState(int productId) async* {
    try {
      final response = await homeRepository.getProductDetail(productId: productId);
      yield ProductDetailLoaded(detail: response.data);
    } catch (e) {
      yield ProductDetailNotLoaded(DioErrorUtil.handleError(e));
    }
  }
}
