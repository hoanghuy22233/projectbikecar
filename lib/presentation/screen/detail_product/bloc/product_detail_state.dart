import 'package:equatable/equatable.dart';
import 'package:project_bike_car/model/entity/product_detail.dart';
import 'package:project_bike_car/utils/dio/barrel_dio.dart';

abstract class ProductDetailState extends Equatable {
  const ProductDetailState();

  @override
  List<Object> get props => [];
}

class ProductDetailLoading extends ProductDetailState {}

class ProductDetailLoaded extends ProductDetailState {
  final ProductDetail detail;
  const ProductDetailLoaded({this.detail});

  @override
  List<Object> get props => [detail];

  @override
  String toString() {
    return 'ProductDetailLoaded{news: $detail}';
  }
}

class ProductDetailNotLoaded extends ProductDetailState {
  final DioStatus status;

  ProductDetailNotLoaded(this.status);

  @override
  String toString() {
    return 'ProductDetailNotLoaded{error: $status}';
  }
}
