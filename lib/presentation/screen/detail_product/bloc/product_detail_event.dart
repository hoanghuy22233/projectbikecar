import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class ProductDetailEvent extends Equatable {
  const ProductDetailEvent();

  List<Object> get props => [];
}

class LoadProductDetail extends ProductDetailEvent {
  final int productId;

  LoadProductDetail(this.productId);

  @override
  String toString() {
    return 'LoadProductDetail{productId: $productId}';
  }

  List<Object> get props => [productId];
}

class RefreshProductDetail extends ProductDetailEvent {
  final int productId;

  RefreshProductDetail(this.productId);

  @override
  String toString() {
    return 'RefreshProductDetail{productId: $productId}';
  }

  List<Object> get props => [productId];
}