import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_spacer.dart';
import 'package:project_bike_car/utils/common/common_utils.dart';
import 'package:project_bike_car/utils/locale/app_localization.dart';
import 'package:project_bike_car/utils/snackbar/get_snack_bar_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_icons/flutter_icons.dart';

import '../forgot_password_verify/forgot_password_verify_resend/widget_forgot_password_verify_resend.dart';
import '../forgot_password_verify/sc_forgot_password_verify.dart';
import 'bloc/forgot_password_reset_bloc.dart';
import 'bloc/forgot_password_reset_event.dart';
import 'bloc/forgot_password_reset_state.dart';

class WidgetForgotPasswordResetForm extends StatefulWidget {
  final String username;
  final String otpCode;

  const WidgetForgotPasswordResetForm({Key key, this.username, this.otpCode})
      : super(key: key);

  @override
  _WidgetForgotPasswordResetFormState createState() =>
      _WidgetForgotPasswordResetFormState();
}

class _WidgetForgotPasswordResetFormState
    extends State<WidgetForgotPasswordResetForm> {
  ForgotPasswordResetBloc _forgotPasswordBloc;

  final TextEditingController _passwordController = TextEditingController();
 final TextEditingController _otpCodeController = TextEditingController();


  bool autovalidate = false;
  bool obscurePassword = true;

  bool get isPopulated =>
      _passwordController.text.isNotEmpty ;

  @override
  void initState() {
    super.initState();
    _forgotPasswordBloc = BlocProvider.of<ForgotPasswordResetBloc>(context);
    _passwordController.addListener(_onPasswordChange);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ForgotPasswordResetBloc, ForgotPasswordResetState>(
      listener: (context, state) async {
        if (state.isSubmitting) {
          GetSnackBarUtils.createProgress();
        }

        if (state.isSuccess) {
          await GetSnackBarUtils.createSuccess(message: state.message);
          AppNavigator.navigateLogin();
        }

        if (state.isFailure) {
          GetSnackBarUtils.createError(message: state.message);
          setState(() {
            autovalidate = true;
          });
        }
      },
      child: BlocBuilder<ForgotPasswordResetBloc, ForgotPasswordResetState>(
        builder: (context, state) {
          return Padding(
            padding: EdgeInsets.all(20),
            child: Form(
              child: Column(
                children: [
                  _buildCodeField(),
                  WidgetSpacer(
                    height: 20,
                  ),
                  _buildResend(),
                  WidgetSpacer(
                    height: 20,
                  ),
                  _buildTextFieldPassword(),
                  WidgetSpacer(
                    height: 10,
                  ),
                  WidgetSpacer(
                    height: 15,
                  ),
                  _buildButtonForgotPasswordReset(state),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  bool isForgotPasswordResetButtonEnabled() {
    return _forgotPasswordBloc.state.isFormValid &&
        isPopulated &&
        !_forgotPasswordBloc.state.isSubmitting;
  }

  _buildButtonForgotPasswordReset(ForgotPasswordResetState state) {
    return                   Container(
      width: MediaQuery.of(context).size.width-40,
      height:  50,
      child: GestureDetector(
        onTap: () {
          // AppNavigator.navigateLogin();
          if (isForgotPasswordResetButtonEnabled()) {
            _forgotPasswordBloc.add(ForgotPasswordResetSubmitted(
                username: widget.username,
                otpCode: _otpCodeController.text,
                password: _passwordController.text,
            ));
            AppCommonUtils.disposeKeyboard();
            print("______otp________");
            print(widget.otpCode);
          }
        },
        // isEnable: isForgotPasswordResetButtonEnabled(),
        child: Card(
          elevation: 2,
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
          color: Color(0xff33CC66),
          child: Center(
              child: Text(
                "Cập nhật",
                style: TextStyle(color: Colors.white,fontSize: 16),
              )),
        ),
      ),
    );

  }

  Widget _buildCodeField() {
    return PinCodeTextField(
      length: 6,
      obsecureText: false,
      animationType: AnimationType.fade,
      pinTheme: PinTheme(
        shape: PinCodeFieldShape.box,
        borderWidth: 0,
        inactiveColor: AppColor.GREY_LIGHTER_3,
        inactiveFillColor: AppColor.GREY_LIGHTER_3,
        selectedColor: AppColor.GREY,
        selectedFillColor: AppColor.GREY,
        activeColor: AppColor.GREY_LIGHTER_3,
        activeFillColor: AppColor.GREY_LIGHTER_3,
      ),
      animationDuration: Duration(milliseconds: 300),
      backgroundColor: Colors.transparent,
      enableActiveFill: true,
      textInputType: TextInputType.number,
      controller: _otpCodeController,
      onCompleted: (v) {
        print("Completed");
      },
      onChanged: (value) {},
      beforeTextPaste: (text) {
        print("Allowing to paste $text");
        //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
        //but you can show anything you want here, like your pop up saying wrong paste format or etc
        return true;
      },
    );
  }

  _buildTextFieldPassword() {
    return           Container(
      width: MediaQuery.of(context).size.width / 1.15,
      child: TextFormField(
        enableInteractiveSelection: false,
        controller: _passwordController,
        obscureText: obscurePassword,
        onChanged: (value) {
        },
        autovalidate: autovalidate,
        validator: (_) {
          return !_forgotPasswordBloc.state.isPasswordValid
              ? AppLocalizations.of(context)
              .translate('forgot_password_reset.password_invalid')
              : null;
        },
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: "Mật khẩu mới",
          hintStyle: TextStyle(color: Colors.white),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          ),
          suffixIcon: IconButton(
            icon: Icon(
              obscurePassword
                  ? MaterialCommunityIcons.eye_outline
                  : MaterialCommunityIcons.eye_off_outline,
              color: AppColor.WHITE,
            ),
            onPressed: () {
              setState(() {
                obscurePassword = !obscurePassword;
              });
            },
          ),
        ),
        style: TextStyle(color: Colors.white, fontSize: 16),
        textAlign: TextAlign.start,
      ),
    );// Container(

  }
  void _onOtpCodeChange() {
    _forgotPasswordBloc.add(OtpCodeChanged(otpCode: _otpCodeController.text));
  }
  void _onPasswordChange() {
    _forgotPasswordBloc.add(PasswordChanged(
        password: _passwordController.text,
    ));
  }
  _buildResend() => WidgetForgotPasswordVerifyResend(username: widget.username);

}
