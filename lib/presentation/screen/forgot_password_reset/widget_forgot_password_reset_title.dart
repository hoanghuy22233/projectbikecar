import 'package:project_bike_car/presentation/common_widgets/widget_spacer.dart';
import 'package:project_bike_car/utils/locale/app_localization.dart';
import 'package:flutter/material.dart';

import '../../../utils/common/common_utils.dart';

class WidgetForgotPasswordResetTitle extends StatelessWidget {
  final String username;
  const WidgetForgotPasswordResetTitle({Key key, @required this.username})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [

          Text(
            'Vui lòng điền đầy đủ thông tin',
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold,color: Colors.white),
          ),
          WidgetSpacer(
            height: 15,
          ),
          Text(
           " Nhập mã được gửi tới",
            style: TextStyle(color: Colors.white
            ),
            textAlign: TextAlign.center,
          ),
          WidgetSpacer(
            height: 10,
          ),
          Text(
            '${AppCommonUtils.hideUserName("${username}")}',
            style: TextStyle(fontSize: 16,color: Colors.white),
          ),
        ],
      ),
    );
  }
}
