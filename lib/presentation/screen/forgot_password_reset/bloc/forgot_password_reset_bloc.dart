import 'package:project_bike_car/app/constants/endpoint/app_endpoint.dart';
import 'package:project_bike_car/model/repo/user_repository.dart';
import 'package:project_bike_car/utils/validator/validator.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

import 'forgot_password_reset_event.dart';
import 'forgot_password_reset_state.dart';

class ForgotPasswordResetBloc
    extends Bloc<ForgotPasswordResetEvent, ForgotPasswordResetState> {
  final UserRepository _userRepository;

  ForgotPasswordResetBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  ForgotPasswordResetState get initialState => ForgotPasswordResetState.empty();

  @override
  Stream<ForgotPasswordResetState> mapEventToState(
      ForgotPasswordResetEvent event) async* {
    if (event is PasswordChanged) {
      yield* _mapPasswordChangedToState(event.password);
    }  else if (event is ForgotPasswordResetSubmitted) {
      yield* _mapFormSubmittedToState(
          event.username, event.otpCode, event.password);
    }
  }

  Stream<ForgotPasswordResetState> _mapPasswordChangedToState(
      String password) async* {
    var isPasswordValid = Validator.isValidPassword(password);
    yield state.update(
        isPasswordValid: isPasswordValid);
  }


  Stream<ForgotPasswordResetState> _mapFormSubmittedToState(String username,
      String otpCode, String password) async* {
    yield ForgotPasswordResetState.loading();

    var isValidPassword = Validator.isValidPassword(password);


    var newState = state.update(
        isPasswordValid: isValidPassword,
        );

    yield newState;

    if (newState.isFormValid) {
//      yield ForgotPasswordResetState.loading();
      try {
        var response = await _userRepository.forgotPasswordReset(
          username: username,
          otpCode: otpCode,
          password: password,
        );
//        await Future.delayed(Duration(seconds: 3));
        if (response.status == Endpoint.SUCCESS) {
          yield ForgotPasswordResetState.success(message: response.msg);
        } else {
          yield ForgotPasswordResetState.failure(message: response.msg);
        }
      } catch (e) {
        print(e);
        yield ForgotPasswordResetState.failure(message: e.toString());
      }
    }
  }
}
