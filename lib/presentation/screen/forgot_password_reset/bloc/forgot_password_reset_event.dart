import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class ForgotPasswordResetEvent extends Equatable {
  const ForgotPasswordResetEvent();

  @override
  List<Object> get props => [];
}

class PasswordChanged extends ForgotPasswordResetEvent {
  final String password;

  PasswordChanged({@required this.password});

  @override
  List<Object> get props => [];

  @override
  String toString() {
    return 'PasswordChanged{password: $password}';
  }
}

class OtpCodeChanged extends ForgotPasswordResetEvent {
  final String otpCode;

  OtpCodeChanged({@required this.otpCode});

  @override
  List<Object> get props => [];

  @override
  String toString() {
    return 'otpCodeChanged{otpCode: $otpCode}';
  }
}
class ForgotPasswordResetSubmitted extends ForgotPasswordResetEvent {
  final String username;
  final String otpCode;
  final String password;

  const ForgotPasswordResetSubmitted({
    @required this.username,
    @required this.otpCode,
    @required this.password,
  });

  @override
  List<Object> get props => [username, otpCode, password];

  @override
  String toString() {
    return 'ForgotPasswordResetSubmitted{username: $username, otpCode: $otpCode, password: $password}';
  }
}
