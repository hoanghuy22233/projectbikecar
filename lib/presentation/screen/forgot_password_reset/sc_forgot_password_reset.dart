import 'package:project_bike_car/app/constants/navigator/navigator.dart';
import 'package:project_bike_car/model/repo/user_repository.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_appbar_profile.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_spacer.dart';
import 'package:project_bike_car/presentation/common_widgets/widget_touch_hide_keyboard.dart';
import 'package:project_bike_car/presentation/screen/forgot_password_reset/widget_forgot_password_reset_form.dart';
import 'package:project_bike_car/presentation/screen/forgot_password_reset/widget_forgot_password_reset_title.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/forgot_password_reset_bloc.dart';

class ForgotPasswordResetScreen extends StatefulWidget {
  @override
  _ForgotPasswordResetScreenState createState() =>
      _ForgotPasswordResetScreenState();
}

class _ForgotPasswordResetScreenState extends State<ForgotPasswordResetScreen> {
  String _username;
  String _otpCode;

  _onArgument() {
    Future.delayed(Duration.zero, () async {
      final Map arguments = ModalRoute.of(context).settings.arguments as Map;
      setState(() {
        _username = arguments['username'];
        _otpCode = arguments['otp_code'];
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _onArgument();
  }

  @override
  Widget build(BuildContext context) {
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return WidgetTouchHideKeyBoard(
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Color(0xff33CC66),
          body: BlocProvider(
            create: (context) =>
                ForgotPasswordResetBloc(userRepository: userRepository),
            child: Container(
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 15),
                      child: _buildAppbar(),
                    ),

                    Expanded(
                      child: SingleChildScrollView(
                        child: Column(
                          children: [
                            WidgetSpacer(
                              height: 45,
                            ),
                            _buildTop(),
                            _buildForgotPasswordResetForm(),
                            WidgetSpacer(
                              height: 20,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                )),
          ),
        ),
      ),
    );
  }

  _buildAppbar() => WidgetAppbarProfile(
    backgroundColor: Color(0xff33CC66),
    textColor: Colors.white,
    left: [
      Padding(
        padding: const EdgeInsets.only( left: 10),
        child: GestureDetector(
          onTap: () {
            AppNavigator.navigateBack();
          },

          child: Image.asset(
            "assets/images/ic_arrow.png",
            width: 20,
            height: 20,
            color: Colors.white,
          ),
        ),
      )
    ],
  );


  _buildTop() => WidgetForgotPasswordResetTitle(username: _username,);

  _buildForgotPasswordResetForm() => WidgetForgotPasswordResetForm(
        username: _username,
        otpCode: _otpCode,
      );
}
