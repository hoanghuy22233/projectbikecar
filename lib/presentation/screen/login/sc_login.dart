
import 'package:project_bike_car/app/constants/barrel_constants.dart';
import 'package:project_bike_car/model/repo/barrel_repo.dart';
import 'package:project_bike_car/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:project_bike_car/presentation/screen/login/widget_login_form.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../app/auth_bloc/authentication_bloc.dart';
import 'bloc/login_bloc.dart';


class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return WidgetTouchHideKeyBoard(
      child: Scaffold(
        backgroundColor: Color(0xff33CC66),
        body: SafeArea(
          child:BlocProvider(
            create: (context) => LoginBloc(
                userRepository: userRepository,
                authenticationBloc: BlocProvider.of<AuthenticationBloc>(context)),
            child: Container(

              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 25,vertical: 20),
                    child: GestureDetector(
                      onTap: () {
                        AppNavigator.navigateBack();
                      },
                      child: Image.asset(
                        "assets/images/ic_arrow.png",
                        width: 20,
                        height: 20,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Expanded(
                      child: ListView(
                        padding: EdgeInsets.zero,
                        scrollDirection: Axis.vertical,
                        children: [
                          SizedBox(
                            height: 30,
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 25),
                            child: Text("Đăng nhập",style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold,color: Colors.white,fontFamily: 'Roboto'),),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          _buildLoginForm(),
                        ],
                      )),
                ],
              ),
            ),

          ),
        ),
      ),
    );
  }
  _buildLoginForm() => WidgetLoginForm();

}
