import 'package:project_bike_car/app/auth_bloc/authentication_bloc.dart';
import 'package:project_bike_car/app/auth_bloc/bloc.dart';
import 'package:project_bike_car/app/constants/endpoint/app_endpoint.dart';
import 'package:project_bike_car/model/api/dio_provider.dart';
import 'package:project_bike_car/model/repo/barrel_repo.dart';
import 'package:project_bike_car/presentation/screen/login/bloc/login_event.dart';
import 'package:project_bike_car/presentation/screen/login/bloc/login_state.dart';
import 'package:project_bike_car/utils/validator/barrel_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rxdart/rxdart.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final UserRepository _userRepository;
  final AuthenticationBloc _authenticationBloc;

  LoginBloc(
      {@required UserRepository userRepository,
      AuthenticationBloc authenticationBloc})
      : assert(userRepository != null),
        _userRepository = userRepository,
        _authenticationBloc = authenticationBloc;

  @override
  get initialState => LoginState.empty();

  @override
  Stream<Transition<LoginEvent, LoginState>> transformEvents(
      Stream<LoginEvent> events, transitionFn) {
    final nonDebounceStream = events.where((event) {
      return (event is! LoginUsernameChanged && event is! LoginPasswordChanged);
    });

    final debounceStream = events.where((event) {
      return (event is LoginUsernameChanged || event is LoginPasswordChanged);
    }).debounceTime(Duration(milliseconds: 300));

    return super.transformEvents(
        nonDebounceStream.mergeWith([debounceStream]), transitionFn);
  }

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is LoginSubmitUsernamePasswordEvent) {
      yield* _mapLoginSubmitUsernamePasswordEventToState(
          event.email, event.password,);
    } else if (event is LoginUsernameChanged) {
      yield* _mapLoginUsernameChangedToState(event.email);
    } else if (event is LoginPasswordChanged) {
      yield* _mapLoginPasswordChangedToState(event.password);
    }
  }


  Stream<LoginState> _mapLoginSubmitUsernamePasswordEventToState(
      String username, String password,) async* {
    try {
      yield LoginState.loading();
      print("ten : ${username} matkhau:${password}");
      var response = await _userRepository.loginApp(
          username: username, password: password,);
      print('---token----');
      print("nhan token respon ${response.apiToken}");
      print("du lieu ${response.data}");
      // print("Co tra ve data ${response.data}");

      if (response.status == true) {
        DioProvider.bearer(response.apiToken);
        print("Nhan data qua login ${response.data}");
        yield LoginState.success(
            message: response.msg, token: response.apiToken);
        // await _userRepository.deviceToken(
        //     deviceIdentifier: await _firebaseMessaging.getToken());
        _authenticationBloc.add(LoggedIn(response.apiToken));
      } else {
        yield LoginState.failure(message: response.msg);
      }
    } catch (e) {
      yield LoginState.failure(message: 'Bạn nhập sai email hoặc mật khẩu');
    }
  }

  Stream<LoginState> _mapLoginUsernameChangedToState(String userName) async* {
    yield state.update(isUsernameValid: Validator.isValidUsername(userName));
  }

  Stream<LoginState> _mapLoginPasswordChangedToState(String password) async* {
    yield state.update(isPasswordValid: Validator.isValidPassword(password));
  }
}
