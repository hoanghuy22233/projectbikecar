import 'package:project_bike_car/model/repo/barrel_repo.dart';
import 'package:project_bike_car/presentation/screen/all_product/sc_product.dart';
import 'package:project_bike_car/presentation/screen/detail_product/sc_detail_product.dart';
import 'package:project_bike_car/presentation/screen/forgot_password/sc_forgot_password.dart';
import 'package:project_bike_car/presentation/screen/forgot_password_reset/sc_forgot_password_reset.dart';
import 'package:project_bike_car/presentation/screen/forgot_password_verify/sc_forgot_password_verify.dart';
import 'package:project_bike_car/presentation/screen/login/sc_login.dart';
import 'package:project_bike_car/presentation/screen/menu/account/change_password/sc_change_password.dart';
import 'package:project_bike_car/presentation/screen/menu/account/contact/sc_contact.dart';
import 'package:project_bike_car/presentation/screen/menu/account/history_order/ic_history_order.dart';
import 'package:project_bike_car/presentation/screen/menu/account/term/sc_term.dart';
import 'package:project_bike_car/presentation/screen/menu/news/new_detaill/sc_new_detail.dart';
import 'package:project_bike_car/presentation/screen/menu/home/search/search_product/bloc/bloc.dart';


import 'package:project_bike_car/presentation/screen/register/sc_register.dart';
import 'package:project_bike_car/presentation/screen/register_verify/sc_register_verify.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'screen/all_brand_screen/sc_brand_screen.dart';
import 'screen/detail_product/bloc/bloc.dart';
import 'screen/menu/account/history_order/bloc/bloc.dart';
import 'screen/menu/account/profile_detail/barrel_profile_detail.dart';
import 'screen/menu/home/search/search_product/search_screen.dart';
import 'screen/menu/notification/bloc/bloc.dart';
import 'screen/payment/sc_payment.dart';
import 'screen/search_product_can/sc_search_product.dart';
import 'screen/splash/sc_splash.dart';
import 'screen/work_navigation/work_navigation.dart';


class BaseRouter {
  static const String SPLASH = '/splash';
  static const String LOGIN = '/login';
  static const String WORK_NAVIGATION = '/work_navigation';
  static const String FORGOT_PASSWORD = '/forgot_password';
  static const String FORGOT_PASSWORD_VERIFY = '/forgot_password_verify';
  static const String FORGOT_PASSWORD_RESET = '/forgot_password_reset';
  static const String REGISTER = '/register';
  static const String REGISTER_VERIFY = '/register_verify';
  static const String PROFILE_DETAIL = '/profile_detail';
  static const String TERM = '/term';
  static const String CONTACT = '/contact';
  static const String DELIVERY_ADDRESS = '/delivery_address';
  static const String DETAIL_PRODUCT = '/detail_product';
  static const String ADD_ODER = '/add_order';
  static const String HISTORY_ORDER = '/history_order';
  static const String HISTORY_PRODUCT_FIND = '/history_product_find';
  static const String CHANGE_PASSWORD = '/change_password';
  static const String DETAIL_NEW = '/detail_new';


  static const String SEARCH = '/search';
  static const String ALL_PRODUCT = '/all_product';
  static const String ALL_BRAND = '/all_brand';

  static const String SEARCH_PRODUCT = '/search_product';




  static const String INSTALLMENT = '/INSTALLMENT';



  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case SPLASH:
        return MaterialPageRoute(builder: (_) => SplashScreen());
      case LOGIN:
        return MaterialPageRoute(builder: (_) => LoginScreen());
      case WORK_NAVIGATION:
        return MaterialPageRoute(builder: (_) => WorkNavigationScreen());
      case CONTACT:
        return MaterialPageRoute(builder: (_) => ContactScreen());
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                    child: Text('No route defined for ${settings.name}'),
                  ),
                ));
    }
  }

  static Map<String, WidgetBuilder> routes(BuildContext context) {
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return {
      SPLASH: (context) => SplashScreen(),
      LOGIN: (context) => LoginScreen(),
      WORK_NAVIGATION: (context) => MultiBlocProvider(providers: [
        BlocProvider(
          create: (context) =>
              NotificationPromotionBloc(userRepository: userRepository),
        ),
      ], child: WorkNavigationScreen()),

      FORGOT_PASSWORD: (context) => ForgotPasswordScreen(),
      FORGOT_PASSWORD_VERIFY: (context) => ForgotPasswordVerifyScreen(),
      FORGOT_PASSWORD_RESET: (context) => ForgotPasswordResetScreen(),
      REGISTER: (context) => RegisterScreen(),
      REGISTER_VERIFY: (context) => RegisterVerifyScreen(),
      PROFILE_DETAIL: (context) => ProfileDetailScreen(),
      TERM: (context) => TermScreen(),
      CONTACT: (context) => ContactScreen(),
      DETAIL_PRODUCT: (context) => MultiBlocProvider(providers: [
        BlocProvider(
          create: (context) =>
              ProductDetailBloc(homeRepository: userRepository),
        ),
      ], child: DetailProductPageScreen()),
      ADD_ODER: (context) => MultiBlocProvider(providers: [
        BlocProvider(
          create: (context) =>
              ProductDetailBloc(homeRepository: userRepository),
        ),
      ], child: PaymentScreen()),

      HISTORY_ORDER: (context) => MultiBlocProvider(providers: [
        BlocProvider(
          create: (context) =>
              HistoryBloc(userRepository: userRepository),
        ),
      ], child: HistoryOrderPageScreen()),
      CHANGE_PASSWORD: (context) => ChangePasswordScreen(),
      DETAIL_NEW: (context) => NewDetailPageScreen(),
      SEARCH: (context) => MultiBlocProvider(providers: [
        BlocProvider(
          create: (context) =>
              SearchBloc(userRepository: userRepository),
        ),
      ], child: SearchScreen()),

      ALL_PRODUCT: (context) => ProductPageScreen(),
      ALL_BRAND: (context) => BrandPageScreen(),
      SEARCH_PRODUCT:(context) => SearchProductScreen(),

    };
  }
}
