import 'package:project_bike_car/app_config.dart';
import 'package:project_bike_car/model/api/dio_provider.dart';
import 'package:project_bike_car/model/local/pref.dart';
import 'package:project_bike_car/model/repo/barrel_repo.dart';
import 'package:project_bike_car/model/repo/session_repository.dart';
import 'package:project_bike_car/presentation/barrel_screen.dart';
import 'package:project_bike_car/presentation/router.dart';
import 'package:project_bike_car/presentation/screen/menu/account/change_password/bloc/change_password_bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/account/contact/add_contact/bloc/add_contact_bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/account/profile/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/account/profile_detail/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/all_brands/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/bloc/home_bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/bike/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/car/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/electronic_device/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/gps/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/phone/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/home/home_product/sim/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/news/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/news/new_detaill/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/news/news_bike/bloc/bloc.dart';
import 'package:project_bike_car/presentation/screen/menu/notification/bloc/notification_bloc.dart';
import 'package:project_bike_car/presentation/screen/payment/payment_form/buy_bloc/bloc.dart';
import 'package:project_bike_car/utils/locale/app_localization.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:get/get.dart';
import 'package:project_bike_car/app/simple_bloc_delegate.dart';

import '../presentation/screen/forgot_password_verify/forgot_password_verify_resend/bloc/forgot_password_verify_resend_bloc.dart';
import 'auth_bloc/bloc.dart';
import 'constants/barrel_constants.dart';

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();

  static void initSystemDefault() {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarIconBrightness: Brightness.dark,
        statusBarColor: AppColor.STATUS_BAR));
     SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  }

  static Widget runWidget(String token) {
    BlocSupervisor.delegate = SimpleBlocDelegate();
    Dio dio = DioProvider.instance(token);

    final UserRepository userRepository = UserRepository(dio: dio);
    final SessionRepository sessionRepository =
        SessionRepository(pref: LocalPref());

    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider<UserRepository>(
          create: (context) => userRepository,
        ),
        RepositoryProvider<SessionRepository>(
            create: (context) => sessionRepository),
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (context) => AuthenticationBloc(
                userRepository: userRepository,
                sessionRepository: sessionRepository)
              ..add(AppInitialized()),
          ),
          BlocProvider(
            create: (context) => HomeBloc(
                userRepository: userRepository),
          ),
          BlocProvider(
            create: (context) => BrandBloc(
                userRepository: userRepository),
          ),
          BlocProvider(
            create: (context) => ProductBloc(
                userRepository: userRepository),
          ),
          BlocProvider(
            create: (context) => ProductShowBloc(
                userRepository: userRepository),
          ),
          BlocProvider(
            create: (context) => ProductCarBloc(
                userRepository: userRepository),
          ),
          BlocProvider(
            create: (context) => ProductSimBloc(
                userRepository: userRepository),
          ),
          BlocProvider(
            create: (context) => ProductPhoneBloc(
                userRepository: userRepository),
          ),
          BlocProvider(
            create: (context) => ProductGpsBloc(
                userRepository: userRepository),
          ),
          BlocProvider(
            create: (context) => ProductElectronicBloc(
                userRepository: userRepository),
          ),
          BlocProvider(
            create: (context) => BuyBloc(
                userRepository: userRepository,
                ),
          ),

          BlocProvider(
            create: (context) => ProfileBloc(userRepository: userRepository),
          ),
          BlocProvider(
            create: (context) => ProfileDetailBloc(userRepository: userRepository),
          ),
          BlocProvider(
            create: (context) => NewBloc(userRepository: userRepository),
          ),
          BlocProvider(
            create: (context) => NewsDetailBloc(homeRepository: userRepository),
          ),
          BlocProvider(
            create: (context) => ForgotPasswordVerifyResendBloc(
                userRepository: userRepository),
          ),
          BlocProvider(
            create: (context) => NotificationPromotionBloc(userRepository: userRepository),
          ),
          BlocProvider(
            create: (context) => ChangePasswordBloc(userRepository: userRepository),
          ),
          BlocProvider(
            create: (context) => AddContactBloc(userRepository: userRepository),
          ),
        ],
        child: MyApp(),
      ),
    );
  }
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
   // getKeyHash();
    final config = AppConfig.of(context);
    FlutterStatusbarcolor.setStatusBarColor(Color(0xff33CC66));
    return GetMaterialApp(
      debugShowCheckedModeBanner: config.debugTag,
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: AppColor.WORK_COLOR,
        accentColor: AppColor.WORK_COLOR,
        hoverColor: AppColor.WORK_COLOR,
        fontFamily: 'Montserrat',
      ),
      supportedLocales: AppLanguage.getSupportLanguage().map((e) => e.locale),
      localizationsDelegates: [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        DefaultCupertinoLocalizations.delegate
      ],
      localeResolutionCallback: (locale, supportedLocales) {
        if (locale == null) {
          debugPrint("*language locale is null!!!");
          return supportedLocales.first;
        }
        for (var supportedLocale in supportedLocales) {
          if (supportedLocale.languageCode == locale.languageCode &&
              supportedLocale.countryCode == locale.countryCode) {
            return supportedLocale;
          }
        }
        return supportedLocales.first;
      },
      locale: Locale('vi', 'VN'),
      routes: BaseRouter.routes(context),
      home: SplashScreen(),
    );
  }

  static const platform = const MethodChannel('flutter.key_hash');

  // getKeyHash() async {
  //   String response = "";
  //   try {
  //     final String result =  await platform.invokeMethod('getKeyHash');
  //     response = result;
  //   } on PlatformException catch (e) {
  //     response = "Failed to Invoke: '${e.message}'.";
  //   }
  //   print('---------------------------');
  //   print("Response: $response");
  // }
}
