class Endpoint {
  Endpoint._();

  static const BASE_URL = 'http://103.127.207.24:81/api_ok_xe/public';
  //AUTH ROUTE

  //banner
  static const BANNER = '/api/banners';
  //catehory
  static const CATEGORY = '/api/category';
  //home
  static const HOME = '/api/home';
  //new
  static const NEW = '/api/new';

  //login
  static const LOGIN_APP = '/api/login';
  //register
  static const REGISTER = '/api/register';
  //change password
  static const   CHANGE_PASSWORD = '/api/password/change';
  //product
  static const DETAIL_PRODUCT = '/api/product';
//new
  static const DETAIL_NEW = '/api/detail';
  //new
  static const ADD_CONTACT = '/api/send-contact';

  //buy product
  static const BUY_PRODUCT='/api/add-bill';

  //profile
  static const PROFILE='/api/profile';
  //ORDER
  static const ORDER='/api/bills';

  static const REGISTER_BIKE_CAR_VERIFY = '/api/confirm-account';
  static const RESEND_OTP_REGISTER = '/api/resend-otp';
  //forgot password
  static const FORGOT_PASSWORD = '/api/password/email';
  static const FORGOT_PASSWORD_VERIFY = '/api/confirm-account';
  static const RESEND_OTP = '/api/password/email-resend';
  static const FORGOT_PASSWORD_RESET = '/api/password/reset';

  //update profile
  static const UPDATE_PROFILE = '/api/profile/update';

  //NOTIFICATION
  static const NOTIFICATION = '/api/notifications';

  //post search product
  static const SEARCH_PRODUCT = '/api/send-contact-find-product';
  //
  static const int DEFAULT_LIMIT = 20;

  static const int DEFAULT_LIMITS = 10;

  // request failed
  static const int FAILURE = 0;

  // request success
//  static const int SUCCESS = 1;
  static const bool
  SUCCESS = true;

  // request with token expire
  static const int TOKEN_EXPIRE = 2;

  // receiveTimeout
  static const int receiveTimeout = 15000;

  // connectTimeout
  static const int connectionTimeout = 15000;

  // method
  static const GET = 'GET';
  static const POST = 'POST';
  static const PUT = 'PUT';
  static const DELETE = 'DELETE';

  // get path
  static String getPath(String path) {
    return '$BASE_URL$path';
  }
}
