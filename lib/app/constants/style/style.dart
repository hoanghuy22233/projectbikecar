import 'package:project_bike_car/app/constants/color/color.dart';
import 'package:project_bike_car/app/constants/value/value.dart';
import 'package:flutter/material.dart';


class AppStyle {
  AppStyle._();

  //DEFAULT STYLE
  static const DEFAULT_VERY_SMALL = TextStyle(
      fontSize: AppValue.FONT_SIZE_VERY_SMALL,
      color: AppColor.BLACK,
      height: 1.2);

  static const DEFAULT_SMALL = TextStyle(
      fontSize: AppValue.FONT_SIZE_SMALL, color: AppColor.BLACK, height: 1.2);

  static const DEFAULT_MEDIUM = TextStyle(
      fontSize: AppValue.FONT_SIZE_MEDIUM, color: AppColor.BLACK, height: 1.2);

  static const DEFAULT_LARGE = TextStyle(
      fontSize: AppValue.FONT_SIZE_LARGE, color: AppColor.BLACK, height: 1.2);

  static const DEFAULT_VERY_LARGE = TextStyle(
      fontSize: AppValue.FONT_SIZE_VERY_LARGE,
      color: AppColor.BLACK,
      height: 1.2);

  static const APP_MEDIUM = TextStyle(
      fontSize: AppValue.FONT_SIZE_MEDIUM,
      color: AppColor.PRIMARY_COLOR,
      height: 1.2);

  //DEFAULT STYLE MIX
  static final defaultSMALLBOLD =
      DEFAULT_SMALL.copyWith(fontWeight: FontWeight.bold);
  static final defaultMEDIUMBOLD =
      DEFAULT_MEDIUM.copyWith(fontWeight: FontWeight.bold);
  static final defaultLARGEBOLD =
      DEFAULT_LARGE.copyWith(fontWeight: FontWeight.bold);
  static final defaultVERYLARGEBOLD =
      DEFAULT_VERY_LARGE.copyWith(fontWeight: FontWeight.bold);
  static final RED_HINT_SMALL = DEFAULT_SMALL.copyWith(color: Colors.red);

  static final productPRICE = DEFAULT_VERY_SMALL.copyWith(
      height: 1.2,
      color: AppColor.GREY,fontSize: 14,
      decoration: TextDecoration.lineThrough);
  static final productSALEPRICE = DEFAULT_SMALL.copyWith(
    height: 1.2,
    color: Color(0xFF960909),
  );
  static final productPRICEDETAIL = DEFAULT_MEDIUM.copyWith(
      color: AppColor.GREY, decoration: TextDecoration.lineThrough);
  static final productSALEPRICEDETAIL = DEFAULT_MEDIUM.copyWith(
    color: Color(0xFF960909),
  );
  static final PRODUCT_SALE_PRICE = DEFAULT_SMALL.copyWith(
    height: 1.2,
    color: Color(0xFF960909),
  );

  //APP STYLE MIX
  static final APP_MEDIUM_BOLD =
      APP_MEDIUM.copyWith(fontWeight: FontWeight.bold);
}
